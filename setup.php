<?php


/**
 * bypass SSO for local development
 */
if ('localhost' == $_SERVER['HTTP_HOST'] || 'rogercparevewdev.com' == $_SERVER['HTTP_HOST'] || 'testcenter.dingo.dev:8081' == $_SERVER['HTTP_HOST']) {

include_once('rcpar_common.php');

//no cookie no showie
if (!isset($_COOKIE['data'])) {
		header("Location: index.php");
		die();
}

// Get user roles and Section Access
$roles = get_user_roles();
$username=$roles['student']['name'];
$roles = $roles['sections'];

// don't cache me bro
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

} else {
	
/*
 * this is the production SSO code
 * 
 */	

ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);


include_once('rcpar_common.php');
/**
 * Decrypt cookie and extract goodies
 */ 
require_once('DecryptCookie.php');

// don't cache me bro
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

/**
 * first check for the drupal cookie
 */
if (!isset($_COOKIE['Drupal_visitor_ticket'])) {
    header('location: '.LOGIN_URL);
	exit;
	
}

//check for drupal session cookie
$drupal_session=FALSE;
$names = array_keys($_COOKIE);
foreach ($names as $name) {
    
    if (substr($name, 0, 4)=='SESS') {$drupal_session=TRUE;}
    
    
}

if ($drupal_session==FALSE) {
//    header('location: '.LOGIN_URL);
//    exit;
}

//no cookie, no showie. Cookie set on authorize_drupal.php
if (!$_COOKIE['data']){
    header('location: authorize_drupal.php');
    exit;
};

$con=mysql_conn();
//get the id from the cookie
$sid=get_sid();
$student = mysqli_query($con,"SELECT * FROM students WHERE id = $sid");
$row = mysqli_fetch_array($student);
$roles=get_sections($row);

//var_dump($roles);
$obj = new DecryptCookie($_COOKIE['Drupal_visitor_ticket']);
//username
$username=$obj->get_username();
	
}

// Browser detect
include_once('browser_detect.php');
$browserCheckStatus = browserCheckStatus();

?>

<!DOCTYPE html>
<html ng-app="TestingModule">
<head>
<meta charset="utf-8">
<link rel="shortcut icon" href="favicon.png" type="image/png" />
<meta http-equiv="cache-control" content="no-cache, no-store">
<meta http-equiv="pragma" content="no-cache">

<title>Rogercpareview.com Test Center</title>

    <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="screen" />
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.1.0/css/font-awesome.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Merriweather:400,700,300' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,700' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="css/normalize.css">
    <link rel="stylesheet" href="css/pietimer.css">
    <link rel="stylesheet" href="css/pieslices.css">
    <link rel="stylesheet" href="css/pieslices-round-corners.css">

    <link href="css/jquery-ui.css" rel="stylesheet">
    <link href="css/cobalt/jquery-wijmo.css" rel="stylesheet" title="cobalt-jqueryui" type="text/css" />
    <link href="css/wijmo/jquery.wijmo-pro.all.3.20132.9.min.css" rel="stylesheet" type="text/css" />
    <link href="css/style.css" rel="stylesheet" />
    <link href="css/style2.css" rel="stylesheet" />
    <link href="css/mobile.css" rel="stylesheet" />
    <link href='//fonts.googleapis.com/css?family=Inconsolata:700,400' rel='stylesheet' type='text/css'>

    <script src="bower_components/jquery/jquery.min.js"></script>
    <script src="bower_components/jquery-ui/ui/minified/jquery-ui.min.js" type="text/javascript"></script>

    <script src="js/swfobject.js"></script>
    <script src="js/masonry.js"></script>
    <script src="js/moment.js"></script>
    <script src="js/pietimer.js"></script>

    <script src="bower_components/angular/angular.min.js"></script>
    <script src="js/angular-sanitize.js"></script>
    <script src="js/angular-timer.min.js"></script>
    <script src="js/tinymce4/tinymce.min.js"></script>
    <script src="js/angular-bootstrap-scrollspy.min.js"></script>

    <script src="js/wijmo/jquery.wijmo-open.all.3.20133.20.min.js" type="text/javascript"></script>
    <script src="js/wijmo/jquery.wijmo-pro.all.3.20133.20.min.js" type="text/javascript"></script>
    <script src="js/wijmo/interop/angular.wijmo.3.20133.20.min.js" type="text/javascript"></script>
    <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>

    <!-- add directive to replace SELECT elements with Bootstrap Dropdown -->
    <link href="bower_components/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" type="text/css" />
    <script src="bower_components/bootstrap-select/bootstrap-select.min.js"></script>
    <script src="bower_components/angular-bootstrap-select/build/angular-bootstrap-select.min.js"></script>
    <!-- add browser check status data -->
    <script type="text/javascript">
      var browserCheckStatus = function() {
        return JSON.parse('<?php echo json_encode($browserCheckStatus); ?>');
      };
    </script>
  <script src="js/es5.js"></script>
  <script src="js/app.js"></script>
  <script src="js/QuizControllers.js"></script>
  <script src="js/TBS-writtencommunication.js"></script>
  <script src="js/FinalScoreController.js"></script>
  <script src="js/TBS-research.js"></script>
  <script type="text/javascript">
   $(document).ready(function () {
    window.initMasonLayout = function() {
      var container = $('.pin-container');
      if(container != null && container.length > 0) {
        var msnry = new Masonry( container[0], {itemSelector: '.pin'});
      }
    }
  });
  </script>
<script type="text/javascript" src="js/jwplayer/jwplayer.js" ></script>
<script type="text/javascript">jwplayer.key="AjPOi/iypGrPhBFmbTzMfqBZqPvwlwZWT728xA==";</script>
</head>
<?php
// adding path id to body tag
//$path = $_GET['q'])=;
//list($section, ) = explode('/', $path, 2);
?>
<body ng-controller="MainNavController" id="<?php //print $section; ?>">

<div class="navbar navbar-fixed-top header-section">
  <div class="banner container upper-header">
    <div class="upper-header-wrapper">
        <div class="container">
          <div class="row clearfix">
              <div class="col-sm-5 col-md-5 logo-wrapper">
                <a href="https://www.rogercpareview.com/dashboard/"><img src="img/ipq-rcpar-logo.png" id="logo" width="43" height="52"></a> Interactive Practice Questions
              </div>


		<div class="text-center pick-category pull-right" ng-controller="SectionController">
			<div class="" id="pick-section-nav">
				<button type="button" class="btn btn-primary btn-lg aud-btn" rel="AUD" ng-model="model.pickSection" btn-radio="'AUD'" ng-class="{'active': getPicksection('AUD')}" <?php if (!in_array('aud', $roles)) {?>disabled="disabled"<?php }?>>
					AUD
				</button>		
			
				<button type="button" class="btn btn-success btn-lg bec-btn" rel="BEC" ng-model="model.pickSection" btn-radio="'BEC'" ng-class="{'active': getPicksection('BEC')}" <?php if (!in_array('bec', $roles)) {?>disabled="disabled"<?php }?>>
					BEC
				</button>
				
				<button type="button" class="btn btn-info btn-lg far-btn" rel="FAR" ng-model="model.pickSection" btn-radio="'FAR'" ng-class="{'active': getPicksection('FAR')}" <?php if (!in_array('far', $roles)) {?>disabled="disabled"<?php }?>>
					FAR
				</button>
				
				<button type="button" class="btn btn-danger btn-lg reg-btn" rel="REG" ng-model="model.pickSection" btn-radio="'REG'" ng-class="{'active': getPicksection('REG')}" <?php if (!in_array('reg', $roles)) {?>disabled="disabled"<?php }?>>
					REG
				</button>
				
			</div>
		</div>

          </div>
        </div>


      </div>
  </div>
  <div class="container col-md-12 col-sm-12 col-lg-12 lower-header">
    <div class="container content-container">

    <div class="nav nav-pills navbar-nav clearfix">

          <a ng-repeat="item in menu" ng-class="{active:item.isActive}" ng-model="item.isActive" class="btn btn-default" ng-click="navigateTo(item.path)">{{item.label}}</a>
  <!--        <a href="#show-video" prevent-default="" ng-click="showVideo()" class="btn btn-default"><i class="fa fa fa-info-circle"></i> Introduction Video</a>
-->

    </div>

    <div class="nav nav-pills navbar-nav clearfix  pull-right">
    <div class="pull-left student-name">Welcome: <?php echo $username; ?></div>
    <a class="btn btn-default" href="https://www.rogercpareview.com/user/logout">Logout</a>
    </div>
    </div>
  </div>

</div>
<?php
if(isset($_COOKIE['isadmin'])) {
?>
<div class="score-diagnostic" ng-controller="ScoreDiagnosticController">

  <div>{{navState | json}}
  </div>

    <div style="padding:10px 0;border-top:1px solid #fff;">
    <div ng-repeat="score in scores" style="padding:2px 2px 0;" ng-class="{oddScore:$index % 2 == 0}">
      {{score | json}}
    </div>

  </div>
</div>
<?php
}
?>
<div class="container" id="setupcontainer" ng-controller="GeneralController">


  <!-- div class="alert alert-danger"><b>Attention Students!</b> At 10pm PST tonight, we will launch major updates to the Interactive Practice Questions.  Please note that any quizzes you complete between now and the 10pm launch will not be saved in the transition.  We apologize for the inconvenience.</div -->
	<div class="timer-text exam-timer" ng-show="session_type == 'exam' && navState.showInstructions == false && getCurrentPath().indexOf('/quiz/question') > -1">
		<div class="text-wrapper">
<!--
				<div id="digital-timer" ng-show="getSection() == 'AUD' || getSection() == 'FAR'">
				<timer interval="1000" countdown="14400"><span ng-show="hours > 1">{{hours}} hours</span><span ng-show="hours == 1">1 hour</span> <span>{{minutes}} minutes</span></timer>
				</div>
				<div  id="digital-timer" ng-show="getSection() == 'BEC' || getSection() == 'REG'">
				<timer interval="1000" countdown="10800"><span ng-show="hours > 1">{{hours}} hours</span><span ng-show="hours == 1">1 hour</span> <span>{{minutes}} minutes</span></timer>
				</div>
-->
			<div class="digital-timer digi-timer">
				<div class="time-remaining">Time Remaining</div>
				
				<div id="digital-timer">
					<span ng-show="getTimerHoursRemaining() > 1">{{ getTimerHoursRemaining() }} hours</span><span ng-show="getTimerHoursRemaining() == 1">{{ getTimerHoursRemaining() }} hour</span> <span>{{ getTimerMinutesRemaining() }} minutes</span>
				</div>
			</div>
		</div>
	</div>
  <ng-view>

  </ng-view>
		<div class="clearfix copyright">
		Copyright © 2013-2015 Roger CPA Review. All rights reserved
		</div>
</div>
<br /> <br />
<!--
<div class="navbar navbar-fixed-bottom roger">
            <div class="col-sm-2 col-md-2 col-md-offset-5">
		<div class="copyright">
		Copyright © 2014 Roger CPA Review. All rights reserved
		</div>

            </div>
</div>
-->
<!--
<div class="navbar navbar-inverse navbar-fixed-top black-footer roger" ng-show="showSetupFooter == true">

      <div class="container col-sm-12 col-md-12 col-lg-12">
        <div class="container">
        <div class="row">
		<div class="pull-left col-md-2">
		      <div class="btn-toolbar pull-right">
			<div class="btn-group pull-right">
			  <a href="/testmodule" class="btn PreviousButton" ng-show="QuizConfigState.model.pickSection && location.path() == '/quiz/setup/step1'" rel="0">Back</a>
			</div>
			<div class="btn-group pull-right" ng-show="QuizConfigState.model.pickedTopics.length > 0">
			  <a ng-click="prevWizardStep()" class="btn PreviousButton" ng-show="location.path() == '/quiz/setup/step2'" rel="1">Back</a>
			  </div>
			<div class="btn-group pull-right" ng-show="QuizConfigState.model.pickedTopics.length > 0">
			  <a ng-click="prevWizardStep()" class="btn PreviousButton" ng-show="location.path() == '/quiz/setup/step3'" rel="2">Back</a>
			</div>

		      </div>
		</div>
                <div class="pull-right col-sm-2 col-md-2">
                  <div class="btn-toolbar">
                    <div class="btn-group">
                      <button class="NextButton pull-right btn btn-success" ng-click="nextWizardStep()" ng-show="location.path() != '/quiz/setup/step3' && QuizConfigState.model.pickedTopics.length > 0" ng-disabled="QuizConfigState.model.pickedTopics.length == 0">
				Next
			</button>
                      </div>
                    </div>
                </div>

              </div>

              </div>
            </div>
          </div>

-->

<!-- script type="text/javascript" src="https://jira.rogercpareview.com/s/dfe7009c9a2733acf0858fea4f1adca7-T/en_USucnlw5/64022/7/1.4.26/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=8765f00c"></script -->

<script type="text/javascript" src="https://jira.rogercpareview.com/s/b536d7f8aea79bbc83d8d4bf34750c43-T/en_US-jtgmxq/64022/7/1.4.26/_/download/batch/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector/com.atlassian.jira.collector.plugin.jira-issue-collector-plugin:issuecollector.js?locale=en-US&collectorId=7d217e8d"></script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-4923653-3', 'auto');
  ga('send', 'pageview');

</script>
