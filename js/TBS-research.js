TestSimModule.filter('researchInputType', function() {
	return function(type) {
		return type == 'AR' ? 'AR-C' : type;
	};
});
TestSimModule.directive('researchInput', function($compile, SessionService) {
	return {
		restrict:"E",
		require:"ngModel",
		template: '<a class="btn btn-link" href="{{model.model.content.type.url}}"" target="_blank"><i class="fa fa fa-book"></i> View research materials online </a>' +

				'<div class="text-center">' +
					'<div class="research-input-container" style="margin:20px auto;height:80px;">' +

					'<span class="research-badge pull-left"><strong>{{model.model.content.type.type | researchInputType}}</strong></span>' +
					'<input ng-repeat="(i, clause) in maskParts" type="text" ng-model="inputAnswers[i]" ng-change="keyEntry()" class="span1 researchMask" id="researchMask_{{$index}}" data-expect="{{clause}}"/>' + 
					
					'</div>' +
				'</div>',
		link:function($scope, $element, $attrs, ngModel) {
			$scope.model = $scope.$parent.$eval($attrs.ngModel);
			SessionService.content = $scope.model.model.content;
			SessionService.maskParts = SessionService.content.mask.split("-");
			$scope.content = SessionService.content;
			$scope.maskParts = SessionService.maskParts;
//			$scope.maskParts = $scope.answer.mask.split("-");
		}
	}
});


TestSimModule.controller('TBSResearchController',function($scope, $timeout, $rootScope, $routeParams, $browser, 
																			 $timeout, $location, $modal, 
																			TopicSections,
																			SessionService, QuizletService, ChapterService, QuestionService, CalculatorService) {

	$scope.keyEntry = function() {
		var answer = $scope.getMyAnswer();
		$scope.question.answer_id.push(answer);
		$scope.questionToBeSaved = angular.copy($scope.question);
// 		SessionService.saveSession(SessionService.sessionName, $scope.question);

		  $scope.setStartTime();
	}

	$scope.$on('TBS-Research-gotoNextQuestion', function(evt) {
		var answer = $scope.getMyAnswer();
		$scope.question.answer_id.push(answer);
		QuestionService.addQuestionAttempt($scope.question);
	});

	if ($scope.question.type == 'tbs-research') {
	if($rootScope.trackedNASBA == null) {
		$rootScope.trackedNASBA = true;//only record this event once per session
	}
/*
	if (!QuizConfigState.previewMode && $scope.navState.question == 0) {
		$scope.navState.showInstructions = true;
	}
	$rootScope.$on("$routeChangeStart", function(o, n) {
		if(o == n || n == undefined) return;
		QuizNavigation.navState = angular.copy($scope.navState);
	});
	$scope.viewQuestions = function(ch, category, stat) {
		QuizConfigState.viewQuestions(ch, category, stat, oldquiz);
	};
*/


	$scope.isLastQuestion = (QuestionService.question == SessionService.quizlets[QuizletService.quizlet].questions.length - 1);
	$scope.$on('TBSResearchSetStartTime', function() {
		var researchInput = null;
		for(var i = 0; i < $scope.question.question.widgets.length; i++) {
			if($scope.question.question.widgets[i].type == "researchInput") {
				$scope.answer = $scope.question.question.widgets[i].model.content;
				break;
			}
		}
	});

        $scope.questionList = SessionService.quizlets[QuizletService.quizlet].questions;
	
	$scope.getPreviousAnswer = function() {
		if($scope.question.answer_id) {
			if (typeof $scope.question.answer_id == 'string') {
				$scope.question.answer_id = JSON.parse($scope.question.answer_id);
			}
			var question_answers = [];
			angular.forEach($scope.question.answer_id, function(current, idx) {
				if(current) {
					question_answers.push(current);
				}
			});
			if (question_answers && question_answers[question_answers.length - 1]) {
				var answer = question_answers[question_answers.length - 1];
				return angular.isDefined(answer['key']) ? answer['key'] : '';
			}
		}
	}

	var parseQuestion = $scope.questionList[QuestionService.question];
	var topics = parseQuestion.topics;
	var topics = $scope.question.topics;

	var newModel = JSON.parse(parseQuestion.question_json);
	newModel.type = parseQuestion.type;
	var question = decodeURIComponent(newModel.question); 
	$scope.question = JSON.parse(question);

        $scope.question.chapter_id = SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].chapter_id;
        $scope.question.status = SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].status;
        $scope.question.noted = $scope.question.noted || [];

       $scope.inputAnswers = [];
 

       angular.forEach(SessionService.sessionScore, function(question) {
                       if (parseInt(question.question_id) == parseInt($scope.question.id)) {
                               // converts, for example, AU-C-323-352 into ['323','352']
                               var maskParts = question.answers[question.answers.length-1].split('-').splice(2,2);
//                             $scope.maskParts = maskParts;
                               $scope.inputAnswers = maskParts;
                       }
       });

	$scope.question.id = parseQuestion.id;
	$scope.question.quizletID = parseQuestion.quizletID;
	$scope.question.type = newModel.type;
	$scope.question.topics = topics;
	$scope.question.answer_id = $scope.question.answer_id || [];

	if (SessionService.previewMode) {
//		$scope.question.answer_id = JSON.parse(SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].answer_id);
		if (SessionService.quizlets[QuizletService.quizlet]) {
			$scope.question.answer_id = SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].answer_id;
		}
	}
	
	var answer = SessionService.findSessionAnswer($scope.question.id);
	if(answer != null) {
		$scope.question.answer_id = answer.answers;
		var currentAnswer = answer.answers[answer.answers.length - 1];
		if(currentAnswer) {
			$scope.reloadedAnswer = currentAnswer.key;
			if(answer.noted != null) {
				$scope.question.noted = answer.noted;
			}
			$timeout(function() {
			   	var answerClauses = $scope.reloadedAnswer.split(SessionService.content.type.type + '-').pop().split('-');
			   	var sortClauses = [];
			   	for(var i = 0; i < answerClauses.length; i++) {
			   		if( (String(answerClauses[i]).indexOf("<span class='label label") == -1) && (String(answerClauses[i]).indexOf("important'>not answered</span>") == -1) ) {
			   			sortClauses.push(answerClauses[i]);
			   		}
			   	}
			   	for(i = 0; i < sortClauses.length; i++) {
			   		//if(String(sortCloases[i]).indexOf("<span class='label label-danger'>not answered</span>") != -1) {
			   			$("#researchMask_" + i).val(sortClauses[i]);
			   		//}
			   	}
			});
		}
	}
	


	if($scope.navState.showInstructions == false) {
		$scope.setStartTime();
	}

	if (!SessionService.previewMode) {
		var topicTitles = [];
		if(topics) {
			for(var i = 0; i < topics.length; i++) {
				var topic = topics[i];
				var titleObj = SessionService.findTopic(topic.chapter_id, topic.topic_id);
				if (titleObj.topic !== "") {
					topic.topic = titleObj.topic;
					topic.topicID = titleObj.topicID;
					topic.chapter = titleObj.chapter;
				}
				if (!topic.chapter) {
					topic.chapter = $scope.question.chapter;
				}
				topicTitles.push(topic);
			}
		}

		$scope.topicTitles = topicTitles;
	}
//	$scope.inputAnswers = [];
	$scope.getMyAnswer = function() {
		$scope.content = SessionService.content;
		for(var i = 0; i < $scope.question.question.widgets.length; i++) {
			if($scope.question.question.widgets[i].type == "researchInput") {
				$scope.answer = $scope.question.question.widgets[i].model.content;
				break;
			}
		}
		if ($scope.answer) {
			$scope.maskParts = $scope.answer.mask.split("-");
		}
		else {
			$scope.maskParts = $scope.content.mask.split("-");
		}
		var keyClauses = $scope.maskParts;
		var key = $scope.answer.type.type;
		var isCorrect = true;
		for(var i = 0; i < keyClauses.length; i++) {
//			var clause = $("#researchMask_" + i).val();
                       var clause = $('.researchMask')[i].value;
                       if (clause == undefined) {
                               clause = "";
                       }
/*
			if(clause == "") {
				clause = "<span class='label label-danger'>not answered</span>";
			}
*/
			key = key + "-" + clause;
			if(clause.replace(/[^0-9]/g,'') != keyClauses[i].replace(/[^0-9]/g,'')) {
				isCorrect = false;
			}
		}
		$scope.question.answer = {
			"key":key,
			"isCorrect":isCorrect,
			"correct":$scope.answer.type + "-" + $scope.answer.mask,
			"time":(new Date().getTime() - $scope.startTime)
		};

		$scope.question.standalone_answer = $scope.question.answer;

		return $scope.question.answer;
	};
	$scope.$on('TBS-Research-showExplanation', function() {
		var answer = $scope.getMyAnswer();
		var opts = {
			    backdrop: true,
			    keyboard: true,
			    backdropClick: false,
			    templateUrl: 'views/tbsresearch-solution.php',
			    controller: 'TBSResearchSolutionController',
      			dialogFade: true,
			    //dialogClass: 'modal',
			    resolve:{
			    	currentQuestion: function() {
			    		return $scope.question;
			    	},
			    	solution:function() {
			    		return $scope.question.question.solution;
			    	},
			    	answer:function() {
			    		return $scope.getMyAnswer();
			    	}
			    }
			};

		    var d = $modal.open(opts);

		   	d.opened.then(function() {
				$timeout(function() {
					$('.modal-dialog').draggable();
				}, 0);
			});
			d.result.then(
		    	function() { //success callback
			    		/*	
					var score = 0;
					if($scope.question.answer.isCorrect == true) {
						score = 1;
					}
					QuestionService.addQuestionAttempt($scope.question);
*/
			    		$scope.gotoNextQuestion();
			    	
			    },
			    function() { //reject callback
/*
			    	var score = 0;
		    		if($scope.question.answer.isCorrect == true) {
		    			score = 1;
		    		}
				QuestionService.addQuestionAttempt($scope.question);
*/
		    		$scope.setStartTime();
			    }

		    );
	});
	}
});


TestSimModule.controller("TBSResearchSolutionController", function($scope, currentQuestion, solution, answer, $modalInstance, $filter) {
	$scope.question = currentQuestion;

	var init = function() {
		if($scope.question.question && $scope.question.question.widgets && $scope.question.question.widgets.length >= 2) {
			var widget = $scope.question.question.widgets[1] || null;
			if(widget && widget.model && widget.model.content && widget.model.content.type && widget.model.content.type.type) {
				var type = $filter('researchInputType')(widget.model.content.type.type);
				if(answer.key) {
					answer.key = answer.key.replace(widget.model.content.type.type, type);
				}
				if(solution) {
					solution = solution.replace(widget.model.content.type.type, type);
				}
			}
		}
		$scope.solution = solution;
		$scope.givenAnswer = answer;
	};

	$scope.close = function() {
		$modalInstance.dismiss('cancel');
	};

	$scope.saveAndClose = function() {
		$modalInstance.close('ok');
	};

	init();
});

TestSimModule.run(["$templateCache", function($templateCache) {
  $templateCache.put("researchInput",
    "<research-input ng-model='template'></research-input>");
}]);
