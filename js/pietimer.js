(function( $ ){

    $.fn.pietimer = function( method ) {
        // Method calling logic
        if ( methods[method] ) {
            return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
        } else if ( typeof method === 'object' || ! method ) {
            return methods.init.apply( this, arguments );
        } else {
            $.error( 'Method ' +  method + ' does not exist on jQuery.pietimer' );
        }
    };

    var methods = {
        init : function( options ) {
            var state = {
                timer: null,
                timerSeconds: 10,
                callback: function () {},
                timerCurrent: 0,
		animated: true,
		number: 100,
                showPercentage: false,
                fill: false,
                color: '#CCC'
            };

            state = $.extend(state, options);

            return this.each(function() {

                var $this = $(this);
                var data = $this.data('pietimer');
                if ( ! data ) {
                    $this.addClass('pietimer');
                    $this.css({fontSize: $this.width()});
                    $this.data('pietimer', state);
                    if (state.showPercentage) {
                        $this.find('.percent').show();
                    }
                    if (state.fill) {
                        $this.addClass('fill');
                    }
                    $this.pietimer('start');
                }
            });
        },

        stopWatch : function() {
            var data = $(this).data('pietimer');
            if ( data ) {
                if (!data.isPaused) {
	                if (data.lengthPaused) {
				var seconds = (data.timerFinish-(new Date().getTime() - data.lengthPaused))/1000;
			}
			else {
				var seconds = (data.timerFinish-(new Date().getTime()))/1000;
			}
                if (seconds <= 0) {
                    clearInterval(data.timer);
                    $(this).pietimer('drawTimer', 100);
                    data.callback();
                } else {
                    var percent = 100-((seconds/(data.timerSeconds))*100);
                    $(this).pietimer('drawTimer', percent);
                }
                }
            }
        },

        drawTimer : function (percent) {
            $this = $(this);
            var data = $this.data('pietimer');
            if (data) {
		if (data.number && !data.animated) {
			percent = data.number;
		}
                $this.html('<div class="percent"></div><div class="slice'+(percent > 50?' gt50"':'"')+'><div class="pie"></div>'+(percent > 50?'<div class="pie fill"></div>':'')+'</div>');
                var deg = 360/100*percent;
                $this.find('.slice .pie').css({
                    '-moz-transform':'rotate('+deg+'deg)',
                    '-webkit-transform':'rotate('+deg+'deg)',
                    '-o-transform':'rotate('+deg+'deg)',
                    'transform':'rotate('+deg+'deg)'
                });
                $this.find('.percent').html(Math.round(percent)+'%');
                if (data.showPercentage) {
                    $this.find('.percent').show();
                }
                if ($this.hasClass('fill')) {
                    $this.find('.slice .pie').css({backgroundColor: data.color});
                }
                else {
                    $this.find('.slice .pie').css({borderColor: data.color});
                }
            }
        },
        
        start : function () {
            var data = $(this).data('pietimer');
            if (data) {
		if (data.animated) {
			data.timerFinish = new Date().getTime()+(data.timerSeconds*1000);
			$(this).pietimer('drawTimer', 0);
			data.timer = setInterval("$this.pietimer('stopWatch')", 50);
		}
		else if (!data.animated && data.number) {
                	$(this).pietimer('drawTimer', data.number);
			data.isPaused = true;
		}
            }
        },

        togglepause: function() {
            var data = $(this).data('pietimer');
            if (data) {
		data.isPaused = !data.isPaused;
		if (data.isPaused) {
			data.lastPausedTime = new Date();
		}
		else {
			data.lengthPaused = new Date().getTime() - data.lastPausedTime.getTime();
		}	
	    }
	},
        
        pause: function() {
            var data = $(this).data('pietimer');
            if (data) {
               data.isPaused = true;
	       data.lastPausedTime = new Date();
            }
        },
        
        unpause: function() {
               var data = $(this).data('pietimer');
            if (data) {
                data.isPaused = false;
		data.lengthPaused = new Date().getTime() - data.lastPausedTime.getTime();
            }
        },

        reset : function () {
            var data = $(this).data('pietimer');
            if (data) {
                clearInterval(data.timer);
                data.isPaused = false;
           
                $(this).pietimer('drawTimer', 0);
            }
        }

    };
})(jQuery);
