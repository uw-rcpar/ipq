/* QUESTION TYPE FLAGS */
/*
	
0: Multiple Choice
1: TBS-Journal
2: TBS-WC
3: TBS-Research
*/

TestSimModule.service('BrowserInfoService', function() {
	this.info = {};
});

TestSimModule.directive('browserInfo', function(BrowserInfoService) {
	return {
		restrict: "ACE",
		link: function($scope, $element, $attrs) {
			BrowserInfoService.info = {
					useragent: $attrs.browserUseragent,
					name: $attrs.browserName,
					version: $attrs.browserVersion,
					allowed: !!parseInt($attrs.allowedBrowser),
					device: $attrs.deviceType,
					platform: $attrs.platform
			};
		}
	}
});

TestSimModule.directive('instructionsContainer', function() {
	return {
		restrict: "ACE",
		templateUrl: "views/question-instructions.php"
	}
});
TestSimModule.directive('sectionSelectionContainer', function() {
	return {
		restrict: "ACE",
		templateUrl: "views/section-selection.php"
	}
});
/* IB-164: This directive is affecting the performance because when makes
$apply is calling an extra $digest on whole the app and this eval all watchers
TestSimModule.directive('scrollerSticky', function($window) {
  return {
    scope: false,
    link: function(scope, element, attrs) {
      var windowEl = angular.element($window);
      var handler = function() {
        scope.scroll = windowEl.scrollTop();
      }
      windowEl.on('scroll', scope.$apply.bind(scope, handler));
      handler();
    }
  };
});
*/
QuestionTimer = function() {};
QuestionTimer.prototype = {};
QuestionTimer.prototype.init = function(t) {
	if(t == null) {
		t = 90 * 1000; //90 seconds
	}
	this.duration = t; 
	this.startTime = new Date().getTime();
	this.count = 0;
	this.elapsed = 0;
	this.isPaused = false;
	this.pausedAt = 0;
	this.canvas = $("#timerchart");

	this.canvas.pietimer({
		timerSeconds: t/1000,
/*		color: '#2560AC', */
		color: '#85929a',
		width: 39,
		height: 38,
		fill: false,
		showPercentage: false
	});
/*
	this.canvas.removeLayers();
	this.canvas.clearCanvas();
	
	this.canvas.drawArc({
		layer:true,
		name:"circle",

		strokeStyle: "#e0e0e0",
		strokeWidth: 15,
		x: 40, 
		y: 40,
		radius: 31,
		start: 0, 
		end: 360
	});

	this.canvas.drawArc({
		layer:true,
		name:"timerArc",
		strokeStyle: "#2560ac",
		strokeWidth: 8,
		x: 40, 
		y: 40,
		radius: 32,
		start: 0, 
		end: 360
	});
*/
	var self = this;

	$(".timerControl").on("click", function(e) {
		self.pauseTime(true);
		e.preventDefault();
	});

	$(".btn-reportproblem").on("click", function(e) {
		self.pauseTime(); 
	});

	$('.btn-reportproblem').on('hidden.bs.modal', function () {
	});
	
	window.timer = self;
	//$("canvas").drawArc({ strokeStyle: "#000", strokeWidth: 5, x: 40, y: 100, radius: 20, start: 90, end: 180 });
};

QuestionTimer.prototype.resumeTime = function() {
/*
	//first clear layer
	//$("#timerchart").removeLayer("timerArc").drawLayers();
	//draw arc from end:ellapsed time
	var startAngle = ( this.elapsed / this.duration ) * 360;
	if(startAngle < 360) {
		this.canvas.drawArc({
			layer:true,
			name:"timerArc",
			strokeStyle: "#2560ac",
			strokeWidth: 8,
			x: 40, 
			y: 40,
			radius: 31,
			start: startAngle, 
			end: 360
		});
		
		this.duration -= this.elapsed;
		this.startTime += (new Date().getTime() - this.pauseStart);
		

		this.canvas.animateLayer("timerArc", {	strokeStyle: "#2560ac",
												strokeWidth: 8,
												x: 40, 
												y: 40,
												radius: 31,
												start: 360, 
												end: 360
												}, this.duration, "linear");
	}
*/

		if (this.isPaused) {
			this.canvas.pietimer('unpause');
			this.isPaused = false;
/*
			var scope = angular.element("#digital-timer").scope();
			scope.$broadcast("timer-resume");
*/
/*
			var scope = angular.element(".quiz-digital-timer").scope();
			scope.$broadcast("unpauseTimer");
*/
			$(".blastShield").css("display", "none");
			$("#question").css("visibility", "visible");
			$(".navbar.navbar-fixed-bottom").css("visibility", "visible");
		}
};

QuestionTimer.prototype.pauseTime = function(hideScreen) {

	if (!this.isPaused) {
		if(hideScreen == undefined) {
			hideScreen = false;
		}
		var self = this;
		self.pauseStart = new Date().getTime();
		self.elapsed =  self.pauseStart - self.startTime;

	/*
		$("#timerchart").stopLayer("timerArc");
	*/

		this.canvas.pietimer('pause');
		this.isPaused = true;
/*
		var scope = angular.element(".quiz-digital-timer").scope();
		scope.$broadcast("pauseTimer");
*/
		
		if(hideScreen) {
			$(".blastShield").css("display", "block");
			$("#question").css("visibility", "hidden");
			$(".navbar.navbar-fixed-bottom").css("visibility", "hidden");
		}
	}
};

QuestionTimer.prototype.unload = function() {
/*
	$("#timerchart").stopLayer("timerArc");
*/
};

QuestionTimer.prototype.draw = function(t) {
/*
	try{
		$("#timerchart").animateLayer("timerArc", 
			{	start:360
			}, this.duration, "linear");
	} catch(e) {

	}
*/
};

TestSimModule.service("QuizNavigation", function(){
	this.navState = {
		showInstructions:true,
		quizlet:0,
		showTimer: false,
		question:0,
		scoreAsIGo:false
	};

});

TestSimModule.service("CalculatorService", function( $timeout, $modal) {
	this.lockCalculator = false;
	var self = this;
	this.showCalculator = function() {
		if(this.lockCalculator == false) {
			this.lockCalculator = true;
			var opts = {
				    backdrop: false,
				    keyboard: false,
				    backdropClick: false,
				    templateUrl: 'views/tools-calculator.php',
				    controller: 'CalculatorController',
	      			dialogFade: true,
				    windowClass: 'modal-calc'
				    
				  };
			    this.calc = $modal.open(opts).result.then(function(cmd) {
			    	self.lockCalculator = false;
			    });

/*
			if(Keen) {
				var keenEvent = {"interaction":"Open Calculator",}
			}
*/
		}
	};

	


});

TestSimModule.controller("ScoreDiagnosticController", function($scope, $rootScope, QuizSession, QuizNavigation) {
	$scope.scores = QuizSession.sessionScore;
	$scope.navState = QuizNavigation.navState;

	$scope.$watch(function() {
		return QuizSession.sessionScore;
	}, function(n) {
		$scope.scores = n;
	})
});

TestSimModule.controller("CalculatorController", function($scope, $rootScope, $timeout, $modalInstance) {
	$timeout(function() {
		$(".modal").draggable();
	});

	$scope.close = function() {
		$modalInstance.close();
	};
/*
	$rootScope.$on('$routeChangeSuccess', function(e, current, old) { 
		$modalInstance.close();
	});
*/
	$scope.hideContent = function() {
		$('.modal-calc iframe').css('display','none');	
		$('.modal-calc .drag-text').css('display','block');
	};

	$scope.showContent = function() {
		$('.modal-calc iframe').css('display','block');	
		$('.modal-calc .drag-text').css('display','none');
	};
});

TestSimModule.service("QuizletService", function($rootScope, $http, $location) {
	this.requestedQuizlets = [];
	this.quizlet = 0;
	var self = this;

	$rootScope.$on("reset", function() {
		self.requestedQuizlets = [];
		self.quizlet = 0;
	});

	this.addQuizlet = function(m) {
		m.id = this.requestedQuizlets.length;
		this.requestedQuizlets.push(m);
	};

});

TestSimModule.controller("QuizletController", function($rootScope, $scope, $http, $location, SessionService, QuizletService, QuestionService) {
	$scope.goingToNextQuizlet = false;
	$scope.removeQuizlet = function(index) {
		SessionService.requestedQuizlets.splice(index, 1);
		$scope.quizlets = SessionService.requestedQuizlets;
	};

	$scope.setupNewSection = function() {
		var section = QuizConfigState.model.pickSection;
		QuizConfigState.model = {pickSection:section};
		QuizConfigState.furthestStepReached = 1;
		QuizConfigState.selectedChapters = [];
		QuizConfigState.chosenChaptersList = {};
		QuizConfigState.lastStep = 1;
		$location.path("/quiz/setup");
	};

	$scope.$on("nextQuizlet", function() {
		$scope.nextQuizlet();
	});

	$scope.nextQuizlet = function() {
	    if(!$scope.goingtoNextQuizlet) {
		$scope.goingToNextQuizlet = true;
		    if(!SessionService.previewMode) {
					QuestionService.addQuestionAttempt(QuestionService.question);
					$scope.advanceQuizlet();
	//				$scope.initQuizlet();
					QuestionService.question = 0;
					$location.path(SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].path);
			}
			else {
				$scope.advanceQuizlet();
	//			$scope.initQuizlet();
				SessionService.question = 0;
				$location.path(SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].path);
			}
	    }
	};
	$scope.advanceQuizlet = function() {
		if(QuizletService.quizlet < (SessionService.quizlets.length - 1)) {
			QuizletService.quizlet++;
			$scope.goingToNextQuizlet = false;
			QuestionService.question = 0;
			SessionService.showInstructions = true;
			$rootScope.$broadcast("sessionStateUpdate");
		} else {
			$scope.goingToNextQuizlet = false;
			$location.path("/final-score");
		}
	};

	$scope.resetQuizlet = function() {
		QuizletService.quizlet = 0;
		QuestionService.question = 0;
		SessionService.showTimer = false;
		SessionService.scoreAsIGo = false;
		$location.path(SessionService.quizlets[0].questions[0].path);
	}
});

TestSimModule.service("ChapterService", function($rootScope) {
	this.inspectedChapter = null;
	this.chosenChapters = 0;
	this.chosenChaptersList = {};
	this.selectedChapters = [];
	this.topics = {};
	this.chapterScores = false;

		this.getChapterById = function(id) {
			var foundchapter = null;
			if(this.topics.$$v) {
				this.topics = this.topics.$$v;
			}
			angular.forEach(this.topics, function(chapter) {
				if (parseInt(chapter.id) === parseInt(id)) {
					foundchapter = chapter;
				}
			});
			return foundchapter;
		};

		this.inspectChapter = function(id) {
			this.inspectedChapter = id;	
		    	$location.path("/chapter-overview");
		}
		this.toggleChapter = function(chapter) {
			if(chapter.include == null) {
				chapter.include = true;
				this.chosenChapters++;
			} else if (chapter.include) {
				chapter.include = !chapter.include;
				this.chosenChapters--;
			}
			else if (!chapter.include) {
				chapter.include = !chapter.include;
				this.chosenChapters++;
			}
			angular.forEach(chapter.topics, function(topic) {
				if(chapter.include) {
					$scope.toggleTopic(topic.id, topic.chapterID,  topic.m, topic.t, false);

				} else {
					$scope.toggleTopic(topic.id, topic.chapterID,  topic.m, topic.t, true);

				}
				topic.include = chapter.include;
			});
		};

		// get the count of questions for a specific Chapter
		// @param type [total,attempted,all]
		// @param category [mcq,tbs] Pick form only the Multiple chose or Task based simulation
		this.getQuestionCount = function(chapterId, category, type) {
			var chapterCount = 0,
				chap;

			// type is required if category is not equal to total|attempted
			// type is ignored if category is equal to total|attempted

			// if no chapter scores loaded exit now
			if (!this.chapterScores) {
				// console.debug('!this.chapterScores', this.chapterScores)
				return chapterCount;
			}

			// get chapter scores for the requested chapter
			chap = this.chapterScores[chapterId];

			// if no chapter found exit now
			if (!chap) {
				// console.debug('!chap', chap)
				return chapterCount;
			}

			switch (type) {
				case 'total':
					// asking for a total of correct  /incorrect / skipped question counts
					if (category !== 'all') {
						// category = all means we want both 'tbs' and 'mcq' questions
/*
						chapterCount = 0;
						angular.forEach(this.totalQuestions, function(chapter) {
							chapterCount += parseInt(chapter);
						});
						
*/
						if (chapterId !== 'total') {
							chapterCount = parseInt(this.totalQuestions[category][chapterId]);
						}
						else {
							chapterCount = parseInt(this.totalQuestions[category]['total']);
						}
					} else {
						if (chapterId !== 'total') {
							chapterCount = parseInt(this.totalQuestions['combined'][chapterId]);
						}
						else {
							chapterCount = parseInt(this.totalQuestions['combined']['total']);
						}

					}
					break;
				case 'non_nasba_total': 
					// asking for a total of correct  /incorrect / skipped question counts
					if (category !== 'all') {
						// category = all means we want both 'tbs' and 'mcq' questions
/*
						chapterCount = 0;
						angular.forEach(this.totalQuestions, function(chapter) {
							chapterCount += parseInt(chapter);
						});
						
*/
						if (chapterId !== 'total') {
							if (category == 'tbs') {
								chapterCount = parseInt(this.totalQuestions[category][chapterId]) - (parseInt(this.totalQuestions['tbs'][chapterId]) - parseInt(chap['journal_total_available'].length));
							}
							else {
								chapterCount = parseInt(this.totalQuestions[category][chapterId]);
							}
						}
						else {
							chapterCount = parseInt(this.totalQuestions[category]['total']);
						}
					} else {
						if (chapterId !== 'total') {
								chapterCount = parseInt(this.totalQuestions['combined'][chapterId]) - (parseInt(this.totalQuestions['tbs'][chapterId]) - parseInt(chap['journal_total_available'].length));
						}
						else {
							chapterCount = parseInt(this.totalQuestions['combined']['total']);
						}

					}
					break;
				case 'incorrect':
					if (category !== 'all') {
						// category = all means we want both 'tbs' and 'mcq' questions
						chapterCount = parseInt(chap[category]['incorrect']);
					} else {
						chapterCount =
							parseInt(chap['mcq']['incorrect']) +
							parseInt(chap['tbs']['incorrect']);
					}
					break;
				case 'non_nasba_incorrect':
					if (category !== 'all') {
						chapterCount = parseInt(chap['journal_incorrect'].length);
					}
					else {	
						chapterCount = parseInt(chap['mcq']['incorrect']) + parseInt(chap['journal_incorrect'].length);
					}
					break;	
				case 'unanswered': 
				case 'skipped':
					if (category !== 'all') {
//						chapterCount = parseInt(this.totalQuestions[category][chapterId]) - (chap[category]['correct'] + chap[category]['incorrect']);
						chapterCount = parseInt(chap[category]['unanswered']);
					}
					else {
//						chapterCount = parseInt(this.totalQuestions['combined'][chapterId]) - (chap['mcq']['correct'] + chap['mcq']['incorrect'] + chap['tbs']['correct'] + chap['tbs']['incorrect']);
						chapterCount = parseInt(chap['mcq']['unanswered']) + parseInt(chap['tbs']['unanswered']);
					}
					break;
				case 'non_nasba_skipped':
				case 'non_nasba_unanswered':
					if (category !== 'all') {
						chapterCount = parseInt(chap['journal_unanswered'].length);
					}
					else {
						chapterCount = parseInt(chap['mcq']['unanswered']) + parseInt(chap['journal_unanswered'].length);
					}
					break;
				default:
					// asking for a correct, incorrect or skipped
					if (category !== 'all') {
						chapterCount = parseInt(chap[category][type]);
					} else {
						chapterCount = parseInt(chap['mcq'][type]) + parseInt(chap['tbs'][type]);
					}
					break;
			}
			// Parse chapter count to int
			chapterCount = parseInt(chapterCount);
			// Set chapter count as 0 when chapter count var is NaN
			if(isNaN(chapterCount)) {
				chapterCount = 0;
			}
			return chapterCount;
		};

});

TestSimModule.factory("SessionService", function($rootScope, $http, $location, $timeout, $q, $modal, SectionService, ChapterService, QuizletService) {
	this.savingQuestions = [];
	this.hasNASBA = true;
	this.model = {};
	this.savingInProgress = false;
	this.savingSession = false;
	this.specialPrefs = false;
	this.tbsmodel = {};
	this.quizlets = [];
	this.totalQuestions = {};
	this.model.pickSection = null;
	this.model.pickedTopics = [];

	this.realTotalQuestions = 0;
	this.saveMode = "new";
	this.quizStarted = false;
	this.quizID = false;
	this.question = 0;
	this.sessionName = false;
	this.previewMode = false;
	this.firstLoad = false;
	this.examSimulator =  false;
	this.showOptions =  false;
	this.scoreAsIGo = false;
	this.qPageMax = 30;
	this.showTimer = false;
	this.showInstructions = true;
	this.currentPage = 0;
	this.pages = [];
	this.quizStarted = false;
	this.sessionScore = [];
	this.sessioninfo = false;
	this.specificCategory = false;
	this.section = false;
	this.max = {
		'tbs': 15,
		'mcq': 50
	};

	this.currentPercent = 0;
	this.attemptsAverage = 0;
	this.timeAverage = 0;
	this.completePercent = 0;
	this.totalPercent = 0;

	var self = this;

	this.initLoad = function() {
		this.sessionScore = [];
		this.totalQuestions = 0;
	};

	this.savingQuestion = function(id) {
		if (self.savingQuestions.indexOf(id) < 0) {
			self.savingQuestions.push(id);
		}
	};

	this.notSavingQuestion = function(id) {
		if (self.savingQuestions.indexOf(id) >= 0) {
			var i = self.savingQuestions.indexOf(id);
			self.savingQuestions.splice(i, 1);
		}
		if (self.savingQuestions.length == 0) {
			$rootScope.$broadcast("finishedSavingQuestions");
		}
	};

	this.filterQuestionsByCategory = function(category, step) {
		if (step == 1) {
			var new_ids = {
				'tbs': [],
				'mcq': []
			};

			var id_obj = {
				'tbs': 0,
				'mcq': 0,
				'quizlets': []
			};
			angular.forEach(ChapterService.chosenChaptersList, function(ch, id) {
				new_ids['mcq'] = new_ids['mcq'].concat(ChapterService.chapterScores[id]['mcq'][category]);
				new_ids['tbs'] = new_ids['tbs'].concat(ChapterService.chapterScores[id]['tbs'][category]);
			});
					if (new_ids['mcq'].length > 0) {
//						max.mcq = 50;
						if (this.max.mcq > 0) {
							if (this.max.mcq < new_ids.mcq.length) {
								new_ids.mcq.length = this.max.mcq;
							}
							id_obj.quizlets.push({qids: new_ids.mcq, type: 'mcq'});
							id_obj.mcq = new_ids.mcq.length;
						}
					}
					if (new_ids['tbs'].length > 0) {
//						max.tbs = 15;	
						if (this.max.tbs > 0) {
							if (this.max.tbs < new_ids.tbs.length) {
								new_ids.tbs.length = this.max.tbs;
							}
							id_obj.quizlets.push({qids: new_ids.tbs, type: 'tbs'});
							id_obj.tbs = new_ids.tbs.length;
						}
					}
			this.specialPrefs = id_obj;

		} else if (step == 2) {
			angular.forEach(this.specialPrefs.quizlets, function(quizlet, i) {
				if (quizlet.type == 'tbs') {
					self.specialPrefs.quizlets[i].qids = quizlet.qids.slice(0, self.max.tbs);
					self.specialPrefs.tbs = self.specialPrefs.quizlets[i].qids.length; 
				} else if (quizlet.type == 'mcq') {
					self.specialPrefs.quizlets[i].qids = quizlet.qids.slice(0, self.max.mcq);
					self.specialPrefs.mcq = self.specialPrefs.quizlets[i].qids.length; 
				}

			});
		}
		else if (step == 3) {
			this.viewQuestions(this.specialPrefs.quizlets, null, null, 'multitype');
		}
	};
	this.loadQuizByQids = function(qids, category, multiquizlet, multitype) {
    		$rootScope.showTabLoading = false;
		if (!multiquizlet) {
			if (!multitype) {
				self.newQuizFromSectionOverview = true;
				$http.post(ServiceURLS.quizConfigQid, {
					'qids': qids,
					'type': category
				}).success(function(data) {
					self.processLoadedQuiz(data);
				});
			}
			else if (multitype) {
				var session = {};
				var requestedLength = QuizletService.requestedQuizlets.length;
//				angular.forEach(qids, function(qidset) {
					$http.post(ServiceURLS.quizConfigQid, {
						'qids': JSON.stringify(qids),
						'multitype': true
// add support for fetching both MCQ and TBS questions tomorrow
					}).success(function(data) {
						if (session.quizlets) {
							if (session.quizlets.length == requestedLength) {
								self.processLoadedQuiz(session);
							}
							else {
								session.quizlets.push(data.quizlets[0]);
								if (session.quizlets.length == requestedLength) {
									self.processLoadedQuiz(session);
								}
							}
						}
						else {
							session['quizlets'] = data.quizlets;
							if (session.quizlets.length == requestedLength) {
								self.processLoadedQuiz(session);
							}
						}
					});
//				});
			}
			else {
				var session = {};
				var requestedLength = QuizletService.requestedQuizlets.length;
//				angular.forEach(qids, function(qidset) {
					$http.post(ServiceURLS.quizConfigQid, {
						'qids': qidset.qids,
// add support for fetching both MCQ and TBS questions tomorrow
						'type': qidset.type
					}).success(function(data) {
						if (session.quizlets) {
							if (session.quizlets.length == requestedLength) {
								self.processLoadedQuiz(session);
							}
							else {
								session.quizlets.push(data.quizlets[0]);
								if (session.quizlets.length == requestedLength) {
									self.processLoadedQuiz(session);
								}
							}
						}
						else {
							session['quizlets'] = data.quizlets;
							if (session.quizlets.length == requestedLength) {
								self.processLoadedQuiz(session);
							}
						}
					});
//				});
			}
		}
		else {
			$http.post(ServiceURLS.quizConfigQid, {
				'qids': JSON.stringify(qids),
				'multiquizlet': true,
				'type': category
			}).success(function(data) {
				self.processLoadedQuiz(data);
			});
		}
	};

	this.viewQuestions = function(chapterquizlets, category, type, multitype) {
		// this needs to be synchronously, use promis method instead
		// self.updateSessionNameByQuizzesCount();
		self.examSimulator = false;
		$rootScope.showTabLoading = true;

		var onUpdateComplete = function(){
			if (typeof chapterquizlets !== 'object') {
				self.viewQuestionsByChapter(chapterquizlets, category, type, multitype);
			} else {
				self.viewQuestionsByQuizlets(chapterquizlets, multitype);
			}
		};

		self.updateSessionNameByQuizzesCountPromise()
			.then(onUpdateComplete);
	};

	this.viewQuestionsByQuizlets = function(chapterquizlets, multitype) {
		if (multitype) {
			// If multitype is true, then we need to turn several arrays of IDs into multiple quizlets of different types
			if (chapterquizlets.length > 0) {
				$rootScope.showTabLoading = true;
				self.previewMode = false;
				self.loadQuizByQids(chapterquizlets, null, false, 'multitype');
				// this.resetQuizlet();
			}
		} else if (chapterquizlets.length > 0) {
			// Else, chapterquizlets is an array of quizlets
			$rootScope.showTabLoading = true;
			self.previewMode = false;
			self.loadQuizByQids(chapterquizlets, null, 'multiquizlet');
			self.resetQuizSession();
		}
	}

	this.viewQuestionsByChapter = function(chapterId, category, type, multitype) {
	
		// If this is true, then chapterquizlets = a chapter number
		if (category == 'multiple-choice') {
			category = 'mcq';
		}
		else if (category !== 'mcq' && category !== 'multiple-choice') {
			category = 'tbs';
		}

		$rootScope.showTabLoading = true;
		self.previewMode = false;

		// this.reloadQuiz = function(session, type, subtype, previewmode, blankslate) {
		// self.reloadQuiz({
		//     'quizlets': self.quizlets
		// }, null, null, self.previewMode, true);

		// console.log(self, ChapterService.chapterScores);

		var session = { 'quizlets': self.quizlets };
		var quizlets = angular.copy(self.quizlets);
		var buckets = self.sortMcqAndTbsQuestionsIntoBuckets(self.quizlets[0].questions);
		self.quizlets = [];
		var mp = buckets.mcq;
		var tbs = buckets.tbs;

		$http.post(ServiceURLS.reloadSessionUrl, {
			'questions': {
				'multipleChoice': mp,
				'tbs': tbs
			}
		}).then(function(data) {
			var questions = data.data.questions;
			if (questions.multipleChoice.length > 0) {
				self.reloadQuizData(session, quizlets[0], true, questions.multipleChoice);
			}

			if (questions.tbs.length > 0)  {
				self.reloadQuizData(session, quizlets[0], true, questions.tbs);
			}

			$rootScope.$broadcast("launchReloadedQuiz");
		});

		// if (!self.hasReviewedQuizDetails) {
		// 	var opts = {
		// 		    backdrop: true,
		// 		    keyboard: true,
		// 		    backdropClick: true,
		// 		    templateUrl: 'views/startquizconfirmation.php',
		// 		    controller: 'StartQuizConfirmation',
		// 			dialogFade: true,
		// 		    windowClass: ''
		// 		 };
		// 	    var d = $modal.open(opts).result.then(function(result) {
		// 		if (result == 'ok') {
		// 			self.hasReviewedQuizDetails = true;
		// 			self.saveMode = "new";
		// 			self.model.pickedTopics = [];
		// 			angular.forEach(ChapterService.topics, function(chapter) {
		// 				if (chapter.id == chapterquizlets) {
		// 					ChapterService.chosenChaptersList[chapter.id] = chapter;
		// 					angular.forEach(chapter.topics, function(topic) {
		// 						self.model.pickedTopics.push({topicID:topic.id, chapterID:chapterquizlets, m: topic.m, t: topic.t});
		// 					});
		// 				}
		// 			});
		// 			if (!self.tbsmodel.pickedTopics) {
		// 				self.tbsmodel = angular.copy(self.model);
		// 			}
		// 			var qty  = ChapterService.chapterScores[chapterquizlets][category][type].length;
		// 			if (category == 'mcq') {
		// 				self.model.qty = qty;
		// 				self.tbsmodel.qty = 0;
		// 				if (qty > 50) {
		// 					self.model.qty = 50;
		// 					self.tbsmodel.qty = 0;
		// 				}
		// 			}
		// 			else {
		// 				self.tbsmodel.qty = qty;
		// 				self.model.qty = 0;
		// 				if (qty > 15) {
		// 					self.model.qty = 0;
		// 					self.tbsmodel.qty = 15;
		// 				}
		// 			}
		// 			self.model.type = category;
		// 			self.viewingPreConfiguredQuiz = true;
		// 			self.previewMode = false;
		// 			

		// 			$location.path('/quiz/setup/step2');
		// 		}
		// 	}.bind(this));
		// }
		// else {
		// 	$rootScope.showTabLoading = true;
		// 	self.previewMode = false;

		// 	self.reloadQuiz({'quizlets': self.quizlets}, self.previewMode);
		// 	console.log(self.quizlets, ChapterService.chapterScores);
		// 	var mp = [];
		// 	var tbs = [];
		// 	if (category == 'mcq') {
		// 		var mp = ChapterService.chapterScores[ch][category][type];
		// 	}
		// 	if (category == 'tbs') {
		// 		var tbs = ChapterService.chapterScores[ch][category][type];
		// 	}

		// 	$http.post(ServiceURLS.reloadSessionUrl, {'questions': {'multipleChoice':mp,'tbs':tbs}}).then(function(data) {
		// 		if(data.questions.multipleChoice.length > 0) {
		// 			self.reloadQuizData(session, quizlets, {'content':data.questions.multipleChoice, 'category': 'mcq'}, 'multiple-choice');
		// 		} 
		// 		if (data.questions.tbs.length > 0)  {
		// 			self.reloadQuizData(session, quizlets, {'content': angular.copy(data.questions.tbs), 'category': 'tbs'}, 'tbs');
		// 		}
		// 		$rootScope.$broadcast("launchReloadedQuiz");	
		// 	});
		// 	// self.loadQuizByQids(ChapterService.chapterScores[chapterquizlets][category][type], category);
			// console.error('Fixme: SessionService.resetQuizlet() was called causing an error, its undefined');
		// 	// SessionService.resetQuizlet();
		// }
	};

	this.getSessionQuestionPosition = function() {
		var position = self.question;
		if(QuizletService.quizlet > 0) {
			for (var i = 0; i < QuizletService.quizlet; i++) {
				position = position + parseInt(QuizletService.requestedQuizlets[i].qty);
			};
		}
		return position;
	};

	this.updateCompletePercent = function(realTotalQuestions) {
		if(!realTotalQuestions) {
			self.realTotalQuestions = self.countRealTotalQuestions() || 1;
			realTotalQuestions = self.realTotalQuestions;
		}
		self.completePercent = Math.round((self.getSessionQuestionPosition() + 1) / realTotalQuestions * 100);
	};

	this.updateSessionScore = function() {
		var correct = 0;
		var attemptCount = 0;
		var times = [];
		var avtime = 0;
		var scoredIDs = [];
		for(var i = (self.sessionScore.length-1); i >= 0; i--) {
			var q = self.sessionScore[i];
			if (q.attempts) {
				if(typeof q.attempts == 'string') {
					q.attempts = JSON.parse(q.attempts);
				}
				times = times.concat(q.attempts);
				attemptCount += q.attempts.length;
				for(var j = 0; j < q.attempts.length; j++) {
						avtime += q.attempts[j];
				}
			}
			if (scoredIDs.indexOf(q.id) == -1) {
				if (q.type == 'tbs-research' || q.type == 'multiple-choice') {
					scoredIDs.push(q.id);
				}
				// only get the most recent score of a question if it has been answered more than once
				correct += Number(q.score);
			}
		
			if (!q.choices) {
				var type = "tbs";
			}
			else {
				var type = "multipleChoice";
			}
			
		}
		// Count real total questions
		self.realTotalQuestions = self.countRealTotalQuestions() || 1;
		self.attemptsAverage = parseFloat(times.length / self.sessionScore.length).toFixed(1);
		self.timeAverage = parseFloat((avtime / attemptCount) / 1000).toFixed(1) + " seconds";
		self.updateCompletePercent(self.realTotalQuestions);
		self.totalPercent = parseFloat("" + ((correct / self.realTotalQuestions) * 100)).toFixed(1);
		self.currentPercent = self.totalPercent;
	};

	this.reloadQuizData = function(session, quizlets, /*obj, */blankslate /*, quizletType*/, newData) {
//				var data = obj.content;
				var data = quizlets.questions;
//				var category = obj.category;
				var newArray = [];
				var counter = 0;
				var quizletType = null;
				var quizletChapters = [];
				if (blankslate) {
					self.saveMode = "new";
					self.updateSessionNameByQuizzesCount(function() {
						if(blankslate == 'retake') {
							self.saveSession(self.sessionName);
						}
					});
				}
				for(var i = 0; i < data.length; i++) {
						var q = data[i];
						var ch = parseInt(q.chapter_id);
						if (quizletChapters.indexOf(ch) == -1) {
							quizletChapters.push(ch);
						}
						if (typeof type == 'string' && typeof subtype == 'string') {
							if (self.chapterScores[ch][type][subtype].indexOf(q.id) == -1) {
								q.exclude = true;
							}
						
						}
						else if (typeof type == 'object') {
							if (type.indexOf(q.id) == -1) {
								q.exclude = true;
							}
						}
				
						if (!q.exclude /*!type || (isMcqType > -1) || (isTbsType > -1)*/) {
						if (self.previewMode) {
							if (q.choices != null) {
								quizletType = 'multiple-choice';
								q.type = 'multiple-choice';
								q.path = '/preview/question/mp/' + i + '/' + q.id;
							}
							else {
								quizletType = 'tbs';
								q.path = '/preview/question/' + q.type + '/' + i + '/' + q.id;
							}
						}
						else {
							if(q.choices != null) {
								quizletType = 'mcq';
								q.type = 'multiple-choice';
								q.path = '/quiz/question/mp/' + counter + '/' + q.id;
								q.quizletID = "multiple-choice-" + self.loadCount;
							} else {
								quizletType = 'tbs';
								q.path = '/quiz/question/' + q.type + '/' + counter + '/' + q.id;
								q.quizletID = q.type + "-" + self.loadCount;
							}
						}
/*
								angular.forEach(session.quizlets, function(quizlet, key) {
									quizlets.qty = quizlet.questions.length;
									angular.forEach(quizlet.questions, function(value, question_key) {
*/
										// if (session.id) {
										if (typeof session != 'undefined' && session) {
										// Check to see if session ID is set.  If it is, this means that the user is coming from the Score History page. If this is the case, then we want to reload the users' past answers, notes and bookmarks.
//											if (parseInt(value.id) === parseInt(q.id)) {
												if (blankslate) {
													q.flagged = false;
													q.noted = [];
													q.answer_id = null;
													q.answers = [];
													q.attempts = [];
												}
	
												q.isFlagged = !!parseInt(q.flagged);
												if (q.noted !== '' && typeof q.noted == 'string') {
													q.noted = JSON.parse(q.noted);
												}
/*
												else if (value.noted !== '' && typeof value.noted == 'object') {
													q.noted = value.noted;
												}
												else if (q.noted == undefined) {
													q.noted = [];
												}
*/
//												q.answer_id = value.answer_id;
//											}
										}
										else {
										// else, the user is coming from the Section Overview page, and we want to start a completely new quiz without any previous answers, notes or bookmarks.
											q.answer_id = null;
										}
/*
									});
								});
*/
							q.isFlagged = q.isFlagged || false;
/*
							if (q.noted) {
								q.noted = q.noted;
							}
							else {
								q.noted = [];
							}
*/
							if (!q.noted) {
								q.noted = [];
							}
							q.hasNotes = !!q.noted.length;
							q.index = i;
							newArray.push(q);
							counter++;
						}
				}
				if (newData) {
					angular.forEach(newData, function(question) {
						angular.forEach(newArray, function(new_question) {
								if (question.id == new_question.id) {
									new_question.topics = question.topics;
									new_question.chapter = question.chapter;
								}	
						});
					});
				}
				self.realTotalQuestions = data.length;
				self.quizlets.push( {questions:newArray, type: quizletType, chapters: quizletChapters} );
					self.loadCount++;
				self.quizID = session.id;
					angular.forEach(session.quizlets, function(quizlet) {
						quizlet.qty = quizlet.questions.length;
						angular.forEach(quizlet.questions, function(question) {
							if (question.answer_id != null) {
			//				QuizSession.reloadQuestionAttempt(question.answer_id);
							$rootScope.$broadcast("reloadQuestionAttempt", question);
//							QuestionService.reloadQuestionAttempt(question);
							}
						});
					});
	};

	this.reloadQuiz = function(session, /*pdata,*/ type, subtype, previewmode, blankslate) {
		if (blankslate) {
			self.resetQuizSession();
			if(blankslate == 'retake') {
				this.updateSessionNameByQuizzesCount();
			}
		}
		if (previewmode) {
			self.previewMode = true;
			self.sessioninfo = session;
		}
		if (session.id) {
			// If sessioninfo is true, it means that we are reloading a previously-saved quiz from the Score History page
			self.saveMode = "update";
			self.firstLoad = false;
		}
		else {
			// If sessioninfo is false, it means we are loading a completly new quiz from the Section Overview page 
			self.saveMode = "new";
			this.updateSessionNameByQuizzesCount();
		}
		$http.get(ServiceURLS.relaunchQuizUrl + '?session_id=' + session.id ).then(function(response) {
			session = response.data.sessions[0];
			var quizlets = [];
			angular.forEach(session.questions, function(question) {
				if (question.status == "skipped") {
					session.completed = false;
				}
				if (typeof quizlets[question.quizlet_num] == "undefined") {
					quizlets[question.quizlet_num] = {'questions': []};
					
				}
				quizlets[question.quizlet_num].questions.push(question);
			});
			session.quizlets = quizlets;
			if (session.quizlets) {
				self.model.pickSection = session.section;
				self.chosenChapters = [];
				angular.forEach(session.quizlets, function(quizlet) {
					angular.forEach(quizlet.questions, function(q) {
						self.chosenChapters.push(q.chapter);
					});
				});
				var quizlets = session.quizlets;
			}
			else {
				var quizlets = session;
			}
			self.loadCount = 0;
			var mp = [];
			var tbs = [];
			
			angular.forEach(quizlets, function(quizlet) {
				var quizletType = null;
				var quizletChapters = [];
				angular.forEach(quizlet.questions, function(question) {
					if (!type) {
						if(parseInt(question.type) == 0 || question.type == 'mcq' || question.type == 'multiple-choice') {
							mp.push(question.id);
							quizletType = 'multiple-choice';
						} else {
							tbs.push(question.id);
							quizletType = 'tbs';
						}
					}
					else {
						if (question.id) {
							question = question.id;
						}
/*
						if (type == 'mcq') {
							mp.push(question);
						}
						else {
							tbs.push(question);
						}
*/
					}
				});
				//var loadCount = 0;
	//			var promise = $q.defer();
			});
			var data = {
				questions:{"multipleChoice":mp,"tbs":tbs}
			};

//			$http.post( ServiceURLS.reloadSessionUrl, data)
//				.success(function(data, status, headers, config) {
//					if(data.questions.multipleChoice.length > 0) {
						angular.forEach(session.quizlets, function(quizlet) {
							self.reloadQuizData(session, quizlet, blankslate/*{'content':data.questions.multipleChoice, 'category': 'mcq'}*/);
						});
//					} 
//					if (data.questions.tbs.length > 0)  {
//						self.reloadQuizData(session, quizlets, {'content': angular.copy(data.questions.tbs), 'category': 'tbs'});
//					}
					$rootScope.$broadcast("launchReloadedQuiz");	
//				});
		});
	};	

	/**
	 * utility function to break out an array of quesitons into 2 buckets with the question ids
	 * @param Array questions A list of questions that have id's and types.
	 * @param (optional) String type An optional type to avoid having to check the type in the function itself.
	 */
	this.sortMcqAndTbsQuestionsIntoBuckets = function(questions, type) {

		var type = type || false;
		var mcq = [];
		var tbs = [];

		var isMcq = function(questionType) {
			return (parseInt(questionType) == 0 || questionType == 'mcq' || questionType == 'multiple-choice');
		}

		angular.forEach(questions, function(question) {
			var questionType;

			if (type) {
				questionType = type;
			} else {
				questionType = question.type;
			}

			if (isMcq(questionType)) {
				mcq.push(question.id);
			} else {
				tbs.push(question.id);
			}
		});

		return {
			"mcq": mcq,
			"tbs": tbs
		};
	};

	this.resetQuizSession = function() {
		this.newQuizFromSectionOverview = false;
		this.hasReviewedQuizDetails = true;
		this.specialPrefs = false;
		this.completePercent = 0;
		this.isSaved = false;
		this.currentPercent = "0.0";
		this.attemptsAverage = "0";
		this.sessionScore = [];
		this.sessioninfo = false;
		this.previewMode = false;
		this.model = {};
		this.saveMode = "new";
		this.model.pickSection = null;
		this.model.pickedTopics = [];
		this.tbsmodel.pickSection = null;
		this.tbsmodel.pickedTopics = [];
		ChapterService.selectedChapters = [];
		this.quizID = null;
		this.originalQuizlets = [];
		this.reloadableSession = [];
		this.totalQuestions = 0;
		this.realTotalQuestions = null;
		this.quizStarted = false;
		this.pages = [];
		this.lastStep = 1;
		this.furthestStepReached = 1;
		this.currentPage = 0;
		ChapterService.chosenChaptersList = {};
		ChapterService.chosenChapters = 0;
		this.quizlets = [];
		this.totalPercent = 0;
		$rootScope.$broadcast("reset");
/*
		this.correct = {"multipleChoice":[], "tbs":[]};
		this.incorrect = {"multipleChoice":[], "tbs":[]};
		this.flagged = {"multipleChoice":[], "tbs":[]};
		this.noted = {"multipleChoice":[], "tbs":[]};
		this.skipped = {"multipleChoice":[], "tbs":[]};
*/
	};

	this.reinit = function() {
		this.model.pickedTopics = [];
		this.previewMode = false;
	};
	this.previewQuestions = function(ch, category, type) {
		if (ChapterService.getQuestionCount(ch, category, type) == 0) {
			return;
		}
		$rootScope.showTabLoading = true;
			// ch = chapter id
			// category = mcq or tbs
			// type = 'incorrect', 'correct', 'skipped', 'noted' or 'flagged'
			if (type == 'skipped') {
				this.viewQuestions(ch,category,type);
				return;
			}
				this.showInstructions = false;
				this.previewMode = true;

                console.log("@TODO: create loading event. (Preview quiz init)");

				$http.post(ServiceURLS.questionPreviewUrl, {'chapter_id': ch, 'category': type, 'type': category}).then(function(response) {
					if (type == 'flagged') {
						self.previewingFlaggedQuestions = true;	
					}
					else {
						self.previewingFlaggedQuestions = false;
					}
	//				$rootScope.quizlets = {'quizlets': [{'questions':[]}]}; 
					self.quizlets = [{'questions':[],'type':null}];
					self.quizlets[0].type = response.data.type;
					angular.forEach(response.data.questions, function(q) {
						self.quizlets[0].questions.push(q);
						$rootScope.$broadcast("reloadQuestionAttempt", q);
					});
					self.processLoadedQuiz(self, ch, category, type, 'preview');	
				});
//			}
	};
	this.specificCategoryQuiz = function(ch, category, type, maxQtys) {
		$http.post(ServiceURLS.questionPreviewUrlV2, {'chapter_id': ch, 'category': type, 'type': category, 'nasba': self.hasNASBA}).then(function(response) {
//					self.quizlets = [{'questions':[],'type':null}];
					self.quizlets = [];
					var mcq_quizlet = {'questions': [], 'type': 'mcq'};
					var tbs_quizlet = {'questions': [], 'type': 'tbs'};
					var mcqIncluded = false;
					var tbsIncluded = false;
//					self.quizlets[0].type = response.data.type;
					angular.forEach(response.data.questions, function(questions) {
						angular.forEach(questions, function(q) {
							if (q.choices) {
								if (!mcqIncluded && maxQtys.mcq > 0) {
									self.quizlets[0] = angular.copy(mcq_quizlet);
									mcqIncluded = true;
								}
								if (self.quizlets[0].questions.length <= maxQtys.mcq && maxQtys.mcq > 0) {
									self.quizlets[0].questions.push(q);
								}
							}
							else {
								if (maxQtys.mcq > 0) {
									if (!tbsIncluded && maxQtys.tbs > 0) {
										self.quizlets[1] = angular.copy(tbs_quizlet);
										tbsIncluded = true;
									}
									if (self.quizlets[1].questions.length <= maxQtys.tbs && maxQtys.tbs > 0) {
										self.quizlets[1].questions.push(q);
									}
								}
								else {
									if (!tbsIncluded && maxQtys.tbs > 0) {
										self.quizlets[0] = angular.copy(tbs_quizlet);
										tbsIncluded = true;
									}
									if (self.quizlets[0].questions.length <= maxQtys.tbs && maxQtys.tbs > 0) {
										self.quizlets[0].questions.push(q);
									}
								}
							}		
						});
					});
					self.viewQuestions(self.quizlets);
//					self.processLoadedQuiz(self);	
		});
	};
	this.saveQuestionScores = function(postput, singleQuestion, sessionName, session_type, section) {
		if (!self.savingInProgress) {
			self.savingInProgress = true;
			var currentId = false;
			if (self.currentQuestion && !singleQuestion) {
				currentId = self.currentQuestion.id;
			}
/*
			if (singleQuestion) {
				currentId = singleQuestion.id;
			}
*/
			var dataobj = [];
			angular.forEach(self.quizlets, function(quizlet, quizlet_num) {
				var qStatus = null;
				var type = null;
				angular.forEach(quizlet.questions, function(question) {
					var json_answer = null;
					qStatus = "skipped";
					if (question.type == 'multiple-choice' || question.type == 'mcq') {
						type = 'mcq';
					}
					else if (question.type !== 'multiple-choice' && question.type !== 'mcq') {
						type ='tbs';
					}

					var answer = null;
					if (!singleQuestion) {
						for(var i = 0; i < self.sessionScore.length; i++) {
								//var quizlet = self.sessionScore[i].quizlet.match(/\d/g).join("");
								if (self.sessionScore[i].id == question.id/* && quizlet_num == quizlet*/) {
									answer = self.sessionScore[i];
									break;
								}
						}
					}
					else if (singleQuestion) {
						if (singleQuestion.answers) {
							answer = singleQuestion;
						}
					}
					if (answer != null) {
						if (typeof answer.answers == 'object') {
							answer.answers = answer.answers.filter(function(e){
								return (e && e != 0);
							});
							if(type !== 'mcq'){
							if (answer.answers[0] !== undefined && answer.question_id == question.id) {
								if (typeof answer.answers[0] !== 'string') {
									json_answer = JSON.stringify(answer.answers);
									if (qStatus == 'skipped') {
										if (answer.type == 'tbs-journal' || answer.type == 'tbs-wc') {
											if (parseFloat(answer.score) >= 0.75) {
												qStatus = 'correct';
											}
											else {
												qStatus = 'incorrect';
											}
											if (answer.type == 'tbs-wc') {
												if (answer.answer_id.solution == '<p></p>' || answer.answer_id.solution == '' || answer.answer_id.solution == undefined) {
													qStatus = 'skipped';
												}
											}
										} else {
											// TBS-Research
											var tbs_answer = answer.answers[answer.answers.length - 1];
											if (tbs_answer && tbs_answer.isCorrect !== undefined) {
												if (tbs_answer.isCorrect) {
													qStatus = 'correct';
												}
												else if (!tbs_answer.isCorrect) {
													qStatus = 'incorrect';
												}
											}
										}
									}
								}
								else if (typeof answer.answers[0] == 'string') {
									json_answer = answer.answers[answer.answers.length-1];
									if (answer.score !== null) {
										if (parseFloat(answer.score) > 0.75) {
											qStatus = 'correct';
										}
										else {
											qStatus = 'incorrect';
										}
									}
								}
							}
						} else {
								if (answer.answers.length > 0) {
										json_answer = answer.answers[answer.answers.length-1];
										if (parseInt(answer.score) != 0) {
											qStatus = 'correct';
										}
										else {
											qStatus = 'incorrect';
										}
								}
								else {
									qStatus = 'skipped';
								}
							}
						}
						else {
							if (question.answer_id) {
								answer.answers = [answer.answer_id];
								json_answer = answer.answer_id;
								if (parseInt(answer.score) != 0) {
									qStatus = 'correct';
								}
								else {
									qStatus = 'incorrect';
								}
							} 
						}
						if (answer.attempts) {
							if (answer.attempts.length > 0) {
								var time = answer.attempts[answer.attempts.length-1];
							}
						}
						else {
							var time = 0;
						}
						var score = answer.score;
						var attempts = JSON.stringify(answer.attempts);
					}
					else {
						var score = 0;
						var time = null;
						var attempts = JSON.stringify([]);
					}
/*
					if(typeof(question.noted) !== "undefined"){
						question.notes = JSON.parse(question.noted);
						delete question.noted;
					}
*/
					if (!singleQuestion && currentId == question.id) { 
						if (postput == 'update') {
							if (self.prev_answer_id !== json_answer || self.prev_id !== question.id) {
								self.savingQuestion(question.id);
								self.saveThisQuestion(question.id, quizlet_num, type, qStatus, json_answer, score, attempts, time, sessionName, session_type, section);
								self.prev_answer_id = json_answer;
								self.prev_id = question.id;
							}
							else {
								self.isSaved = true;
								self.savingSession = false;
								self.savingInProgress = false;
								self.notSavingQuestion(question.id);
							}
						}
					}
				if (singleQuestion) { 
					if (singleQuestion.id == question.id) {
						if (postput == 'update') {
							if (self.prev_answer_id !== json_answer || self.prev_id !== question.id) {
								self.savingQuestion(question.id);
								self.saveThisQuestion(question.id, quizlet_num, type, qStatus, json_answer, score, attempts, time, sessionName, session_type, section);
								self.prev_answer_id = json_answer;
								self.prev_id = question.id;
							}
							else {
								self.isSaved = true;
								self.savingSession = false;
								self.savingInProgress = false;
								self.notSavingQuestion(question.id);
							}
						}
					}
				}
					if (postput == 'new') {
						dataobj.push(
								{
									id: question.id,
									session_id: String(self.quizID),
									student_id: JSON.parse($.cookie('data')).studentID,
									quizlet_num: String(quizlet_num),
									timestamp: new Date(),
									type: type,
									status: qStatus,
//									noted: JSON.stringify(question.noted),
//									flagged: question.isFlagged ? "1" : "0",
									answer_id: json_answer,
									score: String(score),
									attempts: attempts,
									time: time
								}
						);
					}
				});
			});
				if (postput == 'new') {
					$rootScope.showTabLoading = true;
					$http.post(ServiceURLS.saveQuestionScores, 
						dataobj)
					.success(function(data) {
						self.savingInProgress = false;
						$rootScope.showTabLoading = false;
					});
				}
		} // savingInProgress check			
	}

	this.saveThisQuestion = function(id, quizlet_num, type, qStatus, json_answer, score, attempts, time, sessionName, session_type, section) {
//marker
			$http.put(ServiceURLS.saveQuestionScores, 
				{
					'quiz': {
						id: self.quizID,
						student_id: JSON.parse($.cookie('data')).studentID,
						session_name: sessionName,
						average_time: self.timeAverage,
						total_percent: self.totalPercent,
						current_percent: self.currentPercent,
						attempts_average: self.attemptsAverage,
						session_type: session_type,
						section: section,
						complete_percent: self.completePercent,
						timestamp: new Date(),
						active: 1
					},
					'question_scores': {
						id: id,
						session_id: String(self.quizID),
						student_id: JSON.parse($.cookie('data')).studentID,
						quizlet_num: String(quizlet_num),
						timestamp: new Date(),
						type: type,
						status: qStatus,
	//									noted: JSON.stringify(question.noted),
	//									flagged: question.isFlagged ? "1" : "0",
						answer_id: json_answer,
						score: String(score),
						attempts: attempts,
						time: time
					}
				})
			.success(function(data) {
				self.isSaved = true;
				self.savingSession = false;
				self.savingInProgress = false;
				self.notSavingQuestion(id);
/*
				self.prev_answer_id = json_answer;
				self.prev_id = question.id;
*/
				if ($location.path().indexOf('/quiz/session/save') !== -1) {
					$location.path('/final-score');
				}
			});
	};


	this.calculateFinalScore = function() {
		self.topics = {};
		self.topicsByArray = [];
		self.chapterScores = {};

		for(i = 0; i < self.quizlets.length; i++) {
			var quizlet = self.quizlets[i];
			var qLen = quizlet.questions.length;
			for(var j = 0; j < qLen; j++) {
				var question = quizlet.questions[j];
				if (question.topics) { 
				for(var t = 0; t < question.topics.length; t++) {
					var topic = question.topics[t];
					
					var key = "t_" + topic.topic_id + "_c_" + topic.chapter_id;
					if(self.topics[key] == null) {
						self.topics[key] = {
							topic_id:topic.topic_id,
							chapter_id:topic.chapter_id,
							course_url:topic.course_url,
							num_chapter_questions: topic.num_chapter_questions,
							questions:[]
						}
					}
					self.topics[key].meta = self.findTopic(topic.chapter_id, topic.topic_id);
						/* // Just keeping this here in case we ever need to remove deprecated questions from old saved quizzes. The below code will mark how many deprecated questions there are for a given topic, so that if a question is deprecated, you won't see "X out of 0 questions" on the Score History page.
					if (QuizConfigState.saveMode == "new") {
						if (question.deprecated) {
							if (self.topics[key].deprecated >= 1) {
								self.topics[key].deprecated++;
							}
							else {
								self.topics[key].deprecated = question.deprecated;
							}
						}
					}
					*/
					var sessionAnswer = self.findSessionAnswer(question.id);
					self.topics[key].questions.push(sessionAnswer);
				}
			}
			}
		}
		angular.forEach(self.topics, function(topic) {
			topic.totalQuestions = topic.questions.length;
			topic.qtyCorrect = 0;
			angular.forEach(topic.questions, function(question) {
				if(question != null && question.score != null) {
					topic.qtyCorrect += question.score;
					topic.score = parseFloat("" + (topic.qtyCorrect / topic.totalQuestions * 100)).toFixed(1);
				}
			});
/*
			if (parseFloat(topic.score) > 0) {
			}
			else {
			}
*/
			self.topicsByArray.push(topic);
		});

		self.topicsByArray.sort(function(a, b) {
			if(a.score == null) {
				a.score = 0;
			}
			if(b.score == null) {
				b.score = 0;
			}
			return a.score - b.score;
		});

		angular.forEach(self.topicsByArray, function(t) {
			t.scoreClass = "";
			if(t.score > 90) {
				t.scoreClass = "label label-success";
			} else if(t.score > 84) {
				t.scoreClass = "label label-warning";
			} else {
				t.scoreClass = "label label-danger";
			}
		});

		if(self.totalPercent > 90) {
			self.scoreClass = "label label-success";

		} else if(self.totalPercent > 84) {
			self.scoreClass = "label label-warning";
		} else {
			self.scoreClass = "label label-danger";
		}


	};

	this.getQuestionType = function(q) {
		if (q.choices) {
			return "multiple-choice";
		}
		else if (q.question_json.indexOf('questionGridBlock') !== -1) {
			return "tbs-journal";
		}
		else if (q.question_json.indexOf('researchInput') !== -1) {
			return "tbs-journal";
		}
		else {
			return "tbs-wc";
		}
	};

	this.processLoadedQuiz = function(data, chapter, type, stat, preview, sessioninfo) {
				var quizletsList = [];
//				var newQuizlets = [];
				if (data.quizlets) {
					var chapters = 0;
					if (preview) {
						this.previewMode = true;
					}
					for(var j = 0; j < data.quizlets.length; j++) {
						if (data.quizlets[j] !== null && data.quizlets[j]['questions'] && data.quizlets[j]['questions'].length) {
								if (data.quizlets[j].type || type) {
									var i = 0;
									quizletsList.push(data.quizlets[j]);
									var q = data.quizlets[j];
									if (q.type == 'mcq' || type == 'mcq') {
										q.type = 'multiple-choice';
									}
									else if (q.type !== 'mcq' && type !== 'mcq' && q.type !== 'multiple-choice' && type !== 'multiple-choice' && q.type !== 'multipleChoice' && type !== 'multipleChoice'){
										q.type = 'tbs';
									}
//									newQuizlets.push(angular.copy(data.quizlets[j]));
//									var newQuestions = [];
//									var nonDeprecated = 0;
									if(q.type == "multiple-choice") {
										for(i = 0; i < q.questions.length; i++) {
											q.questions[i].type = self.getQuestionType(q.questions[i]);
											if (q.questions[i].answer && !q.questions[i].answer_id) {
												q.questions[i].answer_id = q.questions[i].answer.id;
											}
//											if (!q.questions[i].deprecated) { 
											if (q.questions[i].noted) {
												if (typeof q.questions[i].noted == "string") {
													// there was a time when question_scores.noted database data was limited to 1000 chars,
													// if that was the case, we had a potential JSON string saved as courpted, e.g.
													// ["string
													// to be valid its required to be a valid array of notes, e.g.
													// ["string"]
													//
													// to solve this when we load, we can try/catch the JSON parse, and if it fails, we'll strip
													// off the initial [" in cases that JSON pars fails
													try {
														q.questions[i].noted = JSON.parse(q.questions[i].noted);
													} catch(err) {
														// console.log('Caught invalid JSON note. Try to fix here if we are able', i);
														q.questions[i].noted = q.questions[i].noted.replace(/\["/, "");
														// console.log(q.questions[i].noted);
													}
												}
											}
												if (q.questions[i].topics) {
													angular.forEach(ChapterService.topics, function(chapter) {
														angular.forEach(q.questions[i].topics, function(topic) {
															if (chapter.id == parseInt(topic.chapter_id)) {
															topic.num_chapter_questions = self.totalQuestions[chapter.id];
															}
														});
													});
												}
												q.questions[i].type = "mcq";
												if (preview) {
													q.questions[i].path = '/preview/question/mp/' + i + '/' + q.questions[i].id;
													if (!chapter && !type && !stat) {
													q.questions[i].oldquiz = true;
													}
													else {
														q.questions[i].chapter_id = chapter;
														//q.questions[i].type = type;
														q.questions[i].status = stat;
													}
												}

												else {
													q.questions[i].path = '/quiz/question/mp/' + i + '/' + q.questions[i].id;
												}
												q.questions[i].quizletID = q.type + "-" + j;
												if (q.questions[i].isFlagged == undefined) {
													q.questions[i].isFlagged = false;
												}
												if (q.questions[i].noted == undefined) {
													q.questions[i].noted = [];
													q.questions[i].hasNotes = false;
												}
												else if (q.questions[i].noted) {
													if (q.questions[i].noted.length > 0) {
														q.questions[i].hasNotes = true;
													}
												}
												q.questions[i].index = i;
//												nonDeprecated++;
//												newQuestions.push(q);
//											}
										}
									} else {
										for(i = 0; i < q.questions.length; i++) {
//											if (!q.questions[i].deprecated) { 
											if (q.questions[i].noted) {
												if (typeof q.questions[i].noted == "string") {
													q.questions[i].noted = JSON.parse(q.questions[i].noted);
												}
											}
												if (q.questions[i].topics) {
													angular.forEach($rootScope.topics, function(chapter) {
														angular.forEach(q.questions[i].topics, function(topic) {
															if (chapter.id == parseInt(topic.chapter_id)) {
															topic.num_chapter_questions = self.totalQuestions[chapter.id];
															}
														});
													});
												}

//												q.questions[i].type = "tbs";
												if (q.questions[i].type_long_name) {
													q.questions[i].type = q.questions[i].type_long_name;
												}
												if (preview) {
													q.questions[i].path = '/preview/question/' + q.questions[i].type + '/' + i + '/' + q.questions[i].id;
													if (!chapter && !type && !stat) {
														q.questions[i].oldquiz = true;
													}
													else {
														q.questions[i].chapter_id = chapter;
														//q.questions[i].type = type;
														q.questions[i].status = stat;
													}
												}
												else {
													q.questions[i].path = '/quiz/question/' + q.questions[i].type + '/' + i + '/' + q.questions[i].id;
												}
												q.questions[i].quizletID = q.type + "-" + j;
												if (q.questions[i].isFlagged == undefined) {
													q.questions[i].isFlagged = false;
												}
												if (q.questions[i].noted == undefined) {
													q.questions[i].noted = [];
													q.questions[i].hasNotes = false;
												}
												else if (q.questions[i].noted) {
													if (q.questions[i].noted.length > 0) {
														q.questions[i].hasNotes = true;
													}
												}
												q.questions[i].index = i;
//												nonDeprecated++;
//												newQuestions.push(q);
//											}
										}
									}
//								newQuizlets[j].questions = newQuestions;
								}
						}
					}
				}
			if (quizletsList.length > 0) {
					self.quizlets = angular.copy(quizletsList);
					self.quizlet = 0;
					self.question = 0;	
					if (self.newQuizFromSectionOverview) {
						self.saveMode = "new";
					}
					if (preview) {
						self.saveMode = "update";
					}
					self.realTotalQuestions = self.quizlets[0].questions.length;
					if (sessioninfo) {
						self.sessioninfo = sessioninfo;
					}
					self.quizStarted = true;
					self.firstLoad = true;
					if (self.examSimulator) {
						self.showTimer = true;
					}
					$location.path(self.quizlets[0].questions[0].path);
			}
			else {
				var opts = {
					    backdrop: true,
					    keyboard: true,
					    backdropClick: true,
					    templateUrl: 'views/warning-noquizlets.php',
					    controller: 'NewQuizWarningController',
						dialogFade: true,
					    windowClass: '',
					 };
				    var d = $modal.open(opts).result.then(function() {
				    });
			}
	}

	this.loadQuiz = function() {
		var specificCategory = self.specificCategory;
		if (self.specificCategory == false || self.examSimulator) {
			specificCategory = 'none';
		}
		$http.post(ServiceURLS.quizConfig, {'quizlets': QuizletService.requestedQuizlets, 'nasba': self.hasNASBA, 'filterQuestions': specificCategory}).success(function(data, status, headers, config) {
			self.processLoadedQuiz(data);
		});
	};

	this.findSessionAnswer = function(questionID) {
		/**
		locates object used for the quiz session score created in addQuestionAttempt()
		*/
		var question = null;
		for(var i = 0; i < this.sessionScore.length; i++) {
			var q = this.sessionScore[i];
//			if(q.question_id == questionID) {
			if (q.id == questionID) {
				question = q;
			}
		}
		return angular.copy(question);
	};

	this.findTopic = function(chapterID, topicID) {
			var topicTitle = "";
			var answer = { chapter:"", topic:""};
				angular.forEach(ChapterService.topics, function(chapter) {
					if(chapter.id == chapterID) {
						answer.chapter = chapter.chapter;
						answer.chapterID = chapter.id;
						angular.forEach(chapter.topics, function(topic) {
							if(topic.id == topicID) {
								answer.topic = topic.topic;
								answer.topicID = topic.id;
							}
						});
					}
				});
			return answer;
		};
	this.saveSession = function(sessionName, currentQuestion, dontCountAsAttempt) {
		if (!self.savingSession) {
		self.savingSession = true;
		var section = SectionService.section;
		if (self.examSimulator) {
			var session_type = 'exam';
		}
		else {
			var session_type = 'quiz';
		}
			var unanswered = [];
			var quizletIndex = 0;
			var questionIndex = 0;
			var quizlets = [];
			angular.forEach(self.quizlets, function(quizlet, quizlet_num) {
				var saveQuizlet = [];
				quizlets.push(saveQuizlet);
				var qStatus = null;
				var type = null;
				angular.forEach(quizlet.questions, function(question) {
/*
					if(typeof(question.flagged) !== "undefined"){
						question.isFlagged = question.flagged === "1";
						delete question.flagged;
					}
*/
/*
					if(typeof question.noted !== "undefined"){
consle.log(question.noted);
						question.notes = JSON.parse(question.noted);
						question.hasNotes = question.notes.length > 0;
						delete question.noted;
					}
*/
					saveQuizlet.push({id:question.id, isFlagged: question.isFlagged, noted: question.noted, type:$rootScope.typeToInt(question.type)});
					for(var i = 0; i < self.sessionScore.length; i++) {
						var answer = self.sessionScore[i];
					}
				}); 
				quizletIndex++;
			});
			self.calculateFinalScore();
			if(self.saveMode == "new") {
				self.timestamp = new Date();
				$http.post(ServiceURLS.saveQuizUrl, 
					{student_id: JSON.parse($.cookie('data')).studentID,
					session_name: sessionName,
					average_time: self.timeAverage,
					total_percent: self.totalPercent,
					current_percent: self.currentPercent,
					attempts_average: self.attemptsAverage,
					complete_percent: self.completePercent,
					session_type: session_type,
					section: section,
					timestamp: self.timestamp,
					active: 1
					})
				.success(function(data, status, headers, config) {
					self.quizID = data.id;
					self.savingInProgress = false;
					angular.forEach(self.quizlets, function(quizlet) {
						quizlet.chapters = quizlet.chapters || [];
						angular.forEach(quizlet.questions, function(question) {
							angular.forEach(question.topics, function(topic) {
								if (topic && topic.chapter_id && quizlet.chapters.indexOf(topic) == -1) {
									quizlet.chapters.push(topic.chapter_id);
								}
							});
						});
					});
					self.originalQuizlets = self.quizlets;
					self.isSaved = true;
					self.savingSession = false;
					if (self.saveMode == "new") {
						self.saveMode = "update";
					}
			
					self.saveQuestionScores('new');
					self.reloadableSession = $rootScope.quizlets;
				});

			} else {
/*
				$timeout(function() {
//marker
					if (self.savingInProgress) {
						var opts = {
						    backdrop: true,
						    keyboard: true,
						    backdropClick: false,
						    templateUrl: 'views/save-try-again.php',
						    controller: 'genericModalController',
							dialogFade: true,

						};
					    var d = $modal.open(opts).result.then(function(result) {
							if (result == "ok" && self.savingInProgress) {
								self.saveQuiz(sessionName, session_type, section, currentQuestion);
							}
						    });
					}

				}, 120000);
*/
//				self.saveQuiz(sessionName, session_type, section, currentQuestion);
//marker
				self.originalQuizlets = $rootScope.quizlets;
				self.sessionName = sessionName;
				self.saveQuestionScores('update', currentQuestion, sessionName, session_type, section);
			}
		} // savingSession check
	};

	this.saveQuiz = function(sessionName, session_type, section, currentQuestion) {
		$http.put(ServiceURLS.saveQuizUrl, 
			{
			id: self.quizID,
			student_id: JSON.parse($.cookie('data')).studentID,
			session_name: sessionName,
			average_time: self.timeAverage,
			total_percent: self.totalPercent,
			current_percent: self.currentPercent,
			attempts_average: self.attemptsAverage,
			session_type: session_type,
			section: section,
			complete_percent: self.completePercent,
			timestamp: new Date(),
			active: 1
			})
			.success(function(data, status, headers, config) {
				self.savingInProgress = false;
				self.originalQuizlets = $rootScope.quizlets;
				self.isSaved = true;
				self.savingSession = false;
				self.sessionName = sessionName;
				self.saveQuestionScores('update', currentQuestion);
			});
	};

	this.updateSessionNameByQuizzesCount = function(callback) {
		callback = callback || function() {};
		self.updateSessionNameByQuizzesCountPromise().then(callback);
	};

	this.updateSessionNameByQuizzesCountPromise = function() {
		var httpData = {
			method:'GET',
			url: ServiceURLS.getQuizzesCount + "?student_id=" + JSON.parse($.cookie('data')).studentID + '&section=' + SectionService.section
		};
		var httpPromise = $http(httpData)
			.success(function(data, status, headers, config) {
				var count = parseInt(data[0]['count']) + 1;
				self.sessionName = SectionService.section + " Session #" + count;
			});
		return httpPromise;
	}

	this.countRealTotalQuestions = function() {
		var count = 0;
		angular.forEach(self.quizlets, function(qzlt, idx) {
			if(angular.isDefined(qzlt['questions'])) {
				count += qzlt['questions'].length;
			}
		});
		return count;
	}

	return this;
});

TestSimModule.service("QuestionService", function($rootScope, SessionService) {
	this.type = "multiple-choice";
	this.question = 0;
	var self = this;

	$rootScope.$watch(function() { return self.question }, function(newValue, oldValue) {
		SessionService.question = newValue;
	});

	$rootScope.$on("reset", function() {
		self.question = 0;
	});

	$rootScope.$on("reloadQuestionAttempt", function(evt, question) {
		self.reloadQuestionAttempt(question);
	});

	this.reloadQuestionAttempt = function(attempt) {
//		var question = null;
//		attempt.question_id = attempt.id;
		if (attempt.answer_id !== "" && typeof attempt.answer_id !== 'object' && attempt.answer_id.indexOf("<") !== 0) { 
			attempt.answers = JSON.parse(attempt.answer_id);
		}
		else if ((attempt.answer_id !== "" && typeof attempt.answer_id == 'object') || attempt.answer_id.indexOf("<") == 0) {
			attempt.answers = attempt.answer_id;
		}
		else {
			attempt.answers = [];
		}
		SessionService.sessionScore.push(attempt);
		SessionService.updateSessionScore();
	};

	this.addQuestionAttempt = function(question, notedorflagged) {
		var noted = question.noted, 
		isFlagged = question.isFlagged,
		questionID = question.id, 
		quizletID = question.quizletID, 
		questionType = question.type, 
		unanswered = false,
		topics = question.topics;
		if (question.type == 'tbs-journal') {
			question.answer_id = question.question.solution;
		}
		if (question.answer_id) {
			if (question.type !== 'multiple-choice' && question.type !== 'tbs-wc') {

				if(typeof question.answer_id == 'string') {
					try {
						question.answer = JSON.parse(question.answer_id);
					}
					catch(err) {
						question.answer = question.answer_id;
					}
				} else {
					question.answer = question.answer_id;
				}
			}
/*
			else {
				question.answer = question.answer_id;
			}
*/
			if (question.type == 'tbs-research') {
				question.answer = question.answer[question.answer.length-1];
			}
		}

		if (!question.answer && !question.answers) {
			time = 0;
			score = 0;
			answerID = 0;
			unanswered = true;
		}
		else {
			if (question.answer && question.type != 'tbs-journal') {
				time = question.answer.time;
				if (question.type == 'multiple-choice') {
					score = (question.answer.is_correct == "1") ? 1 : 0; 
				}
				else if (question.type == 'tbs-research') {
					score = question.answer.isCorrect ? 1 : 0;
				}
				else {
					score = parseFloat(question.answer.score);
				}

				answerID = question.answer.id; 

				if (question.answer.solution != null) {
				// TBS-WC
					answerID = question.answer.solution;
				}
			} else if (question.answers) {
				time = question.time;
				score = question.score;
				answerID = question.answers; 
			}
		}

		if (notedorflagged) {
			time = 0;
		}

		var savedNotes = null;
		if(noted && noted.length > 0) {
			savedNotes = noted;
		}
		self.totalQuestions = 0;
		for(i = 0; i < SessionService.quizlets.length; i++) {
			self.totalQuestions += SessionService.quizlets[i].questions.length;
		}
		if(questionType == null) {
			questionType = 'multiple-choice';
		} else if (questionType == "tbs-journal") { //must filter out references to DOM elements
			var newAnswerID = [];
			for(var i = 0; i < answerID.length; i++) {
				newAnswerID.push({noted:savedNotes, answer:answerID[i].answer, buttonID: answerID[i].answerButton.attr("id"), givenAnswer:answerID[i].givenAnswer, keyID: answerID[i].list.model.id});
			}
			answerID = newAnswerID;
		}
		else if (questionType == "tbs-research" || questionType == 'tbs-wc') {
			answerID = question.answer;
		}
		var question = null;
		if(SessionService.sessionScore.length > 0) {
			//find existing question in sessionScore
			for(var i = 0; i < SessionService.sessionScore.length; i++) {
				var q = SessionService.sessionScore[i];
				if(q.question_id == questionID) {
					question = q;
				}
			}
		}
		if(question == null) {
			question = {noted:savedNotes, id:questionID, question_id:questionID, quizlet:quizletID, type:questionType, answers:[answerID], score:score, flagged: isFlagged};
			if (time > 0) {
				question['attempts'] = [time];
			}
			else {
				question['attempts'] = [];
			}
			SessionService.sessionScore.push(question);
		} else {
			if (typeof question.answers !== 'object') {
				question.answers = [];
			}
			question.answers.push(answerID);
			if (typeof question.attempts !== 'object' || question.attempts == undefined) {
				question.attempts = [];
			}
			if (time > 0) {
				question.attempts.push(time);
			}
			question.score = score;
		}
			var repeatedChapters = [];
			angular.forEach(topics, function(topic) {
				if (repeatedChapters.indexOf(parseInt(topic.chapter_id)) == -1) {
					repeatedChapters.push(parseInt(topic.chapter_id));
				}
			});
		// UPDATE SESSION SCORE
		SessionService.updateSessionScore();
		if (question.answers) {
			question.answer_id = question.answers[question.answers.length-1];
		}
		else if (question.answer) { //tbs-wc
			question.answer_id = question.answer;
		}
		SessionService.saveSession(SessionService.sessionName, question);
	};
});

TestSimModule.controller("TimerController", function($rootScope, $scope, $timeout, $location, SessionService, SectionService) {
	$scope.section = SectionService.section;
	$scope.wait = false;

	$scope.setTimer = function() {
		$scope.pastTime = new Date();	
		$scope.getTimeElapsed();
	};	

	$scope.setCountdown = function(obj) {
		$scope.curTime = new Date().getTime();
		$scope.futureTime = new Date();

		$scope.timerSettings = obj;
		if (obj.hours) {
			$scope.curHour = new Date().getHours();
			$scope.futureTime.setHours($scope.curHour + obj.hours);
			if ($scope.curTime >= $scope.futureTime.getTime()) {
				console.log('cannot set countdown to time in the past');
				return false;
			}
			$scope.futureHour = $scope.futureTime.getHours();
		}
		if (obj.minutes) {
			$scope.curMin = new Date().getMinutes();
			$scope.futureTime.setMinutes($scope.curMin + obj.minutes);
			if ($scope.curTime >= $scope.futureTime.getTime()) {
				console.log('cannot set countdown to time in the past');
				return false;
			}
			$scope.futureMin = $scope.futureTime.getMinutes();
		}

		$scope.getTimeRemaining();

/*
		$scope.timerReached = false;

		$scope.convertedCurHour = $scope.curHour;
		$scope.curHourPM = false;
		if ($scope.curHour > 12 || $scope.curHour == 0) {
			$scope.curHourPM = true;
			$scope.convertedCurHour = $scope.hourLookup[$scope.curHour];
		}
		$scope.convertedFutureHour = $scope.futureHour;
		$scope.futureHourPM = false;
		if ($scope.futureHour > 12 || $scope.futureHour == 0) {
			$scope.futureHourPM = true;
			$scope.convertedFutureHour = $scope.hourLookup[$scope.futureHour];
		}
*/		
	}

	$scope.getTimeElapsed = function() {
		if (!$scope.timerPaused) {
			$scope.timerMinutesElapsed = Math.floor((((new Date().getTime() - new Date($scope.pastTime.getTime())) % 86400000) % 3600000) / 60000);
			$scope.timerHoursElapsed = Math.floor(((new Date().getTime() - new Date($scope.pastTime.getTime())) % 86400000) / 3600000);
			$scope.timerSecondsElapsed = Math.floor(((((new Date().getTime() - new Date($scope.pastTime.getTime())) % 86400000) % 3600000) % 60000) / 1000);
			$scope.elapsedInterval = $timeout(function() {
				$scope.getTimeElapsed();
			}, 1000);
		}
	};
	
	$scope.getTimeRemaining = function() {
		if (!$scope.checkIfTimerDone()) {
			$scope.timerSecondsRemaining = Math.floor(((((new Date($scope.futureTime.getTime()) - new Date().getTime()) % 86400000) % 3600000) % 60000) / 1000);
			$scope.timerMinutesRemaining = Math.floor((((new Date($scope.futureTime.getTime()) - new Date().getTime()) % 86400000) % 3600000) / 60000);
			$scope.timerHoursRemaining = Math.floor(((new Date($scope.futureTime.getTime()) - new Date().getTime()) % 86400000) / 3600000);

			$scope.timeRemainingInterval = $timeout(function() {
				$scope.getTimeRemaining();
			}, 1000);
		}
	}

	$scope.$on("pauseTimer", function() {
		$scope.pauseTimer();
	});

	$scope.$on("unpauseTimer", function() {
		$scope.unpauseTimer();
	});

	$scope.unpauseTimer = function() {
		var pauseAmount = new Date() - $scope.timerPaused; 
		$scope.pastTime.setTime($scope.pastTime.getTime() + pauseAmount);
		$scope.timerPaused = false;
		$scope.getTimeElapsed();
	};

	$scope.pauseTimer = function() {
		$scope.timerPaused = new Date();
		$timeout.cancel($scope.elapsedInterval);
	};

	$scope.resetTimer = function() {
		$timeout.cancel($scope.elapsedInterval);
	};

	$scope.resetCountdown = function() {
		$timeout.cancel($scope.timeRemainingInterval);
	};

	$scope.getTimerMinutesElapsed = function() {
		
		return $scope.timerMinutesElapsed;
	};

	$scope.getTimerHoursElapsed = function() {
		return $scope.timerHoursElapsed;
	};

	$scope.getTimerSecondsElapsed = function() {
		return $scope.timerSecondsElapsed;
	};

	$scope.getTimerMinutesRemaining = function() {
		return $scope.timerMinutesRemaining;
	}

	$scope.getTimerHoursRemaining = function() {
		return $scope.timerHoursRemaining;
	}

	$scope.handleTimerDone = function() {
		$scope.resetCountdown();
		$scope.resetTimer();
		$location.path('/final-score');
	}

	$scope.checkIfTimerDone = function() {
		if (new Date().getTime() >= $scope.futureTime.getTime()) {
			$scope.handleTimerDone();
			return true;
		}
		return false;
/*
			if (convertedCurHour >= convertedFutureHour && curHourPM == futureHourPM) {
				if (!$scope.timerSettings.minutes) {
					$scope.timerReached = true;
				}
				else if ($scope.curMin >= $scope.futureMin) {
					$scope.timerReached = true;		
				}
			}
*/
	}

	$scope.$on('examSimulatorTimerStart', function() {
		if (SessionService.examSimulator) {
				$scope.resetCountdown();
				if (SectionService.section == 'FAR' || SectionService.section == 'AUD') {
					$scope.setCountdown({hours: 4});
//					$scope.setCountdown({minutes: 2});
				}
				if (SectionService.section == 'BEC' || SectionService.section == 'REG') {
					$scope.setCountdown({hours: 3});
				}
		}
	});

	$scope.$on('quizTimerStart', function() {
		if (!SessionService.examSimulator) {
			$scope.resetTimer();
			$scope.setTimer();	
		}
	});
	
	if (SessionService.examSimulator) {
			$scope.resetCountdown();
			if (SectionService.section == 'FAR' || SectionService.section == 'AUD') {
				$scope.setCountdown({hours: 4});
//					$scope.setCountdown({minutes: 2});
			}
			if (SectionService.section == 'BEC' || SectionService.section == 'REG') {
				$scope.setCountdown({hours: 3});
			}
	}
	if (!SessionService.examSimulator) {
		$scope.resetTimer();
		$scope.setTimer();	
	}

});

TestSimModule.controller("SessionController", function($rootScope, $scope, $modal, $templateCache, $http, $timeout, $location, SessionService, SectionService, CalculatorService, QuizletService, ChapterService, QuestionService, LoginService) {
//        LoginService.verify();

	$scope.section = SectionService.section;
	$scope.sessioninfo = SessionService.sessioninfo;
//	$scope.currentType = SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].type;
	$scope.currentPath = SessionService.quizlets.length ? SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].path : '';
	$http({method: 'GET', url: 'views/save-try-again.php'}).success(function(data) {
	      $templateCache.put('views/save-try-again.php', data);
	    });

	$scope.navState = {};
	// Set scope current type
	if ($scope.currentPath.indexOf("mp") > -1) {
		$scope.currentType = 'mp';
	}
	else if ($scope.currentPath.indexOf("tbs") > -1){
		$scope.currentType = 'tbs';
	}

	var currentQuizlet = SessionService.quizlets[QuizletService.quizlet] || null,
		currentPath = $location.path(),
		currentQuestionIdx = 0,
		currentQuestion = null,
		prevQuestion = null,
		getQuestionType = function(type) {
			if(type.indexOf('tbs') < 0) {
				type = 'multiple-choice';
			}
			return type;
		};
	if(SessionService.quizlets.length == 0) {
		if($.cookie("isadmin")) {
	//		location.pathname = "//testcenter.rogercpareview.com/testmodule-admin/search-edit.php#/question/multiple-choice/edit/" + $routeParams.id;
			location.href = location.protocol + '//' + location.hostname + "/testmodule-admin/search-edit.php#/question/multiple-choice/edit/" + $routeParams.id;
//				location.href = location.protocol + '//' + location.hostname + "/testmodule-admin/build-widget-question.php#/edit/" + $routeParams.id;
// location.href = location.protocol + "//" + location.hostname + "/testmodule-admin/build-wc-question.php#/edit/" + $routeParams.id;
		} else {
			SessionService.exitedSavedQuiz = true;	
			$location.path('/quiz/setup');
		}
		return;
	}
	if (!SessionService.previewMode && SessionService.firstLoad && SessionService.saveMode == 'new') {
		SessionService.saveSession(SessionService.sessionName);
	}
	// Set current question and idx according with current path
	for(var i = 0; i < currentQuizlet.questions.length; i++) {
		if(currentQuizlet.questions[i].path == currentPath) {
			currentQuestionIdx = i;
			currentQuestion = currentQuizlet.questions[currentQuestionIdx];
			break;
		}
	}
	if(currentQuestion) {
		// Set prev question vars according how user is go throw quiz
		if(currentQuestionIdx == QuestionService.question) {
			// Is using "next question" button
			prevQuestion = currentQuizlet.questions[QuestionService.question - 1] || null;
		} else {
			// Is using pagination
			prevQuestion = currentQuizlet.questions[QuestionService.question];
		}
		// Eval if previous question exist
		if(prevQuestion) {
			// Overwritte type
			currentQuestion['type'] = getQuestionType(currentQuestion['type']);
			prevQuestion['type'] = getQuestionType(prevQuestion['type']);
			// Set show instructions according prev and current question types
			if(currentQuestion['type'] != prevQuestion['type']) {
				SessionService.showInstructions = true;
			} else {
				SessionService.showInstructions = false;
			}
		} else {
			SessionService.showInstructions = true;
		}
		$rootScope.$broadcast("sessionStateUpdate");
	}

	$scope.navState.showInstructions = SessionService.showInstructions;
	$scope.navState.quizStarted = SessionService.quizStarted;
	$scope.examSimulator = SessionService.examSimulator;

	$rootScope.showTabLoading = false;

	if (SessionService.examSimulator) {
		$scope.session_type = 'exam';
		SessionService.session_type = 'exam';
		$rootScope.$broadcast("sessionStateUpdate");

	}
	else {
		$scope.session_type = 'quiz';
		SessionService.session_type = 'quiz';
		$rootScope.$broadcast("sessionStateUpdate");
	}

	$timeout(function() {
		window.scrollTo(0, 0);
	});


/*
	if (SessionService.firstLoad) {
		$scope.resetQuizlet();
	}
*/

	$scope.getChapterTitle = function() {
		try {
			var q = SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question];
			if (q.chapter) {
				return q.chapter;
			}
			if (q.topics) {
				return q.topics[0].chapter;
			}
		} catch (err) {
			console.log('Unable to get chapter title.');
			return '';
		}
	};

	$scope.getTotalQuestionsInQuizlet = function() {
		var quizlet = SessionService.quizlets[QuizletService.quizlet];
		return quizlet ? quizlet.questions.length : 0;
	};

	$scope.getCurrentQuestionIndex = function() {
		return QuestionService.question + 1;
	};

        $scope.gotoInstructions = function() {
		SessionService.showInstructions = true;
		$scope.navState.showInstructions = true;
		$rootScope.$broadcast("sessionStateUpdate");
	};

	$scope.setStartTime = function() {
		$scope.startTime = new Date().getTime();
		$scope.timerLoop = $timeout(function() {
			$scope.timer = new QuestionTimer();
			if ($scope.currentType == "mcq" || $scope.currentType == "multiple-choice") {
				$scope.timer.init(90*1000);
			}
			else if (SectionService.section !== "BEC") {
				$scope.timer.init(780*1000);
			}
			else if (SectionService.section == "BEC") {	
				$scope.timer.init(1200*1000);
			}
//			$scope.timer.init();
			$scope.timer.draw();
			$scope.$broadcast('timer-start');
		});
		$rootScope.$broadcast('TBSResearchSetStartTime');
	};

	$scope.$on("unpauseTimer", function() {
		$scope.timer.resumeTime();
	});

	$scope.refreshQuiz = function() {
		$location.path('/quiz/setup');
		SessionService.refresh = true;
	}
	$scope.startNewQuiz = function() {
		$location.path('/quiz/setup');
	};

	$scope.dismissInstructions = function() {
		SessionService.showInstructions = false;
		SessionService.quizStarted = true;
		$scope.navState.showInstructions = false;
		$rootScope.$broadcast("TBS-Journal-renderQuestion");
		$rootScope.$broadcast("dismissInstructions");
		$rootScope.$broadcast("sessionStateUpdate");
		$scope.quizStarted = true;
		$scope.setStartTime();
		$timeout(function() {
			window.scrollTo(0, 0);
		});
	};

	$scope.showInstructions = function() {
		SessionService.showInstructions = true;
		$scope.navState.showInstructions = true;
		$rootScope.$broadcast("sessionStateUpdate");
		$scope.pauseTime();
	};
	$scope.viewQuestions = function(ch, category, stat, oldquiz) {
		SessionService.viewQuestions(ch, category, stat, oldquiz);
	};

/*
	$rootScope.$on("$routeChangeStart", function(o, n) {
		if(o == n || n == undefined) return;
		QuizNavigation.navState = angular.copy($scope.navState);
	});
*/

});

TestSimModule.controller("PaginationController", function($rootScope, $scope, $location, $routeParams, SessionService, QuizletService, QuestionService) {
	$scope.navState = {};
	$scope.navState.showInstructions = SessionService.showInstructions;

	$scope.getShowInstructions = function() {
		return SessionService.showInstructions;
	}
	$scope.$on("dismissInstructions", function() {
		$scope.navState.showInstructions = false;
		SessionService.showInstructions = false;
	});
	$scope.setPath = function(p) {
		$rootScope.$broadcast("setPath");
		$location.path(p);
		$scope.initPagination();
	};

	$scope.initPagination = function() {
		if (SessionService.currentPage > 0) {
			$scope.questionList = SessionService.pages[SessionService.currentPage];
		}
		else {
			$scope.questionList = SessionService.quizlets.length ? SessionService.quizlets[QuizletService.quizlet].questions : [];
			if (SessionService.previewMode) {
		//		$scope.questionList = QuestionService.questionList;
				SessionService.pages = [];
				if ($scope.questionList.length > SessionService.qPageMax) {
					var qPage = [];
					angular.forEach($scope.questionList, function(q) {
						if (qPage.length < SessionService.qPageMax) {
							qPage.push(q);
						}
						else if(qPage.length == SessionService.qPageMax){
							SessionService.pages.push(qPage);
							qPage = [];
							qPage.push(q);
						}
					});
					if (qPage.length > 0) {
						SessionService.pages.push(qPage);
					}
					$scope.questionList = SessionService.pages[0];
				}
			}
		}
//		$rootScope.$broadcast("paginationReady");
		$scope.navState = {};
		$scope.currentPage = SessionService.currentPage;
//		$scope.questionList = QuestionService.questionList;
		$scope.navState.question = QuestionService.question;
		$scope.pages = SessionService.pages;
		$scope.qPageMax = SessionService.qPageMax;
//		QuestionService.qID = $routeParams.qID;
//			QuestionService.qID = ($scope.navState.question) - (SessionService.qPageMax*$scope.currentPage);
		$scope.qID = parseInt($routeParams.qID);
	}

	$scope.selectPage = function(p) {
		SessionService.currentPage = p;
		$scope.questionList = SessionService.pages[p];
		QuestionService.question = (SessionService.qPageMax*$scope.currentPage)+1;
		$location.path($scope.questionList[0].path);
	};

	$scope.initPagination();
});

TestSimModule.controller("QuestionController", function($rootScope, $scope, $location, $modal, SessionService, QuizletService, QuestionService, CalculatorService, ChapterService, $routeParams, $timeout) {
	$scope.getSavingInProgress = function() {
		return SessionService.savingInProgress;
	};
	$scope.showOptions = SessionService.showOptions;

	$scope.toggleOptions = function() {
		$scope.showOptions = !$scope.showOptions;
	};
	$rootScope.$broadcast('quizTimerStart');

	$scope.showTimer = SessionService.showTimer;
	$scope.optionLabels = ["a", "b", "c", "d"];
	$scope.qID = $routeParams.qID;	
	QuestionService.question = parseInt($scope.qID);
	$scope.navState.quizlet = QuizletService.quizlet;
	$scope.navState.question = parseInt($scope.qID);
	$scope.enableExplanation = false;
	$scope.quizlets = SessionService.quizlets;

	$scope.isLastQuestion = (SessionService.quizlets.length && QuestionService.question == SessionService.quizlets[QuizletService.quizlet].questions.length - 1);

	if(SessionService.showInstructions == false) {
		$scope.setStartTime();
	}
	$scope.question = SessionService.quizlets.length ? SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question] : {};
	SessionService.currentQuestion = $scope.question;
	if (location.href.indexOf('tbs-wc') !== -1) {
		QuestionService.type = 'tbs-wc';
	}
	else if (location.href.indexOf('tbs-journal') !== -1) {
		QuestionService.type = 'tbs-journal';
	}
	else if (location.href.indexOf('tbs-research') !== -1) {
		QuestionService.type = 'tbs-research';
	}
	else {
		QuestionService.type = 'multiple-choice';
	}
	$scope.question.type = QuestionService.type;
	$scope.question.newNoteText = "";
	if($scope.question.answer_id !== null) {
//		var oldAnswer = SessionService.findSessionAnswer($scope.question.id);
//		if(oldAnswer != null) {
//			var answer = oldAnswer.answers[oldAnswer.answers.length - 1];
			angular.forEach($scope.question.choices, function(choice){
				if(choice.id == $scope.question.answer_id) {
					$scope.question.answer = choice;
					$scope.enableExplanation = true;
//					if(oldAnswer.noted != null) {
//						$scope.question.noted = oldAnswer.noted;
//					}
				}
			});
//		}
	}
	$scope.calculator = CalculatorService;
	$scope.calculator.question = $scope.question; //for analytics
	if (!SessionService.previewMode) {
		var topicTitles = [];
		if ($scope.question.topics) {
			for(var i = 0; i < $scope.question.topics.length; i++) {
				var topic = $scope.question.topics[i];
				var titleObj = SessionService.findTopic(topic.chapter_id, topic.topic_id);
				if (titleObj.topic !== "") {
					topic.topic = titleObj.topic;
					topic.topicID = titleObj.topicID;
					topic.chapter = titleObj.chapter;
				}
				if (!topic.chapter) {
					topic.chapter = $scope.question.chapter;
				}
					topicTitles.push(topic);
			}

			$scope.topicTitles = topicTitles;
		}
	}

	$scope.getTotalQuestions = function(chapter) {
		if (QuizConfigState.totalQuestions) {
			if (QuizConfigState.totalQuestions[chapter]) {
				return QuizConfigState.totalQuestions[chapter];
			}
		}
	};

	$scope.endSession = function() {
		if (QuizletService.quizlet == 0 && SessionService.quizlets.length > 1 && SessionService.examSimulator == false) {
					var opts = {
					    backdrop: true,
					    keyboard: true,
					    backdropClick: false,
					    templateUrl: 'views/quiz-confirm-nextquizlet.php',
					    controller: 'EndQuizController',
						dialogFade: true,
					    
					};
				    var d = $modal.open(opts).result.then(function(result) {
						if (result == "ok") {
//							$scope.advanceQuestion();
							$rootScope.$broadcast("nextQuizlet");
						}
					    }, function() {
						$scope.timer.resumeTime();
					    });
		}
		else if (SessionService.examSimulator == false) {
			var opts = {
			    backdrop: true,
			    keyboard: true,
			    backdropClick: false,
			    templateUrl: 'views/quiz-confirm-endquiz.php',
			    controller: 'EndQuizController',
				dialogFade: true,
			    
			};
		    var d = $modal.open(opts).result.then(function(result) {
				if (result == 'ok') {
					$location.path('/final-score');
				}
		    }, function() {
			$scope.timer.resumeTime();
		    });
	    }
		else if (SessionService.examSimulator) {
			var opts = {
			    backdrop: true,
			    keyboard: true,
			    backdropClick: false,
			    templateUrl: 'views/exam-confirm-nextquizlet.php',
			    controller: 'EndQuizController',
				dialogFade: true,
			    
			};
		    var d = $modal.open(opts).result.then(function(result) {
				if (result == 'ok') {
					if ((QuizletService.quizlet + 1) == SessionService.quizlets.length) {
						$location.path('/final-score');
					}
					else {
//						$scope.advanceQuizlet();
						$rootScope.$broadcast("nextQuizlet");
					}
				}
		    }, function() {
			$scope.timer.resumeTime();
		    });
	    }
		$scope.timer.pauseTime(false);
	};

	$scope.previousQuestion = function() {
		QuestionService.question--;
/*
		if (!$scope.question.answers && !SessionService.previewMode) {
			SessionService.addQuestionAttempt($scope.question); 
                }
*/
		if(SessionService.question >= 0) {
			$location.path(SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].path);
		} else if(QuizletService.quizlet >= 0) {
			QuizletService.quizlet--;
			QuestionService.question = 0;
			SessionService.showInstructions = true;
			$rootScope.$broadcast("sessionStateUpdate");
			$location.path(SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].path);
		} else {
			$location.path("/final-score");
		}
	}
	$scope.gotoNextQuestion = function() {
		SessionService.updateCompletePercent();
		if ($scope.question.type == 'tbs-research') {
//			QuestionService.addQuestionAttempt($scope.question);
			$rootScope.$broadcast('TBS-Research-gotoNextQuestion');
		}
		if ($scope.question.type !== 'tbs-wc') {
			if(SessionService.scoreAsIGo) {
				var opts = {
				    backdrop: true,
				    keyboard: true,
				    backdropClick: false,
				    templateUrl: 'views/score-as-i-go.php',
				    controller: 'ScoreViewController',
				dialogFade: true,
				    dialogClass: 'modal modal-score',
				    resolve:{
					solution:function() {
						return $scope.solution;
					}
				    }
				  };
			    var d = $modal.open(opts).result.then(function(cmd) {
				if (cmd === 'continue') {
					$scope.advanceQuestion();
				}
			    });
			} else {
				$scope.advanceQuestion();
			}
		}
		else {
			$rootScope.$broadcast("TBS-WC-gotoNextQuestion");
		}
	};


	$scope.advanceQuestion = function() {
		if(QuestionService.question < (SessionService.quizlets[QuizletService.quizlet].questions.length-1)) {
			QuestionService.question++;
/*
			if(!$scope.question.answer && !SessionService.previewMode && $scope.question.type !== 'tbs-research' && $scope.question.type !== 'tbs-journal' && $scope.question.type !== 'tbs-wc') {
				QuestionService.addQuestionAttempt($scope.question);
			}
*/
			$location.path(SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].path);
		} else if(QuizletService.quizlet < (SessionService.quizlets.length - 1)) {
				QuizletService.quizlet++;
				QuestionService.question = 0;
	//			SessionService.showInstructions = true;
				$location.path(SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].path);
		} else {
			if (SessionService.savingQuestions.length > 0) {
				$rootScope.showTabLoading = true;
				$scope.$on("finishedSavingQuestions", function() {
					$rootScope.showTabLoading = false;
					$location.path("/final-score");
				});
			}
			else {
				$location.path("/final-score");
			}
		}
	}
	$scope.setAnswer = function (id) {
		for(var i = 0; i < $scope.question.choices.length; i++) {
			if($scope.question.choices[i].id == id) {
				$scope.question.answer = $scope.question.choices[i];
				$scope.question.answer_id = $scope.question.choices[i].id;
				$scope.question.answer.time = new Date().getTime() - $scope.startTime;
			}
		}
		$scope.enableExplanation = true;
		QuestionService.addQuestionAttempt($scope.question);
//		QuizSession.addQuestionAttempt($scope.question.notes, $scope.question.isFlagged, $scope.question.answer.time, parseInt($scope.question.answer.is_correct), $scope.question.id, $scope.question.answer.id, $scope.question.quizletID, $scope.question.type, $scope.question.topics);
	};

	$scope.showExplanation = function() {
		if ($scope.question.type == 'multiple-choice') {
		$scope.solution = {	explanation:$scope.question.answer.explanation,
							title: $scope.question.answer.is_correct == true ? 'You answered correctly' : 'You answered incorrectly',
							buttons: $scope.question.answer.is_correct == true? [{result:'ok', label: 'Continue', cssClass: 'btn-inverse'}] : [{result:'retry', label: 'Try Again', cssClass: 'btn-inverse'}, {result:'ok', label: 'Continue', cssClass: 'btn-inverse'}]
							};
		var opts = {
			    backdrop: true,
			    keyboard: true,
			    backdropClick: false,
			    templateUrl: 'views/quiz-mp-solution.php',
			    controller: 'MPSolutionController',
      			dialogFade: true,
			    //dialogClass: 'modal',
			    resolve:{
			    	solution:function() {
			    		return $scope.solution;
			    	}
			    }
			  };
	    var d = $modal.open(opts);

		   d.opened.then(function() {
				$timeout(function() {
					$('.modal-dialog').draggable();
				}, 0);
			});


		d.result.then(function(cmd) {
	    	if(cmd === "ok") {
	    		$scope.gotoNextQuestion();
	    	} else {
/*
	    		QuizSession.addQuestionAttempt($scope.question.notes, $scope.question.isFlagged, $scope.question.answer.time, parseInt($scope.question.answer.is_correct), $scope.question.id, $scope.question.answer.id, $scope.question.quizletID, $scope.question.type);
*/
	    	}
	    });
		}
		else if ($scope.question.type == 'tbs-journal') {
			$rootScope.$broadcast('TBS-Journal-showExplanation');
		}
		else if ($scope.question.type == 'tbs-research') {
			$rootScope.$broadcast('TBS-Research-showExplanation');
		}
	};

	$scope.toggleShowTimer = function() {
		SessionService.showTimer = !SessionService.showTimer;
		$scope.showTimer = SessionService.showTimer;
	};

	$scope.getScoreAsIGo = function() {
		return SessionService.scoreAsIGo;
	}

	$scope.getShowTimer = function() {
		return SessionService.showTimer;
	}

	$scope.toggleScoreAsIGo = function() {
		SessionService.scoreAsIGo = ! SessionService.scoreAsIGo;
		$scope.scoreAsIGo = SessionService.scoreAsIGo;
	};

	$scope.setStartTime = function() {
		$scope.startTime = new Date().getTime();
		$scope.timerLoop = $timeout(function() {
			$scope.timer = new QuestionTimer();
			$scope.timer.init();
			$scope.timer.draw();
			$scope.$broadcast('timer-start');
		});
	};

	$scope.adminEdit = function() {
		window.open("/testmodule-admin/search-edit.php#/question/multiple-choice/edit/" + $routeParams.id, "_blank");
//                window.open("/testmodule-admin/build-wc-question.php#/edit/" + $routeParams.id, "_blank");
//		window.open("/testmodule-admin/build-widget-question.php#/edit/" + $routeParams.id, "_blank");

	};

});

TestSimModule.controller("BookmarkController", function($rootScope, $scope, $http, $location, SessionService, QuestionService, QuizletService) {
	$scope.question = SessionService.quizlets.length ? SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question] : {};
	$scope.toggleBookmark = function(qid) {
		SessionService.savingQuestion($scope.question.id);
		var q = SessionService.quizlets[QuizletService.quizlet].questions[qid];
		if (!SessionService.previewMode || SessionService.sessioninfo.id) {
			if (q.isFlagged) {
				q.isFlagged = false;
				q.flagged = false;
			}
			else {
				q.isFlagged = true;
				q.flagged = true;
			}
			if (q.path.indexOf('mp') !== -1) {
				var type = 'mcq';
			}
			else {
				var type = 'tbs';
			}
		$http.put(ServiceURLS.saveQuestionScores, 
			{
				'question_scores': {
					id: q.id,
					session_id: String(SessionService.quizID),
					student_id: JSON.parse($.cookie('data')).studentID,
					type: type,
					flagged: q.isFlagged ? "1" : "0",
				}
			}).success(function() {
				SessionService.notSavingQuestion(q.id);
			});

		}
/*
		if(!q.answer && !SessionService.previewMode) {
			QuestionService.addQuestionAttempt(q);
		} else if (!SessionService.previewMode) {
			QuestionService.addQuestionAttempt(q, 'noscore');
		}
*/
	};
	$scope.isPrevFlaggedQuestion = function() {
		var flaggedQ = false;
		if($scope.question.index > 0) {
			for(var i = $scope.question.index - 1; i >= 0; i--) {
				var quizlet = SessionService.quizlets[QuizletService.quizlet];
				if(quizlet && quizlet.questions[i] && quizlet.questions[i].isFlagged) {
//					flaggedQ = quizlet.questions[i].questions[i];
					flaggedQ = SessionService.quizlets[QuizletService.quizlet].questions[i];
					break;
				}
			};
		}
		return flaggedQ;
	};

	$scope.gotoPrevFlaggedQuestion = function() {
		var prevQuestion = $scope.isPrevFlaggedQuestion();
		
		if (prevQuestion) {
			$location.path(prevQuestion.path);
		}
	};

	$scope.isNextFlaggedQuestion = function() {
		var flaggedQ = false;

		if (SessionService.quizlets[QuizletService.quizlet]) {
			if (QuestionService.question < SessionService.quizlets[QuizletService.quizlet].questions.length - 1) {
				for(var i = QuestionService.question + 1; i < SessionService.quizlets[QuizletService.quizlet].questions.length; i++) {
					if (SessionService.quizlets[QuizletService.quizlet].questions[i].isFlagged) {
						flaggedQ = SessionService.quizlets[QuizletService.quizlet].questions[i];
						break;
					}
				}
			}
		}

		return flaggedQ;
	};

	$scope.gotoNextFlaggedQuestion = function() {
		var nextQuestion = $scope.isNextFlaggedQuestion();

		if (nextQuestion) {
			$location.path(nextQuestion.path);
		}
	};
});

TestSimModule.controller("NoteController", function($rootScope, $scope, $http, $timeout, $modal, SessionService, QuizletService, QuestionService) {
	$scope.question = SessionService.quizlets.length ? SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question] : {};
	$scope.editedNoteText = '';
	$scope.createNote = function() {
		if ($scope.question.type == 'mcq'|| $scope.question.type == 'multiple-choice') {
			var type = 'mcq';
		}
		else {
			var type = 'tbs';
		}
		if (typeof $scope.question.noted !== 'object') {
			$scope.question.noted = [];
		}	
		$scope.question.noted.push( $scope.question.newNoteText );// "" + $("#noteInput").val() );
		$http.put(ServiceURLS.saveQuestionScores, 
			{
				'question_scores': {
					id: $scope.question.id,
					session_id: String(SessionService.quizID),
					student_id: JSON.parse($.cookie('data')).studentID,
					type: type,
					noted: JSON.stringify($scope.question.noted)
				}
			}).success(function() {
				SessionService.notSavingQuestion($scope.question.id);
			});
		$scope.question.newNoteText = "";//$("#noteInput").val(""); 
	};
	$scope.editNote = function(i) {
		SessionService.savingQuestion($scope.question.id);
		if ($scope.noteBeingEdited !== -1) {
			if ($scope.question.type == 'mcq'|| $scope.question.type == 'multiple-choice') {
				var type = 'mcq';
			}
			else {
				var type = 'tbs';
			}
			$scope.question.noted[$scope.noteBeingEdited] = angular.copy($scope.editedNoteText);
			
			$http.put(ServiceURLS.saveQuestionScores, 
				{
					'question_scores': {
						id: $scope.question.id,
						session_id: String(SessionService.quizID),
						student_id: JSON.parse($.cookie('data')).studentID,
						type: type,
						noted: JSON.stringify($scope.question.noted)
					}
				}).success(function() {
					SessionService.notSavingQuestion($scope.question.id);
				});
		}
		$scope.noteBeingEdited = i;
		$scope.editedNoteText = $scope.question.noted[$scope.noteBeingEdited];
	}
	$scope.saveEditedNote = function(i, editedNoteText) {
		SessionService.savingQuestion($scope.question.id);
		if ($scope.question.type == 'mcq'|| $scope.question.type == 'multiple-choice') {
			var type = 'mcq';
		}
		else {
			var type = 'tbs';
		}
		$scope.question.noted[$scope.noteBeingEdited] = editedNoteText;
		$scope.editedNoteText = "";
		$scope.noteBeingEdited = -1;
		$http.put(ServiceURLS.saveQuestionScores, 
			{
				'question_scores': {
					id: $scope.question.id,
					session_id: String(SessionService.quizID),
					student_id: JSON.parse($.cookie('data')).studentID,
					type: type,
					noted: JSON.stringify($scope.question.noted)
				}
			}).success(function() {
				SessionService.notSavingQuestion($scope.question.id);
			});
	}
	$scope.deleteNote = function(i) {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: false,
		    templateUrl: 'views/quiz-confirm-deletenote.php',
		    controller: 'EndQuizController',
  			dialogFade: true,
		    
		};
	    var d = $modal.open(opts).result.then(function() {
		SessionService.savingQuestion($scope.question.id);
	    		
		if ($scope.question.type == 'mcq'|| $scope.question.type == 'multiple-choice') {
			var type = 'mcq';
		}
		else {
			var type = 'tbs';
		}
	    	$scope.question.noted.splice(i, 1);
		$http.put(ServiceURLS.saveQuestionScores, 
			{
				'question_scores': {
					id: $scope.question.id,
					session_id: String(SessionService.quizID),
					student_id: JSON.parse($.cookie('data')).studentID,
					type: type,
					noted: JSON.stringify($scope.question.noted)
				}
			}).success(function() {
				SessionService.notSavingQuestion($scope.question.id);
			});
		if (!SessionService.previewMode) {
		    	$scope.timer.resumeTime();
		}
	    }, function() {
		if (!SessionService.previewMode) {
	    		$scope.timer.resumeTime();
		}
	    });
		if (!SessionService.previewMode) {
			$scope.timer.pauseTime(false);
		}
	};

	$timeout(function() {
		$("#noteInput").on('focusin', function(e) {
			$(this).addClass("noteFocus");
		});

		$("#noteInput").on('focusout', function(e) {
			var anchor = $(this);
			setTimeout(function() {
				anchor.removeClass("noteFocus");
			}, 200);
		});

	});
});

TestSimModule.controller("MultipleChoiceQuestionController", function($scope, $rootScope, $routeParams, $browser, $timeout, $location, $modal, $anchorScroll,
																			TopicSections, QuizConfigState, QuizNavigation, QuizSession, CalculatorService) {

});


TestSimModule.controller("ScoreViewController", function($scope, SessionService, $modalInstance) {
	$scope.idealMultipleChoiceTime = 90;
	$scope.idealMultipleTBSTime = 15 * 60 * 1000;

	$scope.session = SessionService;
	$scope.currentPercent = SessionService.currentPercent;
	$scope.attemptsAverage = SessionService.attemptsAverage;
	$scope.timeAverage = SessionService.timeAverage;
	$scope.completePercent = SessionService.completePercent;
	$scope.getScoreAsIGo = function() {
		return SessionService.scoreAsIGo;
	}
	$scope.toggleScoreAsIGo = function() {
		SessionService.scoreAsIGo = !SessionService.scoreAsIGo;
	};
	$scope.scoreClass = "";
	if($scope.currentPercent > 90) {
		$scope.scoreClass = "label label-success";

	} else if($scope.currentPercent > 84) {
		$scope.scoreClass = "label label-warning";
	} else {
		$scope.scoreClass = "label label-danger";
	}

	$scope.close = function(cmd) {
		$modalInstance.close(cmd);
	};
});

TestSimModule.controller("NextQuizletWarningController", function($scope, QuizSession, $modalInstance) {
	$scope.session = QuizSession;
	$scope.currentPercent = QuizSession.currentPercent;
	$scope.attemptsAverage = QuizSession.attemptsAverage;
	$scope.timeAverage = QuizSession.timeAverage;
	$scope.completePercent = QuizSession.completePercent;
	$scope.scoreClass = "";


	$scope.title = "Are you sure?";

	if($scope.currentPercent > 90) {
		$scope.scoreClass = "label label-success";

	} else if($scope.currentPercent > 84) {
		$scope.scoreClass = "label label-warning";
	} else {
		$scope.scoreClass = "label label-danger";
	}

	$scope.close = function() {
		$modalInstance.dismiss();
	};
	$scope.saveAndClose = function() {
		$modalInstance.close("ok");
	}

});

TestSimModule.controller("MPSolutionController", function($scope, solution, $modalInstance) {
	$scope.solution = solution;

	$scope.close = function(result) {
		$modalInstance.close(result);
	};

});

TestSimModule.controller('EndQuizController', function ($scope, $modalInstance, SectionService) {
	$scope.section = SectionService.section;

	$scope.cancel = function() {
		$modalInstance.dismiss();
	}
	
	$scope.confirm = function(result) {
		$modalInstance.close(result);
	}
});


TestSimModule.controller("PreviewController", function($scope, QuizSession) {
	var pdata = gup("data");
	if(pdata != null && pdata != "") {
		QuizSession.preview();
	}
});

TestSimModule.controller("TBSJournalQuestionController", function($scope, $timeout, $rootScope, $routeParams, $browser, $timeout, $location, $modal, $modalStack, SessionService, QuizletService, QuestionService) {
	if ($scope.question.type == 'tbs-journal') {
	$scope.qID = $routeParams.qID;
/*
	$rootScope.$on("$routeChangeStart", function(o, n) {
		if(o == n || n == undefined) return;
		QuizNavigation.navState = angular.copy($scope.navState);
	});
*/
	$scope.questionList = SessionService.quizlets[QuizletService.quizlet].questions;
	var parseQuestion = $scope.questionList[QuestionService.question];

	$scope.styleDictionary = {};

	$scope.addStyleDictionary = function(id, style) {
		$scope.styleDictionary[id] = style;
	}

	$scope.gridInstances = {};

	if (!SessionService.previewMode) {
		var topics = parseQuestion.topics;
	}
	try {
	var newModel = JSON.parse(parseQuestion.question_json);
	newModel.type = parseQuestion.type;
	}catch (e) {

		if($.cookie("isadmin")) {
			var opts = {
			    backdrop: true,
			    keyboard: true,
			    backdropClick: false,
			    templateUrl: 'views/quiz-admin-errordetail.php',
			    controller: 'ErrorDetailController',
				dialogFade: true,
				resolve: {
					data:function() {
						return parseQuestion.question_json;
					},
					msg:function() {
						return e.msg;
					}
				}
			    
			};
		    var d = $modal.open(opts).result.then(function() {
			
			$location.path('/final-score');
		    }, function() {
			$scope.timer.resumeTime();
		    });
		} else {
			//do some
		}
	}

	var question = decodeURIComponent(newModel.question); 
	question = JSON.parse(question);
	question.noted = parseQuestion.noted;
	question.quizletID = parseQuestion.quizletID;
	question.type = newModel.type;
	question.id = parseQuestion.id;

	for(var i = 0; i < question.question.widgets.length; i++) {
		var w = question.question.widgets[i];
		if(w.type == "questionGridblock") {
			for( var j = 0; j < w.model.content.length; j++) {
				var row = w.model.content[j];
				for (var k in row) {
					var col = row[k];
					if(String(col).indexOf("answerlink") > -1) {
						
						var dataList = col.split("data-list='")[1];
						dataList = dataList.split("'")[0];

						var dataAnswer = col.split("data-answer='")[1];
						dataAnswer = dataAnswer.split("'")[0];
						dataAnswer = parseInt(dataAnswer);

						col = col.split("ng-click='clickAnswerKeyLink'").join("");//ng-click='$parent.$parent.openAnswerKeyWindow(\"" + dataList + "\", " + dataAnswer + ")'");
						col = col.split("<a ").join("<button ");
						col = col.split("</a>").join("</button>");

					}
					row[k] = col;
				}
			}
		}
	}
	for(j = 0; j < question.question.answerkeys.length; j++) {
		if(question.question.answerkeys[j].letter == null) {
			var key = question.question.answerkeys[j];
			for(i = 0; i < key.model.content.length; i++) {
				key.model.content[i].letter = window.letters[i];
			}
		}
	}

	$scope.question = question;
	$scope.question.chapter_id = SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].chapter_id;
	$scope.question.status = SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].status;
	$scope.question.noted = $scope.question.noted || [];
	$scope.question.answer_id = SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].answer_id;
	$scope.calculator.question = question;

	$scope.renderQuestion = function() {
		$timeout(function() {
			angular.forEach($scope.question.question.widgets, function(w) {
				if(w.type == "questionGridblock") {
					if($scope.gridInstances[w.model.id] == null) {
						$scope.addStyleDictionary(w.model.id, w.model.styles);
						var grid = $("#" + w.model.id).wijgrid({
							data:w.model.content,
							columns:w.model.columns,
							selectionMode:"none",
							highlightOnHover:false,
							highlightCurrentCell: false,
							cellStyleFormatter: function(args) {
								function getDictionaryEntryByFormatter(cell) {
									var key = "cell_" + cell.column.dataIndex  + "_" + cell.row.dataItemIndex;
									return $scope.styleDictionary[w.model.id][key];
								}
								var style = getDictionaryEntryByFormatter(args);
								if(style != null) {
									angular.forEach(style.css, function(rule) {
										var cssKey;
										var val;
										for(key in rule) {
											cssKey = key;
											val = rule[key];
											args.$cell.css(cssKey, val);
										}
									});
								}
							}
						});
						$scope.gridInstances[w.model.id] = grid;
						$scope.addStyleDictionary(w.model.styles);
					}
				}
			});
		});
		$timeout(function() {
			var links = $(".answerlink").toArray();
			$scope.question.buttonQuantity = links.length;
			$scope.question.totalAnswers = $('tr').has('.answerlink').length;
			for(var i = 0; i < links.length; i++) {
				var inner = $(links[i]).html();
				$(links[i]).html(inner.split(" :")[0]);
				$(links[i]).attr("id", "link_" + i);
			}
			if (SessionService.previewMode) {
				$(".answerlink").addClass("disabled");
			}
			$(".answerlink").on("click", function(e) {
				e.preventDefault();
				e.stopPropagation();
				window.answerButton = $(this);
				var anchor = $(this);
				var dataList = anchor.data("list");
				var dataAnswer = anchor.data("answer");
				dataAnswer = parseInt(dataAnswer);
				if ($scope.currentAnswerKeyWindow) {
					$scope.currentAnswerKeyWindow.close();
				}
				if (!$scope.modalOpen) {
					if (!SessionService.previewMode) {
						$scope.openAnswerKeyWindow( dataList , dataAnswer);
						$scope.modalOpen = true;
					}
				}
			});
			var answerHistory = SessionService.findSessionAnswer($scope.question.id);
			if(answerHistory) {
				$timeout(function() {
					if(answerHistory.notes != null) {
						$scope.question.notes = answerHistory.notes;
					}
					var len1 = answerHistory.answers.length;
					if (len1 > 0) {
						var len2 = answerHistory.answers[len1 - 1].length;
						for(i = 0; i < len2; i++) {
							var a = answerHistory.answers[len1 - 1][i];
							var btn = $("#" + a.buttonID);
							var answer = {
								answer:a.answer,
								answerButton:btn,
								givenAnswer:a.givenAnswer,
								list:$scope.getListWidget(a.keyID)
							}
							$scope.question.answers.push(answer);
							var btnLabel = btn.html();
							if(btnLabel && btnLabel.indexOf(":") > -1) {
								btnLabel = btnLabel.split(":")[0] + " : <span class='label-answer'>" + answer.list.model.content[answer.givenAnswer].letter + "</span>";
							} else {
								btnLabel = btnLabel + " : <span class='label-answer'>" + answer.list.model.content[answer.givenAnswer].letter + "</span>";
							}
							answer.answerButton.html(btnLabel);
						}
					}
				});
			} else {
				$scope.question.answers = [];
			}
		});
	};


	$scope.cellStyleFormatter = function(args) {
		 if ((args.row.type & wijmo.grid.rowType.data) && (args.row.state & wijmo.grid.renderState.rendering)) {
		 	var content = args.$cell[0].innerHTML;
			if(content.indexOf("answerlink") > -1) {
				var dataList = content.split('data-list="')[1];
				dataList = dataList.split('\"')[0];
				var dataAnswer = content.split('data-answer="')[1];
				dataAnswer = dataAnswer.split("'")[0];
				dataAnswer = parseInt(dataAnswer);
		        var scope = angular.element("#tbs-journal-container").scope();
		        args.$cell
		            .empty()
		            .append($(content).click(function () {
		                scope.openAnswerKeyWindow( dataList , dataAnswer);
		            })
		        );
	        }
	        return true;
	    }

	};

	$scope.$on("TBS-Journal-renderQuestion", function() {
		$timeout(function() {
			angular.forEach($scope.question.question.widgets, function(w) {
				if(w.type == "questionGridblock") {
				
					if($scope.gridInstances[w.model.id] == null) {
						$scope.addStyleDictionary(w.model.id, w.model.styles);
						
						
						var grid = $("#" + w.model.id).wijgrid({ 	data:w.model.content, 
																	columns:w.model.columns,
																	selectionMode:"none",
																	highlightOnHover:false,
																	highlightCurrentCell: false,
							cellStyleFormatter:function(args) {
								function getDictionaryEntryByFormatter(cell) {
									var key = "cell_" + cell.column.dataIndex  + "_" + cell.row.dataItemIndex;
									return $scope.styleDictionary[w.model.id][key];
								}
					 			
								var style = getDictionaryEntryByFormatter(args);

					 			if(style != null) {
					 				angular.forEach(style.css, function(rule) {
					 					var cssKey;
					 					var val;
					 					for(key in rule) {
					 						cssKey = key;
					 						val = rule[key];
					 						args.$cell.css(cssKey, val);
					 					}
					 				});
					 			}
						}}); 

						$scope.gridInstances[w.model.id] = grid;
						$scope.addStyleDictionary(w.model.styles);
						
						}

					}


				});
			}); //end $timeout()

			$timeout(function() {
				var links = $(".answerlink").toArray();
				$scope.question.buttonQuantity = links.length;
				$scope.question.totalAnswers = $('tr').has('.answerlink').length;
				for(var i = 0; i < links.length; i++) {
					var inner = $(links[i]).html();
					$(links[i]).html(inner.split(" :")[0]);
					$(links[i]).attr("id", "link_" + i);
				}
					
				$(".answerlink").on("click", function(e) {
					e.preventDefault();
					e.stopPropagation();
					window.answerButton = $(this);
						var anchor = $(this);
						var dataList = anchor.data("list");
						var dataAnswer = anchor.data("answer");
						dataAnswer = parseInt(dataAnswer);
//				        var scope = angular.element("#tbs-journal-container").scope();
					if ($scope.currentAnswerKeyWindow) {
						$scope.currentAnswerKeyWindow.close();
					}
					if (!$scope.modalOpen) {
						$scope.openAnswerKeyWindow( dataList , dataAnswer);
						$scope.modalOpen = true;
					}
				});
		    		if(!SessionService.previewMode) {
					var answerHistory = SessionService.findSessionAnswer($scope.question.id);
				}
				else {
					var answerHistory = {};
					answerHistory.answers = JSON.parse(SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].answer_id);
				}
				if(answerHistory) {
					$timeout(function() {
						if(answerHistory.noted != null) {
							$scope.question.noted = answerHistory.noted;
						}

						var len1 = answerHistory.answers.length;
						if (len1 > 0) {
							var len2 = answerHistory.answers[len1 - 1].length;
							for(i = 0; i < len2; i++) {
								var a = answerHistory.answers[len1 - 1][i];
								var btn = $("#" + a.buttonID);
								var answer = {
									answer:a.answer,
									answerButton:btn,
									givenAnswer:a.givenAnswer,
									list:$scope.getListWidget(a.keyID)
								}
								$scope.question.answers.push(answer);
								var btnLabel = btn.html();
								if(btnLabel && btnLabel.indexOf(":") > -1) {
								btnLabel = btnLabel.split(":")[0] + " : <span class='label-answer'>" + answer.list.model.content[answer.givenAnswer].letter + "</span>";
								} else {
									btnLabel = btnLabel + " : <span class='label-answer'>" + answer.list.model.content[answer.givenAnswer].letter + "</span>";
								}
								answer.answerButton.html(btnLabel);

							}
						}
					});
				}
			});
		});

		if($scope.navState.showInstructions == false || SessionService.previewMode) {
			$scope.setStartTime();
			$scope.renderQuestion();
			if(!$scope.question.answer_id) {
				$scope.question.answer_id = $scope.question.question.solution;
			}
		}

		$scope.getListWidget = function(listID) {
			var answer = null;
			var widgets = $scope.question.question.answerkeys;
			for(var i = 0; i < widgets.length; i++) {
				if(widgets[i].model.id == listID) {
					answer = widgets[i];
					break;
				}
			}
			return answer;
		};

		$scope.highlightAnswers = function (){
			$(".tbs-journal-container .wijmo-wijgrid-innercell a.btn-info").removeClass("btn-info").addClass("btn-unanswered");
			for(var i = 0; i < $scope.question.answers.length; i++) {
				var a = $scope.question.answers[i];
				if(a.answer == a.givenAnswer) {
					a.answerButton.removeClass("btn-unanswered").removeClass('btn-answered-wrong').addClass("btn-answered");
				} else {
					a.answerButton.removeClass("btn-info").addClass("btn-answered-wrong");
				}
			}
		};

		$scope.unhighlightAnswers = function () {
			$(".btn-answered").removeClass("btn-answered").addClass("btn-info");
			$(".btn-unanswered").removeClass("btn-unanswered").addClass("btn-info");
			$(".btn-answered-wrong").removeClass("btn-answered-wrong").addClass("btn-info");
		};

		$scope.openAnswerKeyWindow = function(list, answer) {
			$scope.tbs = {list:null, answer:answer};
			$scope.unhighlightAnswers();
			$scope.tbs.answerButton = window.answerButton;
			var keys = $scope.question.question.answerkeys;
			for(var i = 0; i < keys.length; i++) {
				if(keys[i].model.id == list) {
					$scope.tbs.list = keys[i];
					break;
				}
			}
			$timeout(function() {
				var opts = {
				    backdrop: false,
				    keyboard: true,
				    backdropClick: false,
				    templateUrl: 'views/tbsjournal-answerlist.php',
				    controller: 'AnswerListController',
	      			dialogFade: false,
				    resolve:{
				    	tbs:function() {
				    		return $scope.tbs;
				    	}
				    }
				};
			    $scope.currentAnswerKeyWindow = $modal.open(opts);
			    $scope.currentAnswerKeyWindow.result.then(function(tbsAnswer) {
				$scope.currentAnswerKeyWindow = false;
				$scope.modalOpen = false;
				if (tbsAnswer !== 'cancel') { 
					$scope.tbs.givenAnswer = tbsAnswer.givenAnswer;
					$scope.question.answers.push($scope.tbs);
					var btnLabel = window.answerButton.html();
						if(btnLabel.indexOf(":") > -1) {
						btnLabel = btnLabel.split(":")[0] + " : <span class='label-answer'>" + $scope.tbs.list.model.content[$scope.tbs.givenAnswer].letter + "</span>";
					
					} else {
						btnLabel = btnLabel + " : <span class='label-answer'>" + $scope.tbs.list.model.content[$scope.tbs.givenAnswer].letter + "</span>";
					}
					window.answerButton.html(btnLabel);
					window.button = null;
				$scope.question.time = $scope.getAnswerTime();
				$scope.question.score = $scope.calculateScore();
		    		if(!SessionService.previewMode) {
			    		QuestionService.addQuestionAttempt($scope.question);
				}
		    
				}
			    	
			    });
		    });
		};

	$scope.topicTitles = [];

	$scope.findTopic = function(chapterID, topicID) {
		var topicTitle = "";
		var answer = { chapter:"", topic:""};
		angular.forEach(ChapterService.topics, function(chapter) {
					if(chapter.id == chapterID) {
							answer.chapter = chapter.chapter;
							answer.chapterID = chapter.id;
						angular.forEach(chapter.topics, function(topic) {
							if(topic.id == topicID) {
									answer.topic = topic.topic;
									answer.topicID = topic.id;
							}
						});
					}
				});
		return answer;
	};

	if(!SessionService.previewMode) {
		var topicTitles = [];
		if ($scope.question.topics) {
			for(var i = 0; i < topics.length; i++) {
				var topic = topics[i];
				var titleObj = $scope.findTopic(topic.chapter_id, topic.topic_id)
				if (titleObj.topic !== "") {
					topic.topic = titleObj.topic;
					topic.topicID = titleObj.topicID;
					topic.chapter = titleObj.chapter;
				}
				if (!topic.chapter) {
					topic.chapter = $scope.question.chapter;
				}
				topicTitles.push(topic);
				
			}

			$scope.topicTitles = topicTitles;
			$scope.question.topics = $scope.topicTitles;
		}
	}

	$scope.question.answers = [];

	var getQuestionAnswersByBtnId = function() {
		var score = 0,
			answersByBtnId = {};
		angular.forEach($scope.question.answers, function(current, idx) {
			var btnId = current.answerButton.attr('id'),
				isCorrect = (current.answer == current.givenAnswer);
			if(answersByBtnId[btnId]) {
				if(isCorrect) {
					if(answersByBtnId[btnId]['isCorrect'] == 0) {
						answersByBtnId[btnId]['isCorrect'] = isCorrect;
					}
				} else {
					answersByBtnId[btnId]['isCorrect'] = isCorrect;
				}
			} else {
				answersByBtnId[btnId] = { btnId: btnId, isCorrect: isCorrect };
			}
			answersByBtnId[btnId]['answer'] = current;
		});
		return answersByBtnId;
	};

	//$scope.question.totalAnswers = $scope.question.answermap.length;
	$scope.$on("TBS-Journal-showExplanation", function() {
		var opts = {
			    backdrop: true,
			    keyboard: true,
			    backdropClick: false,
			    templateUrl: 'views/tbsjournal-solution.php',
			    controller: 'TBSJournalSolutionController',
      			dialogFade: true,
			    //dialogClass: 'modal',
			    resolve:{
			    	solution:function() {
			    		return $scope.question.question.solution;
			    	},
			    	answers:function() {
			    		return getQuestionAnswersByBtnId();
			    	},
			    	totalAnswers:function() {
						return $scope.question.buttonQuantity;
			    	}
			    }
			  };

	  	var d = $modal.open(opts);

		   d.opened.then(function() {
				$timeout(function() {
					$('.modal-dialog').draggable();
				}, 0);
			});

		    d.result.then(function(cmd) {
		    	if(cmd === "ok") {
		    		$scope.gotoNextQuestion();
		    	} 
		    });
		    $scope.highlightAnswers();
	});

	$scope.getAnswerTime = function() {
		return (new Date().getTime() - $scope.startTime);
	}

	$scope.calculateScore = function() {
		var score = 0,
			scoreByBtnId = getQuestionAnswersByBtnId();
		angular.forEach(scoreByBtnId, function(current, idx) {
			if(current.isCorrect) {
				score++;
			}
		});
		if (score > 0) {
			var totalQuestions = $.makeArray($(".answerlink")).length;
			score = parseFloat((score / totalQuestions)).toFixed(2);
		}
		return score;
	};
	$scope.question.newNoteText = "";

	}
});

TestSimModule.controller("ErrorDetailController", function($scope, $modalInstance, msg, data) {
	$scope.msg = msg;
	$scope.data = data;
	$scope.close = function() {
		$modalInstance.dismiss();
	};

	$scope.saveAndClose = function() {
		$modalInstance.close();
	};
});

TestSimModule.controller("AnswerListController", function($scope, QuizNavigation, $modalInstance, tbs) {
	$scope.tbs = tbs;
	$scope.close = function() {
		$modalInstance.close('cancel');
	};

	$scope.saveAndClose = function() {
		$modalInstance.close(tbs);
	};

});

TestSimModule.controller("TBSJournalSolutionController", function($scope, solution, answers, totalAnswers, $modalInstance) {
	$scope.solution = solution;
	$scope.totalAnswers = totalAnswers;
	$scope.close = function() {
		$modalInstance.close('cancel');
	};

	$scope.saveAndClose = function() {
		$modalInstance.close('ok');
	};

	$scope.correctAnswers = 0;
	angular.forEach(answers, function(current, idx) {
		if(current.isCorrect) {
			$scope.correctAnswers++;
		}
	});
});


TestSimModule.controller("SaveQuizSessionController", function($scope, $rootScope, $routeParams, $browser, $timeout, $location, $modal, SessionService, SectionService) {

	if(QuizSession.sessionName !== null) {
		$scope.sessionName = SessionService.sessionName;
	} else {
		$scope.sessionName = SectionService.section + " Quiz " + moment().format('MMMM Do YYYY, h:mm:ss a');
  	}
  	$scope.isSaved = false;
  	$scope.saveSession = function() {
		if (!SessionService.previewMode) {
			SessionService.saveSession($scope.sessionName);
		}
		$scope.isSaved = true;
  	};
});


TestSimModule.controller("ReportIssueController", function($scope, $modalInstance, $http, $timeout, question, BrowserInfoService){

	$scope.question = question;
	$scope.issueReported = false;
//	$scope.browser = new WhichBrowser().toString();
	$scope.browser = BrowserInfoService.info.useragent;

	$scope.description = {};
	$scope.description.text = "";

	$scope.alerts = [];

	$scope.studentID = $.cookie("studentid");
	$scope.userData = $.cookie("data");

	$scope.close = function() {

		$modalInstance.dismiss();
		window.timer.resumeTime();
	};

	$scope.isSubmitting = false;

	$scope.reason = function(type) {
		$scope.issueHasReason = type;
	};

	$scope.submitIssue = function() {

		$scope.alerts = [];

		if($scope.description.text != "") {
		$scope.isSubmitting = true;
		$http.post(ServiceURLS.reportIssueUrl, {
			issue:
			{question:$scope.question.id, 
			type:$scope.question.type, 
			description: $("#question_problem_description").val(), 
			reason: $scope.issueHasReason,
			studentID:$scope.studentID
			}
			})
			.success(function(data, status, headers, config) {
				$scope.isSubmitting = false;
				$scope.issueReported = true;
/*
				if(Keen) {
					var hasAnswer = $scope.question.answer != null;
					var keenEvent = {"interaction":"problem report", question:$scope.question.id, type:$scope.question.type, description:$scope.description, hasAnswer:hasAnswer};
					Keen.addEvent(keenEvent);
				}
*/
			});
		} else {
			$scope.alerts.push({"type":"alert-error","msg":"Please describe the problem you are experiencing."});
			$("#question_problem_description").focus();
		}
	};
});

TestSimModule.service("ScoreHistoryService", function($rootScope, $location, $http, $q) {
	var self = this;
	self.gettingQuizzes = false;

	this.getScores = function(sessionid, section, page) {
		if (!self.gettingQuizzes) {
			self.gettingQuizzes = true;
		if (!sessionid) {
			sessionid = '0';
		}
		var promise = $q.defer();
		if (section) {
		$http({method:'GET', url: ServiceURLS.getQuizUrl  + "?student_id=" + JSON.parse($.cookie('data')).studentID + '&session_id=' + sessionid + '&section=' + section + '&page=' + page}).
			success(function(data, status, headers, config) {
				self.gettingQuizzes = false;
				var counter = 0;
				self.sessions = [];
				var i = 0;
				angular.forEach(data.sessions, function(session, key) {

					if(i >= 5) return;
					i++;

//					session.completed = true;
//					var quizlets = [];
/*
*/
//					session.quizlets = angular.copy(quizlets);
/*
					angular.forEach(session.quizlets, function(quizlet) {
						angular.forEach(quizlet, function(question) {
							if (question.status == "skipped") {
								session.completed = false;
							}
						});
					});
*/
					self.sessions.push(session);
				});
				promise.resolve(self.sessions);
			}).
			error(function(data, status, headers, config) {
				//promise.
			});
		}
		return promise.promise;
		}
	}

	this.deleteSession = function(sessionID) {
		$http({method:'GET', url:ServiceURLS.deleteSessionUrl + sessionID}).
			success(function(data, status, headers, config) {
				
			});
	};
});
TestSimModule.controller("MyScoresController", function($scope, $rootScope, $http, $modal, ScoreHistoryService, SectionService, SessionService, QuizletService, QuestionService, ChapterService, LoginService) {
//        LoginService.verify();

	$scope.editingSessionName = false;
	$scope.savingSessionName = false;
	$scope.section = false;
	$scope.gettingQuizzes = false;
	SessionService.resetQuizSession();
	$scope.model = {};
	$scope.scorePage = 1;
/*
	$rootScope.$on('$routeChangeSuccess', function(e, current, old) { 
		$scope.getSessions();
	});
*/
	$scope.getSessions = function() {
		if (window.location.href.indexOf('section=aud') >= 0) {
			$scope.section = 'aud';
		}
		if (window.location.href.indexOf('section=bec') >= 0) {
			$scope.section = 'bec';
		}
		if (window.location.href.indexOf('section=far') >= 0) {
			$scope.section = 'far';
		}
		if (window.location.href.indexOf('section=reg') >= 0) {
			$scope.section = 'reg';
		}
		if ($scope.section) {
			var promise = ScoreHistoryService.getScores(0, $scope.section, $scope.scorePage);
			if (promise) {
				promise.then(function(results) {
				$scope.gettingQuizzes = false;
				$scope.history = results;
				});
			}
		}
	};
	$scope.getSessions();
/*
	$scope.$on("topics", function() {
		$scope.getSessions();
	});
*/
	$scope.moreQuizHistory = function() {
		if (!$scope.gettingQuizzes) {
			var page = $scope.scorePage + 1;
			$scope.gettingQuizzes = true;
			ScoreHistoryService.getScores($scope.history[$scope.history.length-1].id, SectionService.section, page).then(function(results) {
				$scope.gettingQuizzes = false;
				$scope.history = $scope.history.concat(results);
				$scope.scorePage = page;
			});
		}
	};

	$scope.getSessionTime = function(session) {
		return new Date(session.timestamp.replace('-','/','g') + ' GMT').toLocaleString();
	};

	$scope.editSessionName = function(session) {
		$scope.editingSessionName = session.id;
	};

	$scope.saveSessionName = function(session) {
		$scope.savingSessionName = session.id;
		$http.put(ServiceURLS.saveQuizUrl, 
			{id: session.id,
			session_name: session.session_name
			})
		.success(function(data, status, headers, config) {
			$scope.editingSessionName = false;
			$scope.savingSessionName = false;
		});
	};
	$scope.toggleExpandSession = function(session) {
		session.isExpanded = !session.isExpanded;
	};

	$scope.previewQuestions = function(session) {
			SessionService.previewMode = true;
			$rootScope.$broadcast("reset");

			$http.post(ServiceURLS.questionPreviewUrl, {'quizlets': session.quizlets}).then(function(response) {
				SessionService.quizlets = response.data['quizlets'];
				SessionService.sessionName = session.session_name;
				SessionService.quizID = session.id;
				SessionService.processLoadedQuiz(SessionService, null, null, null, 'review', session);	
			});
	}

	$scope.viewQuestions = function(session) {
		SessionService.viewQuestions(session.quizlets);
	};

	$scope.toggleInclude = function (session, topic) {
		var included = [];
		angular.forEach(session.summary.session.summary.breakdown, function(t) {
			if(t.topic_id == topic.topic_id) {
				t.isIncluded = ! t.isIncluded;
			}
			if(t.isIncluded) {
				included.push(t);
			}
		});

		session.selectedTopics = included;

	};

	$scope.precannedQuiz = function(session) {
		$http.get(ServiceURLS.relaunchQuizUrl + '?session_id=' + session.id ).then(function(response) {
			var session = response.data.sessions[0];
			var quizlets = [];
			angular.forEach(session.questions, function(question) {
				if (typeof quizlets[question.quizlet_num] == "undefined") {
					quizlets[question.quizlet_num] = {'questions': []};
					
				}
				quizlets[question.quizlet_num].questions.push(question);
			});
//			session.quizlets = quizlets;
//			QuizletService.requestedQuizlets = session.quizlets;
//			SessionService.loadQuiz();
			SessionService.viewQuestions(quizlets);
		});
	}

	$scope.relaunchQuiz = function(session, previewmode, blankslate) {
		SessionService.initLoad();
		if (previewmode) {
			SessionService.previewMode = true;
		}
		SectionService.section = session.section.toUpperCase();
		SessionService.sessionName = session.session_name;
		SessionService.quizID = session.id;
		ChapterService.chosenChapters = session.total_chapters;
/*
		angular.forEach(session.quizlets, function(quizlet) {
			quizlet.qty = quizlet.questions.length;
			angular.forEach(quizlet.questions, function(question) {
				if (question.answer_id != null) {
//				QuizSession.reloadQuestionAttempt(question.answer_id);
				QuestionService.reloadQuestionAttempt(question);
				}
			});
		});
*/
		if (session) {
			QuizletService.requestedQuizlets = session.quizlets;
		}
//		$rootScope.$on("topics", function() {
			if (previewmode) {
				SessionService.reloadQuiz(session, false, false, 'review');
			}
			else if (blankslate) {
				SessionService.reloadQuiz(session, false, false, false, blankslate);
			}
			else {
				SessionService.reloadQuiz(session);
			}
//		});
//		$rootScope.initTopics(SectionService.section);
		
	};

	$scope.deleteQuiz = function(session) {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: false,
		    templateUrl: 'views/confirm-delete-quizsession.php',
		    controller: 'ConfirmDeleteSessionController',
  			dialogFade: true,
		    resolve:{
		    	sessionID:function() {
		    		return session.id;
		    	}
		    }
		};
	    var d = $modal.open(opts).result.then(function() {
	    	var page = 1;
	    	ScoreHistoryService.deleteSession(session.id);
	    	ScoreHistoryService.getScores(0, SectionService.section, page).then(function(results) {
			$scope.history = results;
			$scope.scorePage = page;
		});
	    }, function() {
	    	//was cancelled
	    });
	};



});


TestSimModule.controller("ConfirmDeleteSessionController", function($scope, $modalInstance, sessionID) {
	$scope.deleteForever = function() {
		$modalInstance.close();
	}
	$scope.cancel = function() {
		$modalInstance.dismiss();
	}
});

TestSimModule.run(["$templateCache", function($templateCache) {
  $templateCache.put("questionTextblock",
    "<question-textblock ng-model='template.model'></question-textblock>");
}]);

TestSimModule.run(["$templateCache", function($templateCache) {
  $templateCache.put("questionGridblock",
    "<div class='widgetQuestion-gridContainer'><question-gridblock ng-model='template'></question-gridblock></div>");
}]);
/*
TestSimModule.run(["$templateCache", function($templateCache) {
  $templateCache.put("questionGridblock", '<div class="widgetQuestion-gridContainer">' +
	  	'<wij-grid  data="template.model.content" allow-editing="false" highlightCurrentCell="false" highlightOnHover="false" cellStyleFormatter="cellStyleFormatter">' +
		  '<columns>' +
		    '<column dataKey="col_0" ></column>' +
	      '</columns>' + 
		'</wij-grid>' +
		'</div>');
}]);*/

TestSimModule.directive('digiTimer', function() {
	return {
		restrict: "ACE",
		controller: "TimerController",
		scope: true,
		link: function() {
		}
	}
});

TestSimModule.directive('reportProblem', [ '$rootScope', '$compile', '$modal', '$timeout', function($rootScope, $compile, $modal, $timeout) {

	return {
		restrict:"A",
		template:"",
		replace:false,
		scope: {
			question:"="
		},
		link:function($scope, $element, $attrs, ctrl) {
			$element.on("click", function(e) {
				$timeout(function() {
					var opts = {
					    backdrop: true,
					    keyboard: true,
					    backdropClick: false,
					    templateUrl: 'views/quiz-reportissue-window.php',
					    controller: 'ReportIssueController',
		      			dialogFade: true,
					    resolve: {
					    	question:function() {
					    		return $scope.question;
					    	}
					    }
					};
					if (window.timer) {
						window.timer.pauseTime(false);
					}
				    var d = $modal.open(opts).result.then(function() {}, function() {
						$rootScope.$broadcast("unpauseTimer");
					});
				})
		});
		}
	}
}]);

TestSimModule.directive('questionTextblock', ['$compile', '$templateCache', function($compile, $templateCache) {
	return {
		restrict:"E", 
		template:'',//<div> content: <span ng-bind-html="getContent()"></span></div>',
		transclude : true,
		scope:true,
		replace: false,
	    require: '?ngModel',
	    link: function($scope, elem, attr, ctrl) {
		   $scope.model = $scope.$parent.$eval(attr.ngModel);
		   elem.html($scope.model.content);

		   $scope.$parent.$watch(function() {
		   	return $scope.model.content;
		   }, function(n) {
		   	elem.html($scope.model.content);
		   });

		   $compile(elem.contents())($scope);
	    }
	};
}]);

TestSimModule.directive('questionGridblock', ['$compile', '$templateCache', function($compile, $templateCache) {
	return {
		restrict:"E", 
		template:'<table></table>',
		transclude : true,
		scope:{ngModel:"="},
		replace: true,
	    require: 'ngModel',

	    link: function($scope, elem, attr, ctrl) {
		    $scope.model = $scope.$parent.$eval(attr.ngModel);
		    attr.$set('id',$scope.model.model.id);
		    $scope.getData = function() {
			    return $scope.model.model.content;
			};
			$scope.getColumns = function() {
			  	return $scope.model.model.columns;
			};

	      
	    }
	};
}]);



window.letters = [
	"A",
	"B",
	"C",
	"D",
	"E",
	"F",
	"G",
	"H",
	"I",
	"J",
	"K",
	"L",
	"M",
	"N",
	"O",
	"P",
	"Q",
	"R",
	"S",
	"T",
	"U",
	"V",
	"W",
	"X",
	"Y",
	"Z"
	];





