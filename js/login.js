var Login = angular.module("Login", ['ui.bootstrap']).run(function($rootScope) {
	//check cookies and redirect to setup.php if everything is good...
});
/*
Login.directive('loginPrompt', function($scope) {
	return {
		restrict: 'ACE',
		templateUrl: 'views/form-login.php',
		controller: 'DialogController'
	}
});
*/

Login.controller("LoginController", function ($scope, $modal, $timeout){
  $scope.opts = {
    backdrop: 'static',
    keyboard: true,
    backdropClick: false,
    templateUrl: 'views/form-login.php',
    controller: 'DialogController'
  };

  $scope.openDialog = function(){
//    var d = $dialog.dialog($scope.opts);
    $modal.open($scope.opts);
/*.then(function(result){
      if(result)
      {
        alert('dialog closed with result: ' + result);
      }
    });*/
  };

  $scope.openMessageBox = function(){
    var title = 'This is a message box';
    var msg = 'This is the content of the message box';
    var btns = [{result:'cancel', label: 'Cancel'}, {result:'ok', label: 'OK', cssClass: 'btn-primary'}];

    $dialog.messageBox(title, msg, btns)
      .open()
      .then(function(result){
        alert('dialog closed with result: ' + result);
    });
  };

	$scope.openDialog();
});

// the dialog is injected in the specified controller
Login.controller("DialogController", function($scope, $location){
 /* $scope.close = function(result){
    dialog.close(result);
  };*/
  $scope.loginError = null;

  $scope.getLoginErrors = function() {
	if ($location.search().error) {
		var error = $location.search().error;
		if (error === 'notfound') {
			$scope.loginError = "Incorrect email or password";
		}
	}
  };

  $scope.getLoginErrors();
});
