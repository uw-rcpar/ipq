var ServiceURLS = {
topics: "/restserver/index.php/api/testcenter/chapters/format/json",
quizConfig: "/restserver/index.php/api/testcenter/quizlets/format/json",
quizConfigQid: "/restserver/index.php/api/testcenter/quizlets_by_qid/format/json",
getQuizzesCount: "/restserver/index.php/api/testcenter/quizzes_count/format/json", // GET
//saveQuizUrl: "/restserver/index.php/api/testcenter/save_quizlet_results/format/json",
saveQuizUrl: "/restserver/index.php/api/testcenter/save_quiz/format/json", // PUT or POST
saveQuestionScores: "/restserver/index.php/api/testcenter/save_question_scores/format/json", // PUT or POST
getQuestionScores: "/restserver/index.php/api/testcenter/get_question_scores/format/json", // GET
getQuizUrl: "/restserver/index.php/api/testcenter/get_saved_quizzes_summary/format/json", // GET
relaunchQuizUrl: "/restserver/index.php/api/testcenter/relaunch_quiz/format/json", // GET
reportIssueUrl:"/restserver/index.php/api/testcenter/issue_report/format/json",
//scoreHistoryUrl:"/restserver/index.php/api/testcenter/return_quizlet_results/format/json",
reloadSessionUrl:"/restserver/index.php/api/testcenter/questions_by_id/format/json",
//updateSessionUrl:"/restserver/index.php/api/testcenter/update_session/format/json",
deleteSessionUrl:"/restserver/index.php/api/testcenter/delete_session/format/json?id=", //use GET
questionCountUrl:"/restserver/index.php/api/testcenter/number_of_questions/format/json", //POST
chapterInfoUrl:"/restserver/index.php/api/testcenter/get_chapter_info/format/json", //POST
getChapterInfo:"/restserver/index.php/api/testcenter/get_chapter_info/format/json", //GET
questionPreviewUrl:"/restserver/index.php/api/testcenter/get_question_preview/format/json", //POST
questionPreviewUrlV2:"/restserver/index.php/api/testcenter/get_question_preview_v2/format/json", //POST
getSessionMetadataUrl:"/restserver/index.php/api/testcenter/get_session_metadata/format/json", //POST
summaryQuestionScores:"/restserver/index.php/api/testcenter/summary_question_scores/format/json", //GET
};
var scrollSpyModule = angular.module('scrollSpyModule', []).config(function() {});
scrollSpyModule.directive('spy', function($location) {
  return {
    restrict: "A",
    require: "^scrollSpy",
    link: function(scope, elem, attrs, scrollSpy) {
      var _ref;
      if ((_ref = attrs.spyClass) == null) {
        attrs.spyClass = "active";
      }
      elem.click(function() {
        return scope.$apply(function() {
        	var loc = $("#" + attrs.spy).offset().top - $('.navbar-fixed-top').height() - 30;
        	$(document).scrollTop(loc);
        });
      });
      return scrollSpy.addSpy({
        id: attrs.spy,
        "in": function() {
          return elem.addClass(attrs.spyClass);
        },
        out: function() {
          return elem.removeClass(attrs.spyClass);
        }
      });
    }
  };
});
scrollSpyModule.directive('preventDefault', function() {
    return function(scope, element, attrs) {
        jQuery(element).click(function(event) {
            event.preventDefault();
        });
    }
});
scrollSpyModule.directive('scrollSpy', function($window) {
  return {
    restrict: 'A',
    controller: function($scope) {
      $scope.spies = [];
      return this.addSpy = function(spyObj) {
        return $scope.spies.push(spyObj);
      };
    },
    link: function(scope, elem, attrs) {
      var spyElems, topBuffer;
      spyElems = {};
      topBuffer = attrs.topBuffer ? attrs.topBuffer : 0;
      scope.$watch('spies', function(spies) {
        var spy, _i, _len, _results;
        _results = [];
        for (_i = 0, _len = spies.length; _i < _len; _i++) {
          spy = spies[_i];
          if (spyElems[spy.id] == null) {
            _results.push(spyElems[spy.id] = elem.find('#' + spy.id));
          } else {
            _results.push(null);
          }
        }
        return _results;
      }, true);
      return $($window).scroll(function() {
        var highlightSpy, pos, spy, _i, _len, _ref;
        highlightSpy = null;
        _ref = scope.spies;
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
          spy = _ref[_i];
          spy.out();
          if (!(spyElems[spy.id] != null) || spyElems[spy.id].length === 0) {
            spyElems[spy.id] = elem.find('#' + spy.id);
          }
          if ((spyElems[spy.id] != null) && spyElems[spy.id].length !== 0) {
          	var _top = spyElems[spy.id].offset().top;
            if (( _top - $window.scrollY )<= topBuffer) {
              spy.pos = _top + topBuffer;
              if (highlightSpy == null) {
                highlightSpy = spy;
              }
              if (highlightSpy.pos < spy.pos) {
                highlightSpy = spy;
              }
            }
          }
        }
        return highlightSpy != null ? highlightSpy["in"]() : null;
      });
    }
  };
});

var TestSimModule = angular.module("TestingModule", ['angular-bootstrap-select', 'ui.bootstrap', 'ngSanitize', 'wijmo', 'timer', 'scrollSpyModule']).
  config(function($routeProvider) {
  	$routeProvider
	  	.when('/quiz/setup',
	        {
//	            controller: 'CreateQuizController',
	            templateUrl: 'views/form-new-quiz.php'
	        })
		.when('/simulator/summary',
		{
//			controller: 'CreateQuizController',
			templateUrl: 'views/simulator-summary.php'
		})
	  	.when('/quiz/setup/reg',
	        {
//	            controller: 'CreateQuizController',
	            templateUrl: 'views/form-new-quiz.php'
	        })
	  	.when('/quiz/setup/far',
	        {
//	            controller: 'CreateQuizController',
	            templateUrl: 'views/form-new-quiz.php'
	        })
	  	.when('/quiz/setup/aud',
	        {
//	            controller: 'CreateQuizController',
	            templateUrl: 'views/form-new-quiz.php'
	        })
	  	.when('/quiz/setup/bec',
	        {
//	            controller: 'CreateQuizController',
	            templateUrl: 'views/form-new-quiz.php'
	        })
	  	.when( '/quiz/setup/step2',
	  		{
//	  			controller:'CreateQuizController',
	  			templateUrl:'views/form-new-quiz-step2.php'
	  		})
	  	.when( '/quiz/setup/step3',
	  		{
//	  			controller:'CreateQuizController',
	  			templateUrl:'views/form-new-quiz-step3.php'
	  		})
	    .when('/preview/question/mp/:qID/:id',
	        {
//	            controller: 'ReviewController',
	            templateUrl: 'views/preview.php'
	        })
	    .when('/preview/question/mcq/:qID/:id',
	        {
//	            controller: 'ReviewController',
	            templateUrl: 'views/preview.php'
	        })
	    .when('/preview/question/tbs-journal/:qID/:id',
		    {
	            controller: 'ReviewController',
	            templateUrl: 'views/preview.php'
		    })
	    .when('/preview/question/tbs-research/:qID/:id',
		    {
	            controller: 'ReviewController',
	            templateUrl: 'views/preview.php'
		    })
	    .when('/preview/question/tbs-wc/:qID/:id', {
	            	controller: 'ReviewController',
	    		templateUrl: 'views/preview.php'
	    	})
	    .when('/quiz/question/mp/:qID/:id',
	        {
//	            controller: 'SessionController',
	            templateUrl: 'views/question-template.php'
	        })
	    .when('/quiz/question/tbs-journal/:qID/:id',
		    {
//	            controller: 'SessionController',
	            templateUrl: 'views/question-template.php'
		    })
	    .when('/quiz/question/tbs-research/:qID/:id',
		    {
//	            controller: 'SessionController',
	            templateUrl: 'views/question-template.php'
		    })
	    .when('/quiz/question/tbs-wc/:qID/:id', {
//	            controller: 'TBSWrittenCommunicationController',
	    		templateUrl: 'views/question-template.php'
	    	})
	    .when('/quiz/session/save',
	        {
	            controller: 'SaveQuizSessionController',
	            templateUrl: 'views/form-save-quiz-session.php'
	        })
	    .when('/my-scores',
	        {
//	            controller: 'MyScoresController',
	            templateUrl: 'views/my-scores.php'
	        })
		    .when('/dashboard', {
//			controller: 'CreateQuizController',
			templateUrl: 'views/dashboard.php'
		    })
	   	.when('/final-score',
		   	{
		 		controller:'FinalScoreController',
		 		templateUrl:'views/final-score.php'
		   	}
	   	)
	   	.when('/preview/:type/:id',
		   	{
		   		controller:'PreviewController',
		 		templateUrl:'views/preview.php'

		   	}
	   	)
		.when('/login',
			{
				controller: 'LoginController',
				templateUrl: 'views/login.php'
			}
		)
	    .otherwise({ redirectTo: '/dashboard' });
  });

TestSimModule.run(function($rootScope, TopicSections, SessionService, SectionService) {

		$rootScope.typeToInt = function(type) {
			var typeInt = 0;
			switch(type) {
				case "multiple-choice":
					typeInt = 0;
					break;
				case "tbs-journal":
					typeInt = 1;
					break;
				case "tbs-wc":
					typeInt = 2;
					break;
				case "tbs-research":
					typeInt = 3;
					break;
				case "tbs-irs":
					typeInt = 4;
					break;
				case "tbs-calc":
					typeInt = 5;
					break;

			}
			return typeInt;
		};

		$rootScope.intToType = function(typeInt) {
			var _typeInt = typeInt;
			typeInt = parseInt(typeInt);
			var type = "";
			switch(typeInt) {
				case  0:
					type = "multiple-choice";
					break;
				case 1:
					type = "tbs-journal";
					break;
				case 2:
					type = "tbs-wc";
					break;
				case 3:
					type = "tbs-research";
					break;
				case 4:
					type = "tbs-irs";
					break;
				case 5:
					type = "tbs-calc";
					break;
				default:
					typeInt  = _typeInt;
					break;
			}
			return type;
		};

		$rootScope.initTopics = function(section) {
			SectionService.section = section;
			TopicSections.getTopics(section);
			$scope.$on("topics", function(evt, value) {
				SessionService.topics = value;//for linking in-question
			});
//			return promise;//for promise chaining
		}
});

//Browser detect
TestSimModule.factory( 'BrowserCheckStatusFactory', browserCheckStatus);

TestSimModule.controller("BrowserCheckAlertController", function($scope, $rootScope, BrowserCheckStatusFactory) {
    $scope.browserOk = BrowserCheckStatusFactory.allowed_browser ? true : false;
    $scope.deviceType = BrowserCheckStatusFactory.device_type;
});

TestSimModule.directive('browserCheckAlert', function() {
    return { 
        scope: true, 
        restrict: 'ACE', 
        templateUrl: 'views/browser-check-alert.html', 
        controller: 'BrowserCheckAlertController' 
    };
});

TestSimModule.service("TopicSections", function($rootScope, $http, $q, $location, ChapterService) {
	var self = this;
	var inProgress = false;

    // @todo: define how getTopics should be used, it returns a promise, but also fires a broadcat
    // we should do one or the other, so to avoid confusion around what events this will fire
	this.getTopics = function(examSection) {

        console.log("TopicSections.getTopics: " + examSection);
        console.log("inProgress", inProgress);

		self.deferred = $q.defer();
		if (!inProgress) {
            console.log("Fetching topics from REST server");
			inProgress = true;
			$http({method:'GET', url: ServiceURLS.topics + "?section=" + examSection + "&t=" + new Date().getTime() }).then(function(response) {
					angular.forEach(response.data.chapters, function(chapter) {
						chapter.include = false;
						chapter.chapterNumber = chapter.chapter.split(': ')[0];
						chapter.chapterTitle = chapter.chapter.split(': ')[1];
						angular.forEach(chapter.topics, function(topic) {
							topic.include = false;
							topic.chapterID = chapter.id;
						});
					});
					    ChapterService.topics = response.data.chapters; // for linking in-question
					    var data = { t: [] },
					    chapterRequest = {
						chapters: ChapterService.topics,
						student_id: parseInt(JSON.parse($.cookie('data')).studentID)
					    };
//					    var chapterQuantity = 0;
					    angular.forEach(ChapterService.topics, function(chapter) {
//						chapterQuantity++;
						angular.forEach(chapter.topics, function(topic) {
						    data.t.push(topic.id);
						})
					    });

				            var chapterInfo = {data:{counts:{}, scores:{}}};
						$http.get(ServiceURLS.getChapterInfo + '?section=' + examSection ).then(function(chapterInfo) {
							inProgress = false;
							$rootScope.$broadcast("topics", {topics: response.data.chapters, chapter_info: chapterInfo.data});
						}, function(err) {
						});
/*
					    $http.post(ServiceURLS.chapterInfoUrl, chapterRequest).then(function(chapterInfo) {
						
						// if (callback) {
						//     callback(value, response);
						// }
						$rootScope.$broadcast("topics", {topics: response.data.chapters, chapter_info: chapterInfo});

					    });
	*/
					self.deferred.resolve(response.data.chapters);
				}, function(err) {
					if (err.data) {
						if (err.data.error) {
							if (err.data.error == "authentication required") {
								$location.path("/login");
							}
						}
					}
				});
		}

		return self.deferred.promise;
	};
});//TopicSections

TestSimModule.service("QuizConfigState", function($http, $q, $location, $rootScope, $timeout, QuizNavigation, $modal, SessionService, QuizletService){
	this.model = {};
	this.tbsmodel = {};
	this.inspectedChapter = null;
	this.pages = [];
	this.currentPage = 0;
	this.qPageMax = 3;
	this.examSimulator = false;
	this.chosenChapters = 0;
	this.chosenChaptersList = [];
	this.selectedChapters = [];
	this.chapterScores = false;
	this.totalQuestions = {};
	this.previewMode = false;
	this.model.pickSection = null;
	this.model.pickedTopics = [];
	$rootScope.quizlets = [];
	this.requestedQuizlets = [];
	this.realTotalQuestions = 0;
	this.saveMode = "new";
	var self = this;

	this.resetQuizlet = function() {
		QuizNavigation.navState.quizlet = 0;
		QuizNavigation.navState.question = 0;
		QuizNavigation.navState.showTimer = false;
		QuizNavigation.navState.scoreAsIGo = false;
		$location.path(SessionService.quizlets[0].questions[0].path);
	}

	this.viewQuestions = function(chapterquizlets, category, type, multitype) {
		if (typeof chapterquizlets !== 'object') {
		// If this is true, then chapterquizlets = a chapter number
			if (category == 'multiple-choice') {
				category = 'mcq';
			}
			else if (category !== 'mcq' && category !== 'multiple-choice') {
				category = 'tbs';
			}
			if (!this.hasReviewedQuizDetails) {
				var opts = {
					    backdrop: true,
					    keyboard: true,
					    backdropClick: true,
					    templateUrl: 'views/startquizconfirmation.php',
					    controller: 'StartQuizConfirmation',
						dialogFade: true,
					    windowClass: ''
					 };
				    var d = $modal.open(opts).result.then(function(result) {
					if (result == 'ok') {
						this.hasReviewedQuizDetails = true;
						this.model.pickedTopics = [];
						var pushedChapters = {};
						angular.forEach($rootScope.topics, function(chapter) {
							if (chapter.id == chapterquizlets) {
								if (!pushedChapters[chapter.id]) {
									pushedChapters[chapter.id] = true;
									this.chosenChaptersList.push(chapter);
								}
								angular.forEach(chapter.topics, function(topic) {
									this.model.pickedTopics.push({topicID:topic.id, chapterID:chapterquizlets, m: topic.m, t: topic.t});
								}, this);
							}
						}, this);
						var qty  = this.chapterScores[chapterquizlets][category][type].length;
						if (category == 'mcq') {
							if (qty > 50) {
								this.model.qty = 50;
								this.tbsmodel.qty = 0;
							}
						}
						else {
							if (qty > 15) {
								this.model.qty = 0;
								this.tbsmodel.qty = 15;
							}
						}
						this.model.type = category;
						this.viewingPreConfiguredQuiz = true;
						$location.path('/quiz/setup/step2');
					}
				}.bind(this));
			}
			else {
				$rootScope.showTabLoading = true;
				this.previewMode = false;

				this.loadQuizByQids(this.chapterScores[chapterquizlets][category][type], category);
				SessionService.resetQuizlet();
			}
		}
		else {

			if (multitype) {
			// If multitype is true, then we need to turn several arrays of IDs into multiple quizlets of different types
				if (chapterquizlets.length > 0) {
					$rootScope.showTabLoading = true;
					this.previewMode = false;
					this.loadQuizByQids(chapterquizlets, null, false, 'multitype');
//					this.resetQuizlet();
				}
			}
			else if (chapterquizlets.length > 0) {
			// Else, chapterquizlets is an array of quizlets
				$rootScope.showTabLoading = true;
				this.previewMode = false;
				this.loadQuizByQids(chapterquizlets, null, 'multiquizlet');
				SessionService.resetQuizlet();
			}
		}

	};

	this.addQuizlet = function(m) {
		m.id = QuizletService.requestedQuizlets.length;
		QuizletService.requestedQuizlets.push(m);
	};

	this.reinit = function() {
		this.model.pickedTopics = [];
		this.previewMode = false;
	};

	this.updateChapterScores = function(id, correct, total, question) {
		var statuschange = false,
		noted = question.noted,
		flagged = question.isFlagged;
		if (question.attempts) {
			attempts = question.attempts;
		}
		else {
			attempts = [];
		}
		if (question.type == 'multiple-choice') {
			var type = 'mcq';
		}
		else {
			var type = 'tbs';
		}
		if (self.chapterScores[id]) {
/*
			if (attempts.length > 1) {
				statuschange = true;
			}
*/
			if (correct == 'incorrect' || correct == 'correct') {
				if (correct == 'incorrect') {
//					if (statuschange) {
					var statuschange = self.chapterScores[id][type].correct.indexOf(question.id);
					if (statuschange !== -1) {
						self.chapterScores[id][type].correct.splice(loc, 1);
					}
//					}
					var loc = self.chapterScores[id][type].incorrect.indexOf(question.id);
					if (loc == -1) {
						self.chapterScores[id][type].incorrect.push(question.id);
					}
				}
				else if (correct == 'correct') {
//					if (statuschange) {
					var statuschange = self.chapterScores[id][type].incorrect.indexOf(question.id);
					if (statuschange !== -1) {
						self.chapterScores[id][type].incorrect.splice(loc, 1);
					}
//					}
					var loc = self.chapterScores[id][type].correct.indexOf(question.id);
					if (loc == -1) {
						self.chapterScores[id][type].correct.push(question.id);
					}
				}
				var skipped = self.chapterScores[id][type].skipped.indexOf(question.id);
				if (skipped !== -1) {
					self.chapterScores[id][type].skipped.splice(skipped, 1);
				}
			}
			else if (correct == 'unanswered') {
				var loc_correct = self.chapterScores[id][type].correct.indexOf(question.id);
				var loc_incorrect = self.chapterScores[id][type].incorrect.indexOf(question.id);
				if (loc_correct !== -1) {
					self.chapterScores[id][type].correct.splice(loc_correct, 1);
				}
				if (loc_incorrect !== -1) {
					self.chapterScores[id][type].incorrect.splice(loc_incorrect, 1);
				}
				var loc = self.chapterScores[id][type].skipped.indexOf(question.id);
				if (loc == -1) {
					self.chapterScores[id][type].skipped.push(question.id);
				}
			}

			self.chapterScores[id].score = parseFloat((((self.chapterScores[id]['tbs'].correct.length + self.chapterScores[id]['mcq'].correct.length) *100)/total).toFixed(1));

		}
		if (noted) {
			if (self.chapterScores[id][type].noted) {
				self.chapterScores[id][type].noted.push(question.id);
			}
			else {
				self.chapterScores[id][type].noted = [];
				self.chapterScores[id][type].noted.push(question.id);
			}
		}
		if (flagged) {
			if (self.chapterScores[id][type].flagged) {
				self.chapterScores[id][type].flagged.push(question.id);
			}
			else {
				self.chapterScores[id][type].flagged = [];
				self.chapterScores[id][type].flagged.push(question.id);
			}
		}
			var chapterObj = {
				'student_id': parseInt(JSON.parse($.cookie('data')).studentID),
				'chapters': {}
			};
			chapterObj.chapters[id] = self.chapterScores[id];
			$http.post(ServiceURLS.updateSessionUrl, chapterObj).then(function(response) {
			});
		return true;
	}

	this.resetQuizSession = function() {
		this.model = {};
		this.saveMode = "new";
		this.model.pickSection = null;
		this.model.pickedTopics = [];
		this.tbsmodel.pickSection = null;
		this.tbsmodel.pickedTopics = [];
		this.selectedChapters = [];
		this.totalQuestions = {};
		this.realTotalQuestions = null;
		this.quizStarted = false;
		this.pages = [];
		this.lastStep = 1;
		this.furthestStepReached = 1;
		this.currentPage = 0;
		this.chosenChaptersList = [];
		this.chosenChapters = 0;
		$rootScope.quizlets = [];
		this.requestedQuizlets = [];
		$rootScope.quizlets = [];
	};

	this.reloadQuiz = function(session, /*pdata,*/ type, subtype) {
		self.resetQuizSession();
		self.saveMode = "update";
		if (session.quizlets) {
			self.model.pickSection = session.section;
	               	self.chosenChapters = [];
			angular.forEach(session.quizlets, function(quizlet) {
				angular.forEach(quizlet.questions, function(q) {
					self.chosenChapters.push(q.chapter);
				});
			});
			var quizlets = session.quizlets;
		}
		else {
			var quizlets = session;
		}
		if(quizlets == null) {
			return false;
		}

		self.reloadedQuizlets = [];
		self.loadCount = 0;

		angular.forEach(quizlets, function(quizlet) {
			var mp = [];
			var tbs = [];
			angular.forEach(quizlet.questions, function(question) {
				if (!type) {
					if(parseInt(question.type) == 0 || question.type == 'mcq' || question.type == 'multiple-choice') {
						mp.push(question.id);
					} else {
						tbs.push(question.id);
					}
				}
				else {
					if (question.id) {
						question = question.id;
					}
					if (type == 'mcq') {
						mp.push(question);
					}
					else {
						tbs.push(question);
					}
				}
			});

			//var loadCount = 0;
			var promise = $q.defer();

			promise.promise.then( function(obj) {
/*
				if (type && subtype) {
					self.processLoadedQuiz({'quizlets':[{'questions':obj.content}]}, obj.category);
					return false;
				}
*/
				var data = obj.content;
				var category = obj.category;
				var newArray = [];
				var counter = 0;
				for(var i = 0; i < data.length; i++) {
						var q = data[i];
						var ch = parseInt(q.chapter_id);
						if (typeof type == 'string' && typeof subtype == 'string') {
/*
							if (session.summary[type]) {
								var isMcqType = session.summary[type].multipleChoice.indexOf(q.id);
								var isTbsType = session.summary[type].tbs.indexOf(q.id);
							}
*/
							if (self.chapterScores[ch][type][subtype].indexOf(q.id) == -1) {
								q.exclude = true;
							}

						}
						else if (typeof type == 'object') {
							if (type.indexOf(q.id) == -1) {
								q.exclude = true;
							}
						}

						if (!q.exclude /*!type || (isMcqType > -1) || (isTbsType > -1)*/) {
							if(q.choices != null) {
								q.path = '/quiz/question/mp/' + counter + '/' + q.id;
								q.quizletID = "multiple-choice-" + self.loadCount;
							} else {
								q.path = '/quiz/question/' + q.type + '/' + counter + '/' + q.id;
								q.quizletID = q.type + "-" + self.loadCount;
							}
							angular.forEach(quizlets, function(quizlet, key) {
								angular.forEach(quizlet.questions, function(value, question_key) {
									if (parseInt(value.id) === parseInt(q.id)) {
										q.isFlagged = value.isFlagged;
										q.noted = JSON.parse(value.noted);
									}
								});
							});
							q.isFlagged = q.isFlagged || false;
							q.noted = q.noted || [];
							q.hasNotes = !!q.noted.length;
							q.index = i;
							newArray.push(q);
							counter++;
						}
				}
				self.realTotalQuestions = data.length;
				SessionService.quizlets.push( {questions:newArray} );
					self.loadCount++;
//					if(self.loadCount == quizlets.length) {
						self.launchReloadedQuiz();
//					}
			});

			self.reloadedQuizlets.push(promise.promise);
			var data = {
				questions:{"multipleChoice":mp,"tbs":tbs}
			};
			$http.post( ServiceURLS.reloadSessionUrl, data)
				.success(function(data, status, headers, config) {
					if(data.questions.multipleChoice.length > 0) {
						$timeout(function() {
							promise.resolve({'content':data.questions.multipleChoice, 'category': 'mcq'});
						});
					} else {
						promise.resolve({'content': angular.copy(data.questions.tbs), 'category': 'tbs'});
					}
				});

		});
	};

	this.getQuiz = function() {
		return this.model.quizlets;
	};

});

TestSimModule.factory("FlashService", function() {
	var FlashService = function() {};
	FlashService.prototype = {
		getFlashVersion: function() {
		  // ie
		  try {
		    try {
		      // avoid fp6 minor version lookup issues
		      // see: http://blog.deconcept.com/2006/01/11/getvariable-setvariable-crash-internet-explorer-flash-6/
		      var axo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash.6');
		      try { axo.AllowScriptAccess = 'always'; }
		      catch(e) { return '6,0,0'; }
		    } catch(e) {}
		    return new ActiveXObject('ShockwaveFlash.ShockwaveFlash').GetVariable('$version').replace(/\D+/g, ',').match(/^,?(.+),?$/)[1];
		  // other browsers
		  } catch(e) {
		    try {
		      if(navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin){
			return (navigator.plugins["Shockwave Flash 2.0"] || navigator.plugins["Shockwave Flash"]).description.replace(/\D+/g, ",").match(/^,?(.+),?$/)[1];
		      }
		    } catch(e) {}
		  }
		  return '0,0,0';
		},
		getFlashVersionMajor: function() {
			return this.getFlashVersion().split(',').shift();
		}
	};
	return new FlashService();
});

TestSimModule.controller("VideoController", function($scope, $modalInstance, $timeout, FlashService) {
	$scope.hasFlash = false;
	$scope.close = function() {
		$modalInstance.close();
	};
	$timeout(function() {

	if (FlashService.getFlashVersionMajor() > 11) {
		jwplayer("video1").setup({
			file: "rtmp://s20x7tomqngbxh.cloudfront.net/cfx/st/mp4:2014_software_demo/software_demo_final.mp4",
			height: 528,
			image: "img/intro-poster.png",
			width: 920,
			autostart: "false"
		});
	}

	});
});


TestSimModule.controller("NewQuizWarningController", function($scope, $location, $modalInstance) {
	$scope.save = function () {
		$modalInstance.close();
	};
	$scope.startOver = function() {
		$modalInstance.dismiss();
	};

});

TestSimModule.controller("StartQuizConfirmation", function($rootScope, $scope, $location, $modalInstance, QuizConfigState) {
	$scope.submit = function () {
		$modalInstance.close('ok');
	};
	$scope.cancel = function() {
		$modalInstance.dismiss();
	};

});


TestSimModule.controller("HowManyQuestionsController", function($rootScope, $scope, $location, $modalInstance, QuizConfigState, realquiz, ch, type, category) {
	$scope.submit = function (num) {
		if (realquiz) {
			$modalInstance.close(num);
			var newArray = QuizConfigState.chapterScores[ch][type][category];
			newArray.sort(function() {
			  return .5 - Math.random();
			});

			newArray = newArray.slice(0,num);
			$rootScope.showTabLoading = true;
			QuizConfigState.previewMode = false;
			QuizConfigState.loadQuizByQids(newArray, type);
		}
		else {
			$modalInstance.close(num);
		}
	};
	$scope.cancel = function() {
		$modalInstance.dismiss();
	};

});

TestSimModule.controller("MainNavController", function($scope, $rootScope, $location, $modal, /*QuizSession, QuizConfigState, */SessionService, ChapterService, SectionService, CreateQuizService){
	$scope.location = $location;
	$scope.QuizConfigState = SessionService;
	$scope.menu = [
		{label:"Section Overview", path:"/dashboard", pathKey: "/dashboard"},
		{label:"New Quiz", path:"/quiz/setup", pathKey:"/quiz/setup"},
		{label:"Exam Simulator", path:"/simulator/summary", pathKey:"/simulator/summary"},
		//{label:"New Test", path:"/test/setup/step1", pathKey:"/test/"},
		//{label:"Reports", path:"/reports/", pathKey:"/reports/"},
		{label:"Score History", path:"/my-scores", pathKey:"/my-scores"},
	];
	$scope.selectSection = function(section) {
		$rootScope.$broadcast("RequestNewSection", section);
/*
		window.location.href = window.location.protocol + "//" + window.location.host + window.location.pathname + window.location.hash +  "?section=" + section;
		SectionService.section = section;
		$rootScope.$broadcast("SectionChange", section);
//		window.location.reload();
*/
	};
	$scope.navigateTo = function(path) {
		if($location.path() !== '/final-score' && (path == "/quiz/setup" || path == "/dashboard" || path == "/simulator/summary" || path == "my-scores") && SessionService.quizlets.length > 0/* &&  == false*/ && !SessionService.previewMode) {
			var opts = {
			    backdrop: true,
			    keyboard: true,
			    backdropClick: true,
			    templateUrl: 'views/warning-newquiz.php',
			    controller: 'NewQuizWarningController',
	  			dialogFade: true,
			    windowClass: '',
			 };
		    var d = $modal.open(opts).result.then(function() {
		    			$location.path("/final-score");
		    },
		    function() {
/*
		    	var l = window.location;
		    	l = l.href.split("#")[0];
		    	window.location.href = l;
*/
			SessionService.resetQuizSession();
			$location.path(path);
		    });
		} else if(path == "/quiz/setup") {
//			var l = window.location;
//	    	l = l.href.split("#")[0];
//	    	window.location.href = l;
			SessionService.resetQuizSession();
			$location.path(path);
		}
		else if (path == "/simulator/summary") {
			SessionService.resetQuizSession();
			$location.path(path);
		}
		else {
			switch(path) {
				case "/my-scores":
					SessionService.resetQuizSession();
					$location.path(path);
					break;
				case "/dashboard":
					SessionService.resetQuizSession();
					$location.path(path);
					break;
			}

		}
	}
	$scope._path = new String($location.path());
	angular.forEach($scope.menu, function(i) {
		if($scope._path.indexOf(i.pathKey) != -1) {
			i.isActive = true;
		} else {
			i.isActive = false;
		}
	});
	$scope.videoCookie = $.cookie("introVideo");
	if($scope.videoCookie == null) {
		$.cookie("introVideo", true, {expires:180, path:"/"});
		$scope.videoCookie = $.cookie("introVideo").com_rcpa_bool;
	}
	$scope.showVideo = function() {
		if(window.timer != null) {
			window.timer.pauseTime();
		}
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/intro-video.php',
		    controller: 'VideoController',
  			dialogFade: true,
		    windowClass: 'modal-video inverse',


		  };
	    var d = $modal.open(opts).result.then(function(cmd) {
	    	$.cookie("introVideo", false, {expires:180, path:"/"});
	    	if(window.timer != null) {
				window.timer.resumeTime();
			}
	    });
	}

	if($scope.videoCookie == true) {
		// deprecated
//		$scope.showVideo();
	}

	//update the main nav when the hashbang changes
	$rootScope.$on('$routeChangeSuccess', function(e, current, old) {

		$scope._path = new String($location.path());
		angular.forEach($scope.menu, function(i) {
			if($scope._path.indexOf(i.pathKey) != -1) {
				i.isActive = true;
			} else {
				i.isActive = false;
			}
		});
		$scope.showSetupFooter = false;
		for(var i = 0; i < $scope.wizardMenu.length; i++) {
			if($scope._path == $scope.wizardMenu[i].path) {
				$scope.wizardIndex = i;
				$scope.showSetupFooter = true;
			}
		}
	});

	$scope.wizardMenu = [
			{ label:"Step 1", path:"/quiz/setup", enabled:false },
			{ label:"Step 2", path:"/quiz/setup/step2", enabled:false },
			{ label:"Step 3", path:"/quiz/setup/step3", enabled:false }
		];
	$scope.wizardIndex = 0;

	$scope.prevWizardStep = function() {
		$scope.wizardMenu[$scope.wizardIndex].enabled = false;
		$scope.wizardIndex--;
		if ($scope.wizardIndex == 0) {
			ChapterService.chosenChaptersList = [];
			ChapterService.chosenChapters = 0;
			if (CreateQuizService.furthestStepReached < 2) {
				ChapterService.selectedChapters = [];
			}
//			QuizConfigState.furthestStepReached = 1;
		}
		$scope.wizardMenu[$scope.wizardIndex].enabled = true;
		$location.path($scope.wizardMenu[$scope.wizardIndex].path);
	}

	$scope.nextWizardStep = function() {
		$scope.wizardMenu[$scope.wizardIndex].enabled = false;
		$scope.wizardIndex++;
		if ($scope.wizardIndex == 1) {
            SessionService.updateSessionNameByQuizzesCount();
			ChapterService.chosenChaptersList = [];
			var pushedChapters = {};
			angular.forEach(ChapterService.topics, function(chapter) {
				angular.forEach(SessionService.model.pickedTopics, function(topic) {
					if (chapter.id == topic.chapterID) {
						if (!pushedChapters[chapter.id]) {
							pushedChapters[chapter.id] = true;
							ChapterService.chosenChaptersList.push(chapter);
						}
					}
				});
				angular.forEach(SessionService.tbsmodel.pickedTopics, function(topic) {
					if (chapter.id == topic.chapterID) {
						if (!pushedChapters[chapter.id]) {
							pushedChapters[chapter.id] = true;
							ChapterService.chosenChaptersList.push(chapter);
						}
					}
				});
			});
			// Check to see if there are enough questions available for the chapters the user has selected
			var questionsAvailable = 0;
			angular.forEach(ChapterService.chosenChaptersList, function(chapter) {
						if (SessionService.hasNASBA || SectionService.section == "BEC") {
							if (SessionService.specificCategory == 'incorrect') {
								questionsAvailable += ChapterService.getQuestionCount(chapter.id, 'all', 'incorrect');
							}
							else if (SessionService.specificCategory == 'skipped') {
								questionsAvailable += ChapterService.getQuestionCount(chapter.id, 'all', 'skipped');
							}
							else {
								questionsAvailable += ChapterService.getQuestionCount(chapter.id, 'all', 'total');
							}
						}
						else {
							if (SessionService.specificCategory == 'incorrect') {
								questionsAvailable += ChapterService.getQuestionCount(chapter.id, 'all', 'non_nasba_incorrect');
							}
							else if (SessionService.specificCategory == 'skipped') {
								questionsAvailable += ChapterService.getQuestionCount(chapter.id, 'all', 'non_nasba_skipped');
							}
							else {
								questionsAvailable += ChapterService.getQuestionCount(chapter.id, 'all', 'non_nasba_total');
							}
						}
			});
			if (questionsAvailable == 0) {
				var opts = {
				    backdrop: true,
				    keyboard: true,
				    backdropClick: false,
				    templateUrl: 'views/not-enough-questions.php',
				    controller: 'genericModalController',
				    dialogFade: true,
				    resolve:{
				    }
				};
			    var d = $modal;
				    d.open(opts);
				// not enough questions available, so user cannot go on to step 2
				$scope.wizardIndex--;
				return;
			}
			if (SessionService.specificCategory) {
//				SessionService.filterQuestionsByCategory(SessionService.specificCategory, 1);
			}
		}
		if ($scope.wizardIndex == 2) {
			if (SessionService.specificCategory) {
//				SessionService.filterQuestionsByCategory(SessionService.specificCategory, 2);
			}
		}

		if($scope.wizardIndex >= $scope.wizardMenu.length) {
			$scope.wizardIndex = $scope.wizardMenu.length;
		}
		$scope.wizardMenu[$scope.wizardIndex].enabled = true;
		$location.path($scope.wizardMenu[$scope.wizardIndex].path);
	}
});

TestSimModule.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
		if(input === undefined || input.slice === undefined) return [];

			return input.slice(start);
    }
});

Number.prototype.floor = function() {
	return Math.floor(this);
};

Number.prototype.ceil = function() {
	return Math.ceil(this);
};

TestSimModule.service("LoginService", function($location) {
	this.verify = function() {
		if ($.cookie('data') == undefined || $.cookie('Drupal.visitor.ticket') == undefined) {
			$location.path('/login');
		}
		else if ($location.path() == "/login") {
			$location.path('/dashboard');
		}
	};
});

TestSimModule.controller("LoginController", function($scope, LoginService) {
//	LoginService.verify();

	$scope.isDevSite = function() {
		if (window.location.href.indexOf("testcenter.rogercpareviewdev.com") > -1 || window.location.href.indexOf("ipqdev.rogercpareview.com") > -1) {
			return true;
		}
		return false;
	};
});

TestSimModule.controller("SectionOverviewController", function($rootScope, $scope, $location, $timeout, $modal, SectionService, ChapterService, SessionService, QuizletService, LoginService) {
//	LoginService.verify();

	$scope.startNewQuiz = function() {
		$location.path('/quiz/setup');
	};

    // console.info('TestSimModule.controller("SectionOverviewController"');
	$scope.topics = {};

	if (typeof ChapterService.chapterScores == "object") {
		$timeout(function() {
		$rootScope.showTabLoading = false;
		$scope.topics = ChapterService.topics;
		}, 0);
	}

	$scope.$on("topics", function(evt, obj) {
//		var total = 0;
		angular.forEach(obj.chapter_info.counts, function(counts, type) {
			obj.chapter_info.counts[type].total = 0;
			angular.forEach(counts, function(count, key) {
				if (key !== 'total') {
					obj.chapter_info.counts[type].total += parseInt(count);
				}
			});
		});
//		obj.chapter_info.counts.total = total;
		$scope.totalQuestions = obj.chapter_info.counts;
		$scope.topics = obj.topics;
	});
	$scope.areChaptersReady = function() {
		if (typeof ChapterService.chapterScores == "object") {
			return true;
		}
		return false;
	};
	$scope.viewDashboardHelp = function() {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: false,
			controller: 'genericModalController',
		    templateUrl: 'views/section-overview-help.php',
  		    dialogFade: true,
			windowClass: 'rcpa-modal-lg',
		    resolve:{
		    }
		};
	    var d = $modal;
	    d.open(opts).result.then(function() {
		// clicked ok
	    }, function() {
	    	//was cancelled
	    });
	};

});


TestSimModule.controller("ReviewController", function($rootScope, $scope, SessionService, QuizletService, QuestionService, ChapterService) {
	$scope.navState = {};
	SessionService.previewMode = true;
	$scope.previewingFlaggedQuestions = SessionService.previewingFlaggedQuestions;
});

TestSimModule.controller("ChapterScoreController", function($rootScope, $scope, $timeout, ChapterService, SessionService) {
    $scope.chapterScore = {};
	$scope.previewQuestions = function(chid, type, stat) {
        SessionService.previewMode = true;
        $rootScope.$broadcast("reset");
		SessionService.previewQuestions(chid.id,type,stat);
	};

	$scope.getChapterScorePie = function(chapter) {
		var percent;
		if (ChapterService.chapterScores) {
			if (ChapterService.chapterScores[chapter]) {
				percent = parseInt(ChapterService.chapterScores[chapter].score);
			}
			else {
				percent = 0;
			}
			return (percent/100*360).toFixed(0);
		}
	};

	$scope.getChapterScore = function(chapter) {
		var score;
		if (ChapterService.chapterScores) {
			if (ChapterService.chapterScores[chapter]) {
				score = ChapterService.chapterScores[chapter].score;
			}
			else {
				score = 0;
			}
			return score;
		}
	};
	$scope.scoreBarIncorrect = function(chid) {
		if(ChapterService.chapterScores[chid] === undefined) return 0;
		var mcq_answered = ChapterService.chapterScores[chid]['mcq']['incorrect'] + ChapterService.chapterScores[chid]['mcq']['correct'];
		var mcq_incorrect = ChapterService.chapterScores[chid]['mcq']['incorrect'];

		var tbs_answered =  ChapterService.chapterScores[chid]['tbs']['incorrect'] + ChapterService.chapterScores[chid]['tbs']['correct'];
		var tbs_incorrect = ChapterService.chapterScores[chid]['tbs']['incorrect'];

//		var total = mcq_total + tbs_total;
		var answered = mcq_answered + tbs_answered;
		var incorrect = mcq_incorrect + tbs_incorrect;

		var result = (incorrect*100)/answered;

		if (typeof result == 'number') {
			return result;
		}
		else {
			return 0;
		}
	}
	$scope.scoreBarAttempted = function(chid) {
		if (ChapterService.chapterScores[chid] === undefined) return 0;
		var mcq_total = ChapterService.chapterScores[chid]['mcq']['skipped'] + ChapterService.chapterScores[chid]['mcq']['incorrect'] + ChapterService.chapterScores[chid]['mcq']['correct'];
		var mcq_attempted = ChapterService.chapterScores[chid]['mcq']['incorrect'] + ChapterService.chapterScores[chid]['mcq']['correct'];

		var tbs_total = ChapterService.chapterScores[chid]['tbs']['skipped'] + ChapterService.chapterScores[chid]['tbs']['incorrect'] + ChapterService.chapterScores[chid]['tbs']['correct'];
		var tbs_attempted = ChapterService.chapterScores[chid]['tbs']['incorrect'] + ChapterService.chapterScores[chid]['tbs']['correct'];

		var total = mcq_total + tbs_total;
		var attempted = mcq_attempted + tbs_attempted;

		var result = (attempted*100)/total;

		if (typeof result == 'number') {
			return result;
		}
		else {
			return 0;
		}
	};
	$scope.scoreBarCorrect = function(chid) {
		if(ChapterService.chapterScores[chid] === undefined) return 0;
		var mcq_answered = ChapterService.chapterScores[chid]['mcq']['incorrect'] + ChapterService.chapterScores[chid]['mcq']['correct'];
		var mcq_correct = ChapterService.chapterScores[chid]['mcq']['correct'];

		var tbs_answered =  ChapterService.chapterScores[chid]['tbs']['incorrect'] + ChapterService.chapterScores[chid]['tbs']['correct'];
		var tbs_correct = ChapterService.chapterScores[chid]['tbs']['correct'];

//		var total = mcq_total + tbs_total;
		var answered = mcq_answered + tbs_answered;
		var correct = mcq_correct + tbs_correct;

		var result = (correct*100)/answered;

		if (typeof result == 'number') {
			return result;
		}
		else {
			return 0;
		}
	};

	// depricating, moving to ChapterService, because that's alwasy needed for this
	// function to be relevant this function needs to be called in various controller scopes
	$scope.getQuestionScore = function(chid, category, type) {
//console.log('????');
//console.log(ChapterService.getQuestionCount(chid, category, type));
		return ChapterService.getQuestionCount(chid, category, type);
	};
});

TestSimModule.service("SectionService", function($rootScope, $http, TopicSections, ChapterService) {
    this.section = false;
    this.gettingChapters = false;
    var self = this;
    this.initTopics = function(callback) {
        console.log('SectionService.initTopics');

        // stop now if we don't yet have a service to load
        if (!this.section) {
            console.debug('SectionService.initTopics: Waiting on section.');
            return false;
        }

    if (this.gettingChapters == false) {
        this.gettingChapters = true;
        var promise = TopicSections.getTopics(this.section);
        if (promise) {
            promise.then(function(value) {
                self.gettingChapters = false;
//                $rootScope.$broadcast("topics", {topics: value, chapter_info: response});


                // ChapterService.topics = value; // for linking in-question
                // var data = { t: [] },
                //     chapterRequest = {
                //         chapters: ChapterService.topics,
                //         student_id: parseInt(JSON.parse($.cookie('data')).studentID)
                //     };

                // angular.forEach(ChapterService.topics, function(chapter) {
                //     angular.forEach(chapter.topics, function(topic) {
                //         data.t.push(topic.id);
                //     });
                // });

                // $http.post(ServiceURLS.chapterInfoUrl, chapterRequest).then(function(response) {
                //     // if (callback) {
                //     //     callback(value, response);
                //     // }
                //     $rootScope.$broadcast("topics", {topics: value, chapter_info: response});
                // });
            });
        }
    }

    $rootScope.$on("topics", function() {
        $rootScope.showTabLoading = false;
        self.gettingChapters = false;
    });
    };
});

TestSimModule.controller("SectionSelectorController", function($rootScope, $scope, SectionService){
    console.info('TestSimModule.controller("SectionSelectorController"');
	$scope.section = SectionService.section;
})

TestSimModule.controller("SectionController", function($rootScope, $scope, $location, $http, $timeout, TopicSections, SectionService, SessionService, ChapterService) {
    console.info('TestSimModule.controller("SectionController"');

	$scope.section = SectionService.section;
	$scope.$on("SectionChange", function(evt, section) {
		$scope.section = section;
	});
	$scope.$on("RequestNewSection", function(evt, section) {
		$scope.pickNewSection(section);
	});
	$scope.pickNewSection = function(new_val) {
		if (window.location.href.indexOf('section=' + new_val.toLowerCase()) == -1) {
            console.log('Same Section');

			if ($location.path() == "/my-scores") {
				window.location = '#/my-scores?section=' + new_val.toLowerCase();
			}
			else if ($location.path() == "/dashboard") {
				window.location = '#/dashboard?section=' + new_val.toLowerCase();
			}
			else if ($location.path() == "/simulator/summary") {
				window.location = "#/simulator/summary?section=" + new_val.toLowerCase();
			}
			else {
				window.location = '#/quiz/setup?section=' + new_val.toLowerCase();
			}
			$rootScope.showTabLoading = true;
//				window.location.reload();
			SectionService.section = new_val.toLowerCase();
			$scope.section = SectionService.section;
			$rootScope.$broadcast("SectionChange", $scope.section);

		}
		else if (window.location.href.indexOf('section=aud') !== -1) {
            console.log('Loading AUD');
			$rootScope.showTabLoading = true;
			$scope.model.pickSection = 'AUD';
		}
		else if (window.location.href.indexOf('section=far') !== -1) {
            console.log('Loading FAR');
			$rootScope.showTabLoading = true;
			$scope.model.pickSection = 'FAR';
		}
		else if (window.location.href.indexOf('section=reg') !== -1) {
            console.log('Loading REG');
			$rootScope.showTabLoading = true;
			$scope.model.pickSection = 'REG';
		}
		else if (window.location.href.indexOf('section=bec') !== -1) {
            console.log('Loading BEC');
			$rootScope.showTabLoading = true;
			$scope.model.pickSection = 'BEC';
		}

        // be sure to update the Sessoin and Section services before running promises
        // promises that will fire depend on the data, the initTopics will need the section
        SessionService.model.pickSection = new_val;
        SectionService.section = new_val; // for quiz session ID

        console.log('Resetting $scope.topics');
        $scope.topics = {};

        // when topics are loaded and updated listen to do stuff
        $scope.$on("topics", function(evt, obj) {
            console.log('$rootScope.$on("topics")', obj.topics);
	    $timeout(function() {
		    $scope.topics = angular.copy(obj.topics);
		    ChapterService.topics = angular.copy(obj.topics);
		    SessionService.totalQuestions = obj.chapter_info.counts;
		    ChapterService.totalQuestions = obj.chapter_info.counts;
		    ChapterService.chapterScores = obj.chapter_info.scores;
			$rootScope.$broadcast("topicsSet");
	    }, 0);
        });

        // this requires SectionService.section to be set
        SectionService.initTopics(function(value, response) {
            $scope.topics = value;
            SessionService.totalQuestions = response.data.counts;
            ChapterService.chapterScores = response.data.scores;
        });

        console.log('Fetching saved quizs for the ' + SectionService.section + ' section');
        SessionService.updateSessionNameByQuizzesCount();
	};

	$scope.$watch('model.pickSection', function(new_val, old_val) {
        console.log('$watch model.pickSection', new_val, old_val);
		if (new_val == undefined || new_val == old_val) {
            return;
        }
		$scope.pickNewSection(new_val);
	});
	$scope.model = SessionService.model;

	if(SectionService.section != null) {
		$scope.section = SectionService.section;
		$scope.sessionName = SessionService.sessionName;
	}

	if (window.location.href.indexOf('section=aud') !== -1) {
        console.log('AUD Section being Picked');
		$scope.pickNewSection('AUD');
		// $scope.model.pickSection = 'AUD';
	}
	else if (window.location.href.indexOf('section=far') !== -1) {
        console.log('FAR Section being Picked');
		$scope.pickNewSection('FAR');
		// $scope.model.pickSection = 'FAR';
	}
	else if (window.location.href.indexOf('section=bec') !== -1) {
        console.log('BEC Section being Picked');
		$scope.pickNewSection('BEC');
		// $scope.model.pickSection = 'BEC';
	}
	else if (window.location.href.indexOf('section=reg') !== -1) {
        console.log('REG Section being Picked');
		$scope.pickNewSection('REG');
		// $scope.model.pickSection = 'REG';
	}

	$scope.getPicksection = function(val) {
		if (val) {
			return (window.location.href.indexOf('section=' + val.toLowerCase()) !== -1);
		}
		else {
		}
	};

});

TestSimModule.service("CreateQuizService", function($rootScope) {
	this.model = {};
	this.tbsmodel = {};
	this.lastStep = 1;
	this.furthestStepReached = 1;
	self = this;
	$rootScope.$on("reset", function() {
		self.lastStep = 1;
		self.furthestStepReached = 1;
	});
});

TestSimModule.controller("StepController", function($rootScope, $scope, $location, $timeout, SessionService, SectionService, ChapterService, QuizletService, CreateQuizService, LoginService) {
//    LoginService.verify();
	if (SessionService.exitedSavedQuiz) {
		$scope.exitedSavedQuiz = true;
	}
    console.info('TestSimModule.controller("StepController"');
	var stepInt = $location.path().replace( /^\D+/g, '');
	if (parseInt(stepInt) > CreateQuizService.furthestStepReached) {
		CreateQuizService.furthestStepReached = parseInt(stepInt);
	}

	$scope.$on("topics", function(evt, obj) {
		$timeout(function() {
			$scope.topics = angular.copy(obj.topics);
			ChapterService.topics = angular.copy(obj.topics);
			$scope.syncChapterIncludedFromPickedTopics();
		}, 0);
	});

    // this function will ensure that when we load the Step1 page, we also take
    // the pickedTopics and update the topics included,
    // it's like we are emulating the first toggle-on click for the topics that
    // got them added to the pickedTopics array in the first place.
    $scope.syncChapterIncludedFromPickedTopics = function() {
        var chapterIDs = [];

        angular.forEach($scope.model.pickedTopics, function(topic) {
            if (jQuery.inArray(topic.chapterID, chapterIDs) === -1) {
                chapterIDs.push(topic.chapterID);
            }
        });

        angular.forEach(chapterIDs, function(chapterID) {
            angular.forEach($scope.topics, function(chapter) {
                if (chapter.id == chapterID) {
                    $scope.setChapterIncluded(chapter, true);
                }
            });
        });

    };

	$scope.getTotalQuestionsLabel = function(chapterId) {
		var count = 0;
		if (SessionService.hasNASBA == false && SectionService.section !== "BEC" && $location.path().indexOf("/quiz/setup") > -1) {
			switch (SessionService.specificCategory) {
				case 'incorrect':
					count = ChapterService.getQuestionCount(chapterId, 'all', 'non_nasba_incorrect');
					count += ' incorrect';
					break;
				case 'skipped':
				case 'unanswered':
					count = ChapterService.getQuestionCount(chapterId, 'all', 'non_nasba_unanswered');
					count += ' unanswered';
					break;
				default:
					count = ChapterService.getQuestionCount(chapterId, 'all', 'total');
					count += ' total questions';
			}
		}
		else {
			switch (SessionService.specificCategory) {
				case 'incorrect':
					count = ChapterService.getQuestionCount(chapterId, 'all', 'incorrect');
					count += ' incorrect';
					break;
				case 'skipped':
				case 'unanswered':
					count = ChapterService.getQuestionCount(chapterId, 'all', 'unanswered');
					count += ' unanswered';
					break;
				default:
					count = ChapterService.getQuestionCount(chapterId, 'all', 'total');
					count += ' total questions';
			}
		}

		return count;
	};

	$scope.toggleTopic = function(topicID, chapterID, maxMultiQ, maxTBSQ, overrideToggle) {
        var obj = false,
            indexOfObj,
            hasTopic = false,
		    len = $scope.model.pickedTopics.length,
            doAdd;

        // check if override is false, if so we need to add, if true we need to remove
		for (indexOfObj = 0; indexOfObj < len; indexOfObj++) {
            // try to find the topic in the pickedTopics
			obj = $scope.model.pickedTopics[indexOfObj];
			if (obj.topicID == topicID && obj.chapterID == chapterID) {
				hasTopic = true;
				break;
			}
		}

        // force or toggle the addition flag
        if (typeof overrideToggle != 'undefined') {
            doAdd = overrideToggle;
        } else {
            doAdd = !hasTopic;
        }

        // decide if we need to add or remove based on if it should be added and if
        // the list has topic already
        if (doAdd && !hasTopic) {
            // add
			$scope.model.pickedTopics.push({ topicID: topicID, chapterID: chapterID, m: maxMultiQ, t: maxTBSQ });
            obj.include = true;

        } else if (!doAdd && hasTopic) {
            // remove object
            $scope.model.pickedTopics.splice(indexOfObj, 1);
            obj.include = false;
        }

        // update the flag to indicate if all chapters are selected after this update
		if (ChapterService.getChapterById(chapterID).topics.length === $scope.model.pickedTopics.length) {
			ChapterService.getChapterById(chapterID).allSelected = true;
		}

		SessionService.model.pickedTopics = $scope.model.pickedTopics;
	};

		$scope.beginQuiz = function() {
			SectionService.section = $scope.model.pickSection;
			$rootScope.showTabLoading = true;
//			if (!SessionService.specificCategory) {
				SessionService.quizStarted = true;
				SessionService.loadQuiz();
/*			}
			else {
				// 3 = last step of the quiz creation process
//				SessionService.filterQuestionsByCategory(SessionService.specificCategory, 3);
				var chIDs = [];
				var types = [];
				var mcqQty = 0;
				var tbsQty = 0;
				angular.forEach(ChapterService.chosenChaptersList, function(chapter) {
					chIDs.push(chapter.id);
				});
				angular.forEach(QuizletService.requestedQuizlets, function(quizlet) {
					if (quizlet.type == 'multiple-choice') {
						mcqQty = quizlet.qty;
						if (types.indexOf('mcq') == -1) {
							types.push('mcq');
						}
					}
					else {
						tbsQty = quizlet.qty;
						if (types.indexOf('tbs') == -1) {
							types.push('tbs');
						}
					}
				});
				if (SessionService.specificCategory == 'skipped') {
					SessionService.specificCategory = 'unanswered';	
				}
				SessionService.specificCategoryQuiz(chIDs, types, SessionService.specificCategory, {'mcq': mcqQty, 'tbs': tbsQty}); 
			}
*/
		};

	$scope.getQuestionsAvailable = function(type, model) {
		var availableAmount = {'mcq': 0, 'tbs': 0};
		if (!SessionService.specificCategory) {
			if (SessionService.hasNASBA == true || SectionService.section == "BEC") {
				angular.forEach(ChapterService.chosenChaptersList, function(ch) {
					availableAmount.mcq += ChapterService.totalQuestions.mcq[ch.id] ? parseInt(ChapterService.totalQuestions.mcq[ch.id]) : 0;
					availableAmount.tbs += ChapterService.totalQuestions.tbs[ch.id] ? parseInt(ChapterService.totalQuestions.tbs[ch.id]) : 0;
				});
			}
			else {
			// if no NASBA 
				angular.forEach(ChapterService.chosenChaptersList, function(ch) {
					availableAmount.mcq += ChapterService.totalQuestions.mcq[ch.id] ? parseInt(ChapterService.totalQuestions.mcq[ch.id]) : 0;
					availableAmount.tbs += ChapterService.totalQuestions.tbs[ch.id] ? parseInt(ChapterService.chapterScores[ch.id]["journal_total_available"].length) : 0;
				});
			}
		}
		else {
			if (SessionService.hasNASBA == true || SectionService.section == "BEC") {
				angular.forEach(ChapterService.chosenChaptersList, function(ch) {
						availableAmount.mcq += ChapterService.chapterScores[ch.id].mcq ? parseInt(ChapterService.chapterScores[ch.id].mcq[SessionService.specificCategory]) : 0;
						availableAmount.tbs += ChapterService.chapterScores[ch.id].tbs ? parseInt(ChapterService.chapterScores[ch.id].tbs[SessionService.specificCategory]) : 0;
				});
			}
			else {
			// if no NASBA
				if (SessionService.specificCategory == 'skipped') {
					SessionService.specificCategory = 'unanswered';
				}
				angular.forEach(ChapterService.chosenChaptersList, function(ch) {
						availableAmount.mcq += ChapterService.chapterScores[ch.id].mcq ? parseInt(ChapterService.chapterScores[ch.id].mcq[SessionService.specificCategory]) : 0;
						availableAmount.tbs += ChapterService.chapterScores[ch.id].tbs ? parseInt(ChapterService.chapterScores[ch.id]["journal_" + SessionService.specificCategory].length) : 0;
				});
			}
		}
		if (availableAmount.mcq > 50) {
			availableAmount.mcq = 50;
		}
		if (availableAmount.tbs > 15) {
			availableAmount.tbs = 15;
		}
		// max values already loaded, just need to discriminate quantities based on type

        // different logic for multiple choice or for topic based simulation
		if (type == 'multiple-choice') {
            // set the display name
			model.typeLongName = "multiple choice";
            // $scope.model.max = $scope.model.maxMCQ;

            // sub-logic if specific category (incorrect || unanswered)
			if (SessionService.specificCategory) {
					model.max = Array.apply(null, Array(availableAmount.mcq)).map(function (x, i) { return i+1; });
			} else {
//                console.log('set max based on number of special prefs (SessionService.specificCategory, SessionService.specialPrefs.mcq)', SessionService.specificCategory, SessionService.specialPrefs.mcq);
				// need an array so that we can loop over it in a selection menu
                // crate an array of max 50 items
				model.max = Array.apply(null, Array(availableAmount.mcq)).map(function (x, i) { return i+1; });
			}
		} else {
            // for BEC section, display these questions differently
			if (SectionService.section == 'BEC') {
				model.typeLongName = "written communication";
			} else {
				model.typeLongName = "task-based simulation";
			}

			// $scope.model.max = $scope.model.maxTBS;

            // sub-logic if specific category (incorrect || unanswered)
				if (SessionService.specificCategory) {
						model.max = Array.apply(null, Array(availableAmount.tbs)).map(function (x, i) { return i+1; });
				} else {
					// need an array so that we can loop over it in a selection menu
					model.max = Array.apply(null, Array(availableAmount.tbs)).map(function (x, i) { return i+1; });
				}
		}

	}

	$scope.$on("recalculateAvailableQuestions", function() {
		$scope.setQuestionQuantities('multiple-choice', $scope.model);
		$scope.setQuestionQuantities('tbs', $scope.tbsmodel);
	});
    //
    // question quantities are determined by type & category (SessionService.specificCategory)
    //
    // examples:
    // type = multiple-choice, category = incorrect
    // type = tbs, category = false (no category, so total)
    // type = multiple-choice, category = unanswered
    //
    // @param type string multiple-choice || tbs
    // @param model object
    // @param model.max array An array representing the max available questions
    // @param SessionService.specificCategory string || bool false
    // @param SessionService.specialPrefs.mcq ???
    // @param SessionService.specialPrefs.tbs ???
    //
    // results
    // update CreateQuizService.model to equal model (if type == mcq)
    // update CreateQuizService.tbsmodel to equal model (if type == tbs)
    //
    // @return model
	$scope.setQuestionQuantities = function(type, model) {
		$scope.getQuestionsAvailable(type, model);
//        console.log('model.max', model.max);
        // here, we'll handle flow if we're in a preconfigured quiz flow
		if (SessionService.viewingPreConfiguredQuiz) {
			if (type == "multiple-choice" ) {
				model.qty = SessionService.model.qty;
			} else {
				model.qty = SessionService.tbsmodel.qty;
			}

			// SessionService.model = angular.copy(model);
		} else {
			// model.qty = 1;
			if (SessionService.specialPrefs) {
				if (SessionService.specialPrefs.mcq == 0) {
					if (type == "multiple-choice") {
						model.qty = 0;
					}
				}
				if (SessionService.specialPrefs.tbs == 0) {
					if (type !== "multiple-choice") {
						model.qty = 0;
					}
				}
			}
		}

        // update the model in the other areas of the app
		if (type == "multiple-choice") {
			CreateQuizService.model = angular.copy(model);
			$scope.model = CreateQuizService.model;
		} else {
			CreateQuizService.tbsmodel = angular.copy(model);
			$scope.tbsmodel = CreateQuizService.tbsmodel;
		}

		// $scope.model.qty = $scope.model.max;

		return model;
	};

	$scope.getTbsmodelQty = function() {
		return $scope.tbsmodel.qty;
	};

	$scope.getTbsmodelMax = function() {
		if ($scope.tbsmodel.qty > $scope.tbsmodel.max.length && $location.path() == "/quiz/setup/step2") {
			$scope.tbsmodel.qty = 0;
		}
		return $scope.tbsmodel.max;
	};

	$scope.pickAllTopics = function() {
		$scope.model.pickedTopics = [];
		$scope.allTopicsPicked = true;
        // Mark chapter and topics as include and add to model.pickedTopics
//	var topics = angular.copy($scope.topics);
	if ($scope.topics.topics) {
		topics = $scope.topics.topics;
	}
	else {
		topics = $scope.topics;
	}
	ChapterService.chosenChapters = topics.length;
        angular.forEach(topics, function(chapter, idx) {
            chapter.include = true;
            angular.forEach(chapter.topics, function(topic, idx) {
                topic.include = true;
                $scope.model.pickedTopics.push({
                    topicID: topic.id,
                    chapterID: topic.chapterID,
                    m: topic.m,
                    t: topic.t
                });
            });
        });
        // Sincronize topics with Step1Controller instance
        $rootScope.$broadcast("Step1Controller::topics", $scope.topics);
	SessionService.model.pickedTopics = $scope.model.pickedTopics;
	SessionService.tbsmodel.pickedTopics = $scope.model.pickedTopics;
	};

	$scope.toggleChapter = function(chapter) {
        // Reset chapter select box if user toggle a chapter
        $rootScope.$broadcast('RESET_CHAPTER_SELECT_BOX');
        if(chapter.include) {
            chapter.include = false;
            $scope.setChapterIncluded(chapter, false);
        } else {
            chapter.include = true;
            $scope.setChapterIncluded(chapter, true);
        }
	};

    // @todo: in the event listener foreach chapters in topics call this to set it
    // the $scope.topics[].include does not retain it's value when 'back' in navigated
    $scope.setChapterIncluded = function(chapter, include) {
        chapter.include = include;

        if (include) {
            ChapterService.chosenChapters++;
        } else {
            ChapterService.chosenChapters--;
        }

		angular.forEach(chapter.topics, function(topic) {
            $scope.toggleTopic(topic.id, topic.chapterID,  topic.m, topic.t, chapter.include);
			topic.include = chapter.include;
		});
    }

	$scope.pickZeroTopics = function() {
		$scope.allTopicsPicked = false;
		$scope.model.pickedTopics = [];
        // Mark chapter and topics as not included
        angular.forEach($scope.topics, function(chapter, idx) {
            chapter.include = false;
            angular.forEach(chapter.topics, function(topic, idx) {
                topic.include = false;
            });
        });
        // Sincronize topics with Step1Controller instance
        $rootScope.$broadcast("Step1Controller::topics", $scope.topics);
		SessionService.model.pickedTopics = $scope.model.pickedTopics;
        SessionService.tbsmodel.pickedTopics = $scope.model.pickedTopics;
	};

});

TestSimModule.controller("Step1Controller", function($rootScope, $scope, $timeout, SessionService, SectionService, ChapterService, QuizletService, CreateQuizService) {
	SessionService.hasNASBA = true;
	console.info('TestSimModule.controller("Step1Controller"');

    // console.info('STEP 1 Debugging');
    // console.log('SessionService.max:', SessionService.max);
    // console.log('SessionService.model:', SessionService.model);
    // console.log('SessionService.tbsmodel:', SessionService.tbsmodel);
    // console.log('ChapterService.chosenChaptersList:', ChapterService.chosenChaptersList);

//	SessionService.chosenChaptersList = {};
//    
    $scope.topicsLoading = false;

	if (typeof ChapterService.chapterScores == "object" ) {
		$timeout(function() {
			$scope.topics = ChapterService.topics;
			$rootScope.showTabLoading = false;
		}, 0);
	}

    $scope.$watch(function() { return SectionService.gettingChapters; }, function(newVal, oldVal) {
        $scope.topicsLoading = newVal;
    });

	$scope.$on("topics", function(evt, chapters) {
		$scope.topics = chapters.topics;
		ChapterService.topics = chapters.topics;
	});

    $scope.$on("Step1Controller::topics", function(evt, topics) {
        $scope.topics = topics;
        ChapterService.topics = topics;
    });

	if (CreateQuizService.furthestStepReached < 2) {
		ChapterService.selectedChapters = [];
		$scope.model.pickedTopics = [];
	}

	angular.forEach($scope.quizlets, function(quizlet) {
		angular.forEach(quizlet.pickedTopics, function(pickedTopic) {
			angular.forEach(chapters, function(chapter) {
				angular.forEach(chapter.topics, function(chTopic) {
					if (chTopic.id == pickedTopic.topicID) {
						if (quizlet.type == "multiple-choice") {
							chTopic.prevIncludeMP = true;
						} else {
							chTopic.prevIncludeTBS = true;
						}
					}
				});
			});
		});
	});

	CreateQuizService.lastStep = 1;

	$scope.allTopicsPicked = false;
	$scope.chaptersSelectBox = 'select';

	SessionService.reinit();

	SessionService.examSimulator = false;
	SessionService.showTimer = false;

	$scope.toggleExpansion = function(chapter) {
		chapter.expanded = !chapter.expanded;
	};
	$scope.areChaptersReady = function() {
		if (typeof ChapterService.chapterScores == "object") {
			return true;
		}
		return false;
	};

	$scope.isSpecificCategory = function(category) {
		return SessionService.specificCategory == category;
	}

	$scope.chaptersSelectBoxChange = function() {
		if ($scope.chaptersSelectBox == 'selectAll') {
			$scope.pickAllTopics();
		}
		if ($scope.chaptersSelectBox == 'selectNone') {
			$scope.pickZeroTopics();
		}

	};

	$scope.toggleSpecificCategory = function(category) {
		if (SessionService.specificCategory == category) {
			SessionService.specificCategory = false;
		}
		else if (SessionService.specificCategory != category) {
			SessionService.specificCategory = category;
		}
	};

	// depending on the specificCategory, we'll show the count of questions
	// if no specificCategory, we show count of all questions
	// if a specificCategory is set we show count of the subset of questions
	// LP ID: 22674527
	$scope.getStep1TotalQuestionsLabel = function(chapterId) {
		var count = 0;

		switch (SessionService.specificCategory) {
			case 'incorrect':
				count = ChapterService.getQuestionCount(chapterId, 'all', 'incorrect');
				count += ' incorrect';
				break;
			case 'skipped':
				count = ChapterService.getQuestionCount(chapterId, 'all', 'skipped');
				count += ' unanswered';
				break;
			default:
				count = ChapterService.getQuestionCount(chapterId, 'all', 'total');
				count += ' total questions';
		}

		return count;
	};
    // Reset chapter select box to initial value
    $scope.$on('RESET_CHAPTER_SELECT_BOX', function() {
        $scope.chaptersSelectBox = 'select';
    });
});

TestSimModule.controller("Step2Controller", function(
    $rootScope, $scope, $modal, $location, $http,
    CreateQuizService, SessionService, SectionService,
    ChapterService, CreateQuizService
    ) {
	if (SessionService.exitedSavedQuiz) {
		$scope.exitedSavedQuiz = false;
		SessionService.exitedSavedQuiz = false;
	}
	console.info('TestSimModule.controller("Step2Controller"');

    // console.info('STEP 2 Debugging');
    // console.log('SessionService.max:', SessionService.max);
    // console.log('SessionService.model:', SessionService.model);
    // console.log('SessionService.tbsmodel:', SessionService.tbsmodel);
    // console.log('ChapterService.chosenChaptersList:', ChapterService.chosenChaptersList);
	$scope.chosenChaptersList = ChapterService.chosenChaptersList;
	SessionService.saveMode = "new";
	$scope.model = angular.copy(SessionService.model);
	$scope.tbsmodel = angular.copy(SessionService.tbsmodel);
	$scope.hasNASBA = SessionService.hasNASBA;
	$scope.step2Ready = false;
	CreateQuizService.lastStep = 2;

    // watch selected quantity, ensure that it's not over the max
	$scope.$watch( function() {
		return $scope.model.qty;
	}, function(newQty) {
        // skip if debugging
		if (! $.cookie("isadmin")) {
			CreateQuizService.model.qty = newQty;
			if(newQty > 50) {
				// setTimeout(function() {
					$scope.model.qty = 50;
					CreateQuizService.model.qty = $scope.model.qty;
				// }, 333);
			} else {
                // console.log('CreateQuizService.model.qty', CreateQuizService.model.qty);
			}
		}
	});

	$scope.$watch( function() {
		return $scope.tbsmodel.qty;
	}, function(newQty) {
		if (! $.cookie("isadmin")) { // skip if debugging
			CreateQuizService.tbsmodel.qty = newQty;
			if(newQty > 15) {
				// setTimeout(function() {
					$scope.tbsmodel.qty = 15;
					CreateQuizService.tbsmodel.qty = $scope.tbsmodel.qty;
				// }, 333);
			} else {
                // console.log('CreateQuizService.tbsmodel.qty', CreateQuizService.tbsmodel.qty);
            }
		}
	});

	if ($scope.hasNASBA == null) {
		$scope.hasNASBA = true;
		SessionService.hasNASBA = true;
//		$.cookie("hasNASBA", $scope.hasNASBA, {expires:180, path:"/"});
	} else {
		$scope.hasNASBA = $scope.hasNASBA.com_rcpa_bool;
	}

    // get the count of available questions based on the available up to the max
    $scope.availableQuestionCountValue = function(available, max) {
//        console.log('mathing stuff: availableQuestionCountValue(available, max)', available, max);
        return (available >= max) ? max : available;
    };

    // @note: Is this needed? Step2 controller should only load on this step right?
	if ($location.path() == "/quiz/setup/step2") {

//        console.log('STEP 2 Setup');
//        console.log('SessionService.model.pickedTopics', SessionService.model.pickedTopics);

        // setup model (multiple choice question)
		if (SessionService.model.pickedTopics) {
			if (SessionService.model.pickedTopics.length > 0) {
				// if we get here, it means the student clicked a number on the Section Overview page
                $scope.model.pickedTopics = SessionService.model.pickedTopics;
			}
		}

		if (!SessionService.viewingPreConfiguredQuiz) {
			if (!$scope.tbsmodel.pickedTopics) {
                console.warn('Setting $scope.tbsmodel by cloning SessionService.model');
				$scope.tbsmodel = angular.copy(SessionService.model);
			}
		}

		$scope.model.type = 'multiple-choice';
		$scope.tbsmodel.type = 'tbs';
		$scope.model = $scope.setQuestionQuantities('multiple-choice', $scope.model);
		$scope.tbsmodel = $scope.setQuestionQuantities('tbs', $scope.tbsmodel);

        // ensure the max is at least the qty selected, but not more than the hard-coded max
        // SessionService.max.mcq needs to be updated to sync with the $scope.model.max.length
        // $scope.model.max.length: this is the max available in the group, it could be less than 50
        // SessionService.max.mcq: this should equal the $scope.model.max.length
//        console.log('loading - SessionService.max.mcq: (SessionService.max.mcq, $scope.model.max.length)', SessionService.max.mcq, $scope.model.max.length);
        SessionService.max.mcq = $scope.model.max.length;

        // ensure the max is at least the qty selected
//        console.log('loading - SessionService.max.mcq: (SessionService.max.mcq, $scope.model.max.length)', SessionService.max.tbs, $scope.tbsmodel.max.length);
        SessionService.max.tbs = $scope.tbsmodel.max.length;

        // @BUGFIX: I believe the bug is here in relation to the max count resetting to 1
		var data = { t:[] }, len;

        if (typeof $scope.model.pickedTopics != 'undefined') {
            len = $scope.model.pickedTopics.length;
            SessionService.model.pickedTopics = $scope.model.pickedTopics || [];
        } else {
            len = 0;
        }

		$scope.chapters = SessionService.topics;

        // should this be pushing id's or should it be pushing objects?
		for (var i = 0; i < len; i++) {
			data.t.push($scope.model.pickedTopics[i].topicID);
		}

        // request the server data to get the available options
		$http.post(ServiceURLS.questionCountUrl, data).then(function(response) {


            // load the max questions available
            $scope.model.maxMCQ = parseInt(response.data[0].mcq);
            $scope.model.maxTBS = parseInt(response.data[1].tbs);
            $scope.tbsmodel.maxMCQ = parseInt(response.data[0].mcq);
            $scope.tbsmodel.maxTBS = parseInt(response.data[1].tbs);

            // trigger the $watch
            $scope.model = $scope.setQuestionQuantities($scope.model.type, $scope.model);
            $scope.tbsmodel = $scope.setQuestionQuantities($scope.tbsmodel.type, $scope.tbsmodel);

            // CreateQuizService.model = angular.copy($scope.model);
            // CreateQuizService.tbsmodel = angular.copy($scope.tbsmodel);

            $scope.step2Ready = true;

            // if (CreateQuizService.lastStep !== 3) {
            //     angular.forEach(ChapterService.chosenChaptersList, function(chapter) {
            //         ChapterService.selectedChapters.push(chapter);
            //     });
            // }

            CreateQuizService.lastStep = 2;
		});
	}
});

TestSimModule.controller("Step3Controller", function($rootScope, $scope, SessionService, SectionService, QuizletService, ChapterService, CreateQuizService) {
		$scope.selectedChapters = [];
		// We need to have an array of chapters instead of an object, because we need to check the length of the list in order to display the text "You have selected X chapters" on step 3.
		angular.forEach(ChapterService.chosenChaptersList, function(chapter) {
			$scope.selectedChapters.push(chapter);
		});
		$scope.sessionName = SessionService.sessionName;

//		SessionService.model = angular.copy(CreateQuizService.model);
//		SessionService.tbsmodel = angular.copy(CreateQuizService.tbsmodel);
		$scope.model = angular.copy(CreateQuizService.model);
		$scope.tbsmodel = angular.copy(CreateQuizService.tbsmodel);

		QuizletService.requestedQuizlets = [];
//		if (CreateQuizService.furthestStepReached !== 3) {
			if (CreateQuizService.model.pickedTopics && CreateQuizService.model.pickedTopics.length > 0) {
//				$scope.tbsmodel = angular.copy(CreateQuizService.tbsmodel);
				if (CreateQuizService.model.qty > 0) {
					QuizletService.addQuizlet(CreateQuizService.model);
				}
				if (CreateQuizService.tbsmodel.qty > 0) {
					QuizletService.addQuizlet(CreateQuizService.tbsmodel);
				}
			}
//		}
		$scope.quizlets = QuizletService.requestedQuizlets;
		if (CreateQuizService.furthestStepReached !== 4) {
			CreateQuizService.furthestStepReached = 3;
		}
		CreateQuizService.lastStep = 3;
/*
		angular.forEach(QuizConfigState.chosenChaptersList, function(chapter) {
			$scope.selectedChapters.push(chapter);
		});
*/
		if (QuizletService.requestedQuizlets) {
			if (QuizletService.requestedQuizlets.length > 0) {
				QuizletService.requestedQuizlets[QuizletService.requestedQuizlets.length-1]['selectedChapters'] = $scope.selectedChapters;
			}
		}
/*
		$http({method:'GET', url: ServiceURLS.getQuizzesCount + "?student_id=" + JSON.parse($.cookie('data')).studentID}).
			success(function(data, status, headers, config) {
				$rootScope.sessionName = $rootScope.section + " Quiz #" + data[0]['count'];
			});
*/
	$scope.toggleScoreAsIGo = function() {
		SessionService.scoreAsIGo = !SessionService.scoreAsIGo;
	};

	$scope.isScoreAsIGo = function(stat) {
		return SessionService.scoreAsIGo == stat;
	};
	$scope.editSessionName = function() {
		$scope.editingSessionName = true;
	};

	$scope.saveSessionName = function() {
		$scope.editingSessionName = false;
		SessionService.sessionName = $scope.sessionName;
	};

	$scope.toggleShowTimer = function() {
		SessionService.showTimer = !SessionService.showTimer;
	};

	$scope.isShowTimer = function(stat) {
		return SessionService.showTimer == stat;
	};

	$scope.todaysDate = (new Date().getMonth()+1) + '/' + new Date().getDate() + '/' + new Date().getFullYear();

	$scope.filterQuestionsByCategory = function(category) {
		if (!SessionService.specialPrefs) {
			var new_ids = {
				'tbs': [],
				'mcq': []
			};
			var max = {
				'tbs': 0,
				'mcq': 0
			};

			var id_obj = [];
			angular.forEach(ChapterService.chosenChaptersList, function(ch, id) {
				new_ids['mcq'] = new_ids['mcq'].concat(ChapterService.chapterScores[id]['mcq'][category]);
				new_ids['tbs'] = new_ids['tbs'].concat(ChapterService.chapterScores[id]['tbs'][category]);
			});
			angular.forEach(QuizletService.requestedQuizlets, function(quizlet) {
				if (quizlet.type == 'tbs') {
					if (new_ids['tbs'].length > 0) {
						max.tbs = quizlet.qty;
						if (max.tbs > 0) {
							if (max.tbs < new_ids.tbs.length) {
								new_ids.tbs.length = max.tbs;
							}
							id_obj.push({qids: new_ids.tbs, type: 'tbs'});
						}
					}
				}
				else {
					max.mcq = quizlet.qty;
					if (new_ids['mcq'].length > 0) {
						if (max.mcq > 0) {
							if (max.mcq < new_ids.mcq.length) {
								new_ids.mcq.length = max.mcq;
							}
							id_obj.push({qids: new_ids.mcq, type: 'mcq'});
						}
					}
				}
			});
			SessionService.specialPrefs = id_obj;
		}
		else {
			SessionService.viewQuestions(SessionService.specialPrefs, null, null, 'multitype');
		}
	};
});

TestSimModule.controller("NASBAController", function($rootScope, $scope, $modal, SessionService) {
    var init = function() {
        $scope.hasNASBA = SessionService.hasNASBA == true ? true : false;
    };

	$scope.toggleHasNASBA = function() {
		$scope.hasNASBA = !$scope.hasNASBA;
		SessionService.hasNASBA = !SessionService.hasNASBA;
		$rootScope.$broadcast("recalculateAvailableQuestions");
/*
		$.cookie('hasNASBA', $scope.hasNASBA, {expires:180, path:"/"});
*/
	};

	$scope.showNASBAPopup = function() {
		var opts = {
		    backdrop: true,
		    keyboard: true,
		    backdropClick: true,
		    templateUrl: 'views/why-nasb-notice.php',
		    controller: 'genericModalController',
  			dialogFade: true,
		    //windowClass: 'modal-calc',
		    resolve: {
		    	hasNASBA:function() {
		    		return $scope.hasNASBA;
		    	}
		    }
		};

        // open the popup window
	    var d = $modal.open(opts).result.then(function(cmd) { });
	}

    init();
});

TestSimModule.controller("ExamSimulatorController", function($rootScope, $scope, SessionService, SectionService, QuizletService) {
	$scope.termsAgreed = false;
	$scope.isPreCanned = null;
	$scope.section = SectionService.section;

	SessionService.examSimulator = true;
	SessionService.showTimer = true;

	$scope.$on("SectionChange", function(evt, section) {
		$scope.section = section;
	});

	$scope.toggleTermsAgreed = function() {
		$scope.termsAgreed = !$scope.termsAgreed;
	};

	$scope.preCanned = function() {
        $rootScope.showTabLoading = true;
        SessionService.updateSessionNameByQuizzesCount();
		var section = SessionService.model.pickSection;
		var models = [];

		$scope.tbsmodel = angular.copy($scope.model);

		$scope.model.type = "multiple-choice";
		$scope.tbsmodel.type = "tbs";

		$scope.model.maxMCQ = 50;
		$scope.model.maxTBS = 15;

		$scope.model = $scope.setQuestionQuantities('multiple-choice', $scope.model);
		$scope.tbsmodel = $scope.setQuestionQuantities('tbs', $scope.tbsmodel);

		$scope.pickAllTopics();
		$scope.model.pickedTopics = angular.copy(SessionService.model.pickedTopics);
		$scope.tbsmodel.pickedTopics = angular.copy($scope.model.pickedTopics);

		if (section == 'BEC') {
			$scope.model.qty = 30;
			models.push(angular.copy($scope.model));
			models.push(angular.copy($scope.model));
			models.push(angular.copy($scope.model));

			$scope.tbsmodel.qty = 3;
			models.push($scope.tbsmodel);


		}
		if (section == 'AUD' || section == 'FAR') {
			$scope.model.qty = 30;
			models.push(angular.copy($scope.model));
			models.push(angular.copy($scope.model));
			models.push(angular.copy($scope.model));

			$scope.tbsmodel.qty = 7;
			models.push($scope.tbsmodel);
		}
		if (section == 'REG') {
			$scope.model.qty = 24;
			models.push(angular.copy($scope.model));
			models.push(angular.copy($scope.model));
			models.push(angular.copy($scope.model));

			$scope.tbsmodel.qty = 6;
			models.push($scope.tbsmodel);
		}

		angular.forEach(models, function(model) {
			QuizletService.addQuizlet(model);
		});

		SessionService.showTimer = true;
		SessionService.scoreAsIGo = false;

		$scope.beginQuiz();

		$rootScope.$broadcast('examSimulatorTimerStart');
/*
			var scope = angular.element("#digital-timer").scope();
			scope.$broadcast("timer-start");
*/
	};

});

TestSimModule.controller("GeneralController", function($scope, $rootScope, $browser, $timeout, $window, $location, $modal, $timeout, $http, TopicSections, /*QuizConfigState, QuizNavigation, QuizSession, */SessionService, QuizletService, QuestionService, CreateQuizService, ChapterService, SectionService) {
    console.info('TestSimModule.controller("GeneralController"');
	$scope.pauseTimer = function() {
		$rootScope.$broadcast("pauseTimer");
	};

	$scope.unpauseTimer = function() {
		$rootScope.$broadcast("unpauseTimer");
	};
	$scope.navState = {};
	$scope.$on('sessionStateUpdate', function(evt) {
		$scope.session_type = SessionService.session_type;
		$scope.navState.showInstructions = SessionService.showInstructions;
		$scope.navState.quizStarted = SessionService.quizStarted;
/*
		if ($scope.examSimulatorTimeRemaining == undefined) {
		       if (SectionService.section == 'AUD' || SectionService.section == 'FAR') {
			       $scope.examSimulatorTimeRemaining = 14400; // 4 hours in seconds
		       }
		       if (SectionService.section == 'BEC' || SectionService.section == 'REG') {
			       $scope.examSimulatorTimeRemaining = 10800; // 3 hours in seconds
		       }
		}
*/
	});

	$scope.getSection = function() {
		return  SectionService.section;
	};

       $scope.$on('timer-stopped', function (event, data) {
		$timeout(function() {
			if (SectionService.section == 'AUD' || SectionService.section == 'FAR') {
				if (event.targetScope.countdownattr == 14400 && event.targetScope.countdown == 0) {
					$location.path('/final-score');
				}
			}
			if (SectionService.section == 'BEC' || SectionService.section == 'REG') {
				if (event.targetScope.countdownattr == 10800 && event.targetScope.countdown == 0) {
					$location.path('/final-score');
				}
			}
		}, 0);
       });

    $rootScope.showTabLoading = true;

	$scope.$on('launchReloadedQuiz', function() {
		$scope.launchReloadedQuiz();
	});

	$scope.launchReloadedQuiz = function() {
			QuizletService.quizlet = 0;
			QuestionService.question = 0;
			SessionService.showTimer = false;
			SessionService.showInstructions = true;
			$location.path(SessionService.quizlets[0].questions[0].path);
	};

	$scope.model = {};
	$scope.editingSessionName = false;
	$scope.tbsmodel = {};
	$scope.session_type = 'quiz';
	$scope.selectedChapters = ChapterService.selectedChapters;
	$scope.model.pickSection = SectionService.section;

	$scope.getCurrentPath = function() {
		return $location.path();
	}

	$scope.$on("SectionChange", function() {
		$scope.model.pickSection = SectionService.section.toUpperCase();
	});

	


/*
	if (SessionService.refresh) {
		SessionService.refresh = false;
		window.location.reload();
	}
	$scope.viewQuestions = function(ch, category, type) {
		if (QuizConfigState.chapterScores[ch][category][type].length > 0) {
			QuizConfigState.viewQuestions(ch, category, type);
		}
	};
	$scope.step2Ready = false;
*/
/*
	$scope.restoreFormState = function() {
		$scope.model = angular.copy(SessionService.model);
		$scope.examSimulator = SessionService.examSimulator;

		$scope.quizlets = QuizletService.requestedQuizlets;
		if($scope.model.pickSection != null) {
		}

	};
*/
			//loop through the course content and check each against all the pickedTopics so
			//it will be .btn-topic-previnclude in the UI
			if($scope.model.pickSection) {
				$scope.topicsPromise = TopicSections.getTopics($scope.model.pickSection);
				$scope.topicsPromise.then(function(chapters) {
					ChapterService.topics = chapters;
					$scope.topics = ChapterService.topics;

				}); //then
			}
		$scope.$on("$routeChangeStart", function(next, current) {
					if (($location.path() == "/quiz/setup" || $location.path() == "/dashboard" || $location.path() == '/my-scores')) {
						if($scope.model.pickSection) {
							$scope.topics = TopicSections.getTopics($scope.model.pickSection);
						}
/*
						if ($scope.topics) {
							$scope.topics.then(function(chapters) {
								ChapterService.topics = chapters;
								$scope.topics = ChapterService.topics;

							}); //then
						}
*/
						$scope.$on("topics", function(evt, chapters) {
							ChapterService.topics = chapters;
							$scope.topics = ChapterService.topics;
						});
						$scope.quizlets = [];
						SessionService.resetQuizSession();

					}
					else {
						if (!SessionService.viewingPreConfiguredQuiz) {
							SessionService.model = angular.copy($scope.model);
							if ($location.path() == '/quiz/setup/step3' || $location.path() == '/quiz/setup/step2') {
								SessionService.tbsmodel = angular.copy($scope.tbsmodel);
							}
						}
					}
		});
	$scope.$on('$viewContentLoaded', $scope.restoreFormState);//window.initMasonLayout);

});

TestSimModule.controller("genericModalController", function($scope, $modalInstance) {
	$scope.close = function() {
		$modalInstance.dismiss('cancel');
	};

	$scope.saveAndClose = function() {
		$modalInstance.close('ok');
	};
});

TestSimModule.directive('onlyDigits', function () {
    return {
      require: 'ngModel',
      restrict: 'A',
      link: function (scope, element, attr, ctrl) {
        function inputValue(val) {
          if (val) {
            var digits = val.replace(/[^0-9]/g, '');

            if (digits !== val) {
              ctrl.$setViewValue(digits);
              ctrl.$render();
            }
            return parseInt(digits,10);
          }
          return undefined;
        }
        ctrl.$parsers.push(inputValue);
      }
    }
});

TestSimModule.filter('nl2br', function() {
    var span = document.createElement('span');
    return function(input) {
        if (!input) return input;
        var lines = input.split('\n');

        for (var i = 0; i < lines.length; i++) {
            span.innerText = lines[i];
            span.textContent = lines[i];  //for Firefox
            lines[i] = span.innerHTML;
        }
        return lines.join('<br />');
    }
});

Object.defineProperty(String.prototype, "com_rcpa_bool", {
    get : function() {
	return (/^(true|1)$/i).test(this);
    }
});

function gup( name )
{
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}
