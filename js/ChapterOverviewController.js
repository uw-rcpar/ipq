TestSimModule.controller("ChapterOverviewController", function($scope, $http, $rootScope, $location, QuizConfigState, QuizSession) {
/*
	$scope.session = QuizSession;
	$scope.sessionName = $rootScope.sessionName;
	$scope.viewingChapter = 0;
	$scope.totalQuestions = QuizConfigState.realTotalQuestions;
	$scope.totalPercent = QuizSession.totalPercent;

	$('.greenpie').pietimer({
		animated: false,
		number: $scope.totalPercent,
		color: '#5cb85c', 
		fill: false,
		showPercentage: false
	});
	$('.redpie').pietimer({
		animated: false,
		number: 100,
		color: '#d9534f', 
		fill: false,
		showPercentage: false
	});

	$scope.viewQuestions = function(type) {
		if ($scope[type + 'Questions'].length > 0) {
			QuizConfigState.reloadQuiz(QuizSession.reloadableSession, type);
		}
	}

	$scope.currentPercent = QuizSession.currentPercent;
	$scope.attemptsAverage = QuizSession.attemptsAverage;
	$scope.timeAverage = QuizSession.timeAverage;
	$scope.completePercent = QuizSession.completePercent;
	$scope.scoreClass = "";
	if($scope.currentPercent > 90) {
		$scope.scoreClass = "label label-success";

	} else if($scope.currentPercent > 74) {
		$scope.scoreClass = "label label-warning";
	} else {
		$scope.scoreClass = "label label-danger";
	}
	$scope.idealMultipleChoiceTime = 90;
	$scope.idealMultipleTBSTime = 15 * 60 * 1000;

	QuizSession.calculateFinalScore();
	$scope.topicsByArray = QuizSession.topicsByArray;
	$scope.totalPercent = QuizSession.totalPercent;
	$scope.scoreClass = QuizSession.scoreClass;
*/
	var ch = QuizConfigState.inspectedChapter;
	$scope.totalPercent = QuizConfigState.chapterScores[ch].score;

	$('.greenpie').pietimer({
		animated: false,
		number: $scope.totalPercent,
		color: '#5cb85c', 
		fill: false,
		showPercentage: false
	});
	$('.redpie').pietimer({
		animated: false,
		number: 100,
		color: '#d9534f', 
		fill: false,
		showPercentage: false
	});
	$scope.viewQuestions = function(category, type) {
		if (QuizConfigState.chapterScores[ch][category][type].length > 0) {
			QuizConfigState.loadQuizByQids(QuizConfigState.chapterScores[ch][category][type], type);
		}
	}
	$scope.tbs = {};
	$scope.mcq = {};
	$scope.tbs.correctQuestions = QuizConfigState.chapterScores[ch].tbs.correct.length;
	$scope.tbs.incorrectQuestions = QuizConfigState.chapterScores[ch].tbs.incorrect.length;
	$scope.tbs.skippedQuestions = QuizConfigState.chapterScores[ch].tbs.skipped.length;
	$scope.tbs.flaggedQuestions = QuizConfigState.chapterScores[ch].tbs.flagged.length;
	$scope.tbs.notedQuestions = QuizConfigState.chapterScores[ch].tbs.noted.length;

	$scope.mcq.correctQuestions = QuizConfigState.chapterScores[ch].mcq.correct.length;
	$scope.mcq.incorrectQuestions = QuizConfigState.chapterScores[ch].mcq.incorrect.length;
	$scope.mcq.skippedQuestions = QuizConfigState.chapterScores[ch].mcq.skipped.length;
	$scope.mcq.flaggedQuestions = QuizConfigState.chapterScores[ch].mcq.flagged.length;
	$scope.mcq.notedQuestions = QuizConfigState.chapterScores[ch].mcq.noted.length;
});
