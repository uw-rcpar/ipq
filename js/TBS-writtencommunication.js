TestSimModule.directive('tbsTextArea', function() {
	return {
		restrict: 'A',
		require: '?ngModel',
		link: function(scope, element, attrs, ngModel) {
			if(!ngModel) return; // do nothing if no ng-model
			// Set basic css properties
			element.css({
				height : '220px',
				overflowY : 'auto'
			});
			// Allow content as editable if not added before
			if(!attrs.contenteditable) {
				element.attr('contenteditable', true);
			}
			// Specify how UI should be updated
			ngModel.$render = function() {
				element.html(ngModel.$viewValue || '');
			};
			// Listen for change events to enable binding
			element.bind('blur keyup change', function() {
				scope.$apply(read);
			});
			// Allow write in data in the model
			function read() {
				var html = element.html(),
					striped = strip(html);
				if(!striped) {
					html = '';
				}
				ngModel.$setViewValue(html);
			}
			// Remove all html tags
			function strip(html) {
			    // Stripping the legal HTML tags
			    html = html.replace(/<[^>]*>/g, '');

			    // Escaping the remaining characters
			    var div = document.createElement('div');
			    div.textContent = html;
			    return div.innerHTML;
			}

			read(); // initialize
		}
	};
});
TestSimModule.controller("TBSWrittenCommunicationController", function($scope, $timeout, $rootScope, $routeParams, $browser, $timeout, $location, $modal, 
											TopicSections, SessionService, QuizletService, QuestionService, CalculatorService) {
	if ($scope.question.type == 'tbs-wc') {
	var parseQuestion = SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question];
	$scope.styleDictionary = {};

	$scope.addStyleDictionary = function(id, style) {
		$scope.styleDictionary[id] = style;
	}
	$scope.gridInstances = {};

	var topics = parseQuestion.topics;

	var newModel = JSON.parse(parseQuestion.question_json);
	var question = decodeURIComponent(newModel.question); 
	question = JSON.parse(question);
	question.quizletID = parseQuestion.quizletID;
	question.type = newModel.type;
	$scope.getPreviousAnswer = function() {
		if (typeof $scope.question.answer_id == 'string') {
			$scope.question.answer_id = JSON.parse($scope.question.answer_id);
		}
		if ($scope.question.answer_id && angular.isArray($scope.question.answer_id) && $scope.question.answer_id[$scope.question.answer_id.length - 1] !== null) {
			return $scope.question.answer_id[$scope.question.answer_id.length - 1].solution;
		}
	}
	$scope.updateScore = function() {
		var a = 0;
		if($scope.question && $scope.question.answer && $scope.question.answer.scores) {
			for(var i = 0; i < $scope.question.answer.scores.length; i++) {
				a += $scope.question.answer.scores[i].value;
			}
		}
		$scope.problemScore = parseFloat(a / 15).toFixed(2);
		$scope.question.answer.score = Math.round(a / 15);
	};

	$scope.question = question;//$scope.quizlets[$scope.navState.quizlet].questions[$scope.navState.question];
	$scope.question.chapter_id = SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].chapter_id;
	$scope.question.status = SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].status;
	$scope.question.notes = parseQuestion.notes;
	$scope.question.id = parseQuestion.id;
	$scope.question.type = "tbs-wc";
	$scope.evalOpen = false;

	if (SessionService.previewMode) {
		$scope.question.answer_id = SessionService.quizlets[QuizletService.quizlet].questions[QuestionService.question].answer_id;
	}

	$scope.$on("TBS-WC-gotoNextQuestion", function() {
		if ($scope.question.answer.solution !== '' && $scope.question.answer.solution !== '<p></p>' && $scope.question.answer.solution !== '<p>Click here and start typing to enter your answer.</p>') {
				var opts = {
				    backdrop: true,
				    keyboard: true,
				    backdropClick: false,
				    templateUrl: 'views/quiz-wc-self-assessment.php',
				    controller: 'TBSWrittenCommSelfEvaluationController',
					dialogFade: true,
				    windowClass: 'modal modal-wc-assess',
				    resolve:{
					solution:function() {
						return $scope.question.question.solution;
					},
					question:function() {
						return $scope.question;
					}
				    }
				  };
			    var d = $modal.open(opts);
				   d.opened.then(function() {
				   	/*
					$timeout(function() {
						$('.modal').draggable();
					}, 0);
					*/
				   });
				d.result.then(function(cmd) {
					$scope.question.answer.time = new Date().getTime() - $scope.startTime;
					$scope.updateScore();
					$scope.question.answer.score = $scope.problemScore;
					if (!SessionService.previewMode) {
						QuestionService.addQuestionAttempt($scope.question);	
					}
					$scope.advanceQuestion();
			    });
		}
		else {
			$scope.question.answer.score = 0;
			$scope.advanceQuestion();
		}
	});
	$timeout(function() {
		// On init
		var answer = SessionService.findSessionAnswer($scope.question.id);
		if(answer == null) {
			$scope.question.answer = {};
		} else {
			// Clear undefined answers
			angular.forEach(answer.answers, function(current, idx) {
				if(angular.isUndefined(current)) {
					answer.answers.splice(idx, 1);
				}
			});
			if (answer.answers.length > 0) {
				$scope.question.answer = answer.answers[answer.answers.length - 1];
			} else {
				$scope.question.answer = {};
			}
			if(answer.notes != null) {
				$scope.question.notes = answer.notes;
			}
		}
		if(angular.isUndefined($scope.question.answer.solution)) {
			$scope.question.answer.solution = '';
		}
		// Set current question answer as answer_id when is undefined
		if(!$scope.question.answer_id && $scope.question.answer && $scope.question.answer.solution) {
			$scope.question.answer_id = [$scope.question.answer];
		}
		// Add question.answer.solution watcher
		$scope.$watch('question.answer.solution', function(newVal, oldVal) {
			if (!$scope.question.answer) {
				$scope.question.answer = {};
			}
			if($scope.question.answer.scores == null) {
				$scope.question.answer.scores = [
					{type:"Organization", value:1},
					{type:"Development", value:1},
					{type:"Expression", value:1}	
				];
			}
			$scope.question.answer.time = new Date().getTime() - $scope.startTime;
			$scope.updateScore();
			$scope.question.answer.score = $scope.problemScore;
/*
			if (!SessionService.previewMode) {
				QuestionService.addQuestionAttempt($scope.question); 
			}
*/
		});
	}, 0);

	$scope.question.notes = $scope.question.notes || [];
	$scope.question.newNoteText = "";
	}
});


TestSimModule.controller("TBSWrittenCommSelfEvaluationController", function($scope, $timeout, $rootScope, $routeParams, 
																			$location, $modalInstance, 
																			TopicSections, SessionService, QuestionService, solution, question) {
	$scope.question = { answer: {scores: null} };
	angular.extend($scope.question, question || {});
	if($scope.question.answer.scores == null) {
		$scope.question.answer.scores = [	{type:"Organization", value:1},
											{type:"Development", value:1},
											{type:"Expression", value:1}	
										];
	}

	$timeout(function() {
		$("#evalOrgainization").wijrating({value: $scope.question.answer.scores[0].value, split:2 /* ,iconWidth:25, iconHeight:25 */, 
				rated:function(e, data) {
					$timeout(function(){
						$scope.question.answer.scores[0].value = data.value;
						$scope.updateScore();
					});
				}});
		$("#evalDevelopment").wijrating({value:  $scope.question.answer.scores[1].value, split:2 /* ,iconWidth:25, iconHeight:25 */,
				rated:function(e, data) {
					$timeout(function(){
						$scope.question.answer.scores[1].value = data.value;
						$scope.updateScore();
					});
				}});
		$("#evalExpression").wijrating({value: $scope.question.answer.scores[2].value, split:2 /* ,iconWidth:25, iconHeight:25 */,
				rated:function(e, data) {
					$timeout(function(){
						$scope.question.answer.scores[2].value = data.value;
						$scope.updateScore();
					});
				}});
	});

	$scope.solution = solution;

	$scope.close = function() {
		$modalInstance.dismiss('cancel');
	};

	$scope.saveAndClose = function() {
		$modalInstance.close();
	};

	$scope.problemScore = 0;

	$scope.updateScore = function() {
		var a = 0;
		for(var i = 0; i < $scope.question.answer.scores.length; i++) {
			a += $scope.question.answer.scores[i].value;
		}
		$scope.problemScore = Math.round((a / 15) * 100);
		$scope.question.answer.score = Math.round(a / 15);
/*
		if (!SessionService.previewMode) {
			QuestionService.addQuestionAttempt($scope.question, 'scoreonly'); 
		}
*/
	};

	$scope.updateScore();
	
});
