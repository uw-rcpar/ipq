TestSimModule.controller("FinalScoreController", function($scope, $http, $rootScope, $location, $filter, SectionService, SessionService, QuizletService, QuestionService, ChapterService, TopicSections, LoginService) {
    	LoginService.verify();

	$rootScope.showTabLoading = true;
	$scope.expanded = {};
	$scope.editingSessionName = false;
	$scope.savingSessionName = false;
	$scope.session = SessionService;
	$scope.section = SectionService.section;
	$scope.sessionName = $scope.session.sessionName;

	TopicSections.getTopics(SectionService.section);

	if (SessionService.examSimulator) {
		$scope.session_type = 'exam';
	}
	else {
		$scope.session_type = 'quiz';
	}

	$rootScope.$on("finishedLoading", function() {
		$rootScope.showTabLoading = false;
	});

	$scope.timestamp = $filter('date')(SessionService.timestamp, 'MM/dd/yyyy h:mm a');
	$scope.sessioninfo = {session_type: $scope.session_type, timestamp: $scope.timestamp, session_name: $scope.sessionName};
//	$scope.chapters = ChapterService.selectedChapters;

	//breakdown by quizlet- by topic-
//	$scope.chapters = QuizConfigState.chosenChapters;
/*
	angular.forEach($scope.session.topics, function() {
		$scope.chapters++;
	});
*/
	$scope.originalQuizlets = SessionService.quizlets;
// work on this later
	$scope.totalQuestions = QuestionService.realTotalQuestions;
	$scope.totalPercent = SessionService.totalPercent;
//marker
	$scope.chapters = []; // this will contain chapter IDs
	$scope.chaptersString = []; // this will contain chapter title numbers, NOT chapter IDs
	angular.forEach($scope.originalQuizlets, function(quizlet) {
		angular.forEach(quizlet.chapters, function(chapter) {
			if ($scope.chapters.indexOf(chapter) == -1) {
				$scope.chapters.push(chapter);
				$scope.chaptersString.push(ChapterService.getChapterById(chapter).chapterNumber.replace(/^\D+/g, ''));
			}
		});
	});
	$scope.chaptersString.sort();
	$scope.chaptersString = $scope.chaptersString.join(', ');
/*
	if (ChapterService.chosenChapters == 0) {
		$scope.chapters = SessionService.chosenChapters.length;
	}
	else {
		$scope.chapters = ChapterService.chosenChapters;
	}
*/
	$scope.chosenChapters = ChapterService.chosenChaptersList;
	$scope.previewQuestions = function() {
			SessionService.previewMode = true;
			$rootScope.$broadcast("reset");
			SessionService.processLoadedQuiz(SessionService, null, null, null, 'review', $scope.sessioninfo);	
/*
			$http.post(ServiceURLS.questionPreviewUrl, {'session_id': String(SessionService.quizID), 'quizlets': $scope.originalQuizlets}).then(function(response) {
console.log(response);
				SessionService.quizlets = response.data['quizlets'];
				SessionService.processLoadedQuiz(SessionService, null, null, null, 'review', $scope.sessioninfo);	
			});
*/
	}
	$scope.scoreBarAttempted = function(chid) {
		var chap = ChapterService.chapterScores[chid];
		if(chap === undefined) return 0;
		var mcq_total = chap['mcq']['skipped'].length + chap['mcq']['incorrect'].length + chap['mcq']['correct'].length;
		var mcq_attempted = chap['mcq']['incorrect'].length + chap['mcq']['correct'].length;

		var tbs_total = chap['tbs']['skipped'].length + chap['tbs']['incorrect'].length + chap['tbs']['correct'].length;
		var tbs_attempted = chap['tbs']['incorrect'].length + chap['tbs']['correct'].length;

		var total = mcq_total + tbs_total;
		var attempted = mcq_attempted + tbs_attempted;
	
		var result = (attempted*100)/total;

		if (typeof result == 'number') {
			return result;
		}
		else {
			return 0;
		}
	};
	$scope.scoreBarCorrect = function(chid) {
		var chap = ChapterService.chapterScores[chid];
		if(chap === undefined) return 0;
		var mcq_total = chap['mcq']['skipped'].length + chap['mcq']['incorrect'].length + chap['mcq']['correct'].length;
		var mcq_correct = chap['mcq']['correct'].length;

		var tbs_total = chap['tbs']['skipped'].length + chap['tbs']['incorrect'].length + chap['tbs']['correct'].length;
		var tbs_correct = chap['tbs']['correct'].length;

		var total = mcq_total + tbs_total;
		var correct = mcq_correct + tbs_correct;
	
		var result = (correct*100)/total;

		if (typeof result == 'number') {
			return result;
		}
		else {
			return 0;
		}
	};
	$scope.getChapterScorePie = function(chapter) {
		var percent;
		if (ChapterService.chapterScores) {
			if (ChapterService.chapterScores[chapter]) {
				percent = parseInt(ChapterService.chapterScores[chapter].score);
			}
			else {
				percent = 0;
			}
			return (percent/100*360).toFixed(0);
		}
	}
	$scope.getChapterScore = function(chapter) {
		var score;
		if (ChapterService.chapterScores) {
			if (ChapterService.chapterScores[chapter]) {
				score = ChapterService.chapterScores[chapter].score;
			}
			else {
				score = 0;
			}
			return score;
		}
	}; 
	$scope.getTotalQuestions = function(chapter) {
		if (SessionService.totalQuestions) {
			if (SessionService.totalQuestions[chapter]) {
				return SessionService.totalQuestions[chapter];
			}
		}
	};
	$scope.getQuestionScore = function(chid, category, type) {
		var chap = ChapterService.chapterScores[chid];	
		if (ChapterService.chapterScores) {
			if (type !== 'total' && type !== 'attempted') {
				if (category !== 'all') {
					return chap[category][type].length;
				}
				else {
					return chap['mcq'][type].length + chap['tbs'][type].length;
				}
			}
			else if (type == 'attempted') {
				if (category !== 'all') {
					return chap[category]['correct'].length + chap[category]['incorrect'].length;
				}
				else {
					return chap['mcq']['correct'].length + chap['mcq']['incorrect'].length + chap['tbs']['correct'].length + chap['tbs']['incorrect'].length;
				}
				
			}
			else if (type == 'total') {	
				if (category !== 'all') {
					return chap[category]['correct'].length + chap[category]['incorrect'] + chap[category]['skipped'].length;
				}
				else {
					return chap['mcq']['correct'].length + chap['mcq']['incorrect'].length + chap['mcq']['skipped'].length + chap['tbs']['correct'].length + chap['tbs']['incorrect'].length + chap['tbs']['skipped'].length;
				}
			}
			
		}
		return 0;
	};
		$http({method:'GET', url: ServiceURLS.topics + "?section=" + SectionService.section + "&t=" + new Date().getTime() }).then(function(response) {
				angular.forEach(response.data.chapters, function(chapter) {
					angular.forEach($scope.chapters, function(chid) {
						if (chid == chapter.id) {
							chapter.include = false;
							chapter.chapterNumber = chapter.chapter.split(': ')[0];
							chapter.chapterTitle = chapter.chapter.split(': ')[1];
							angular.forEach(chapter.topics, function(topic) {
								topic.include = false;
								topic.chapterID = chapter.id;
							});
							$scope.chosenChapters[chid] = chapter;
						}
					});
				});
				chapterRequest = {chapters: $scope.chosenChapters, student_id: parseInt(JSON.parse($.cookie('data')).studentID)};
/*
				angular.forEach($rootScope.topics, function(chapter) {
					angular.forEach(chapter.topics, function(topic) {
						data.t.push(topic.id);
					})
				});
*/
/*
				$http.get(ServiceURLS.chapterInfoUrl, chapterRequest).then(function(response) {
					
					SessionService.totalQuestions = response.data.counts;

					ChapterService.chapterScores = JSON.parse(response.data.scores);

				});
*/
			});
	$scope.editSessionName = function() {
		$scope.editingSessionName = true;
	};

	$scope.saveSessionName = function() {
		$scope.savingSessionName = true;
		$http.put(ServiceURLS.saveQuizUrl, 
			{id: SessionService.quizID,
			session_name: $scope.sessionName
			})
		.success(function(data, status, headers, config) {
			$scope.editingSessionName = false;
			$scope.savingSessionName = false;
			SessionService.sessionName = $scope.sessioninfo.session_name = $scope.sessionName;

		});
	};

	$scope.getPieTotal = function() {
		return ($scope.totalPercent/100*360).toFixed(0);
	}

	$scope.expand = function(type) {
		$scope.expanded[type] = true;
	};

	$scope.isExpanded = function(type) {
		return $scope.expanded[type] || false;
	};
/*
	$('.greenpie').pietimer({
		animated: false,
		number: $scope.totalPercent,
		color: '#5cb85c', 
		fill: false,
		showPercentage: false
	});
	$('.redpie').pietimer({
		animated: false,
		number: 100,
		color: '#d9534f', 
		fill: false,
		showPercentage: false
	});
*/
	$scope.viewQuestions = function(type, category) {
		// type = 'mcq', 'tbs', etc.
		// category = 'correct', 'skipped', etc.
// this is disabled temporarily
/*
// ================= BEGIN NEW CODE =======================================
console.log(SessionService.quizlets);
			if (type == 'mcq') {
				type = 'multiple-choice';
			}
			var newquizlets = [];
			angular.forEach(SessionService.quizlets, function(quizlet) {
				if (type == quizlet.type) {
					var newquizlet = {
						'questions': [],
						'type': type
					};
					angular.forEach(quizlet.questions, function(q) {
						if (category == 'skipped') {
							if (!q.answer) {
								newquizlet.questions.push(q);
							}
						}	
						if (category == 'incorrect') {
							if (q.answer) {
								if (parseInt(q.answer.is_correct) == 0) {
									newquizlet.questions.push(q);
								}
							}
						}
						if (category == 'correct') {
							if (q.answer) {
								if (parseInt(q.answer.is_correct) == 1) {
									newquizlet.questions.push(q);
								}
							}
						}
						if (category == 'noted') {
							if (q.noted.length > 0) {
								newquizlet.questions.push(q);
							}
						}
						if (category == 'flagged') {
							if (q.isFlagged) {
								newquizlet.questions.push(q);
							}
						}
					});
					if (newquizlet.questions.length > 0) {
						newquizlets.push(newquizlet);
					}
				}
				
			});
			if (newquizlets.length > 0) {
				$rootScope.$broadcast("reset");
				SessionService.previewquizlets = {'quizlets': angular.copy(newquizlets)};
				SessionService.processLoadedQuiz(SessionService.previewquizlets, null, null, null, 'preview');	
			}
*/
// ====== END NEW CODE ========= 
/*
// the below is deprecated

			$http.post(ServiceURLS.questionPreviewUrl, {'quizlets': SessionService.reloadableSession, 'category': category, 'type': type, 'session_id': SessionService.quizID}).then(function(response) {
				var newquizlets = [];
				angular.forEach(response.data['quizlets'],function(quizlet) {
					/*
					* client-side ordering. If the questions object is not an array then we can't
					* know for it's not going to be a sparse array. So we just convert the object
					* to an array.
					*/
/*
					if(quizlet.questions.constructor !== Array){
						quizlet.questions = Object.keys(quizlet.questions).map(function (key) {
							return quizlet.questions[key];
							});
					}
					
					newquizlets.push(quizlet);
				});
				SessionService.resetQuizSession();
				SessionService.quizlets = newquizlets;
				SessionService.processLoadedQuiz(SessionService, null, null, null, 'preview');	
			});
*/
	}
	$scope.currentPercent = SessionService.currentPercent;
	$scope.attemptsAverage = SessionService.attemptsAverage;
	$scope.timeAverage = SessionService.timeAverage;
	$scope.completePercent = SessionService.completePercent;
	$scope.scoreClass = "";
	if($scope.currentPercent > 90) {
		$scope.scoreClass = "label label-success";

	} else if($scope.currentPercent > 74) {
		$scope.scoreClass = "label label-warning";
	} else {
		$scope.scoreClass = "label label-danger";
	}
	$scope.idealMultipleChoiceTime = 90;
	$scope.idealMultipleTBSTime = 15 * 60 * 1000;

	SessionService.calculateFinalScore();
	$scope.topicsByArray = SessionService.topicsByArray;
	$scope.totalPercent = SessionService.totalPercent;
	$scope.scoreClass = SessionService.scoreClass;
	
	$scope.correctQuestions = {};
	$scope.incorrectQuestions = {};
	$scope.skippedQuestions = {};
	$scope.flaggedQuestions = {};
	$scope.notedQuestions = {};

	$http({method:'GET', url: ServiceURLS.summaryQuestionScores + "?session_id=" + SessionService.quizID}).
		success(function(data, status, headers, config) {
			$scope.correctQuestions['mcq'] = data['mcq']['correct'];
			$scope.correctQuestions['tbs'] = data['tbs']['correct'];
			$scope.incorrectQuestions['mcq'] = data['mcq']['incorrect'];
			$scope.incorrectQuestions['tbs'] = data['tbs']['incorrect'];
			$scope.skippedQuestions['mcq'] = data['mcq']['skipped'];
			$scope.skippedQuestions['tbs'] = data['tbs']['skipped'];
			$scope.flaggedQuestions['mcq'] = data['mcq']['flagged'];
			$scope.flaggedQuestions['tbs'] = data['tbs']['flagged'];
			$scope.notedQuestions['mcq'] = data['mcq']['notes'];
			$scope.notedQuestions['tbs'] = data['tbs']['notes'];

			$rootScope.$broadcast("finishedLoading");
		});
/*
	$http.post( ServiceURLS.reloadSessionUrl, {questions:QuizSession.incorrect})
		.success(function(data, status, headers, config) {
			$scope.incorrectQuestions['mcq'] = data.questions.multipleChoice;
			$scope.incorrectQuestions['tbs'] = data.questions.tbs;
		});
	$http.post( ServiceURLS.reloadSessionUrl, {questions:QuizSession.skipped})
		.success(function(data, status, headers, config) {
			$scope.skippedQuestions['mcq'] = data.questions.multipleChoice;
			$scope.skippedQuestions['tbs'] = data.questions.tbs;
		});
	$http.post( ServiceURLS.reloadSessionUrl, {questions:QuizSession.flagged})
		.success(function(data, status, headers, config) {
			$scope.flaggedQuestions['mcq'] = data.questions.multipleChoice;
			$scope.flaggedQuestions['tbs'] = data.questions.tbs;
		});
	$http.post( ServiceURLS.reloadSessionUrl, {questions:QuizSession.noted})
		.success(function(data, status, headers, config) {
			$scope.notedQuestions['mcq'] = data.questions.multipleChoice;
			$scope.notedQuestions['tbs'] = data.questions.tbs;
		});
*/

	$scope.saveSession = function() {
		$location.path('/quiz/session/save');
	};

});
