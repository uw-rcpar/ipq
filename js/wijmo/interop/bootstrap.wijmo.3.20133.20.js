/*
 *
 * Wijmo Library 3.20133.20
 * http://wijmo.com/
 *
 * Copyright(c) GrapeCity, Inc.  All rights reserved.
 * 
 * Dual licensed under the MIT or GPL Version 2 licenses.
 * licensing@wijmo.com
 * http://wijmo.com/widgets/license/
 *
 */
/*
* Wijmo Bootstrap Integration library.
*
* Depends:
*  bootstrap.js
*
*/

(function (define) {
    define(["jquery"], function ($) {
        $(document).ready(function () {


            var bootCSS = {
                widget: "ui-widget",
                overlay: "ui-widget-overlay",
                content: "ui-widget-content panel panel-default",
                header: "ui-widget-header btn btn-primary",
                stateDisabled: "ui-state-disabled",
                stateFocus: "ui-state-focus",
                stateActive: "ui-state-active active",
                stateDefault: "ui-state-default btn btn-default",
                stateHighlight: "ui-state-highlight btn-success",
                stateHover: "ui-state-hover",
                stateChecked: "ui-state-checked",
                stateError: "ui-state-error",
                icon: "ui-icon",
                iconCheck: "ui-fa fa-check glyphfa glyphfa fa-ok",
                iconRadioOn: "ui-fa fa-radio-on",
                iconRadioOff: "ui-fa fa-radio-off glyphfa glyphfa fa-record",
                iconClose: "ui-fa fa-close glyphfa glyphfa fa-remove",
                iconArrow4Diag: "ui-fa fa-arrow-4-diag glyphfa glyphfa fa-fullscreen",
                iconNewWin: "ui-fa fa-newwin",
                iconVGripSolid: "ui-fa fa-grip-solid-vertical",
                iconHGripSolid: "ui-fa fa-grip-solid-horizontal",
                iconPlay: "ui-fa fa-play glyphfa glyphfa fa-play",
                iconPause: "ui-fa fa-pause glyphfa glyphfa fa-pause",
                iconStop: "ui-fa fa-stop glyphfa glyphfa fa-stop",
                iconVolumeOn: "ui-fa fa-volume-on glyphfa glyphfa fa-volume-up",
                iconVolumeOff: "ui-fa fa-volume-off glyphfa glyphfa fa-volume-down",
                iconArrowUp: "ui-fa fa-triangle-1-n glyphfa glyphfa fa-chevron-up",
                iconArrowRight: "ui-fa fa-triangle-1-e glyphfa glyphfa fa-chevron-right",
                iconArrowDown: "ui-fa fa-triangle-1-s glyphfa glyphfa fa-chevron-down",
                iconArrowLeft: "ui-fa fa-triangle-1-w glyphfa glyphfa fa-chevron-left",
                iconArrowRightDown: "ui-fa fa-triangle-1-se glyphfa glyphfa fa-chevron-down",
                iconArrowThickDown: "ui-fa fa-arrowthick-1-s glyphfa glyphfa fa-arrow-down",
                iconArrowThickUp: "ui-fa fa-arrowthick-1-n glyphfa glyphfa fa-arrow-up",
                iconCaratUp: "ui-fa fa-carat-1-n glyphfa glyphfa fa-chevron-up",
                iconCaratRight: "ui-fa fa-carat-1-e glyphfa glyphfa fa-chevron-right",
                iconCaratDown: "ui-fa fa-carat-1-s glyphfa glyphfa fa-chevron-down",
                iconCaratLeft: "ui-fa fa-carat-1-w glyphfa glyphfa fa-chevron-left",
                iconClock: "ui-fa fa-clock glyphfa glyphfa fa-time",
                iconPencil: "ui-fa fa-pencil glyphfa glyphfa fa-pencil",
                iconSeekFirst: "ui-fa fa-seek-first glyphfa glyphfa fa-forward",
                iconSeekEnd: "ui-fa fa-seek-end glyphfa glyphfa fa-step-backward",
                iconSeekNext: "ui-fa fa-seek-next glyphfa glyphfa fa-step-forward",
                iconSeekPrev: "ui-fa fa-seek-prev glyphfa glyphfa fa-backward",
                iconCircleArrowN: "ui-fa fa-circle-arrow-n glyphfa glyphfa fa-circle-arrow-up",
                iconCancel: "ui-fa fa-cancel glyphfa glyphfa fa-ban-circle",
                inputSpinnerLeft: "ui-input-spinner-left",
                inputSpinnerRight: "ui-input-spinner-right",
                inputTriggerLeft: "ui-input-trigger-left",
                inputTriggerRight: "ui-input-trigger-right",
                inputSpinnerTriggerLeft: "ui-input-spinner-trigger-left",
                inputSpinnerTriggerRight: "ui-input-spinner-trigger-right",
                lightboxClose: "wijmo-wijlightbox-toolbox-button-close glyphfa glyphfa fa-remove",
                cornerAll: "ui-corner-all",
                cornerLeft: "ui-corner-left",
                cornerRight: "ui-corner-right",
                cornerBottom: "ui-corner-bottom",
                cornerBL: "ui-corner-bl",
                cornerBR: "ui-corner-br",
                cornerTop: "ui-corner-top",
                cornerTL: "ui-corner-tl",
                cornerTR: "ui-corner-tr",
                helperClearFix: "ui-helper-clearfix",
                helperReset: "ui-helper-reset",
                helperHidden: "ui-helper-hidden",
                priorityPrimary: "ui-priority-primary btn active",
                prioritySecondary: "ui-priority-secondary btn",
                button: "ui-button btn",
                buttonText: "ui-button-text",
                buttonTextOnly: "ui-button-text-only",
                tabs: "ui-tabs",
                tabsTop: "ui-tabs-top",
                tabsBottom: "ui-tabs-bottom",
                tabsLeft: "ui-tabs-left",
                tabsRight: "ui-tabs-right",
                tabsLoading: "ui-tabs-loading",
                tabsActive: "ui-tabs-active",
                tabsPanel: "ui-tabs-panel",
                tabsNav: "ui-tabs-nav",
                tabsHide: "ui-tabs-hide",
                tabsCollapsible: "ui-tabs-collapsible",
                //calendar
                datepickerPrev: "ui-datepicker-prev btn btn-primary",
                datepickerNext: "ui-datepicker-next btn btn-primary",
                datepickerHeader: "ui-datepicker-header btn-group",
                //combobox
                //dialog
                wijdialogTitleBarClose: "btn btn-default glyphfa glyphfa fa-remove",
                wijdialogTitleBarMaximize: "btn btn-default glyphfa glyphfa fa-new-window",
                wijdialogTitleBarMinimize: "btn btn-default glyphfa glyphfa fa-minus",
                wijdialogTitleBarPin: "btn btn-default glyphfa glyphfa fa-pushpin",
                wijdialogTitleBarRefresh: "btn btn-default glyphfa glyphfa fa-refresh",
                wijdialogTitleBarRestore: "btn btn-default glyphfa glyphfa fa-chevron-up",
                wijdialogTitleBarToggle: "btn btn-default glyphfa glyphfa fa-chevron-up",
                //inputdate
                comboboxCss: "wijmo-wijcombobox input-group",
                //list
                listItem: "wijmo-wijlist-item",
                listul: "wijmo-wijlist-ul dropdown-menu",
                //input
                wijinputWrapper: "wijmo-wijinput-wrapper form-control",
                //grid
                //slider
                uiSliderHandle: "ui-slider-handle btn btn-default",
                uiSliderRange: "ui-slider-range btn btn-primary",
                //video
                wijvideoIndexSlider: "wijmo-wijvideo-index-slider btn btn-default",
                //formdecorator
                wijtextbox: "wijmo-wijtextbox form-control"
            };

            //Merge Bootstrap CSS into Wijmo Widget Prototypes
            if ($.wijmo.widget) { $.extend($.wijmo.widget.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijaccordion) { $.extend($.wijmo.wijaccordion.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijcalendar) { $.extend($.wijmo.wijcalendar.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijcarousel) { $.extend($.wijmo.wijcarousel.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijcheckbox) { $.extend($.wijmo.wijcheckbox.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijcombobox) { $.extend($.wijmo.wijcombobox.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijdatepager) { $.extend($.wijmo.wijdatepager.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijdialog) { $.extend($.wijmo.wijdialog.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijdropdown) { $.extend($.wijmo.wijdropdown.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijeditor) { $.extend($.wijmo.wijeditor.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijexpander) { $.extend($.wijmo.wijexpander.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijevcal) { $.extend($.wijmo.wijevcal.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijgrid) {
                $.extend($.wijmo.wijgrid.prototype.options.wijCSS, bootCSS, {
                    wijgridTH: "btn btn-primary",
                    wijgridDataRow: "btn"
                });
            }
            if ($.wijmo.wijgallery) { $.extend($.wijmo.wijgallery.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijinputdate) { $.extend($.wijmo.wijinputdate.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijinputmask) { $.extend($.wijmo.wijinputmask.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijinputnumber) { $.extend($.wijmo.wijinputnumber.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijinputtext) { $.extend($.wijmo.wijinputtext.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijtextbox) { $.extend($.wijmo.wijtextbox.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijlightbox) { $.extend($.wijmo.wijlightbox.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijlist) {
                $.extend($.wijmo.wijlist.prototype.options.wijCSS, bootCSS, {
                    listItem: "wijmo-wijlist-item btn btn-default"
                });
            }
            if ($.wijmo.wijmenu) {
                $.extend($.wijmo.wijmenu.prototype.options.wijCSS, {
                    stateDefault: "ui-state-default btn btn-default",
                    iconArrowLeft: "ui-fa fa-triangle-1-w glyphfa glyphfa fa-chevron-left"
                });
            }
            if ($.wijmo.wijmenuitem) {
                $.extend($.wijmo.wijmenuitem.prototype.options.wijCSS, {
                    //wijmenuItem: "wijmo-wijmenu-item btn btn-default",
                    //wijmenuLink: "wijmo-wijmenu-link btn btn-default",
                    wijmenuCss: "wijmo-wijmenu nav navbar-default",
                    wijmenuChild: "wijmo-wijmenu-child panel panel-default",
                    wijmenuList: "wijmo-wijmenu-list nav navbar-nav",
                    //wijmenuParent: "wijmo-wijmenu-parent dropdown",
                    header: "ui-widget-header btn btn-primary",
                    iconArrowDown: "ui-fa fa-triangle-1-s caret",
                    iconArrowRight: "ui-fa fa-triangle-1-e glyphfa glyphfa fa-chevron-right",
                    stateDefault: "ui-state-default btn btn-default"
                });
            }
            if ($.wijmo.wijpager) {
                $.extend($.wijmo.wijpager.prototype.options.wijCSS, {
                    stateDefault: "ui-state-default btn btn-default",
                    stateActive: "ui-state-active btn active",
                    iconSeekEnd: "ui-fa fa-seek-end glyphfa glyphfa fa-forward"
                });
            }
            if ($.wijmo.wijpopup) { $.extend($.wijmo.wijpopup.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijprogressbar) { $.extend($.wijmo.wijprogressbar.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijradio) { $.extend($.wijmo.wijradio.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijrating) { $.extend($.wijmo.wijrating.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijribbon) { $.extend($.wijmo.wijribbon.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijslider) { $.extend($.wijmo.wijslider.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijsplitter) { $.extend($.wijmo.wijsplitter.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijsuperpanel) { $.extend($.wijmo.wijsuperpanel.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijtextbox) { $.extend($.wijmo.wijtextbox.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijtabs) { $.extend($.wijmo.wijtabs.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijtooltip) { $.extend($.wijmo.wijtooltip.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijtree) { $.extend($.wijmo.wijtree.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijtreecheck) { $.extend($.wijmo.wijtreecheck.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijtreenode) { $.extend($.wijmo.wijtreenode.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijupload) { $.extend($.wijmo.wijupload.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijvideo) { $.extend($.wijmo.wijvideo.prototype.options.wijCSS, bootCSS); }
            if ($.wijmo.wijwizard) { $.extend($.wijmo.wijwizard.prototype.options.wijCSS, bootCSS); }

        });
    });
})(typeof define !== "undefined" ? define : function (deps, body) {
    body(jQuery);
});