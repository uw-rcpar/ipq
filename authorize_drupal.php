<?php
ini_set('display_errors',1);
ini_set('display_startup_errors',1);
error_reporting(-1);

include_once('rcpar_common.php'); 
/**
 * Decrypt cookie and extract goodies
 */ 
require_once('DecryptCookie.php');

/**
 * first check for the drupal cookie
 */
if (!isset($_COOKIE['Drupal_visitor_ticket'])) {
    header('location: '.LOGIN_URL);
    exit;
}


$obj = new DecryptCookie($_COOKIE['Drupal_visitor_ticket']);


//get email
$email=$obj->get_email();


//username
$username=$obj->get_username();


//password
//$password=$obj->get_attributes('password');

//drupal uid
$uid=$obj->get_uid();


//last login date
$login_date=$obj->get_lifespan();


//get the login data
//$logged_user = login_user($credentials);

//var_dump($logged_user->user->user_entitlements->products->BEC->bundled_product);
//exit;

//get access from entitlements
$ent=$obj->get_entitlements();

$access=get_entitlement_sections($ent,$username,$uid,$email);

//no access, no IPQ for you
if ($access=='') {
    header('location: https://www.rogercpareview.com/no-ipq-access');
    exit;
}

//connect to db
$con=mysql_conn();

$email = mysqli_real_escape_string($con, $email);
//$result = mysqli_real_escape_string($con, $result);
$student = mysqli_query($con,"SELECT * FROM students WHERE email = '$email'");

$row_cnt = mysqli_num_rows($student);


//if no student, insert
if ($row_cnt == 0) {
    
    //extract first and last name
    $name = $username;
    
    //$lastname = $name[1];
    //if we have a cf_student_id set it, otherwise default to 0
    $cf_student_id = 0;
    $drupal_user_id = $uid;
    
    $qstudent = mysqli_query($con, "INSERT INTO students (firstname, lastname, email, drupal_user_id, cf_student_id, username, password, access) VALUES ('$name','','$email', $drupal_user_id, $cf_student_id, '$name','$password','$access')" );
    $sid = mysqli_insert_id($con);
    
   
    
} else {
    
    
    $row = mysqli_fetch_array($student);
    $sid = $row['id'];
    //update drupal uid and access
    $drupal_user_id=$uid;    
    
    //update if found
    $qstudent = mysqli_query($con, "UPDATE students SET 
                                    email = '$email',
                                    drupal_user_id = $drupal_user_id,
                                    access = '$access' WHERE id = $sid" );
    
    
}

mysqli_close($con);


//format cookie value into json string
$cookie_value = '{"name":"'.$username.'","studentID":"'.$sid.'","uid":"'.$uid.'","login_date":"'.$login_date.'"}';

setcookie("data",$cookie_value,time()+3600*24*30,'/');
//move on, nothing to see here
header("Location: setup.php");

?>