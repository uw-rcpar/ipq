<?php

/**
 * Decrypts and extracts data from the drupal.visitor.ticket cookie created at login
 */

class DecryptCookie {

    private $_encrypted_cookie;
    private $_SSO_KEY;
    private $_SSO_IV;
    private $_SSO_LIFESPAN;

    public function __construct($encrypted_cookie) {

        //set encrypted cookie
        $this -> _encrypted_cookie = $encrypted_cookie;
        //set the decryption values
        $this -> _SSO_KEY = pack('H48', '18DB127CB6A831196632B90E681E55CFAD9DF5554D193A83');
        $this -> _SSO_IV = pack('H16', '757D256B7FA08A98');
        $this -> _SSO_LIFESPAN = time();
    }

    /**
     * return encrypted cookie
     */

    public function get_encrypted_cookie() {
        return $this -> _encrypted_cookie;
    }

    /**
     * return decrypted cookie
     */
    public function get_decrypted_cookie() {

        $fixed = strtr($this -> _encrypted_cookie, '-_,', '+/=');
        $data = base64_decode($fixed);
        $decrypted = $this -> sso_login_decrypt_data($data, $this -> _SSO_KEY, $this -> _SSO_IV);
        return $decrypted;

    }
    
    /**
     * get session name
     */
     Public function get_session_name(){
        $data = explode(',', $this->get_decrypted_cookie());
            $lifespan = explode('=', $data[0]);
            if ($lifespan[0] == "sess_name") {
                return $lifespan[1];
            }
     }
     /**
     * returns the account email
     *
     * @param unknown_type $data
     * @return unknown
     */
    function get_email() {
        $data = explode(',', $this->get_decrypted_cookie());
        $email = explode('=', $data[4]);
        if ($email[0] == "email") {
            return $email[1];
        }
        
    }
    
    /**
     * returns the account username
     *
     * @param unknown_type $data
     * @return unknown
     */
    function get_username() {
        
            $data = explode(',', $this->get_decrypted_cookie());
            $name = explode('=', $data[3]);
            if ($name[0] == "name") {
                return $name[1];
            }
        
    }
    /**
     * returns the set date of the cookie
     *
     * @param unknown_type $data
     * @return unknown
     */
    public function get_lifespan() {
        
            $data = explode(',', $this->get_decrypted_cookie());
            $lifespan = explode('=', $data[1]);
            if ($lifespan[0] == "lifespan") {
                return $lifespan[1];
            }
    }
    /**
     * returns the account uid
     *
     * @param unknown_type $data
     * @return unknown
     */
    public function get_uid() {
        
            $data = explode(',', $this->get_decrypted_cookie());
            $uid = explode('=', $data[2]);
            if ($uid[0] == "uid") {
                return $uid[1];
            }
    }

    /**
     * Decryption function.
     */
    private function sso_login_decrypt_data($iData, $iKey, $iIV) {
        //$iData = base64_decode($iData);
        $theTD = mcrypt_module_open(MCRYPT_3DES, '', MCRYPT_MODE_CBC, '');

        mcrypt_generic_init($theTD, $iKey, $iIV);
        $decData = mdecrypt_generic($theTD, $iData);

        mcrypt_generic_deinit($theTD);
        mcrypt_module_close($theTD);

        // Remove any padding.
        $decData = rtrim($decData, "\0");

        return $decData;
    }


    /**
     * returns an array of roles.
     *
     * @param unknown_type $data
     * @return unknown
     */
    function get_roles() {
        
            $data = explode(',', $this->get_decrypted_cookie());
            $roles = explode('=', $data[5]);
            if ($roles[0] == "roles") {
                $roles = explode('-', $roles[1]);
                return $roles;
            }
        
    }
	/**
	 * Returns and values pass as a string
	 *
	 * @param unknown_type $data
	 * @return unknown
	 */
	public function get_entitlements() {
	  $data = explode(',', $this->get_decrypted_cookie());
	  if (!empty($data)) {	    	
	    $ent = explode('=', $data[5]);
	    if ($ent[0] == "entitlements") {
	      $ent = explode('-|-', $ent[1]);
	      return $ent;
	    }
	  }
	}

    /**
     * Returns and values password as a string
     *
     * @param string $data accepts 'password' or 'cfid'
     * @return password or CFID
     */
    function get_attributes($ret='password') {
        if ($ret=='password' || $ret=='cfid') {
            $data = explode(',', $this->get_decrypted_cookie());

            if (isset($data[6])) {
                $attributes = explode('=', $data[6], 2);
            } else {
                $attributes = explode('=', $data[5], 2);
            }

            if ($attributes[0] == "attributes") {
                $attributes = explode('-', $attributes[1]);
                foreach ($attributes as $key => $value) {
                    $attributes = explode('=', $value);
                    $att[$attributes[0]] = $attributes[1];
                }
                
                $retVal = ($ret=='password') ? $att['pass'] : $att['archiveuid'];
                return $retVal;
            } 
        } return 'use param password or cfid';
    }

}


?>