<div class="modal-header">
	<h3><strong>Are you sure?</strong></h3>
</div>
<div class="modal-body container">
	<div class="">
		<div class="col-sm-1 col-md-1">
		<i class="fa fa fa-exclamation-triangle fa fa-4x" style="margin-top:10px"></i>
		</div>
		<div class="col-sm-5 col-md-5">
		<p class="lead">You are about to delete this session from your score history forever.</p>
		</div>
	</div>
</div>
<div class="modal-footer">
<button class="btn btn-modal btn-default btn-cancel" ng-click="cancel()">Cancel</button>
<button class="btn btn-modal btn-modal-primary btn-delete" ng-click="deleteForever()"><i class="fa fa fa-trash-o"></i> Delete forever</button>
</div>
