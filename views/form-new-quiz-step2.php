<div ng-controller="StepController">

<div ng-controller="Step2Controller">
<div class="row container quiz-creation-step-2 question-quantity">

	<div class="loading-spinner" ng-hide="step2Ready"><i class="fa fa-spin fa-spinner fa-4x"></i></div>
	
<!--		<div class="alert alert-info" style="width:970px;" role="alert">We are currently updating our Interactive Practice Question platform.  If you are experiencing any layout issues or errors, we recommend clearing the cache on your internet browser, restarting your browser, and logging back in to your Interactive Practice Questions.  Thank you for your patience as we make these adjustments</div>
-->

	<div id="quiz-pick-topics-container" class="quiz-setup-step row btn-inverse" ng-show="step2Ready == true">
	<div class="row choose-quiz-type-sticky" scroller-sticky="scroll" ng-class="{'scroller-sticker': scroll > 1}">
		<h3 class="row col-md-12 top-heading pull-left">Question Type

			<div class="pull-right">
				<span class="pull-right">
					  <button ng-click="prevWizardStep()" class="btn btn-arrow gray" rel="1">Back</button>
				      <button class="btn btn-arrow disabled next-button-step-two" ng-click="nextWizardStep()" ng-show="location.path() == '/quiz/setup/step2' && model.pickedTopics.length == 0 || (model.qty > model.max.length || getTbsmodelQty() > getTbsmodelMax().length) || (model.qty == 0 && getTbsmodelQty() == 0) || (model.qty == null || getTbsmodelQty() == null) || (model.qty == 0 && getTbsmodelQty() == null) || (model.qty == null && getTbsmodelQty() == 0)">
					Next
					</button>
				      <button class="btn btn-arrow blue" ng-click="nextWizardStep()" ng-show="location.path() == '/quiz/setup/step2' && model.pickedTopics.length > 0 && (model.qty <= model.max.length || getTbsmodelQty() <= getTbsmodelMax().length) && (model.qty > 0 || getTbsmodelQty() > 0) && !(model.qty == null || getTbsmodelQty() == null)">
					Next
					</button>
				</span>
			</div>
		</h3>
	</div>
	<h4 class="sub-heading row">
		What type of questions do you want to practice?
	</h4>
	<div class="text-left">
		 <div class="row mc-question-amt">
			<p class="lead col-sm-3 text-left question-choice"><span class="question-type-wrapper">Multiple Choice Questions</span></p>
			<p class="lead col-sm-4 number-selector text-left" style="margin-bottom:20px" ng-hide="model.max.length > 0">

                <small>No questions available. Click back to add more chapters if you would like to add questions in this category.</small>
			</p>
			<p class="lead col-sm-1 number-selector text-left" style="margin-bottom:20px" ng-show="model.max.length > 0">
                <!-- ng-show can not be applied to elemetns with selectpicker element, they won't hide -->
                <span>
                    <select selectpicker
                        class="chaptersSelectBox normal-dropdown"
                        ng-model="model.qty"
                        ng-init="model.qty = '0'"
                        >
                        <option ng-value="0">0</option>
                        <option ng-repeat="(key, val) in model.max" ng-value="key+1">{{ key + 1 }}</option>		
                    </select>
                </span>


                <!-- <input ng-class="{error: model.qty > model.max}" ng-model="model.qty" class="form-control text-right" only-digits> -->

			</p>
			<p class="text-left col-md-2" ng-show="model.max.length > 0">
				<span class="text-muted">
					<small>({{availableQuestionCountValue(model.max.length, 50)}} max)</small>
				</span>
			</p>
		</div>

		<div class="row tb-question-amt">
			<p class="lead col-sm-3 text-left question-choice" ng-hide="model.pickSection == 'BEC'"><span class="question-type-wrapper">Task Based Simulations {{ getQuestionScore(chapter.id, 'tbs', 'skipped') }}</span></p>

			<p class="lead col-sm-3 text-left question-choice" ng-show="model.pickSection == 'BEC'"><span class="question-type-wrapper">Written Communication Questions:</span></p>

			<p class="lead col-sm-7 number-selector text-left no-questions-available" style="margin-bottom:20px"  ng-hide="getTbsmodelMax().length > 0">
                No questions available. Click back to add more chapters if you would like to add questions in this category.
			</p>
			<p class="lead col-sm-1 number-selector text-left" style="margin-bottom:20px" ng-show="getTbsmodelMax().length > 0">
                <!-- ng-show can not be applied to elemetns with selectpicker element, they won't hide -->
                <span class="question-number-wrapper">
                    <select selectpicker
                        class="chaptersSelectBox normal-dropdown"
                        ng-model="tbsmodel.qty"
                        ng-init="tbsmodel.qty = '0'"
                        >
                        <option ng-value="0">0</option>
                        <option ng-repeat="(key, val) in tbsmodel.max" ng-value="key+1">{{ key + 1 }}</option>		
                    </select>
                </span>


                <!-- <input ng-class="{error: tbsmodel.qty > tbsmodel.max}" ng-model="tbsmodel.qty" class="form-control text-right" only-digits> -->
			</p>
			<p class="text-left col-md-2" ng-show="getTbsmodelMax().length > 0">
				<span class="text-muted">
					<small>({{availableQuestionCountValue(getTbsmodelMax().length, 15)}} max)</small>
				</span>
			</p>
			<p class="lead nasba-text col-sm-12 text-left" ng-controller="NASBAController" ng-show="section != 'BEC'">
				<i style="cursor:pointer;" class="fa" ng-class="{'fa fa-check-square':hasNASBA == true,'fa fa-square-o':hasNASBA == false}" ng-click="toggleHasNASBA()"></i>
				<span style="cursor:pointer;" ng-click="toggleHasNASBA()" class="checkbox-text"> I am scheduled to sit for the exam and have access to NASBA research materials.</span>
				<span style="font-size:14px;vertical-align:super;">
				<a ng-click="showNASBAPopup()">
				<i class="fa fa fa-question-circle" tooltip="The types of research tasks you will be offered will depend on whether you can access NASBA research materials. Click on the question mark to learn more."></i>
			</a>
			</span>
			</p>
		</div>
	</div>
	</div>
</div><!--.question-quantity-->

<div class="row container quiz-creation-step-2 selected-sections">
	<div class="row text-left col-md-12">
	<h4 class="sub-heading">Selected {{model.pickSection}} Sections:</h4>
	</div>
	<div class="row text-left">
		<div class="pin-container selected-chapters">
			<div class="pin-column pull-left">
				<div ng-repeat="(key, chapter) in chosenChaptersList" class="pin quiz-chapter quiz-chapter-mini col-md-6">
					<h3 class="lead small chapter-border">
						<span class="chapter-info">
							<span class="chapter-number">{{chapter.chapterNumber}}</span>
							<span class="chapter-name" style="display:inline-block;">{{chapter.chapterTitle}}</span>
							<span class="chapter-total" style="">{{ getTotalQuestionsLabel(chapter.id) }}</span>
<!--								{{ getTotalQuestions(chapter.id) }} total questions</span>-->
						</span>
					</h3>
				</div>
			</div>
		</div>
	</div>

<!--
	<div class="row">
			<div class="span2 pull-right">
                  <div class="btn-toolbar">
                    <div class="btn-group">
                      <button class="NextButton pull-right btn btn-success" ng-click="initQuizlet()">Next <i class="fa fa fa-arrow-circle-right"></i></button>
                      </div>
                    </div>
            </div>
	</div>
-->

</div>


</div><!--.quiz-creation-step-2 .selected-sections-->
</div>
</div> <!-- StepController -->
