<div ng-show="showTabLoading" class="row container final-score summary">
	<div class="loading-spinner"><i class="fa fa-spin fa-spinner fa-4x"></i></div>
</div>
<div ng-hide="showTabLoading" class="row container final-score summary">


<div class="row">
	<div>
		  <div class="text-center">
			<div>
				<div class="pull-left col-md-8">
					<!--<div class="session-name"><p>{{sessionName}}</p></div>
-->
					<div ng-show="editingSessionName && savingSessionName">
						Saving...
					</div>
					<div ng-hide="editingSessionName" class="col-sm-12 row">
						<span class="pull-left sub-heading-name">{{sessionName}}</span> 
						<a ng-click="editSessionName()" class="pull-left help-link"><i class="fa fa-pencil"></i>Rename</a>
					</div>
					<div ng-show="editingSessionName && !savingSessionName" class="session-name">
						<div class="alert alert-danger col-md-11" style="font-size:14px;" ng-show="sessionName.length == 0">Session name cannot be left blank. Please enter a name for this session.</div>
						<span class="col-sm-7">
							<input class="row form-control" type="text" ng-model="sessionName"/>
						</span>
						<span class="col-sm-2 no-col-gutter">
						<button class="btn btn-primary pull-left btn-ice btn-save-session-name" ng-click="saveSessionName()" ng-disabled="sessionName.length == 0">Save</button>
						</span>
					</div>

					<h4 class="quizlet-repeat-title text-left row col-sm-12">This Session covers {{ chapters.length }} Sections containing:</h4>
					<div class="quizlet-repeat text-left row col-sm-12">
						<span class="chapter-name clearfix" ng-repeat="(qnum, quizlet) in originalQuizlets">
							<span ng-show="originalQuizlets[qnum].type != 'tbs' && originalQuizlets[qnum].questions.length > 1">
								Quizlet {{qnum+1}}: Contained {{originalQuizlets[qnum].questions.length }} Multiple-Choice Questions
							</span>
							<span ng-show="originalQuizlets[qnum].type != 'tbs' && originalQuizlets[qnum].questions.length == 1">
								Quizlet {{qnum+1}}: Contained {{originalQuizlets[qnum].questions.length }} Multiple-Choice Question
							</span>
							<span ng-show="originalQuizlets[qnum].type == 'tbs' && originalQuizlets[qnum].questions.length > 1">
								<span>Quizlet {{qnum+1}}: Contained {{originalQuizlets[qnum].questions.length }} </span>
								<span ng-hide="model.pickSection == 'BEC'">Task Based Simulations</span>
								<span ng-show="model.pickSection == 'BEC'">Written Communication Questions</span>
							</span>
							<span ng-show="originalQuizlets[qnum].type == 'tbs' && originalQuizlets[qnum].questions.length == 1">
								<span>Quizlet {{qnum+1}}: Contained {{originalQuizlets[qnum].questions.length }} </span>
								<span ng-hide="model.pickSection == 'BEC'">Task Based Simulation</span>
								<span ng-show="model.pickSection == 'BEC'">Written Communication Question</span>
							</span>
						</span>
						<div>Includes: <strong>{{section}}:</strong> {{chaptersString}}</div>
					</div>
<!--
					<div class="NextButton NextButton-small text-left" ng-click="saveSession()">
						Change Session Name
					</div>
-->
				</div>
				
				<button class="btn btn-ice pull-right review-btn" ng-click="previewQuestions()">Review</button>
<!--				<p class="text-gray text-italic lead quiz-saved">Quiz saved</p>
-->
			</div>
		  </div>
<!--
		<div class="finalscore">
			<div class="greenpie"></div>
			<div class="redpie"></div>
	 		<p class="lead text-center totalscore"> 
				<strong class="percentage">{{totalPercent | number:0 }}%</strong>
				Correct
			</p>
		</div>
-->
		
		<div class="question-categories-container">

			<div class="question-categories-row first">
				<div class="text-center final-score-category-title row">
					Multiple-Choice Questions
				</div>
				<br />
				<div class="question-categories row text-center">
					<ul class="list-group col-sm-2 col-sm-offset-1">
						<li class="list-group col-sm-2-item">
							<span ng-click="viewQuestions('mcq', 'skipped')" ng-class="{'link': skippedQuestions.mcq > 0}">
								<span class="sprite-question"></span>
								{{skippedQuestions.mcq}} Unanswered
							</span>
						</li>
					</ul>
					<ul class="list-group col-sm-2">
						<li class="list-group col-sm-2-item">
							<span ng-click="viewQuestions('mcq', 'correct')" ng-class="{'link': correctQuestions.mcq > 0}">
									<span class="sprite-checkmark"></span>{{correctQuestions.mcq}} Correct
							</span>
						</li>
					</ul>
					<ul class="list-group col-sm-2">
						<li class="list-group col-sm-2-item">
							<span ng-click="viewQuestions('mcq', 'incorrect')" ng-class="{'link': incorrectQuestions.mcq > 0}">
								<span class="sprite-x"></span>
								{{incorrectQuestions.mcq}} Incorrect
							</span>
						</li>
					</ul>
					<ul class="list-group col-sm-2">
						<li class="list-group col-sm-2-item">
							<span ng-click="viewQuestions('mcq', 'flagged')" ng-class="{'link': flaggedQuestions.mcq > 0}">
								<span class="sprite-flag"></span>
								{{flaggedQuestions.mcq}} Bookmarked
							</span>
						</li>
					</ul>
					<ul class="list-group col-sm-2">
						<li class="list-group col-sm-2-item">
							<span ng-click="viewQuestions('mcq', 'noted')" ng-class="{'link': notedQuestions.mcq > 0}">
								<span class="sprite-note"></span>
								{{notedQuestions.mcq}} Noted
							</span>
						</li>
					</ul>
				</div>
			</div>
			<br />
			<div class="question-categories-row">
				<div class="text-center final-score-category-title row">
					<span ng-hide="model.pickSection == 'BEC'">Task-Based Simulations</span>
					<span ng-show="model.pickSection == 'BEC'">Written Communication Questions</span>
				</div>
				<br />
				<div class="question-categories row text-center">
					<ul class="list-group col-sm-2 col-sm-offset-1">
						<li class="list-group col-sm-2-item">
							<span ng-click="viewQuestions('tbs', 'skipped')" ng-class="{'link': skippedQuestions.tbs > 0}">
								<span class="sprite-question"></span>
								{{skippedQuestions.tbs}} Unanswered
							</span>
						</li>
					</ul>
					<ul class="list-group col-sm-2">
						<li class="list-group col-sm-2-item">
								<span ng-click="viewQuestions('tbs', 'correct')" ng-class="{'link': correctQuestions.tbs > 0}">
									<span class="sprite-checkmark"></span>{{correctQuestions.tbs}} Correct
								</span>
						</li>
					</ul>
					<ul class="list-group col-sm-2">
						<li class="list-group col-sm-2-item">
							<span ng-click="viewQuestions('tbs', 'incorrect')" ng-class="{'link': incorrectQuestions.tbs > 0}">
								<span class="sprite-x"></span>
								{{incorrectQuestions.tbs}} Incorrect
							</span>
						</li>
					</ul>
					<ul class="list-group col-sm-2">
						<li class="list-group col-sm-2-item">
							<span ng-click="viewQuestions('tbs', 'flagged')" ng-class="{'link': flaggedQuestions.tbs > 0}">
								<span class="sprite-flag"></span>
								{{flaggedQuestions.tbs}} Bookmarked
							</span>
						</li>
					</ul>
					<ul class="list-group col-sm-2">
						<li class="list-group col-sm-2-item">
							<span ng-click="viewQuestions('tbs', 'noted')" ng-class="{'link': notedQuestions.tbs > 0}">
								<span class="sprite-note"></span>
								{{notedQuestions.tbs}} Noted

							</span>
						</li>
					</ul>
				</div>
			</div>
		</div>
 	</div>

</div>
</div><!--.final-score.summary-->

<div ng-hide="showTabLoading" class="row container final-score section-breakdown">
<div class="row">
	<h3 class="section-breakdown sub-heading">Section Breakdown</h3>
		<div class="pin-container" ng-controller="SectionOverviewController">
			<div class="pin-column pull-left">
			<div ng-repeat="chapter in topics" ng-class-even="'pull-right'" ng-class-odd="'pull-left'" class="quiz-chapter col-md-6 col-sm-12 pin">
				<div class="pin-inner-wrapper">
				<h3 class="lead small">
					<div class="chapter-info" data-chapterid="{{chapter.id}}" topics="chapter.topics" ng-click="toggleChapter(chapter)" ng-class="{'active':chapter.include == true}">
						<div class="chapter-info-text">
							<div class="chapter-number">{{chapter.chapterNumber}}</div>
							<div class="chapter-name" style="display:inline-block;">{{chapter.chapterTitle}}</div>
						</div>
					</div>
					<div class="pull-left text-center question-scores" ng-controller="ChapterScoreController">
						<div class="question-score-bar">	
							<div class="incorrect-questions" ng-class="{'double-rounded-corners': getQuestionScore(chapter.id, 'all', 'correct') == 0}" ng-style="{ 'width': scoreBarIncorrect(chapter.id) + '%', 'left': scoreBarCorrect(chapter.id) + '%'}" tooltip="Incorrect Questions" tooltip-enable="true">
								<span class="numbers pull-right" ng-hide="getQuestionScore(chapter.id, 'all', 'incorrect') == 0">{{getQuestionScore(chapter.id, 'all', 'incorrect')}}</span>
							</div>
							<div class="correct-questions" ng-class="{'double-rounded-corners': getQuestionScore(chapter.id, 'all', 'incorrect') == 0}" ng-style="{ 'width': scoreBarCorrect(chapter.id) + '%' }" tooltip="Correct Questions" tooltip-enable="true">
								<span class="numbers pull-right"ng-hide="getQuestionScore(chapter.id, 'all', 'correct') == 0">{{getQuestionScore(chapter.id, 'all', 'correct')}}</span>
							</div>
						</div>

						<div class="questions-attempted">{{ getQuestionScore(chapter.id, 'all', 'correct') + getQuestionScore(chapter.id, 'all', 'incorrect') }} of {{ totalQuestions['combined'][chapter.id] }} Questions Attempted</div>
						<table class="uppercase table table-bordered table-striped question-categories">
							<thead>
								<tr>
									<th class="lbl">Type</th>
									<th><span class="sprite-question" tooltip="Unanswered Questions" tooltip-enable="true"></span></th>
									<th><span class="sprite-checkmark" tooltip="Correct Questions" tooltip-enable="true"></span></th>
									<th><span class="sprite-x" tooltip="Incorrect Questions" tooltip-enable="true"></span></th>
									<th><span class="sprite-flag" tooltip="Bookmarked Questions" tooltip-enable="true"></span></th>
									<th><span class="sprite-note" tooltip="Questions with Notes" tooltip-enable="true"></span></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="td-white lbl">Multiple Choice <div class="total-number-questions">{{getQuestionScore(chapter.id, 'mcq', 'total') | number: 0}} Questions</div></td>
									<td>{{getQuestionScore(chapter.id, 'mcq', 'skipped')}}</td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'mcq', 'correct') == 0}" ng-click="previewQuestions(chapter, 'mcq', 'correct')">{{getQuestionScore(chapter.id, 'mcq', 'correct')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'mcq', 'incorrect') == 0}" ng-click="previewQuestions(chapter, 'mcq', 'incorrect')">{{getQuestionScore(chapter.id, 'mcq', 'incorrect')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'mcq', 'flagged') == 0}" ng-click="previewQuestions(chapter, 'mcq', 'flagged')">{{getQuestionScore(chapter.id, 'mcq', 'flagged')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'mcq', 'noted') == 0}" ng-click="previewQuestions(chapter, 'mcq', 'noted')">{{getQuestionScore(chapter.id, 'mcq', 'noted')}}</a></td>
								</tr>
								<tr>
									<td class="lbl"><div ng-show="model.pickSection != 'BEC'">Task-Based Simulations</div><div ng-show="model.pickSection == 'BEC'">Written Communications</div> <div class="total-number-questions">{{getQuestionScore(chapter.id, 'tbs', 'total') | number: 0}} Questions</div></td>
									<td>{{getQuestionScore(chapter.id, 'tbs', 'skipped')}}</td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'tbs', 'correct') == 0}" ng-click="previewQuestions(chapter, 'tbs', 'correct')">{{getQuestionScore(chapter.id, 'tbs', 'correct')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'tbs', 'incorrect') == 0}" ng-click="previewQuestions(chapter, 'tbs', 'incorrect')">{{getQuestionScore(chapter.id, 'tbs', 'incorrect')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'tbs', 'flagged') == 0}" ng-click="previewQuestions(chapter, 'tbs', 'flagged')">{{getQuestionScore(chapter.id, 'tbs', 'flagged')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'tbs', 'noted') == 0}" ng-click="previewQuestions(chapter, 'tbs', 'noted')">{{getQuestionScore(chapter.id, 'tbs', 'noted')}}</a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</h3>
				</div>
			</div>
			</div>

		</div>
</div>
</div><!--.final-score.section-breakdown-->
