<div ng-controller="StepController">
<div ng-controller="Step3Controller">

<div class="row container quiz-creation-step-3 summary" ng-show="showTabLoading" class="">	
	<div class="loading-spinner"><i class="fa fa-spin fa-spinner fa-4x"></i></div>
</div>

<div class="row container quiz-creation-step-3 summary" ng-show="!showTabLoading">
<!--
		<div class="alert alert-info" style="width:970px;" role="alert">We are currently updating our Interactive Practice Question platform.  If you are experiencing any layout issues or errors, we recommend clearing the cache on your internet browser, restarting your browser, and logging back in to your Interactive Practice Questions.  Thank you for your patience as we make these adjustments</div>
-->
<div class="text-center" ng-animate="{show: 'fadeIn', hide:'fadeOut'}">
	<h3 class="top-heading row text-left">
		Quiz Details
			<div class="pull-right choose-quiz-type-sticky">
	<!--			  <a ng-class="{'scroller-sticker': scroll > 1 && scroll < 178}" scroller-sticky="scroll" ng-click="prevWizardStep()" class="btn btn-ice step3-back" ng-disabled="QuizConfigState.model.pickedTopics.length == 0" rel="2">Back</a>
	-->
				  <button ng-class="{'scroller-sticker': scroll > 1}" scroller-sticky="scroll" ng-click="prevWizardStep()" class="btn btn-arrow gray step3-back" rel="1">Back</button>
				  <button ng-class="{'scroller-sticker': scroll > 1}" scroller-sticky="scroll" class="btn btn-arrow blue" rel="2" ng-click="beginQuiz()" ng-disabled="sessionName.length == 0">Start</button>
			</div>
	</h3>
	<div class="row text-left">
		<div ng-hide="editingSessionName" class="session-name">
			<span class="session-name">{{sessionName}}</span> <a ng-click="editSessionName()" class="help-link"><i class="fa fa-pencil"></i>Rename</a>
		</div>
		<div ng-show="editingSessionName" class="editing-session-name">
			<div class="alert alert-danger" ng-show="sessionName.length == 0">Session name cannot be left blank. Please enter a name for your session.</div>
			<span class="col-md-4">
		 		<input class="form-control" type="text" ng-model="sessionName"/>
			</span>
			<button class="btn btn-primary" ng-click="saveSessionName()" ng-disabled="sessionName.length  == 0">Save</button>
		</div>
		<div class="created-on pull-left">
			<span class="session-type" ng-hide="examSimulator">
				Quiz
			</span>
			<span class="session-type" ng-show="examSimulator">
				Exam Simulator
			</span>
			<span class="created-on-timestamp">
				 | Created on {{ todaysDate }}
			</span>
		</div>
<!--
				<span class="session-type" ng-show="session.session_type == 'quiz'">
					Quiz
				</span>
				<span class="session-type" ng-show="session.session_type == 'exam'">
					Exam Simulator
				</span>
				<span class="created-on-timestamp">
					 | Created on {{ session.timestamp }}
				</span>
			</div>
-->
	</div>
	<p class="lead text-left choose-chapters-title">This Quiz covers {{selectedChapters.length}} sections containing:</p>
	<div class="quizlet-summary pull-left text-left">
		<ul ng-show="quizlets.length > 0">
			<li ng-repeat="quizlet in quizlets" class="quiz-summary-row">
				<span ng-show="quizlet.typeLongName == 'multiple choice'">
					<span>{{quizlet.qty}} Multiple-Choice Question<span ng-show="quizlet.qty > 1">s</span> </span>
				</span>
				<span ng-show="quizlet.typeLongName == 'task-based simulation'">
					<span>{{quizlet.qty}} Task-Based Simulation<span ng-show="quizlet.qty > 1">s</span></span>
				</span>
				<span ng-show="quizlet.typeLongName == 'written communication'">
					<span>{{quizlet.qty}} Written Communication Question<span ng-show="quizlet.qty > 1">s</span></span>
				</span>
<!--
				<button class="pull-right btn inverse btn-xs" ng-click="removeQuizlet($index)" tooltip-placement="top" tooltip="Remove this section from your quiz" tooltip-trigger="mouseenter">
					<i class="fa fa-times fa-white"></i>
				</button>
-->
			</li>
		</ul>
	</div>

	<br /><br />

	<div class="row">
		<div class="alert alert-error col-md-offset-3 col-md-7 alert-warning" ng-show="quizlets.length == 0">
			You do not have any quiz sections set up. <a ng-click="refreshQuiz()">Set some up now.</a>
			<br /><br />
		</div>
	</div>
</div>
</div><!--.summary-->

<div class="row container quiz-creation-step-3 additional-options" ng-show="!showTabLoading">
	<div>
		<div class="row text-left border-line">
			<h4 class="sub-heading">Additional Options:</h4>
		</div>
		<div class="row text-left options-quiz">
			<i class="fa link pull-left" ng-click="toggleShowTimer()" ng-class="{'fa fa-check-square vertical-spacing-2':isShowTimer(true), 'fa fa-square-o vertical-spacing-2':isShowTimer(false)}" style="vertical-align:top;"></i>
			<span class="col-md-10">Show timer</span>
		</div>
		<div class="row text-left options-quiz">
			<i class="fa link pull-left" ng-click="toggleScoreAsIGo()" ng-class="{'fa fa-check-square vertical-spacing-2':isScoreAsIGo(true), 'fa fa-square-o vertical-spacing-2':isScoreAsIGo(false)}" style="vertical-align:top;"></i>
			<span class="col-md-10">Score as you go</span>
		</div>
	</div>
	<br />
</div>

</div><!-- .quiz-creation-step-3.additional-options-->

</div>
</div> <!-- StepController -->
