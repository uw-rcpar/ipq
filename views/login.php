<div class="container row">
	<h3>Session Expired</h3>
	<br />
	<p class="link" ng-hide="isDevSite()"><a href="https://www.rogercpareview.com/user/login">Your session has expired. Please click here to log back in.</a></p> 
	<p class="link" ng-show="isDevSite()"><a href="http://dingo.rogercpareviewdev.com/user/login">Your session has expired. Please click here to log back in.</a></p> 
</div>
