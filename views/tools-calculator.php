<div ng-mousedown="hideContent()" ng-mouseup="showContent()" class="modal-header" popover="Click on the yellow title bar to drag" popover-title="Tip" popover-trigger="focus">
<span class="small" style="padding-left:8px">Calculator - click this title bar to drag window</span>
  <button type="button" class="close" ng-click="close()" style="position:relative;z-index:5000;right:5px;color:#333;opacity:1">×</button>
</div>
<div class="modal-body">
<div class="drag-text">Drag to re-position</div>
<iframe src="/testmodule/js/calculator/index.html" style="border:none;scroll-x:none;scroll-y:none;overflow:hidden;" width="640px" height="347px">

</iframe>
</div>

<div class="modal-footer">

</div>
