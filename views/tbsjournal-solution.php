<div class="modal-header">
    <button type="button" class="close" ng-click="close()">×</button>
    <p class="lead">Solution</p>
    <p>You have answered {{correctAnswers}} out of {{totalAnswers}} correctly</p>
  </div>
  <div class="modal-body" ng-bind-html-unsafe="solution">
 

  </div>

<div class="modal-footer">
	<a class="btn btn-default btn-modal" ng-click="close()">Try Again</a>
	<a class="btn btn-primary btn-modal btn-modal-primary" id="save-option-btn" ng-click="saveAndClose()">Continue</a>
</div>
