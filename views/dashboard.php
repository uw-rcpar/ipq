<?php
//include_once('../rcpar_common.php');

// Get user roles and Section Access
//$roles = get_user_roles();
//$sections = $roles['sections'];

?>

<div ng-controller="SectionOverviewController">

<div class="container container-no-padding row section-overview overall">	
	<div class="section-selection-container"></div>
</div>

<div ng-show="model.pickSection && showTabLoading" class="container row section-overview overall">	
	<div class="loading-spinner"><i class="fa fa-spin fa-spinner fa-4x"></i></div>
</div>

<div ng-hide="model.pickSection && showTabLoading" class="container row section-overview overall">


	
	<div ng-show="!showTabLoading">
	<h1 class="page-title ipq-title">
		{{model.pickSection}} Overview
		<a ng-click="viewDashboardHelp()" class="help-link">How to read this page</a>
		<button ng-click="startNewQuiz()" class="btn btn-orange-big pull-right">Start A New Quiz</button>
	</h1>
	<h2 class="subgray">
		Total {{model.pickSection}} Progress
	</h2>
					<div class="pull-left text-center question-scores" ng-controller="ChapterScoreController">
						<div class="questions-attempted">{{ getQuestionScore('total', 'all', 'correct') + getQuestionScore('total', 'all', 'incorrect') }} of {{ totalQuestions.combined.total }} Questions Attempted</div>
						<div class="question-score-bar long">	
							<div class="incorrect-questions" ng-class="{'double-rounded-corners': getQuestionScore('total', 'all', 'correct') == 0}" ng-style="{ 'width': scoreBarIncorrect('total') + '%', 'left': scoreBarCorrect('total') + '%'}" tooltip="Incorrect Questions" tooltip-enable="true">
								<span class="numbers pull-right"ng-hide="getQuestionScore('total', 'all', 'incorrect') == 0">{{getQuestionScore('total', 'all', 'incorrect')}}</span>
							</div>
							<div class="correct-questions" ng-class="{'double-rounded-corners': getQuestionScore('total', 'all', 'incorrect') == 0}" ng-style="{ 'width': scoreBarCorrect('total') + '%' }" tooltip="Correct Questions" tooltip-enable="true">
								<span class="numbers pull-right" ng-hide="getQuestionScore('total', 'all', 'correct') == 0">{{getQuestionScore('total', 'all', 'correct')}}</span>
							</div>
						</div>
						<div class="question-scores-copy"><span class="right">Green: Questions answered Correctly</span> <span class="wrong pull-right">Orange: Questions answered Incorrectly</span></div>


						<table class="uppercase table table-bordered table-striped question-categories one-colum-table">
							<thead>
								<tr>
									<th class="lbl">Type</th>
									<th><span class="sprite-question" tooltip="Unanswered Questions" tooltip-enable="true"></span></th>
									<th><span class="sprite-checkmark" tooltip="Correct Questions" tooltip-enable="true"></span></th>
									<th><span class="sprite-x" tooltip="Incorrect Questions" tooltip-enable="true"></span></th>
									<th><span class="sprite-flag" tooltip="Bookmarked Questions" tooltip-enable="true"></span></th>
									<th><span class="sprite-note" tooltip="Questions with Notes" tooltip-enable="true"></span></th>
								</tr>
							</thead>
							<tbody>
	<!--
								<tr>
									<td class="td-white lbl">Multiple Choice</td>
									<td><a ng-class="{'not-a-link': getQuestionScore('total', 'mcq', 'skipped') == 0}" ng-click="previewQuestions('total', 'mcq', 'skipped')">{{getQuestionScore('total', 'mcq', 'skipped')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore('total', 'mcq', 'correct') == 0}" ng-click="previewQuestions('total', 'mcq', 'correct')">{{getQuestionScore('total', 'mcq', 'correct')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore('total', 'mcq', 'incorrect') == 0}" ng-click="previewQuestions('total', 'mcq', 'incorrect')">{{getQuestionScore('total', 'mcq', 'incorrect')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore('total', 'mcq', 'flagged') == 0}" ng-click="previewQuestions('total', 'mcq', 'flagged')">{{getQuestionScore('total', 'mcq', 'flagged')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore('total', 'mcq', 'noted') == 0}" ng-click="previewQuestions('total', 'mcq', 'noted')">{{getQuestionScore('total', 'mcq', 'noted')}}</a></td>
								</tr>
								<tr>
									<td class="lbl"><div ng-show="model.pickSection != 'BEC'">Task-Based Simulations</div><div ng-show="model.pickSection == 'BEC'">Written Communications</div></td>
									<td><a ng-class="{'not-a-link': getQuestionScore('total', 'tbs', 'skipped') == 0}" ng-click="previewQuestions('total', 'tbs', 'skipped')">{{getQuestionScore('total', 'tbs', 'skipped')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore('total', 'tbs', 'correct') == 0}" ng-click="previewQuestions('total', 'tbs', 'correct')">{{getQuestionScore('total', 'tbs', 'correct')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore('total', 'tbs', 'incorrect') == 0}" ng-click="previewQuestions('total', 'tbs', 'incorrect')">{{getQuestionScore('total', 'tbs', 'incorrect')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore('total', 'tbs', 'flagged') == 0}" ng-click="previewQuestions('total', 'tbs', 'flagged')">{{getQuestionScore('total', 'tbs', 'flagged')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore('total', 'tbs', 'noted') == 0}" ng-click="previewQuestions('total', 'tbs', 'noted')">{{getQuestionScore('total', 'tbs', 'noted')}}</a></td>
								</tr>
	-->
								<tr>

									<td class="td-white lbl">Multiple Choice <div class="total-number-questions">{{getQuestionScore('total', 'mcq', 'total') | number: 0}} Questions</div></td>
									<td>{{getQuestionScore('total', 'mcq', 'skipped')}}</td>
									<td>{{getQuestionScore('total', 'mcq', 'correct')}}</td>
									<td>{{getQuestionScore('total', 'mcq', 'incorrect')}}</td>
									<td>{{getQuestionScore('total', 'mcq', 'flagged')}}</td>
									<td>{{getQuestionScore('total', 'mcq', 'noted')}}</td>
								</tr>
								<tr>
									<td class="lbl"><div ng-show="model.pickSection != 'BEC'">Task-Based Simulations</div><div ng-show="model.pickSection == 'BEC'">Written Communications</div> <div class="total-number-questions">{{getQuestionScore('total', 'tbs', 'total') | number: 0}} Questions</div></td>
									<td>{{getQuestionScore('total', 'tbs', 'skipped')}}</td>
									<td>{{getQuestionScore('total', 'tbs', 'correct')}}</td>
									<td>{{getQuestionScore('total', 'tbs', 'incorrect')}}</td>
									<td>{{getQuestionScore('total', 'tbs', 'flagged')}}</td>
									<td>{{getQuestionScore('total', 'tbs', 'noted')}}</td>
								</tr>
							</tbody>
						</table>
					</div>
	</div>
</div><!--.overall-->

<div ng-hide="!model.pickSection || showTabLoading" class="row container section-overview chapter-tables">
	<div class="browser-check-alert"></div>
	<h2 class="subgray">
		{{model.pickSection}} Section Progress
	</h2>
	<div class="row" ng-animate="{show: 'fadeIn', hide:'fadeOut'}" id="quiz-pick-topics-container">
		<div class="pin-container">
			<div class="pin-column pull-left">
			<div ng-repeat="chapter in topics" ng-class-even="'pull-right'" ng-class-odd="'pull-left'" class="quiz-chapter col-md-6 col-sm-12 pin">
				<div class="pin-inner-wrapper">
				<h3 class="lead small">
					<div class="chapter-info" data-chapterid="{{chapter.id}}" topics="chapter.topics" ng-click="toggleChapter(chapter)" ng-class="{'active':chapter.include == true}">
						<div class="chapter-info-text">
							<div class="chapter-number">{{chapter.chapterNumber}}</div>
							<div class="chapter-name" style="display:inline-block;">{{chapter.chapterTitle}}</div>
						</div>
					</div>
					<div class="pull-left text-center question-scores" ng-controller="ChapterScoreController">
						<div class="question-score-bar">	
							<div class="incorrect-questions" ng-class="{'double-rounded-corners': getQuestionScore(chapter.id, 'all', 'correct') == 0}" ng-style="{ 'width': scoreBarIncorrect(chapter.id) + '%', 'left': scoreBarCorrect(chapter.id) + '%'}" tooltip="Incorrect Questions" tooltip-enable="true">
								<span class="numbers pull-right" ng-hide="getQuestionScore(chapter.id, 'all', 'incorrect') == 0">{{getQuestionScore(chapter.id, 'all', 'incorrect')}}</span>
							</div>
							<div class="correct-questions" ng-class="{'double-rounded-corners': getQuestionScore(chapter.id, 'all', 'incorrect') == 0}" ng-style="{ 'width': scoreBarCorrect(chapter.id) + '%' }" tooltip="Correct Questions" tooltip-enable="true">
								<span class="numbers pull-right"ng-hide="getQuestionScore(chapter.id, 'all', 'correct') == 0">{{getQuestionScore(chapter.id, 'all', 'correct')}}</span>
							</div>
						</div>

						<div class="questions-attempted">{{ getQuestionScore(chapter.id, 'all', 'correct') + getQuestionScore(chapter.id, 'all', 'incorrect') }} of {{ totalQuestions['combined'][chapter.id] }} Questions Attempted</div>
						<table class="uppercase table table-bordered table-striped question-categories">
							<thead>
								<tr>
									<th class="lbl">Type</th>
									<th><span class="sprite-question" tooltip="Unanswered Questions" tooltip-enable="true"></span></th>
									<th><span class="sprite-checkmark" tooltip="Correct Questions" tooltip-enable="true"></span></th>
									<th><span class="sprite-x" tooltip="Incorrect Questions" tooltip-enable="true"></span></th>
									<th><span class="sprite-flag" tooltip="Bookmarked Questions" tooltip-enable="true"></span></th>
									<th><span class="sprite-note" tooltip="Questions with Notes" tooltip-enable="true"></span></th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td class="td-white lbl">Multiple Choice <div class="total-number-questions">{{getQuestionScore(chapter.id, 'mcq', 'total') | number: 0}} Questions</div></td>
									<td>{{getQuestionScore(chapter.id, 'mcq', 'skipped')}}</td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'mcq', 'correct') == 0}" ng-click="previewQuestions(chapter, 'mcq', 'correct')">{{getQuestionScore(chapter.id, 'mcq', 'correct')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'mcq', 'incorrect') == 0}" ng-click="previewQuestions(chapter, 'mcq', 'incorrect')">{{getQuestionScore(chapter.id, 'mcq', 'incorrect')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'mcq', 'flagged') == 0}" ng-click="previewQuestions(chapter, 'mcq', 'flagged')">{{getQuestionScore(chapter.id, 'mcq', 'flagged')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'mcq', 'noted') == 0}" ng-click="previewQuestions(chapter, 'mcq', 'noted')">{{getQuestionScore(chapter.id, 'mcq', 'noted')}}</a></td>
								</tr>
								<tr>
									<td class="lbl"><div ng-show="model.pickSection != 'BEC'">Task-Based Simulations</div><div ng-show="model.pickSection == 'BEC'">Written Communications</div> <div class="total-number-questions">{{getQuestionScore(chapter.id, 'tbs', 'total') | number: 0}} Questions</div></td>
									<td>{{getQuestionScore(chapter.id, 'tbs', 'skipped')}}</td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'tbs', 'correct') == 0}" ng-click="previewQuestions(chapter, 'tbs', 'correct')">{{getQuestionScore(chapter.id, 'tbs', 'correct')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'tbs', 'incorrect') == 0}" ng-click="previewQuestions(chapter, 'tbs', 'incorrect')">{{getQuestionScore(chapter.id, 'tbs', 'incorrect')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'tbs', 'flagged') == 0}" ng-click="previewQuestions(chapter, 'tbs', 'flagged')">{{getQuestionScore(chapter.id, 'tbs', 'flagged')}}</a></td>
									<td><a ng-class="{'not-a-link': getQuestionScore(chapter.id, 'tbs', 'noted') == 0}" ng-click="previewQuestions(chapter, 'tbs', 'noted')">{{getQuestionScore(chapter.id, 'tbs', 'noted')}}</a></td>
								</tr>
							</tbody>
						</table>
					</div>
				</h3>
				</div>
			</div>
			</div>

		</div>
	</div>
</div> <!--.chapter-tables-->
</div> <!-- SectionOverviewController -->

