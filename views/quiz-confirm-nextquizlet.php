<div ng-controller="SectionOverviewController">
	<div class="modal-header">
	<h3><i class="fa fa-alert"></i>Are you sure you want to exit the Multiple Choice portion of your quiz?</h3>
	</div>
	<div class="modal-body">
		<p ng-show="section != 'BEC'">Clicking on "Yes" will take you to the Task Based Simulation portion of your quiz. You will not be able to go back to the Multiple Choice portion of this quiz until your entire quiz is complete.</p>
		<p ng-show="section == 'BEC'">Clicking on "Yes" will take you to the Written Communication portion of your quiz. You will not be able to go back to the Multiple Choice portion of this quiz until your entire quiz is complete.</p>
		
	</div>
	<div class="modal-footer">
	<button class="btn btn-default btn-cancel btn-modal" ng-click="cancel()">No</button>
	<button class="btn btn-modal btn-modal-primary" ng-click="confirm('ok')">Yes</button>
	</div>
</div>
