<div class="modal-header">
  <button type="button" class="close" ng-click="close()">×</button>
  <p class="lead">Written Communication Self Assessment</p>
  <p><small>Click on the stars to rate your written communication according to the 
  criteria below. Use the master-level written communication below as a guide.</small></p>
</div>
<div class="modal-body">
		
<div class="col-md-5">
	<div class="row">
	<p class="lead pull-left text-muted">Organization</p>
		<div id="evalOrgainization" class="pull-right">
		    <label for="input1a">
		        Below Average</label>
		    <input id="input1a" value="1" name="rating" type="radio">
		    <label for="input2a">
		        Average</label>
		    <input checked="checked" id="input2a" value="2" name="rating" type="radio">
		    <label for="input3a">
		        Above Average</label>
		    <input id="input3a" value="3" name="rating" type="radio">
		    <label for="input4a">
		        Awesome</label>
		    <input id="input4a" value="4" name="rating" type="radio">
		    <label for="input5a">
		        Epic</label>
		    <input id="input5a" value="5" name="rating" type="radio">
		</div>
	</div>
	<div class="row">
		<p class="lead pull-left text-muted">Development</p>

		<div id="evalDevelopment" class="pull-right">
		    <label for="input1a">
		        Below Average</label>
		    <input id="input1a" value="1" name="rating" type="radio">
		    <label for="input2a">
		        Average</label>
		    <input checked="checked" id="input2a" value="2" name="rating" type="radio">
		    <label for="input3a">
		        Above Average</label>
		    <input id="input3a" value="3" name="rating" type="radio">
		    <label for="input4a">
		        Awesome</label>
		    <input id="input4a" value="4" name="rating" type="radio">
		    <label for="input5a">
		        Epic</label>
		    <input id="input5a" value="5" name="rating" type="radio">
		</div>
</div>

<div class="row">
		<p class="lead pull-left text-muted">Expression</p>
		<div id="evalExpression" class="pull-right">
		    <label for="input1a">
		        Below Average</label>
		    <input id="input1a" value="1" name="rating" type="radio">
		    <label for="input2a">
		        Average</label>
		    <input checked="checked" id="input2a" value="2" name="rating" type="radio">
		    <label for="input3a">
		        Above Average</label>
		    <input id="input3a" value="3" name="rating" type="radio">
		    <label for="input4a">
		        Awesome</label>
		    <input id="input4a" value="4" name="rating" type="radio">
		    <label for="input5a">
		        Epic</label>
		    <input id="input5a" value="5" name="rating" type="radio">
		</div>
	</div>  
</div> 

<div class="score col-md-7">
	<h2>Your score</h2>
	<h1>{{problemScore}}%</h1>
	<p>Your grade for this written communication simulation is an average of the 3 scores to the left</p>
</div>

<div class="row">
	<p class="col-md-12 lead" style="clear:both;float:none;">Master Level Written Communication Example</p>
	<div class="master-solution col-md-12" ng-bind-html="solution">

	</div>

</div>


</div>

<div class="modal-footer">
  	<a class="btn btn-default btn-modal" ng-click="close()">Close</a>
	<a class="btn btn-primary btn-modal btn-modal-primary" id="save-option-btn" ng-click="saveAndClose()">Save Answer</a>
	
</div>
