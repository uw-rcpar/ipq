<div class="modal-header">
  <button type="button" class="close" ng-click="close()">×</button>
  <p class="lead">Send Feedback</p>
</div>
<div class="modal-body">
	<div ng-show="issueReported == false &amp;&amp; isSubmitting == false">
		<input class="form-control" value="{{question.id}}" type="hidden">
		<input class="form-control" value="{{browser}}" type="hidden">
		<div class="text-center" ng-hide="issueHasReason">
			<div class="form-group">
				Please select your feedback category:
			</div> 
			<div class="form-group">
				<button ng-click="reason('content')" class="btn btn-primary">I found a typo or problem with the question text.</button>
			</div>
			<div class="form-group">
				<button ng-click="reason('tech')" class="btn btn-primary">I found a problem with the quiz website functionality.</button>
			</div>
		</div>
			
		<div ng-show="issueHasReason">
			<p class="text-muted" ng-show="alerts.length == 0">Please describe the problem you're having below and our team will address it ASAP. Use this form to report problems with the quiz functionality. Issues relating to the question or solutions should be asked in the <a href="https://www.rogercpareview.com/forum/index.cfm" target="_blank">homework help center</a>. Thanks!</p>

			
			<p ng-repeat="alert in alerts" class="alert {{alert.type}} alert-warning">{{alert.msg}}</p> 

			<textarea name="description" ng-model="description.text" id="question_problem_description" style="width:515px;height:100px;font-family:Arial,sans-serif;color:#333"></textarea>
		</div>
	</div>

	<div ng-show="isSubmitting == true">
	<p class="text-center"><i class="fa fa fa-gear fa fa-spin fa fa-4x"></i></p>
	</div>

	<div ng-show="issueReported == true">
	<p class="lead">Thank you!</p>
	<p>Your problem has been submitted to our team. Thanks for helping us improve our quiz software!</p>
	</div>
</div>
<div class="modal-footer">
	<div ng-show="issueHasReason">
		<button class="btn btn-default" ng-click="reason(false)" ng-show="issueReported == false && isSubmitting == false">Back</button>
		<button class="btn btn-default" ng-click="submitIssue()" ng-show="issueReported == false && isSubmitting == false">Send Report</button>
		<button class="btn btn-default" ng-click="close()" ng-show="issueReported == true">Close Window</button>
	</div>
</div>
