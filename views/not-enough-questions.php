<div class="modal-header">
<h3><i class="fa fa-alert"></i>There are not enough questions available for the chapters you selected</h3>
</div>
<div class="modal-body">
	<p>Please select some chapters that have more questions available.</p>	
</div>
<div class="modal-footer">
<button class="btn btn-modal btn-modal-primary" ng-click="close('ok')">OK</button>
</div>
