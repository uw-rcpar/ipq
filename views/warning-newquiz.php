<div class="modal-body">
<p class="lead">You are about to abandon your current quiz session.</p>
<p>Unless you save your session first, your scores will be lost.</p>
</div>

<div class="modal-footer save-quiz-modal">
 	<button class="btn btn-default btn-modal" ng-click="save()">Save my scores first</button>
 	<button class="btn btn-default btn-modal btn-modal-primary" ng-click="startOver()">I just want to start over, start a new quiz</button>
</div>
