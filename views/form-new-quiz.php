
<?php
include_once('../rcpar_common.php');

// Get user roles and Section Access
$roles = get_user_roles();
$sections = $roles['sections'];

?>
<div ng-controller="StepController">
<div ng-controller="Step1Controller">

<div ng-show="showTabLoading && model.pickSection" class="row container quiz-creation-step-1">
	<div class="loading-spinner"><i class="fa fa-spin fa-spinner fa-4x"></i></div>
</div>

<div ng-hide="showTabLoading && model.pickSection" class="row container quiz-creation-step-1">


<div class="section-selection-container" ng-hide="model.pickSection"></div>
<div class="browser-check-alert"></div>
<div class="row col-md-12" ng-animate="{show: 'fadeIn', hide:'fadeOut'}" id="quiz-pick-topics-container" ng-show="model.pickSection">
	<h3 class="top-heading choose-chapters-sticky" scroller-sticky="scroll" ng-class="{'scroller-sticker': scroll > 1}">{{model.pickSection}}
		<div class="pull-right">
			<span class="pull-right">
                <button class="btn btn-arrow disabled pull-right"
                    ng-click="nextWizardStep()"
                    ng-show="location.path() != '/quiz/setup/step3' && model.pickedTopics.length == 0">
                    Next
				</button>
                <button class="btn btn-arrow blue pull-right"
                    ng-click="nextWizardStep()"
                    ng-show="location.path() != '/quiz/setup/step3' && model.pickedTopics.length != 0">
                    Next
				</button>
			</span>
		</div>
	</h3>
	<h4 class="sub-heading">Select {{model.pickSection}} Sections</h4>
	<div class="alert alert-success" ng-show="exitedSavedQuiz">You were in the middle of a quiz or exam simulation before you refreshed. You can return to your saved session by going to Score History and clicking "Review" or "Resume".</div>
	<p class="blurb">Create practice quizzes that focus on a specific section or sections.  This is best to complete after reviewing the corresponding textbook sections and lectures, or if there are particular areas you need to focus on.</p>
	<h3 class="lead small">Question Filters</h3>
	<p class="blurb filters-blurb">Without filters, questions are selected randomly and can include answered, unanswered, correct and incorrect questions.</p>

	<div class="row col-md-12 vertical-spacing">
		<i class="fa link pull-left show-only-checkbox"
			ng-click="toggleSpecificCategory('unanswered')"
			ng-class="{'fa fa-check-square fa-lg vertical-spacing-2':isSpecificCategory('unanswered'), 'fa fa-square-o fa-lg vertical-spacing-2':!isSpecificCategory('unanswered')}"
			style="vertical-align:top;"></i>
		<span>Show only questions you haven't answered before?</span>
	</div>
	<div class="row col-md-12">
		<i class="fa link pull-left show-only-checkbox"
			ng-click="toggleSpecificCategory('incorrect')"
			ng-class="{'fa fa-check-square fa-lg vertical-spacing-2':isSpecificCategory('incorrect'), 'fa fa-square-o fa-lg vertical-spacing-2':!isSpecificCategory('incorrect')}"
			style="vertical-align:top;"></i>
		<span>Show only questions you've answered incorrectly?</span>
	</div>
    <div class="row col-md-12">

        <select selectpicker
            class="chaptersSelectBox"
            ng-model="chaptersSelectBox"
            ng-change="chaptersSelectBoxChange()"
            ng-init="chaptersSelectBox='select'">

			<option value="select">Select</option>
			<option value="selectAll">All</option>
			<option value="selectNone">None</option>
		</select>
	</div>
	<div class="pull-left pin-container row col-md-12">
        <div ng-show="topicsLoading">
            Loading {{model.pickSection}} chapters... <i class="fa fa-spin fa-spinner fa-1x" style="color: #29abe2;"></i>
        </div>
		<div class="pin-column pull-left" ng-hide="topicsLoading">
    		<div ng-repeat="chapter in topics" class="quiz-chapter quiz-chapter-mini pin col-sm-6">
    			<h3 class="lead small">
    				<!-- <i class="fa" ng-click="toggleExpansion(chapter)" ng-class="{'fa-plus': !chapter.expanded, 'fa-minus': chapter.expanded}" style="vertical-align:top;"></i> -->
                    <a class="pull-left chapter-info"
                        data-chapterid="{{chapter.id}}"
                        topics="chapter.topics"
                        ng-click="toggleChapter(chapter)"
                        ng-class="{ 'active': chapter.include == true }">

                        <span class="chapter-number col-md-13">{{chapter.chapterNumber}}</span>
                        <i class="fa pull-left"
                            ng-class="{ 'checked-square': chapter.include == true, 'fa fa-square-o checkbox-size-color': chapter.include == false }"
                            style="vertical-align:top;"></i>

    					<span class="chapter-name" style="display:inline-block;">{{ chapter.chapterTitle }}</span>
    					<span class="chapter-total col-md-12">{{ getTotalQuestionsLabel(chapter.id) }}</span>
    				</a>
    			</h3>
    			<div class="btn-group btn-group-vertical btn-block" ng-show="chapter.expanded" id="chapter_{{chapter.id}}">
    				<div ng-repeat="topic in chapter.topics">
    					<button class="btn inverse btn-block quiz-topic btn-check btn-default" ng-class="{'btn-topic-previnclude':topic.prevIncludeMP == true || topic.prevIncludeTBS == true}" data-topic="{{topic.id}}" btn-checkbox="" ng-click="toggleTopic(topic.id, topic.chapterID, topic.m, topic.t)" ng-model="topic.include">
    					<i class="fa" ng-class="{'checked-square':topic.include,  'fa fa-square-o':!topic.include}" style="vertical-align:top;"></i>
    					<i ng-show="topic.prevIncludeMP == true" class="fa fa fa-check-circle" popover="You have included this topic in a previous multiple choice quiz section." popover-trigger="mouseenter" popover-append-to-body="true"></i>
    					<i ng-show="topic.prevIncludeTBS == true" class="fa fa fa-check-circle-o" popover="You have included this topic in a previous task-based simulation quiz section." popover-trigger="mouseenter" popover-append-to-body="true"></i>{{topic.prevInclude}} {{topic.topic}}</button>
    				</div>
    			</div>
    		</div>
		</div>
	</div>
</div>


</div>
<!-- .quiz-creation-step-1 -->

</div>
<!-- Step1Controller -->

</div><!-- StepController -->
