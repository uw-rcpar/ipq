<form action="authorize.php" method="POST">
	<div class="modal-header">
		<h3 class="lead text-muted">Please log in using your online course credentials</h3>

	</div>
	<div class="modal-body">
		<div class="login-errors alert alert-error alert-warning" data-ng-show="loginError">{{ loginError }}</div>
		<input placeholder="Email" name="email" ng-model="model.email" required="" class="no-vert-padding form-control" type="text">
		<br>
		<input ng-model="model.password" name="password" placeholder="Password" required="" class="no-vert-padding form-control" type="password">
		<!--
		<br/>
		<label><input type="checkbox" name="remember" /> Remember me </label>
		-->
	</div>
	<div class="modal-footer">
		<input class="btn btn-primary form-control" value="Log in" type="submit">
		<input class="form-control" value="frontend access" name="frontend" type="hidden">
	</div>
</form>
