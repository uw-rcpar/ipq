<script type="text/javascript">
window.onbeforeunload = function(){
    return "Are you sure you want to navigate away from this page?\nAll unsaved changes will be lost.";
};
</script>
<div ng-controller="SessionController">
<div class="row container question-page" ng-controller="QuizletController">
<div ng-show="showTabLoading" class="question-container">	
	<div class="loading-spinner"><i class="fa fa-spin fa-spinner fa-4x"></i></div>
</div>
<div class="blastShield" style="display:none">
	<a><i class="fa fa fa-play fa fa-4x" ng-click="unpauseTimer()"></i></a>
</div>

<div ng-controller="QuestionController">
<div class="instructions-container" ng-hide="navState.showInstructions == false || showTabLoading">
</div>


<div class="question-container" ng-show="navState.showInstructions == false && !showTabLoading">

<?php
if(isset($_COOKIE['isadmin'])) {
?>

		<button class="btn btn-danger" style="position:fixed;top:0;right:0;z-index:2000;" ng-click="adminEdit()"><i class="fa fa-white fa fa-pencil"></i> Edit this question in the administrator</button>


<?php
}
?>

<div class="" id="question">
	<div class="question-header-well"  ng-class="{'timer-hidden':!getShowTimer()}">
		<div class="row timer-container">
			<div class="col-sm-5">
				<div class="chapter-title" ng-show="session_type == 'quiz'">
					{{ getChapterTitle() }}
				</div>
				<div class="question-counter">
					Question {{ getCurrentQuestionIndex() }} out of {{ getTotalQuestionsInQuizlet() }}
				</div>
			</div> <!-- /col-md-5-->
			
			<div class="timer-box col-sm-4">
				<div class="timer-wrapper" ng-show="getShowTimer()">
					<div class="col-sm-12">
										    
				    	<div class="timer-wrapper" ng-show="session_type == 'quiz'">	
				    		<div id="timerchart"></div>
						    <div class="timerControlContainer">
							    <a class="timerControl">
								<i class="fa fa fa-pause fa fa-2x" ng-click="pauseTimer()"></i>
							    </a>
						    </div>
				    	</div>
<!--
					   <div class="timer-text" ng-controller="TimerController" ng-show="session_type == 'exam'">
						<span>{{ getTimerHoursRemaining() }} hours {{ getTimerMinutesRemaining() }} minutes remaining</span>
					   </div>
-->
					    <div class="timer-text" ng-show="session_type == 'quiz'">
					    	<div class="text-wrapper">
								<div class="time-remaining">Time Elapsed</div>
								<div id="digital-timer" class="quiz-digital-timer digi-timer">
									<div>
										<span ng-show="getTimerHoursElapsed() > 0">{{ getTimerHoursElapsed() }}:</span><span ng-show="getTimerMinutesElapsed() > 9">{{ getTimerMinutesElapsed() }}:</span><span ng-show="getTimerMinutesElapsed() < 10">0{{ getTimerMinutesElapsed() }}:</span><span ng-show="getTimerSecondsElapsed() > 9">{{ getTimerSecondsElapsed() }}</span><span ng-show="getTimerSecondsElapsed() < 10">0{{ getTimerSecondsElapsed() }}</span>
<!--										<span ng-show="hours > 0">0{{hours}}:</span><span ng-show="minutes > 9">{{minutes}}:</span><span ng-show="minutes < 10">0{{minutes}}:</span><span ng-show="seconds < 10">0{{seconds}}</span><span ng-show="seconds > 9">{{seconds}}</span>
-->
									</div>
								</div>
<!--
								<div id="digital-timer">
									<div ng-show="question.type == 'multiple-choice'">
										<timer><span ng-show="hours > 0">0{{hours}}:</span><span ng-show="minutes > 9">{{minutes}}:</span><span ng-show="minutes < 10">0{{minutes}}:</span><span ng-show="seconds < 10">0{{seconds}}</span><span ng-show="seconds > 9">{{seconds}}</span></timer>
									</div>
									<div ng-show="(question.type == 'tbs-research' || question.type == 'tbs-wc' || question.type == 'tbs-journal') && section != 'BEC'">
										<timer><span ng-show="hours > 0">0{{hours}}:</span><span ng-show="minutes > 9">{{minutes}}:</span><span ng-show="minutes < 10">0{{minutes}}:</span><span ng-show="seconds < 10">0{{seconds}}</span><span ng-show="seconds > 9">{{seconds}}</span></timer>
									</div>
									<div ng-show="(question.type == 'tbs-research' || question.type == 'tbs-wc' || question.type == 'tbs-journal') && section == 'BEC'">
										<timer><span ng-show="hours > 0">0{{hours}}:</span><span ng-show="minutes > 9">{{minutes}}:</span><span ng-show="minutes < 10">0{{minutes}}:</span><span ng-show="seconds < 10">0{{seconds}}</span><span ng-show="seconds > 9">{{seconds}}</span></timer>
									</div>
								</div>--><!-- /digital-timer -->
					    	</div>
					    </div> 
					    
					</div><!-- /col-sm-12 -->
					<div class="col-sm-12">
					
					<span ng-show="session_type == 'quiz'">
						<span class="timer-label">Recommended Answer Time:</span>
						<span>
							    <span class="timer-label-seconds" ng-show="question.type == 'multiple-choice'">90 Sec</span>
							    <span class="timer-label-seconds" ng-show="question.type != 'multiple-choice' && section != 'BEC'">13 Min</span>
							    <span class="timer-label-seconds" ng-show="question.type != 'multiple-choice' && section == 'BEC'">20 Min</span>
						</span>
					</span>
				    	
					</div><!-- /col-sm-12 -->
					
				</div><!-- /timer-wrapper -->
			</div><!-- /col-sm-4-->

			<div class="col-sm-3 exit-calculator-btn">
				<div class="button-group">
<!--
					<span class="btn btn-plain btn-plain-big btn-plain-border text-center pull-right" ng-click="nextQuizlet()" ng-show="quizlets.length > 1  &amp;&amp; navState.quizlet < quizlets.length - 1">
						<span class="sprite-next-gold pull-right"></span>
						<span class="pull-right">Next section</span>
					</span>
-->
					<div class="btn text-center btn-exit btn-plain-border pull-right" ng-click="endSession()" ng-class="{endQuizSolo:quizlets.length == 1 || navState.quizlet == quizlets.length - 1}">
						<div class="sprite-x-gold"></div>
						<div class="exit-link">Exit</div>
					</div>
					<div class="btn text-center btn-calculator pull-right" ng-click="calculator.showCalculator()">
						<div class="sprite-calculator"></div>
						<div class="calculator-link">Calculator</div>
					</div>

				</div>
			</div><!-- /col-sm-3-->

		</div>

	</div>

	<div class="question-area row quiz" ng-class="{'timer-hidden': !getShowTimer()}">
		<div ng-show="question.type == 'multiple-choice'" class="col-md-9 col-sm-9">
			<div class="question-box">
<!--
		<div class="alert alert-info" role="alert">We are currently updating our Interactive Practice Question platform.  If you are experiencing any layout issues or errors, we recommend clearing the cache on your internet browser, restarting your browser, and logging back in to your Interactive Practice Questions.  Thank you for your patience as we make these adjustments</div>
-->
				<span class="question-number">{{navState.question + 1}}.</span>
				<span ng-bind-html-unsafe="question.question" class="question-text"></span>

				<div class="btn-group btn-group-vertical">
					<button class="btn btn-quizoption" ng-class="{active: question.answer.id == choice.id}" type="button" ng-repeat="choice in question.choices" ng-model="question.answer" ng-radio="choice.id" ng-click="setAnswer(choice.id)">
						<span class="pull-left optionLabel">{{optionLabels[$index]}}. </span>
						<span class="pull-left optionBody" ng-bind-html="choice.choice"></span>
					</button>

				</div>
			</div>
		</div>
		<div ng-show="question.type == 'tbs-journal'" ng-controller="TBSJournalQuestionController" class="col-md-9 col-sm-9">
			<div class="question-box tbs-journal-container col-lg-offset-1 col-sm-9 col-md-9" id="tbs-journal-container">
<!--		<div class="alert alert-info" role="alert">We are currently updating our Interactive Practice Question platform.  If you are experiencing any layout issues or errors, we recommend clearing the cache on your internet browser, restarting your browser, and logging back in to your Interactive Practice Questions.  Thank you for your patience as we make these adjustments</div>
-->
				<span style="display:block;width:1px;height:1px;position:relative;left:-30px">
				<span class="question-number">{{navState.question + 1}}.</span>
				</span>
				<div ng-repeat="template in question.question.widgets" class="row">
				<div ng-include="template.type" ng-model="template" class="templateContainer">

			  </div>
			</div>
			</div>
		</div>
		<div ng-show="question.type == 'tbs-research'" ng-controller="TBSResearchController" class="col-md-9 col-sm-9">
			<div class="question-box tbs-journal-container col-lg-offset-1 col-sm-9 col-md-9" id="tbs-journal-container">
<!--		<div class="alert alert-info" role="alert">We are currently updating our Interactive Practice Question platform.  If you are experiencing any layout issues or errors, we recommend clearing the cache on your internet browser, restarting your browser, and logging back in to your Interactive Practice Questions.  Thank you for your patience as we make these adjustments</div>
-->
				<span style="display:block;width:1px;height:1px;position:relative;left:-30px">
				<span class="question-number">{{navState.question + 1}}.</span>
				</span>
				<div ng-repeat="template in question.question.widgets" class="row">
					<div ng-include="template.type" ng-model="template" class="templateContainer">

					</div>
				</div>
			</div>
		</div>
		<div ng-show="question.type == 'tbs-wc'" ng-controller="TBSWrittenCommunicationController" class="col-md-9 col-sm-9">
			<div class="question-box col-lg-offset-1 col-sm-9 col-md-9">
<!--		<div class="alert alert-info" role="alert">We are currently updating our Interactive Practice Question platform.  If you are experiencing any layout issues or errors, we recommend clearing the cache on your internet browser, restarting your browser, and logging back in to your Interactive Practice Questions.  Thank you for your patience as we make these adjustments</div>
-->
				<span class="question-number">{{navState.question + 1}}.</span> <span ng-bind-html-unsafe="question.question.question" class="question-text"></span>
				<div tbs-text-area ng-model="question.answer.solution" class="form-control"></div>

				<div class="btn-group btn-group-vertical">
					<button class="btn btn-quizoption btn-default" ng-class="{active: question.answer.id == choice.id}" type="button" ng-repeat="choice in question.choices" ng-model="question.answer" ng-radio="choice.id" ng-click="setAnswer(choice.id)"><span class="optionLabel">{{optionLabels[$index]}}</span> <span class="optionBody" ng-bind-html="choice.choice"></span></button>

				</div>
			</div>
		</div>

    <script type="text/javascript">

      (function($) {
        $(document).ready(function() {


          $('#open-options').click(function() {

            $('.q-right-next').removeClass('tools-open').addClass('tools-closed');
            $('.q-right-tools').removeClass('tools-closed').addClass('tools-open');

          });


          $('#hide-options').click(function() {

            $('.q-right-next').removeClass('tools-closed').addClass('tools-open');
            $('.q-right-tools').removeClass('tools-open').addClass('tools-closed');

          });


        });

      })(jQuery);

    </script>

		<div class="tools-closed q-right-next col-sm-3">
			<a id="open-options"><i class="fa fa-caret-left"></i>Show Sidebar</a>
			<button class="btn nextbtn btn-next-question btn-arrow" ng-click="gotoNextQuestion()"><span ng-show="isLastQuestion == false">Next</span><span ng-show="isLastQuestion == true">End</span></button>
		</div>
		<div class="q-right-tools col-md-3 col-sm-3 tools-open" ng-class="{'quiz': session_type == 'quiz'}">
			<a id="hide-options">Hide Sidebar <i class="fa fa-caret-right"></i></a><br />
			<div class="q-tools">
				  <div class="options-menu btn btn-ice btn-ice-no-icon" ng-class="{'open': showOptions}" ng-show="session_type == 'quiz'" ng-click="toggleOptions()" dropdown>
					<strong>
						Options
						<i class="pull-right fa" ng-class="{'fa-chevron-down': showOptions, 'fa-chevron-right': !showOptions}"></i>
					</strong>
					<ul class="dropdown-menu">
						<li>
							<span class="btn-link" ng-click="toggleShowTimer()">
								<i class="fa" ng-class="{true:'fa fa-check-square', false:'fa fa-square-o'}[getShowTimer()]"></i>
								Show Timer
							</span>
						</li>
						<li>
<!--
							<a class="btn-link" ng-click="toggleScoreAsIGo()" ng-hide="question.type == 'tbs-wc'">
-->
							<span class="btn-link" ng-click="toggleScoreAsIGo()">
								<i class="fa" ng-class="{true:'fa fa-check-square', false:'fa fa-square-o'}[getScoreAsIGo()]"></i>
								 Score as I go
							</span>
						</li>
					</ul>
				</div>
<!--
				<button class="btn btn-ice btn-ice-icon text-center" ng-click="toggleBookmark(navState.question)"><span class="sprite-flag-blue"></span>Bookmark</button>
-->

				<div class="movable-buttons" ng-class="{'adjusted': showOptions}">
					<span ng-show="session_type == 'quiz'">
					<button class="btn btn-ice btn-ice-icon text-center" ng-show="enableExplanation == true" ng-click="showExplanation()"><span class="sprite-explain-answer"></span> Explain<br />Answer</button>
					</span>
					<button ng-controller="BookmarkController" class="btn text-center btn-ice btn-ice-icon btn-bookmark" ng-click="toggleBookmark(navState.question)" ng-class="{true: 'bookmark-active', false: 'bookmark-inactive'}[question.isFlagged]"><span class="sprite-bookmark"></span><span ng-hide="question.isFlagged" class="bookmark">Bookmark</span><span class="bookmarked" ng-show="question.isFlagged">Bookmarked</span></button>
					<button class="btn text-center btn-ice btn-ice-icon" ng-hide="session_type != 'quiz' || question.type == 'multiple-choice' || question.type == 'tbs-wc'" ng-click="showExplanation()"><span class="sprite-explain-answer"></span> View Solution</button>
				</div>

				<div class="next-question-wrapper" ng-hide="getSavingInProgress()">
					<button class="btn nextbtn btn-next-question btn-arrow" ng-click="gotoNextQuestion()">
						<span ng-show="isLastQuestion == false">Next Question</span>
						<span ng-show="isLastQuestion == true">End Section</span>
					</button>
				</div>
				<div class="next-question-wrapper" ng-show="getSavingInProgress()">
					<button ng-class="{'btn-opacified': getSavingInProgress()}" class="btn nextbtn btn-next-question btn-arrow">
						Saving...
					</button>
				</div>
			</div>
		</div>
	</div> <!-- .question-area -->

   	<div class="subfooter" ng-show="navState.showInstructions == false" ng-class="{'quiz': session_type == 'quiz'}">
   		<div style="padding-top:70px">
<!--
    	<div class="well question-topic-nav col-sm-7 col-md-7">
    		<p>Review this material in your online course</p>
    		<ul>
    			<li ng-repeat="topic in topicTitles"><i class="fa fa-chevron-right"></i> <a href="{{topic.course_url}}" target="_blank"><strong>{{topic.chapter}}</strong>: {{topic.topic}}</a></li>
    		</ul>
    	</div>
-->

<!--
    	<div class="q-right-tools">

	<div class="pull-right">
    	<button style="white-space:nowrap; color:#2560ac;" class="btn btn-link" ng-click="toggleScoreAsIGo()" ng-hide="question.type == 'tbs-wc'">
		<i class="fa" ng-class="{true:'fa fa-check-square-o', false:'fa fa-square-o'}[getScoreAsIGo()]"></i> Score as I go
	</button>
	<br />
    	<button class="btn pull-right nextbtn" ng-click="gotoNextQuestion()">
		<span ng-show="isLastQuestion == false"><span class="sprite-x-blue-small"></span>Next Question</span>
		<span ng-show="isLastQuestion == true"><span class="sprite-x-blue-small"></span>End Section</span>
	</button>
    	<br>
    	<button class="sprite-left-arrow" data-ng-show="isPrevFlaggedQuestion()" ng-click="gotoPrevFlaggedQuestion()" tooltip="Navigate back to flagged questions"></button>
    	<button class="sprite-left-arrow" data-ng-hide="isPrevFlaggedQuestion()"></button>
    	<button class="sprite-right-arrow" data-ng-show="isNextFlaggedQuestion()" ng-click="gotoNextFlaggedQuestion()" tooltip="Navigate ahead to flagged questions"></button>
    	<button class="sprite-right-arrow" data-ng-hide="isNextFlaggedQuestion()"></button>
	</div>
    	</div>
-->
    	</div>


	</div>
        <div class="question-topic-nav col-sm-9 col-md-9" ng-show="session_type == 'quiz'">
                <p>Review related course topic</p>
                <ul>
                        <li ng-repeat="topic in topicTitles"><i class="fa fa-chevron-right"></i> <a href="{{topic.course_url}}" target="_blank"><strong>{{topic.chapter}}</strong>: {{topic.topic}}</a></li>
                </ul>
        </div>

	<div ng-controller="NoteController" ng-show="session_type == 'quiz'">
	<div class="notes col-sm-9">
		<alert ng-repeat="n in question.noted" class="alert question-note alert-info" close="deleteNote($index)">
			<div ng-show="noteBeingEdited == $index" class="note-text col-md-10">
<!--				<input type="text" class="form-control" ng-model="n" />
-->
				<textarea class="form-control" ng-model="editedNoteText"></textarea>
			</div>
			<div ng-hide="noteBeingEdited == $index" class="note-text col-md-10" ng-bind-html-unsafe="n | nl2br">
			</div>
			<span class="note-buttons link col-md-1">
				<span class="pull-right" ng-click="editNote($index)" ng-hide="noteBeingEdited == $index">Edit</span>
				<span class="pull-right" ng-click="saveEditedNote($index, editedNoteText)" ng-show="noteBeingEdited == $index">Save</span>
			</span>
			<span class="clearfix"></span>
			
		</alert>
	</div>

	<div class="notesFormContainer col-sm-9">
	<i class="fa fa-pencil"></i> Add a note
	<br>
	<textarea id="noteInput" ng-model="question.newNoteText" popover="Your notes will be saved for this quiz only if you choose to save this quiz. However, your notes will not appear on the same question in future quizzes." popover-title="Warning" popover-placement="top" popover-animation="true" popover-trigger="focus" popover-popup-delay="750"></textarea>
	<br />
	<br />
	<button class="btn pull-right btn-ice btn-save-note" ng-click="createNote()" ng-disabled="question.newNoteText == ''">Save note</button>
	</div>
	</div><!-- NoteController -->

	<div class="tiny text-gray col-sm-12">Question ID #{{question.id}}</div>

</div>

</div>





<div class="navbar navbar-fixed-bottom question-footer" ng-hide="getShowInstructions()" ng-controller="PaginationController">
      <div class="quiznav container col-md-12">
      	<div class="container col-md-12">
        	<div class="pager-wrapper" ng-controller="BookmarkController">
	       <button class="button-flag-prev" data-ng-show="isPrevFlaggedQuestion()" ng-click="gotoPrevFlaggedQuestion()" tooltip="Navigate back to flagged questions"></button>
	       <button class="button-flag-prev button-flag-prev-disabled" data-ng-hide="isPrevFlaggedQuestion()"></button>
				<div class="qlist-container" ng-style="{ 'width': ((questionList.length * 18) | number) + 'px' }">
      				<div class="questionflagbar text-center" ng-repeat="q in questionList">
	      				<i ng-class="{'fa fa-pencil':q.noted.length > 0}"></i><br>
	      				<div class="questionflagbar-inner text-center" ng-class="{current:$index == qID, 'btn-danger':q.isFlagged == true}">
	      				<i ng-controller="BookmarkController" class="fa" ng-class="{true: 'fa fa-flag fa-white', false: 'fa fa-flag'}[q.isFlagged]" ng-click="toggleBookmark($index)"></i><br>
	      				<button class="btn btn-link text-center btn-sm" ng-click="setPath(q.path)" tooltip-placement="top" tooltip-html-unsafe="{{q.question}}" tooltip-trigger="mouseenter">{{$index + 1}}</button>
	      				</div>
      				</div>
      			</div>
	       <button class="button-flag-next" data-ng-show="isNextFlaggedQuestion()" ng-click="gotoNextFlaggedQuestion()" tooltip="Navigate ahead to flagged questions"></button>
	       <button class="button-flag-next button-flag-next-disabled" data-ng-hide="isNextFlaggedQuestion()"></button>
      		</div>
      	</div>
      </div>
</div>

<div class="top-links" ng-hide="showTabLoading">
	<div class="row" ng-show="navState.showInstructions == true">
		<!--<div class="col-sm-12"><button class="btn btn-link btn-reportproblem btn-reportproblem-center" report-problem="" question="question"><i class="fa fa fa-warning"></i> Report Problem</button></div>-->
	</div>
	<div class="row" ng-show="navState.showInstructions == false">
		<!--<div class="col-sm-6"><button class="btn btn-link btn-reportproblem" report-problem="" question="question"><i class="fa fa fa-warning"></i> Report Problem</button></div>-->
		<div class="col-sm-12 text-center"><button class="btn btn-link btn-view-instructions" style="position:relative;left:20px;" data-ng-click="gotoInstructions()"><i class="fa fa fa-info-circle"></i> View Instructions</button></div>
	</div>
</div>

</div>

<!--
  <div class="copyright">
	Copyright © 2014 Roger CPA Review. All rights reserved
  </div>
-->
</div>
</div><!-- QuestionController -->
</div> <!-- .question-page-->
</div> <!-- SessionController -->
