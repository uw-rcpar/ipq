<div class="row" style="margin-top:30px;">
<div class="well col-lg-offset-2 col-sm-8 col-md-8" ng-show="isSaved == false">
<p class="lead">Change Session Name</p>
	<div>
	<form novalidate="" class="form-horizontal">
	<div class="alert alert-danger" ng-hide="sessionName.length > 0">Please enter a valid name for this quiz.</div>
	<div class="form-group col-md-12">
	  		<label class="pull-left control-label" for="sessionName">Session name</label>
		  	<div class="col-md-9 controls">
			    <div class="input-group">
			    	<span class="input-group-addon"><i class="fa fa-file"></i></span>
			    	<input class="form-control" id="sessionName" ng-model="sessionName" type="text" />
				<div class="input-group-btn">
			   		<button ng-click="saveSession(sessionName)" class="btn btn-default" ng-class="{'btn-primary': sessionName.length > 0, 'btn-default': sessionName.length == 0}" ng-disabled="sessionName == ''">Update Name</button>
				</div>
			    </div>
		  </div>
	</div>
	</form>
	</div>
</div>


<!--<div class="well col-lg-offset-2 col-sm-8 col-md-8" ng-show="isSaved == true">-->
<!--<p class="lead text-center">Your quiz session has been saved.</p>
-->
<div class="show-tab-loading text-center col-md-12" ng-show="isSaved == true">
	<div class="normal-size">Session saved. One moment please..</div>
	<i class="fa fa-spin fa-pulse fa-gear"></i>

</div>
<!--
<div class="copyright">
	Copyright © 2014 Roger CPA Review. All rights reserved
</div>
-->
</div>
