<div class="modal-header">
<h3 class="lead"><i class="fa fa fa-info-circle"></i> Research Task Authoritative Literature</h3>

</div>
<div class="modal-body">
<p>While you will have access to certain professional literature within this program (e.g. GAAS, IRC, and PCAOB standards), 
to obtain a free six-month subscription to FASB Current Text, FASB Original Pronouncements, and AICPA Professional Standards 
for research task questions we recommend all eligible students submit a free Request for Professional Literature from NASBA.  
To subscribe you must have a valid Notice To Schedule (NTS) and complete the registration at 
<a href="https://proflit.nasba.org/" target="_blank">https://proflit.nasba.org/</a>.</p>

<p>Subscription approval can take up to three weeks to process, so make sure to submit your request in time to practice before your exams!</p>
</div>
<div class="modal-footer">
	<input class="btn form-control btn-default" value="Ok great!" ng-click="close()" type="submit">
	<input class="form-control" value="frontend access" name="frontend" type="hidden">
</div>
