<!-- mcq -->
<div ng-show="question.type == 'multiple-choice'">
	<h1 class="page-title ipq-title col-md-12">Instructions</h1>
	<div class="instructions-beginquiz pull-right col-md-5 col-md-offset-4" ng-class="{'scroller-sticker-button': scroll > 40}" scroller-sticky="scroll">
		<button class="btn btn-orange-big begin-wc-btn" ng-click="dismissInstructions()" >
			Begin MC Questions
		</button>
	</div>


	<div class="container instructions instructons-wrapper" ng-show="navState.showInstructions == true">
	<div class="row" scroll-spy="" data-top-buffer="195">
	<!--<div class="bs-docs-sidebar col-sm-4">
	
		<span class="nav nav-list bs-docs-sidenav affix title-sidenav">Direction Topics</span>
		<ul class="nav nav-list bs-docs-sidenav affix">
			<li>
				<a href="#mc-overview" prevent-default="" spy="mc-overview">Overview <i class="fa fa-chevron-right"></i></a>
			</li>
			<li>
				<a href="#mc-navigation" prevent-default="" spy="mc-navigation">Footer Navigation <i class="fa fa-chevron-right"></i></a>
			</li>
			<li>
				<a href="#mc-responses" prevent-default="" spy="mc-responses">Answer Attempts <i class="fa fa-chevron-right"></i></a>
			</li>
			<li>
				<a href="#mc-timer" prevent-default="" spy="mc-timer">Timer <i class="fa fa-chevron-right"></i></a>
			</li>
			<li>
				<a href="#mc-tools_and_buttons" prevent-default="" spy="mc-tools_and_buttons">Tools <i class="fa fa-chevron-right"></i></a>
			</li>
		</ul>
	</div>
	-->
	<!--<div class="col-sm-8 col-sm-offset-4" id="scrollspy-instructions">-->
	<div id="scrollspy-instructions">
	<div id="mc-overview">
	<h1 class="lead">Multiple Choice Questions</h1>
	<p>Multiple choice questions are answered by selecting only the single best answer. When you navigate away from a question, your answer is recorded.  If you return to that same question later, a separate answer attempt is recorded. Changing your answer after clicking the "Show Explanation" button will also result in a new answer attempt being recorded, even without navigating away from the question.</p>
	</div>

	<h1 class="lead in-copy" id="mc-navigation">Footer Navigation</h1>
	<p>Click on a number in the Footer Navigation bar to jump to any question in the quiz. The Footer Navigation bar is located at the bottom of the quiz screen.</p>

		<div class="content-image">
		<img src="img/directions/footer-nav.png" class="img-polaroid pull-center" style="margin:auto;" />
		</div>
		<ul class="instructions-list">
			<li>Question 7 is highlighted in <span style="background-color: #a8f4ff;padding-left: 3px; padding-right: 3px">blue</span> because the quiz is currently displaying question 7.</li>
			<li>Questions 3 and 6 are highlighted in <span style="background-color: #bf342f; padding-left: 3px; padding-right: 3px; color: #fff;">red</span> because they have been bookmarked by the user. </li>
			<li>Question 12 shows a pencil symbol, showing that the user has saved a note on that question. </li>
			<li>To clear these bookmarks, click the “Bookmark” tool again.  </li>
		</ul>



	</div>
	</div>
	</div>
</div>

<!-- tbs-research -->
<div ng-show="question.type == 'tbs-research' || question.type == 'tbs-journal'">
	<h1 class="page-title ipq-title col-md-12">Instructions</h1>

	<div class="instructions-beginquiz pull-right col-md-5 col-md-offset-4" ng-class="{'scroller-sticker-button': scroll > 40}" scroller-sticky="scroll">
		<button class="btn btn-orange-big begin-wc-btn" ng-click="dismissInstructions()" >
			Begin Task-Based Simulations
		</button>
	</div>


	<div class="container instructions instructons-wrapper" ng-show="navState.showInstructions == true">
	<div class="row" scroll-spy="" data-top-buffer="195">
	<!--
	<div class="bs-docs-sidebar col-sm-4">
		<span class="nav nav-list bs-docs-sidenav affix title-sidenav">Direction Topics</span>
		<ul class="nav nav-list bs-docs-sidenav affix">
			<li>
				<a href="#tbs-overview" prevent-default="" spy="tbs-overview">Overview <i class="fa fa-chevron-right"></i></a>
			</li>
			<li>
				<a href="#tbs-navigation" prevent-default="" spy="tbs-navigation">Navigation <i class="fa fa-chevron-right"></i></a>
			</li>
			<li>
				<a href="#tbs-responses" prevent-default="" spy="tbs-responses">Answer Attempts <i class="fa fa-chevron-right"></i></a>
			</li>
			<li>
				<a href="#tbs-timer" prevent-default="" spy="tbs-timer">Timer <i class="fa fa-chevron-right"></i></a>
			</li>
			<li>
				<a href="#tbs-tools_and_buttons" prevent-default="" spy="tbs-tools_and_buttons">Tools <i class="fa fa-chevron-right"></i></a>
			</li>
		</ul>
	</div>
	-->
	<!--<div class="col-sm-offset-4 col-sm-8" id="scrollspy-instructions">-->
	<div id="scrollspy-instructions">
	<div id="tbs-overview">
	<h1 class="lead">Task-Based Simulations</h1>
	<p>Task-Based Simulation questions are presented in either a table or research question format.</p>
	<p>In table format, each table includes a list of several multiple choice questions. Pick the best answer for each item that appears when you click on the table.</p>
For research questions, you will be provided a link to authoritative literature. Use this literature to fill in the provided spaces.</p>
	<p>Task-based simulation questions are answered by selecting or providing the best answer for all fields. </p>
	<p>When you navigate away from a question and return to it later, a separate event is recorded. Changing your answer after clicking the "Show Explanation" button will also result in a new answer attempt being recorded, even without navigating away from the question.</p>
	</div>

	<h1 class="lead in-copy" id="tbs-navigation">Footer Navigation</h1>
	<p>Click on a number in the Footer Navigation bar to jump to any question in the quiz. The Footer Navigation bar is located at the bottom of the quiz screen.</p>

		<div class="content-image">
		<img src="img/directions/footer-nav.png" class="img-polaroid pull-center" style="margin:auto;" >
		</div>
	  <p>Above you can see a screenshot of the footer navigation displayed along the bottom of the quiz.</p>
		<ul class="instructions-list">
			<li>Question 7 is highlighted in <span style="background-color: #a8f4ff;padding-left: 3px; padding-right: 3px">blue</span> because the quiz is currently displaying question 7.</li>
			<li>Questions 3 and 6 are highlighted in <span style="background-color: #bf342f; padding-left: 3px; padding-right: 3px; color: #fff;">red</span> because they have been bookmarked by the user.</li>
			<li>Question 12 shows a pencil symbol, showing that the user has saved a note on that question.</li>
			<li>To clear these bookmarks, click the “Bookmark” tool again. </li>
		</ul>
	</div>
	</div>
	</div>
</div>

<!-- WC -->

<div ng-show="question.type == 'tbs-wc'">
	<h1 class="page-title ipq-title col-md-12"><span class="ipq-title-inner-wrapper">Instructions</span></h1>

	<div class="instructions-beginquiz pull-right col-md-5 col-md-offset-4" ng-class="{'scroller-sticker-button': scroll > 40}" scroller-sticky="scroll">
		<button class="btn btn-orange-big begin-wc-btn" ng-click="dismissInstructions()" >
			Begin Written Communication
		</button>
	</div>

	<div class="container instructions instructons-wrapper" ng-show="navState.showInstructions == true">
	<div class="row" scroll-spy="" data-top-buffer="195">
	<!--
	<div class="bs-docs-sidebar col-md-4">
		<span class="nav nav-list bs-docs-sidenav affix title-sidenav">Direction Topics</span>
		<ul class="nav nav-list bs-docs-sidenav affix">
			<li>
				<a href="#tbs-wc-overview" prevent-default="" spy="tbs-wc-overview">Overview <i class="fa fa-chevron-right"></i></a>
			</li>
			<li>
				<a href="#tbs-wc-navigation" prevent-default="" spy="tbs-wc-navigation">Navigation <i class="fa fa-chevron-right"></i></a>
			</li>
			<li>
				<a href="#tbs-wc-timer" prevent-default="" spy="tbs-wc-timer">Timer <i class="fa fa-chevron-right"></i></a>
			</li>
			<li>
				<a href="#tbs-wc-tools_and_buttons" prevent-default="" spy="tbs-wc-tools_and_buttons">Tools <i class="fa fa-chevron-right"></i></a>
			</li>
		</ul>
	</div>
	-->
	<!--<div class="col-md-offset-4 col-md-8" id="scrollspy-instructions">-->
	<div id="scrollspy-instructions">
	<div id="tbs-wc-overview">
	<h1 class="lead">Written Communications</h1>
	<p>For Written Communication questions, use the field provided to create a written response that is on topic, clear, concise and grammatically correct. This portion of your session is self-graded. You will be provided a guide to help you rank yourself appropriately.</p>
	</div>

	<h1 class="lead in-copy" id="tbs-wc-navigation">Footer Navigation</h1>
	<p>Click on a number in the Footer Navigation bar to jump to any question in the quiz. The Footer Navigation bar is located at the bottom of the quiz screen.</p>

		<div class="content-image">
		<img src="img/directions/footer-nav.png" class="img-polaroid pull-center" style="margin:auto;" >
		</div>
	  <p>Above you can see a screenshot of the footer navigation displayed along the bottom of the quiz.</p>
		<ul class="instructions-list">
			<li>Question 7 is highlighted in <span style="background-color: #a8f4ff;padding-left: 3px; padding-right: 3px">blue</span> because the quiz is currently displaying question 7.</li>
			<li>Questions 3 and 6 are highlighted in <span style="background-color: #bf342f; padding-left: 3px; padding-right: 3px; color: #fff;">red</span> because they have been bookmarked by the user.</li>
			<li>Question 12 shows a pencil symbol, showing that the user has saved a note on that question.</li>
			<li>To clear these bookmarks, click the “Bookmark” tool again. </li>
		</ul>
	</div>
	</div>
	</div>
</div>
<!--
		<div class="navbar navbar-inverse navbar-fixed-bottom gray-footer">
				<div class="container">
					<div class="container">
						<div class="row">
							<button class="btn btn-ice pull-right" ng-click="dismissInstructions()" >
								Begin quiz
							</button>
						</div>
					</div>
			    </div>
		</div>
-->
