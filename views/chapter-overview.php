<div class="row">
	<div class="col-sm-12 col-md-12">
		<div class="finalscore">
			<div class="greenpie"></div>
			<div class="redpie"></div>
	 		<p class="lead text-center totalscore"> 
				<strong class="percentage">{{totalPercent | number:0 }}%</strong>
				Correct
			</p>
		</div>
		<h3 class="head text-center">Task-based and Written Communications Questions:</h3>
		<div class="question-categories text-center">
			<ul class="list-group col-md-2 col-md-offset-1">
				<li class="list-group col-md-2-item">
						<a ng-click="viewQuestions('correct')">
							<span class="gray-bg"><i class="fa fa-check"></i></span>{{tbs.correctQuestions}} Correct
						</a>
				</li>
			</ul>
			<ul class="list-group col-md-2">
				<li class="list-group col-md-2-item"><a ng-click="viewQuestions('tbs', 'incorrect')"><span class="gray-bg"><i class="fa fa-times"></i></span>{{tbs.incorrectQuestions}} Incorrect</a></li>
			</ul>
			<ul class="list-group col-md-2">
				<li class="list-group col-md-2-item"><a ng-click="viewQuestions('tbs', 'skipped')"><span class="gray-bg"><i class="fa fa-exclamation"></i></span>{{tbs.skippedQuestions}} Unanswered</a></li>
			</ul>
			<ul class="list-group col-md-2">
				<li class="list-group col-md-2-item"><a ng-click="viewQuestions('tbs', 'flagged')">
					<span class="gray-bg"><i class="fa fa-flag"></i></span>{{tbs.flaggedQuestions}} Bookmarked</a></li>
			</ul>
			<ul class="list-group col-md-2">
				<li class="list-group col-md-2-item"><a ng-click="viewQuestions('tbs', 'noted')"><span class="gray-bg"><i class="fa fa-edit"></i></span>{{tbs.notedQuestions}} Noted</a></li>
			</ul>
		</div>
		<br /> <br />
		<h3 class="head text-center">Multiple Choice Questions:</h3>
		<div class="question-categories text-center">
			<ul class="list-group col-md-2 col-md-offset-1">
				<li class="list-group col-md-2-item">
						<a ng-click="viewQuestions('mcq', 'correct')">
							<span class="gray-bg"><i class="fa fa-check"></i></span>{{mcq.correctQuestions}} Correct
						</a>
				</li>
			</ul>
			<ul class="list-group col-md-2">
				<li class="list-group col-md-2-item"><a ng-click="viewQuestions('mcq', 'incorrect')"><span class="gray-bg"><i class="fa fa-times"></i></span>{{mcq.incorrectQuestions}} Incorrect</a></li>
			</ul>
			<ul class="list-group col-md-2">
				<li class="list-group col-md-2-item"><a ng-click="viewQuestions('mcq', 'skipped')"><span class="gray-bg"><i class="fa fa-exclamation"></i></span>{{mcq.skippedQuestions}} Unanswered</a></li>
			</ul>
			<ul class="list-group col-md-2">
				<li class="list-group col-md-2-item"><a ng-click="viewQuestions('mcq', 'flagged')">
					<span class="gray-bg"><i class="fa fa-flag"></i></span>{{mcq.flaggedQuestions}} Bookmarked</a></li>
			</ul>
			<ul class="list-group col-md-2">
				<li class="list-group col-md-2-item"><a ng-click="viewQuestions('mcq', 'noted')"><span class="gray-bg"><i class="fa fa-edit"></i></span>{{mcq.notedQuestions}} Noted</a></li>
			</ul>
		</div>
 	</div>

</div>



<div class="navbar navbar-inverse navbar-fixed-bottom black-footer roger">

	<div class="container">
		<div class="container">
			<div class="row">
		     	   <div class="pull-right col-sm-2 col-md-2">
		               <div class="btn-toolbar">
			            <div class="btn-group">
	              	    	    </div>
	            		</div>
	        	    </div>
	      </div>
		</div>
	</div>
</div>
<!--
<div class="copyright">
Copyright © 2014 Roger CPA Review. All rights reserved
</div>
-->
