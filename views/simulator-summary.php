<div ng-controller="StepController">
<div class="row container exam-simulator-summary" ng-controller="ExamSimulatorController">

<div ng-show="section && showTabLoading" class="">	
	<div class="loading-spinner"><i class="fa fa-spin fa-spinner fa-4x"></i></div>
</div>

<div class="section-selection-container"></div>

<div class="simulator-summary text-left" ng-show="section && !showTabLoading" ng-animate="{show: 'fadeIn', hide:'fadeOut'}">
	<h3 class="top-heading">{{ model.pickSection }} Exam Simulation
		<div class="choose-chapters-sticky pull-right" ng-class="{'scroller-sticker': scroll > 1}" scroller-sticky="scroll">

			<span class="pull-right"><button class="btn btn-arrow blue" ng-show="termsAgreed" ng-click="preCanned()">Next</button></span>
			<span class="pull-right"><button class="btn btn-arrow disabled pull-right" ng-hide="termsAgreed">Next</button></span>
		</div>
<!--
	<div class="row step3-beginquiz">
		<div class="col-md-12">
	      <div class="btn-toolbar">
		  <button class="btn btn-ice" ng-click="beginQuiz()"><span>Start Practice Simulator</span> </button>
		</button>
		</div>
	    </div>
	</div>
-->
	</h3>
	<h4 class="sub-heading">Your practice exam will include the following:</h4>
	<br />
	<div class="quizlet-summary">
		<ul ng-show="model.pickSection == 'AUD' || model.pickSection == 'FAR'">
<!--
			<li ng-repeat="quizlet in quizlets[section]" class="quiz-summary-row">
				<span class="pull-left">{{quizlet.qty}} {{quizlet.typeLongName}} </span>
			</li>
-->
			<li class="quiz-summary-row">
				<span class="pull-left"><strong>Quizlet 1:</strong></span>
				<span class="pull-left">30 multiple-choice questions</span>
			</li>
			<li class="quiz-summary-row">
				<span class="pull-left"><strong>Quizlet 2:</strong></span>
				<span class="pull-left">30 multiple-choice questions</span>
			</li>
			<li class="quiz-summary-row">
				<span class="pull-left"><strong>Quizlet 3:</strong></span>
				<span class="pull-left">30 multiple-choice questions</span>
			</li>
			<li class="quiz-summary-row">
				<span class="pull-left"><strong>Quizlet 4:</strong></span>
				<span class="pull-left">7 task-based simulations</span>
			</li>
		</ul>
		<ul ng-show="model.pickSection == 'REG'">
			<li class="quiz-summary-row">
				<span class="pull-left"><strong>Quizlet 1:</strong></span>
				<span class="pull-left">24 multiple-choice questions</span>
			</li>
			<li class="quiz-summary-row">
				<span class="pull-left"><strong>Quizlet 2:</strong></span>
				<span class="pull-left">24 multiple-choice questions</span>
			</li>
			<li class="quiz-summary-row">
				<span class="pull-left"><strong>Quizlet 3:</strong></span>
				<span class="pull-left">24 multiple-choice questions</span>
			</li>
			<li class="quiz-summary-row">
				<span class="pull-left"><strong>Quizlet 4:</strong></span>
				<span class="pull-left">6 task-based simulations</span>
			</li>
		</ul>
		<ul ng-show="model.pickSection == 'BEC'">
			<li class="quiz-summary-row">
				<span class="pull-left"><strong>Quizlet 1:</strong></span>
				<span class="pull-left">30 multiple-choice questions</span>
			</li>
			<li class="quiz-summary-row">
				<span class="pull-left"><strong>Quizlet 2:</strong></span>
				<span class="pull-left">30 multiple-choice questions</span>
			</li>
			<li class="quiz-summary-row">
				<span class="pull-left"><strong>Quizlet 3:</strong></span>
				<span class="pull-left">30 multiple-choice questions</span>
			</li>
			<li class="quiz-summary-row">
				<span class="pull-left"><strong>Quizlet 4:</strong></span>
				<span class="pull-left">3 written communication questions</span>
			</li>
		</ul>
	</div>
	<div class="row">
		<div class="simulator-summary-note">Note: Questions in each quizlet are selected randomly for each simulation.</div>

		<p class="lead nasba-text text-left" ng-controller="NASBAController" ng-show="model.pickSection != 'BEC'">
			<i style="cursor:pointer;" class="fa fa-square-o" ng-class="{'fa fa-check-square':hasNASBA == true,'fa fa-square-o':hasNASBA == false}" ng-click="toggleHasNASBA()"></i>
			<span style="cursor:pointer;" ng-click="toggleHasNASBA()">I am scheduled to sit for the exam and have access to NASBA research materials.</span>
			<span style="font-size:14px;vertical-align:super;">
				<a ng-click="showNASBAPopup()">
					<i class="fa fa fa-question-circle" tooltip="The types of research tasks you will be offered will depend on whether you can access NASBA research materials. Click the question mark to learn more."></i>

				</a>
			</span>
		</p>
	</div>
	<div class="well">
		<p>This simulation allows you to practice in an exam-like environment.  Upon completion, you will receive a summary to help you assess your strengths and weaknesses. Please note that your final score will not be an exact indication of how you would perform on the actual CPA Exam.
		</p>
		<div class="i-understand">
			<button ng-click="toggleTermsAgreed()"><i style="cursor:pointer;" class="fa" ng-class="{'fa fa-check-square':termsAgreed == true,'fa fa-square-o':termsAgreed== false}"></i></button>
			<span class="i-understand-copy">I understand this information</span>
		</div>
	</div>
</div>

</div> <!--.exam-simulator-summary -->
</div><!-- StepController -->
