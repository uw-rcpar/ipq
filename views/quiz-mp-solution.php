<div class="modal-header">
  <button type="button" class="close" ng-click="close()">&times;</button>
  <p class="lead">{{solution.title}}</p>
</div>
  <div class="modal-body" ng-bind-html="solution.explanation">
  

  </div>

<div class="modal-footer">
  <button ng-repeat="button in solution.buttons" class="{{button.cssClass}} btn" ng-click="close(button.result)">{{button.label}}</button>
	
</div>