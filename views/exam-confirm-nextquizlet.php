<div class="modal-header">
<h3><i class="fa fa-alert"></i>Are you sure you want to exit this part of your Exam Simulation?</h3>
</div>
<div class="modal-body">
	<p>Once you click on "Yes" you will NOT be able to return to this testlet until your Exam Simulation is complete.</p>	
</div>
<div class="modal-footer">
<button class="btn btn-default btn-cancel btn-modal " ng-click="cancel()">No</button>
<button class="btn btn-modal btn-modal-primary" ng-click="confirm('ok')">Yes</button>
</div>
