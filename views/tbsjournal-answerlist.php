<div class="modal-header">
    <button type="button" class="close" ng-click="close()">×</button>
    <p class="lead">{{tbs.list.model.name}}</p>
  </div>
  <div class="modal-body">
  <ul style="padding:0;margin:0 0 0 0;">
  <li ng-repeat="answer in tbs.list.model.content">
    <button class="btn btn-quizoption" ng-class="{active: tbs.givenAnswer == answer.id}" ng-click="tbs.givenAnswer = answer.id"><span class="option-letter">{{answer.letter}}</span>{{answer.answer}}</button>
  </li>
  </ul>

</div>

<div class="modal-footer">
	<button class="btn btn-default btn-modal" ng-click="close()">Close</button>
	<button class="btn btn-primary btn-modal btn-modal-primary" id="save-option-btn" ng-click="saveAndClose()" ng-disabled="tbs.givenAnswer == null">Save Answer</button>
</div>
