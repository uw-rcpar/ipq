<div class="blastShield" style="display:none">
<a><i class="fa fa fa-play fa fa-4x"></i></a>
</div>

<div class="container instructions" ng-show="navState.showInstructions == true" style="margin-top:30px;">
<div class="row" scroll-spy="" data-top-buffer="195">
<div class="bs-docs-sidebar col-sm-3 col-md-3">
	<ul class="nav nav-list bs-docs-sidenav affix">
		<li>
			<i class="fa fa-chevron-right"></i>
			<a href="#overview" prevent-default="" spy="overview">Overview </a>
		</li>
		<li>
			<i class="fa fa-chevron-right"></i>
			<a href="#navigation" prevent-default="" spy="navigation">Navigation </a>
		</li>
		<li>
			<i class="fa fa-chevron-right"></i>
			<a href="#responses" prevent-default="" spy="responses">Answer Attempts </a>
		</li>
		<li>
			<i class="fa fa-chevron-right"></i>
			<a href="#timer" prevent-default="" spy="timer">Timer </a>
		</li>
		<li>
			<i class="fa fa-chevron-right"></i>
			<a href="#tools_and_buttons" prevent-default="" spy="tools_and_buttons">Tools </a>
		</li>
	</ul>
</div>

<div class="col-sm-8 col-md-8" id="scrollspy-instructions">
<div id="overview">
<h1 class="lead">Overview</h1>
<p>In this section, you will be presented with a table of multiple choice questions. Pick the best answer for each question from the list
that appears when you click on a link in a table.</p>
</div>

<h1 class="lead" id="navigation">Navigation</h1>
<p>To move from one question to the next, use the buttons at the lower right of the screen. The 'Next Question' button will 
navigate the quiz to the next question, until you reach the end and either end the quiz or advance to the next question. The arrow
buttons with flags below the 'Next Question' button will iterate through questions that you have flagged.</p> 
<p>Questions can be flagged so that they are highlighted in the numbered list that appears at the bottom of the screen.</p>

	<div class="text-center">
	<img src="img/directions/footernav-screenshot.png" class="img-polaroid pull-center" style="margin:auto;" width="482" height="33">
	</div>
  <p>Above you can see a screenshot of the footer navigation displayed along the bottom of the quiz.</p>
  	<ul style="list-style:circle;">
  		<li>Question number 7 is highlighted in blue because the quiz is currently displaying question 7.</li>
  		<li>Questions 3 and 6 are highlighted in red because they have been flagged for additional attempts.</li>
	</ul>


<h1 class="lead" id="responses">Answer Attempts</h1>
<p>Multiple choice questions are answered by selecting only the <em>single</em> best answer. When you navigate away from a question 
and return to it later, a seperate event is recorded. On the scoring screen you will see the average number of attempts to answer 
questions and the average time per answer attempt.</p>
<p>Changing your answer after clicking the "Show Explanation" button will also result in a new answer attempt being recorded, even 
without navigating away from the question.</p>

<h1 class="lead" id="timer">Timer</h1>
<p>The amount of time remaining for the completion of the exam is displayed at the top left of the screen. For task based simulations
the circular animation counts down from 15 minutes to represent the optimal time for solving task based simulations in the actual
exam.</p>
	<div class="text-center">
	<img src="img/directions/timer-screenshot.png" class="img-polaroid pull-center" style="margin:auto;" width="212" height="95">
	</div>
<p>The digital time will continue to count up after the animation is complete to track your actual time regardless of the optimal 
time represented by the circular animation. To prepare for the exam, keep drilling yourself on multiple choice questions until you 
can solve them within 15 minutes.</p>

<h1 class="lead" id="tools_and_buttons">Buttons and Tools</h1>
<p>Clicking “Test Instructions” will pause the exam and display the instructions. Clicking “End Test” 
will give you the opportunity to create a saved test session, and clicking the “Next Section” button will allow you 
to move to the next part of the exam. Note: Once you confirm your wish to exit this part of the exam, you will be unable to 
return.</p>
<p>You may add a note to any question with the “Add a note” tool. A pencil fa above the question number at the 
bottom of the screen indicates that there is a note attached to this particular question. The “Calculator” tool 
allows you to access a calculator. If you need to a reminder to return to a particular question, you may bookmark it by clicking 
the “Bookmark” tool on the upper right side of the screen. The bookmarked questions are shown as stars at the bottom 
of the screen. To clear this bookmark, click the “Bookmark” tool again. Reminders are only for your use; they do not 
affect your score.</p>




		<div class="navbar navbar-inverse navbar-fixed-bottom black-footer roger">
			<div class="container">
		        <div class="container">
		        <div class="row">
		                <button class="ActionButton btn pull-right btn-default" ng-click="dismissInstructions()">Begin quiz <i class="fa fa fa-arrow-circle-right"></i></button>
		        </div>
		        </div>
		    </div>
		</div>
</div>
</div>
</div>



<div class="container" ng-show="navState.showInstructions == false">

<!--?php
if(isset($_COOKIE['isadmin'])) {
?-->

		<button class="btn btn-danger" style="position:fixed;top:0;right:0;z-index:2000;" ng-click="adminEdit()"><i class="fa fa-white fa fa-pencil"></i> Edit this question in the administrator</button>


<!--?php
}
?-->

<div class="row" id="question">
<div class="col-sm-12 col-md-12" style="position:fixed;top:115px;z-index:5000;background-color:#fff;">
<div class="row">
	<div class="col-sm-4 col-md-4"><button class="btn btn-link btn-default" report-problem="" question="question"><i class="fa fa fa-warning"></i> Report Problem</button></div>
	<div class="pull-right col-sm-4 col-md-4"><button class="btn btn-link pull-right btn-default" style="position:relative;left:20px;" data-ng-click="gotoInstructions()"><i class="fa fa fa-info-circle"></i> View Instructions</button></div>
</div>
</div>

	<div class="question-header-well col-sm-12 col-md-12" style="position:fixed;top:145px;z-index:5000;">
		<div class="row">
			<div class="pull-left timer-box col-sm-5 col-md-5">
				<div class="pull-left" data-ng-show="showTimer">
					<!--<canvas id="timerchart" width="80" height="80">--> 
					<div id="timerchart"></div> 
					     
					    <div class="timerControlContainer">
					    <a class="timerControl"><i class="fa fa fa-pause fa fa-2x"></i></a>
					    </div>
					    <span id="digital-timer">
							<timer><span ng-show="hours > 0">0{{hours}}:</span><span ng-show="minutes > 9">{{minutes}}:</span><span ng-show="minutes < 10">0{{minutes}}:</span><span ng-show="seconds < 10">0{{seconds}}</span><span ng-show="seconds > 9">{{seconds}}</span></timer>
						</span>
				</div>
				<div class="pull-left show-timer-checkbox">
					<input data-ng-model="showTimer" type="checkbox">Show Timer
				</div>
			</div>

			<div class="pull-right col-sm-5 col-md-5">
				<div class="button-group" style="padding-top:4px;padding-right:4px">
					<button class="btn text-center pull-right btn-default" ng-click="nextQuizlet()" ng-show="quizlets.length > 1  &amp;&amp; navState.quizlet < quizlets.length - 1"><img src="img/icons/next-icon.png"><br>Next section</button>
					<button class="btn text-center pull-right btn-default" ng-click="saveSession()" ng-class="{endQuizSolo:quizlets.length == 1 || navState.quizlet == quizlets.length - 1}"><img src="img/icons/x-icon.png"><br>End quiz</button>
					
				</div>
			</div>

		</div>

	</div>

	
	<div class="tbs-journal-container col-lg-offset-1 col-sm-9 col-md-9" id="tbs-journal-container" style="padding-top:145px;">
		<span style="display:block;width:1px;height:1px;position:relative;left:-40px">
		<span class="question-number">{{navState.question + 1}}.</span> 
		</span>
		<div ng-repeat="template in question.question.widgets" class="row">
          	<div ng-include="template.type" ng-model="template" class="templateContainer">

          </div>
        </div>
	</div>

	<div class="q-right-tools pull-right">
		<div class="btn-group-vertical q-tools pull-right" style="width:126px;position:fixed;top:260px;">
			<button class="btn text-center btn-default" ng-click="calculator.showCalculator()"><img src="img/icons/calc-icon.png"><br>Calculator</button>
			<button class="btn text-center btn-default" ng-click="toggleBookmark(navState.question)"><img src="img/icons/bookmark-icon.png"><br>Bookmark</button>

			<button class="btn text-center btn-default" ng-click="showExplanation()"><img src="img/icons/question-icon.png"><br>View Solution</button>
			
			
		</div>
	</div>
   	<div class="row subfooter" ng-show="navState.showInstructions == false">
   		<div style="padding-top:80px">
    	<div class="well question-topic-nav col-lg-offset-1 col-sm-6 col-md-6">
    		<p>Review this material in your online course</p>
    		<ul>
    			<li ng-repeat="topic in topicTitles"><i class="fa fa-chevron-right"></i> <a href="{{topic.course_url}}" target="_blank"><strong>{{topic.chapter}}</strong>: {{topic.topic}}</a></li>
    		</ul>
    	</div>

    	<div class="q-right-tools pull-right" style="position: fixed;left: 1080px;bottom: 60px;">
    	
    	<button style="white-space:nowrap; color:#2560ac;" class="btn btn-link btn-default" ng-click="toggleScoreAsIGo()"><i class="fa" ng-class="{true:'fa fa-check-square-o', false:'fa fa-square-o'}[navState.scoreAsIGo == true]"></i> Score as I go</button><br>
    	
    	<button class="btn pull-right nextbtn btn-default" ng-click="gotoNextQuestion()"><span ng-show="isLastQuestion == false">Next Question</span><span ng-show="isLastQuestion == true">End Section</span></button>
    	<br>
    	<div>
    	<button class="button-flag-prev" data-ng-show="isPrevFlaggedQuestion()" ng-click="gotoPrevFlaggedQuestion()" tooltip="Navigate back to flagged questions"></button>
    	<button class="button-flag-prev button-flag-prev-disabled" data-ng-hide="isPrevFlaggedQuestion()"></button>
    	<button class="button-flag-next" data-ng-show="isNextFlaggedQuestion()" ng-click="gotoNextFlaggedQuestion()" tooltip="Navigate ahead to flagged questions"></button>
    	<button class="button-flag-next button-flag-next-disabled" data-ng-hide="isNextFlaggedQuestion()"></button>
    	</div>
    	</div>
    	</div>


	</div>

	<div class="row notes col-lg-offset-1 col-sm-8 col-md-8">
		<alert ng-repeat="n in question.notes" class="alert question-note alert-info" close="deleteNote($index)">{{n}}</alert>
	</div>

	<div class="row notesFormContainer well col-sm-9 col-md-9">
	<i class="fa fa-pencil"></i> Add a note
	<br>
	<textarea id="noteInput" ng-model="question.newNoteText" popover="Your notes will be saved for this quiz only if you choose to save this quiz. However, your notes will not appear on the same question in future quizzes." popover-title="Warning" popover-placement="top" popover-animation="true" popover-trigger="focus" popover-popup-delay="750"></textarea>
	<button class="btn pull-right btn-default" ng-click="createNote()" ng-disabled="question.newNoteText == ''">Save note</button>
	</div>

</div>
     






<div class="navbar navbar-fixed-bottom navbar-default" ng-show="navState.showInstructions == false">
      <div class="quiznav container">
      	<div class="container">
        	<div class="row">
				<div class="qlist-container" ng-style="{ 'width': ((questionList.length * 18) | number) + 'px' }">
      				<div class="questionflagbar text-center" ng-repeat="q in questionList">
	      				<i ng-class="{'fa fa-pencil':q.notes.length > 0}"></i><br>
	      				<div class="questionflagbar-inner text-center" ng-class="{current:$index == qID, 'btn-danger':q.isFlagged == true}">
	      				<i class="fa fa-flag" ng-class="{'fa fa-white':q.isFlagged == true}" ng-click="toggleBookmark($index)"></i><br>
	      				<button class="btn btn-link text-center btn-default btn-sm" ng-click="setPath(q.path)">{{$index + 1}}</button>
	      				</div>
      				</div>
      			</div>
      		</div>
      	</div>
      </div>
</div>
<!--
  <div class="copyright">
	Copyright © 2014 Roger CPA Review. All rights reserved
  </div>
-->
</div>
