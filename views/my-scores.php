<div ng-controller="MyScoresController">
	<div class="row container container-no-padding score-history">	
		<div class="section-selection-container"></div>
	</div>

	<div ng-show="(showTabLoading || !history) && section" class="row container score-history">
		<div class="loading-spinner"><i class="fa fa-spin fa-spinner fa-4x"></i></div>
	</div>

	<div ng-hide="(showTabLoading || !history) && section" class="row container score-history">
		<div class="well btn-inverse col-sm-12 col-md-12" ng-show="history.length == 0">
			<p class="lead">You do not have any saved quiz scores.</p>
			<p>On this page you will find your latest scores broken down by topic.</p>
		</div>
		<!--<div class="section-selection-container" ng-hide="history.length > 0"></div>-->
		<div class="row score-history" ng-show="history.length > 0">
			<div ng-hide="history" class="alert alert-info text-center" role="alert">
				<div id="loader-sppinner">
					<div class="loader">Loading...</div>
				</div>
			</div>
			<h3 class="top-heading">{{section.toUpperCase()}} History</h3>
			<div class="session-entry" ng-repeat="session in history">
				<!--<a ng-click="toggleExpandSession(session)">-->
				<div class="col-sm-6">
					<div class="session-name">
						<!--<i class="fa" ng-class="{'fa fa-minus-square':session.isExpanded == true, 'fa fa-plus-square':session.isExpanded == false}"></i>-->
						<!--<span class="{{session.summary.scoreClass}}">{{session.summary.session.summary.totalPercent}}%</span></a></h3>-->
						<div ng-show="(editingSessionName == session.id) && (savingSessionName == session.id)" class="saving-indicator">
							Saving...
						</div>
						<div ng-hide="editingSessionName == session.id" class="col-md-12 row editing-session-name">
							<span class="pull-left">
								{{session.session_name}}
							</span>
							<a ng-click="editSessionName(session)" class="pull-left help-link"><i class="fa fa-pencil"></i>Rename</a>
						</div>
						<div ng-show="(editingSessionName == session.id) && !savingSessionName" class="edit-session-name">
							<div class="alert alert-danger col-md-11" style="font-size:14px;" ng-show="session.session_name.length == 0">Session name cannot be left blank. Please enter a name for this session.</div>
							<span class="edit-session-name-span pull-left">
								<input class="row form-control" type="text" ng-model="session.session_name"/>
							</span>
							<button class="btn btn-primary pull-left" ng-disabled="session.session_name.length == 0" ng-click="saveSessionName(session)">Save</button>
						</div>
						<div class="created-on">
							<span class="session-type" ng-show="session.session_type == 'quiz'">
								Quiz
							</span>
							<span class="session-type" ng-show="session.session_type == 'exam'">
								Exam Simulator
							</span>
							<span class="created-on-timestamp">
								 | Created on {{ getSessionTime(session) }}
							</span>
						</div>
					</div>
					<div class="session-metadata">
						<div class="subtitle sub-heading" ng-show="session.chapters_total == 1">
							This session covers {{ session.chapters_total }} Sections containing:
						</div>
						<div class="subtitle sub-heading" ng-show="session.chapters_total > 1">
							This session covers {{ session.chapters_total }} Sections containing:
						</div>
						<div class="session-metadata-body" ng-repeat="entry in session.quizlets">
							<ul class="session-metadata-list">
								<li ng-show="entry.qty > 1" class="quiz-summary-row">
									{{ entry.qty }}
									<span ng-show="entry.type == 'mcq'">
										Multiple-Choice Questions
									</span>
									<span ng-show="entry.type == 'tbs' && section != 'bec'">
										Task-Based Simulations
									</span>
									<span ng-show="entry.type == 'tbs' && section == 'bec'">
										Written Communications
									</span>
								</li>
								<!--<span ng-show="entry.questions.length == 1 && entry.chapters.length > 1">-->
								<li ng-show="entry.qty == 1" class="quiz-summary-row">
									{{ entry.qty }}
									<span ng-show="entry.type == 'mcq'">
										Multiple-Choice Question
									</span>
									<span ng-show="entry.type == 'tbs' && section != 'bec'">
										Task-Based Simulation
									</span>
									<span ng-show="entry.type == 'tbs' && section == 'bec'">
										Written Communications
									</span>
								</li>
							</ul>
						</div>
						<div class="session-metadata-body">
							Includes: <span ng-bind-html-unsafe="session.chapters"></span>
						</div>
					</div>
				</div>
				<div class="col-sm-6">
					<div class="score-history">
						<div class="clearfix text-right score-text">{{session.total_percent | number: 0}}% Correct</div>
						<div class="clearfix text-right pull-right question-score-bar">
							<div class="correct-questions" ng-style="{ 'width': session.total_percent + '%' }">
							</div>
						</div>
					</div>
					<div class="text-right btn-review-history" ng-show="session.quizlets.length > 0">
						<button class="btn btn-ice" ng-click="relaunchQuiz(session)" ng-show="session.completed == false && session.session_type == 'quiz'">Resume</button>
						<!--<button class="btn btn-ice" ng-click="previewQuestions(session)">Review</button>-->
						<button class="btn btn-ice" ng-click="relaunchQuiz(session, 'review')">Review</button>
						<button class="btn btn-ice" ng-click="relaunchQuiz(session, false, 'retake')" ng-show="session.completed == true || session.session_type == 'exam'">Retake</button>
						<!--<button class="btn btn-inverse"><i class="fa fa fa-plus-circle"></i> Launch as new quiz with selected topics and questions</button>-->
						<div class="delete-icon">
							<button class="btn btn-bin clearfix" ng-click="deleteQuiz(session)"></button>
						</div>
					</div>
				</div>
				<div class="col-sm-12 alert-wrapper">
					<div class="alert alert-info" ng-show="session.quizlets.length == 0">
						<p>This quiz predates the “Resume quiz” feature. Newer quiz sessions will include the ability to reload quizes and improve your score.</p>
					</div>
					<span class="pull-right delete-icon" ng-show="session.quizlets.length == 0">
						<button class="btn pull-right btn-default btn-bin" ng-click="deleteQuiz(session)">Delete quiz</button>
					</span>
				</div>
				<!--
				<div ng-show="session.isExpanded">
					<div ng-repeat="topic in session.summary.session.summary.breakdown" ng-hide="(topic.questions.length-topic.deprecated) == 0">
						<ul>
							<li style="height:45px">
								<a href="{{topic.course_url}}" target="_blank" ng-tooltip="Review this topic in the online course">{{topic.meta.chapter}} <strong>{{topic.meta.topic}}</strong> <i class="fa fa fa-share-square-o"></i></a>
								<br>
								<span class="{{topic.scoreClass}}">{{topic.score}}%</span> out of {{topic.questions.length-topic.deprecated}} questions.
							</li>
						</ul>
					</div>
				</div>
				-->
				<div class="clear-empty"></div>
			</div>
			<div class="col-md-12 text-center">
				<button ng-click="moreQuizHistory()" class="btn btn-ice btn-load-more" ng-class="{'disabled': gettingQuizzes}">
					Load More Sessions<i class="fa fa-spin fa-spinner fa-1x" ng-show="gettingQuizzes"></i>
				</button>
			</div>
		</div>
	</div><!-- .score-history-->
</div><!-- MyScoresController -->
