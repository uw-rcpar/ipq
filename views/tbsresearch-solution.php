<div class="modal-header">
    <button type="button" class="close" ng-click="close()">×</button>
    <p class="lead"><span ng-show="givenAnswer.isCorrect == true">Correct</span><span ng-show="givenAnswer.isCorrect == false">Incorrect</span></p>
    
  </div>
  <div class="modal-body">
 	<div>
       <p>The answer you gave:<br> 
               <span ng-show="givenAnswer.key.indexOf('--') < 0" ng-bind-html-unsafe="givenAnswer.key"></span>
               
               <span ng-show="givenAnswer.key.indexOf('--') > 0"><span ng-bind-html-unsafe="givenAnswer.key"></span> <span class='label label-danger'>not answered</span></span>
       </p>

 	</div>
 	<div>The correct answer: <span ng-bind-html-unsafe="solution" class="research-solution-correct"></span></div>

 	</div>
  </div>
<div class="modal-footer">
	<a class="btn btn-default btn-modal" ng-click="close()">Try Again</a>
	<a class="btn btn-primary btn-modal btn-modal-primary" id="save-option-btn" ng-click="saveAndClose()">Continue</a>
</div>
