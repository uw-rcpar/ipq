<div class="modal-header">
	<h3><strong>Choose Question Quantity</strong></h3>
</div>
<div class="modal-body">
		<p class="lead">The maximum number of questions is <strong>50</strong>.  Please type in the number of questions you would like to review.</p>
		<input class="col-md-2" type="number" ng-model="num" />
		<div class="clearfix"></div>
</div>
<div class="modal-footer">
<button class="btn btn-danger btn-modal" ng-click="cancel()">Cancel</button>
<button class="btn btn-primary btn-modal btn-modal-primary" ng-click="submit(num)">Start Quiz</button>
</div>
