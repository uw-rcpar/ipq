<?php
require('../rcpar_common.php');

$con=mysql_conn();
//get the id from the cookie
$sid=get_sid();
$student = mysqli_query($con,"SELECT * FROM students WHERE id = $sid");
$row = mysqli_fetch_array($student);
$sections=get_sections($row);


?>
<div ng-controller="SectionSelectorController">
<h1 class="page-title ipq-title" ng-show="section == false">Please Choose a Course</h1>
<div class="course-section-links" ng-hide="section != false">

<?php if (in_array('aud', $sections)) {?>			
  <div class="col-sm-3">
   <a ng-click="selectSection('aud')" class="aud-link">AUD</a>
  </div>
<?php }?>
<?php if (in_array('bec', $sections)) {?>	
  <div class="col-sm-3">
   <a ng-click="selectSection('bec')" class="bec-link">BEC</a>
  </div>
<?php }?>
<?php if (in_array('far', $sections)) {?>	
  <div class="col-sm-3">
   <a ng-click="selectSection('far')" class="far-link">FAR</a>
  </div>
<?php }?>
<?php if (in_array('reg', $sections)) {?>
  <div class="col-sm-3">
   <a ng-click="selectSection('reg')" class="reg-link">REG</a>
  </div>
<?php }?>

</div>
</div><!-- SectionController -->
