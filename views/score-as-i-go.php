<div class="modal-header">
    <button type="button" class="close" ng-click="close()">×</button>
    <p class="lead">Your score so far is <span class="{{scoreClass}}">{{currentPercent}}%</span> </p>
  </div>
  <div class="modal-body">
 	<p>You have correctly answered {{currentPercent}}% of the questions you have attempted so far.</p>
 	<p>You average {{attemptsAverage}} attempts per question.</p>
 	<p>Your average time per question is {{timeAverage}}.</p>
 	<p>You are {{completePercent}}% of the way through this quiz.</p>

  </div>

<div class="modal-footer">
    <button class="pull-left btn btn-link" ng-click="toggleScoreAsIGo()">
		<i class="fa" ng-class="{true:'fa fa-check-square-o', false:'fa fa-square-o'}[getScoreAsIGo()]"></i> Score as I go
	</button>
	<a class="btn btn-default btn-modal btn-modal-primary" id="save-option-btn" ng-click="close('continue')">Next question</a>
</div>
