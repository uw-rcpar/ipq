<div class="modal-header">
<h3><i class="fa fa-alert"></i>Are you sure you want to exit your quiz?</h3>
</div>
<div class="modal-body">
	<p>This will end your quiz session. You will be taken to the score summary page where you may save this quiz session so that you can reload it later from the Score History page.</p>
	
</div>
<div class="modal-footer">
<button class="btn btn-default btn-cancel btn-modal" ng-click="cancel()">No</button>
<button class="btn btn-modal btn-modal-primary" ng-click="confirm('ok')">Yes</button>
</div>
