<div class="modal-header">
	<h3></h3>
</div>
<div class="modal-body container">
	<div class="">
		<div class="col-sm-1 col-md-1">
		<i class="fa fa fa-exclamation-triangle fa fa-4x" style="margin-top:10px"></i>
		</div>
		<div class="col-sm-5 col-md-5">
		<p class="lead">The quiz is taking longer than usual to save the question. Would you like to try saving again?</p>
		</div>
	</div>
</div>
<div class="modal-footer">
<button class="btn btn-modal btn-modal-primary btn-delete" ng-click="saveAndClose()">Save Again</button>
</div>
