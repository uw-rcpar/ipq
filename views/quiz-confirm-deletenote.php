<div class="modal-header">
<h3><i class="fa fa fa-alert"></i>Are you sure you want to delete this note?</h3>
</div>
<div class="modal-body">
	<p>This action cannot be undone.</p>
	
</div>
<div class="modal-footer">
<button class="btn btn-default btn-cancel btn-modal" ng-click="cancel()">Cancel</button>
<button class="btn btn-modal btn-modal-primary" ng-click="confirm()"><i class="fa fa fa-delete"></i> Delete note</button>
</div>
