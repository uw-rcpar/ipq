<div class="row container preview" ng-controller="ReviewController">
<!--
<div class="text-center show-tab-loading" ng-show="showTabLoading">
	<i class="fa fa-spin fa-pulse fa-gear"></i>
</div>
-->
<div class="row preview" ng-hide="showTabLoading" ng-controller="SessionController">

<div ng-controller="QuizletController">

<?php
if(isset($_COOKIE['isadmin'])) {
?>

		<button class="btn btn-danger" style="position:fixed;top:0;right:0;z-index:2000;" ng-click="adminEdit()"><i class="fa fa-white fa fa-pencil"></i> Edit this question in the administrator</button>


<?php
}
?>

<div class="" id="question" ng-hide="showTabLoading" ng-controller="QuestionController">
	<div class="top-links-heading">
			<div class="text-left">
				<h3 class="top-heading">
					<span ng-show="sessioninfo">
						Review
					</span>
					<span ng-hide="sessioninfo">
						Preview
					</span>
					<div class="pull-right">
						<button class="btn btn-ice btn-ice-nomargin retake-btn" ng-show="navState.question > 0" ng-click="previousQuestion()">Back</button>
						<!--<button class="btn btn-gray" report-problem="" question="question"><i class="fa fa fa-warning"></i> Report Problem</button>-->
<!--						<button class="btn btn-ice" ng-hide="!sessioninfo && question.chapter_id == null" ng-click="viewQuestions(question.chapter_id, question.type, question.status)">Re-Take This Quiz</button>
-->

						<button class="btn btn-ice retake-btn" ng-hide="sessioninfo" ng-click="viewQuestions(question.chapter_id, question.type, question.status)">Take New Quiz</button>

						<button class="btn btn-ice retake-btn" ng-show="sessioninfo" ng-click="viewQuestions(quizlets)">Re-Take This Quiz</button>
						<button class="btn btn-ice next-question-btn" ng-hide="isLastQuestion" ng-click="advanceQuestion()">Next Question</button>
						<button class="btn btn-ice next-question-btn" ng-show="quizlets.length > 1 && navState.quizlet < quizlets.length - 1" ng-click="nextQuizlet()">Next Section</button>

					</div>
				</h3>
			</div>
<!--			<div class="col-md-4"><button class="btn btn-link btn-reportproblem" report-problem="" question="question"><i class="fa fa fa-warning"></i> Report Problem</button></div>-->
	</div>

		<div ng-show="question.type == 'multiple-choice' && !showTabLoading">
			<div class="question-box preview" style="">
<!--
		<div class="alert alert-info" role="alert">We are currently updating our Interactive Practice Question platform.  If you are experiencing any layout issues or errors, we recommend clearing the cache on your internet browser, restarting your browser, and logging back in to your Interactive Practice Questions.  Thank you for your patience as we make these adjustments</div>
-->
				<h4 class="session-name" ng-show="sessioninfo"><strong>{{ sessioninfo.session_name }}</strong></h4>

				<div class="created-on" ng-show="sessioninfo">
					<span class="session-type" ng-show="sessioninfo.session_type == 'quiz'">
						Quiz
					</span>
					<span class="session-type" ng-show="sessioninfo.session_type == 'exam'">
						Exam Simulator
					</span>
					<span class="created-on-timestamp">
						 | Created on {{ sessioninfo.timestamp }}
					</span>
				</div>

				<h2 class="text-gray sub-heading">Question {{navState.question + 1}}:</h2>
				<span class="tiny text-gray question-number-id">Question ID #{{question.id}}</span>
				<span ng-bind-html="question.question" class="question-text"></span>


				<h2 class="text-gray sub-heading sub">Your Answer:</h2>
<!--				<div ng-show="question.student_answer_id.length > 0">
-->
				<div ng-show="question.answer_id.length > 0">
					<div class="btn-group btn-group-vertical preview">
	<!--					<button ng-show="question.answer_id.length > 0" class="btn btn-quizoption dropdown-toggle" ng-class="{'active': question.answer_id == choice.id, 'incorrect': choice.is_correct == 0, 'correct': choice.is_correct == 1}" type="button" ng-repeat="choice in question.choices" ng-model="question.answer" ng-radio="choice.id">
	-->

						<div class="btn-answered clearfix" ng-class="{'active': question.answer_id == choice.id, 'incorrect': choice.is_correct == 0, 'correct': choice.is_correct == 1}" type="button" ng-repeat="choice in question.choices" ng-model="question.answer" ng-radio="choice.id">


	<!--
						<button class="btn btn-quizoption dropdown-toggle" ng-class="{'active incorrect': choice.is_correct == 0, 'active correct': choice.is_correct == 1}" type="button" ng-repeat="choice in question.choices" ng-model="question.answer" ng-radio="choice.id">
	-->
							<span class="pull-left optionLabel">{{optionLabels[$index]}}. </span>
							<span class="pull-left optionBody" ng-bind-html="choice.choice"></span>
						</div>

					</div>
					<div ng-repeat="choice in question.choices">
						<div ng-show="choice.id == question.answer_id && choice.is_correct == 0">
							<h2 class="text-gray sub-heading incorrect-answer-sub">Incorrect Answer Explanation:</h2>
							<div ng-repeat="ch in question.choices" ng-show="ch.id == question.answer_id" ng-bind-html="ch.explanation"></div>
						</div>
					</div>
				</div>
<!--				<div ng-hide="question.student_answer_id.length > 0">
-->
				<div ng-hide="question.answer_id.length > 0" class="alert alert-danger" role="alert">
					<strong>You did not answer this question.</strong>
				</div>

				<div ng-show="question.answer_id.length > 0">
					<h2 class="text-gray sub-heading sub">Correct Answer:</h2>
					<div class="btn-group btn-group-vertical preview">
	
						<div class="btn-answered clearfix" ng-class="{active: choice.is_correct == 1, correct: choice.is_correct == 1}" type="button" ng-repeat="choice in question.choices" ng-model="question.answer" ng-radio="choice.id">
							<span class="pull-left optionLabel">{{optionLabels[$index]}}</span>
							<span class="pull-left optionBody" ng-bind-html="choice.choice"></span>
						</div>
	
					</div>
	
					<h2 class="text-gray sub-heading correct-answer-sub">Correct Answer Explanation:</h2>
					<div ng-repeat="choice in question.choices">
						<div ng-show="choice.is_correct == '1'" ng-bind-html="choice.explanation"></div>
					</div>
				</div>
				
				<div ng-controller="NoteController">
					<div class="notes col-sm-9" ng-show="sessioninfo">
						<alert ng-repeat="n in question.noted" class="alert question-note alert-info" close="deleteNote($index)">
							<div ng-show="noteBeingEdited == $index" class="note-text col-md-10">
				<!--				<input type="text" class="form-control" ng-model="n" />
				-->
								<textarea class="form-control" ng-model="editedNoteText"></textarea>
							</div>
							<div ng-hide="noteBeingEdited == $index" class="note-text col-md-10" ng-bind-html-unsafe="n | nl2br">
							</div>
							<span class="note-buttons link col-md-1">
								<span class="pull-right" ng-click="editNote($index)" ng-hide="noteBeingEdited == $index">Edit</span>
								<span class="pull-right" ng-click="saveEditedNote($index, editedNoteText)" ng-show="noteBeingEdited == $index">Save</span>
							</span>
							<span class="clearfix"></span>
							
						</alert>
					</div>

					<div class="notes col-lg-offset-1 col-md-9" ng-hide="sessioninfo">
						<alert ng-repeat="n in question.noted" class="alert question-note alert-info"> 
							<div ng-bind-html-unsafe="n | nl2br"></div>
						</alert>
					</div>

					<div class="notesFormContainer col-md-9" ng-show="sessioninfo">
					<i class="fa fa-pencil"></i> Add a note
					<br>
					<textarea id="noteInput" ng-model="question.newNoteText" popover="Your notes will be saved for this quiz only if you choose to save this quiz. However, your notes will not appear on the same question in future quizzes." popover-title="Warning" popover-placement="top" popover-animation="true" popover-trigger="focus" popover-popup-delay="750"></textarea>
	<!--				<button class="btn pull-right btn-ice btn-save-note" ng-click="createNote()" ng-disabled="question.newNoteText == ''">Save note</button>
	-->
					<button class="btn pull-right btn-ice btn-save-note" ng-click="createNote()">Save note</button>
					</div>
				</div>


			</div>
		</div>
		<div ng-show="question.type == 'tbs-journal' && !showTabLoading" ng-controller="TBSJournalQuestionController">
			<div class="question-box tbs-journal-container preview" id="tbs-journal-container" style="padding-top:45px;">
<!--		<div class="alert alert-info" role="alert">We are currently updating our Interactive Practice Question platform.  If you are experiencing any layout issues or errors, we recommend clearing the cache on your internet browser, restarting your browser, and logging back in to your Interactive Practice Questions.  Thank you for your patience as we make these adjustments</div>
-->
				<h2 class="text-gray sub-heading">Question {{navState.question + 1}} - Your Answers:</h2>
				<span class="tiny text-gray">Question ID #{{question.id}}</span>
				<div ng-hide="question.answer_id.length > 0" class="alert alert-danger" role="alert" style="margin-top: 20px;">
					<strong>You did not answer this question.</strong>
				</div>
				<div ng-repeat="template in question.question.widgets" class="row">
					<div ng-include="template.type" ng-model="template" class="templateContainer">

					  </div>
				</div>

				<div ng-show="question.answer_id.length > 0">
					<h2 class="text-gray sub-heading">Solutions:</h2>
					<div ng-bind-html="question.question.solution"></div>
				</div>
			</div>
				<div ng-controller="NoteController">
					<div class="notes col-sm-9" ng-show="sessioninfo">
						<alert ng-repeat="n in question.noted" class="alert question-note alert-info" close="deleteNote($index)">
							<div ng-show="noteBeingEdited == $index" class="note-text col-md-10">
				<!--				<input type="text" class="form-control" ng-model="n" />
				-->
								<textarea class="form-control" ng-model="editedNoteText"></textarea>
							</div>
							<div ng-hide="noteBeingEdited == $index" class="note-text col-md-10" ng-bind-html-unsafe="n | nl2br">
							</div>
							<span class="note-buttons link col-md-1">
								<span class="pull-right" ng-click="editNote($index)" ng-hide="noteBeingEdited == $index">Edit</span>
								<span class="pull-right" ng-click="saveEditedNote($index, editedNoteText)" ng-show="noteBeingEdited == $index">Save</span>
							</span>
							<span class="clearfix"></span>
							
						</alert>
					</div>

					<div class="notes col-lg-offset-1 col-md-9" ng-hide="sessioninfo">
						<alert ng-repeat="n in question.noted" class="alert question-note alert-info"> 
							<div ng-bind-html-unsafe="n | nl2br"></div>
						</alert>
					</div>


					<div class="notesFormContainer col-md-9" ng-show="sessioninfo">
					<i class="fa fa-pencil"></i> Add a note
					<br>
					<textarea id="noteInput" ng-model="question.newNoteText" popover="Your notes will be saved for this quiz only if you choose to save this quiz. However, your notes will not appear on the same question in future quizzes." popover-title="Warning" popover-placement="top" popover-animation="true" popover-trigger="focus" popover-popup-delay="750"></textarea>
	<!--				<button class="btn pull-right btn-ice btn-save-note" ng-click="createNote()" ng-disabled="question.newNoteText == ''">Save note</button>
	-->
					<button class="btn pull-right btn-ice btn-save-note" ng-click="createNote()">Save note</button>
					</div>
				</div>
		</div>
		<div ng-show="question.type == 'tbs-research' && !showTabLoading" ng-controller="TBSResearchController">
			<div class="question-box tbs-journal-container preview" id="tbs-journal-container" style="padding-top:45px;">
<!--		<div class="alert alert-info" role="alert">We are currently updating our Interactive Practice Question platform.  If you are experiencing any layout issues or errors, we recommend clearing the cache on your internet browser, restarting your browser, and logging back in to your Interactive Practice Questions.  Thank you for your patience as we make these adjustments</div>
-->
				<h2 class="text-gray sub-heading">Question {{navState.question + 1}}:</h2>
				<span class="tiny text-gray">Question ID #{{question.id}}</span>
				<div ng-hide="question.answer_id.length > 0" class="alert alert-danger" role="alert" style="margin-top: 20px;">
					<strong>You did not answer this question.</strong>
				</div>
				<span ng-bind-html="question.question.widgets[0].model.content" class="question-text"></span>

				<div ng-show="question.answer_id.length > 0">
					<h2 class="text-gray sub-heading">Your Answer:</h2>
					<div ng-bind-html="getPreviousAnswer()"></div>
				</div>

				<div ng-show="question.answer_id.length > 0">
					<h2 class="text-gray sub-heading">Correct Answer:</h2>
					<div ng-bind-html="question.question.solution"></div>
				</div>
				<div ng-controller="NoteController">
					<div class="notes col-sm-9" ng-show="sessioninfo">
						<alert ng-repeat="n in question.noted" class="alert question-note alert-info" close="deleteNote($index)">
							<div ng-show="noteBeingEdited == $index" class="note-text col-md-10">
				<!--				<input type="text" class="form-control" ng-model="n" />
				-->
								<textarea class="form-control" ng-model="editedNoteText"></textarea>
							</div>
							<div ng-hide="noteBeingEdited == $index" class="note-text col-md-10" ng-bind-html-unsafe="n | nl2br">
							</div>
							<span class="note-buttons link col-md-1">
								<span class="pull-right" ng-click="editNote($index)" ng-hide="noteBeingEdited == $index">Edit</span>
								<span class="pull-right" ng-click="saveEditedNote($index, editedNoteText)" ng-show="noteBeingEdited == $index">Save</span>
							</span>
							<span class="clearfix"></span>
							
						</alert>
					</div>

					<div class="notes col-lg-offset-1 col-md-9" ng-hide="sessioninfo">
						<alert ng-repeat="n in question.noted" class="alert question-note alert-info"> 
							<div ng-bind-html-unsafe="n | nl2br"></div>
						</alert>
					</div>

					<div class="notesFormContainer col-md-9" ng-show="sessioninfo">
					<i class="fa fa-pencil"></i> Add a note
					<br>
					<textarea id="noteInput" ng-model="question.newNoteText" popover="Your notes will be saved for this quiz only if you choose to save this quiz. However, your notes will not appear on the same question in future quizzes." popover-title="Warning" popover-placement="top" popover-animation="true" popover-trigger="focus" popover-popup-delay="750"></textarea>
	<!--				<button class="btn pull-right btn-ice btn-save-note" ng-click="createNote()" ng-disabled="question.newNoteText == ''">Save note</button>
	-->
					<button class="btn pull-right btn-ice btn-save-note" ng-click="createNote()">Save note</button>
					</div>
				</div>

<!--
				<span style="display:block;width:1px;height:1px;position:relative;left:-40px">
				<span class="question-number">{{navState.question + 1}}.</span>
				</span>
				<div ng-repeat="template in question.question.widgets" class="row">
					<div ng-include="template.type" ng-model="template" class="templateContainer">

					</div>
				</div>
-->
			</div>
		</div>
		<div ng-show="question.type == 'tbs-wc' && !showTabLoading" ng-controller="TBSWrittenCommunicationController">
			<div class="question-box preview" style="padding-top:45px;">
<!--		<div class="alert alert-info" role="alert">We are currently updating our Interactive Practice Question platform.  If you are experiencing any layout issues or errors, we recommend clearing the cache on your internet browser, restarting your browser, and logging back in to your Interactive Practice Questions.  Thank you for your patience as we make these adjustments</div>
-->
				<h2 class="text-gray sub-heading">Question {{navState.question + 1}}:</h2>
				<span class="tiny text-gray">Question ID #{{question.id}}</span>
				<div ng-hide="question.answer_id.length > 0" class="alert alert-danger" role="alert" style="margin-top: 20px;">
					<strong>You did not answer this question.</strong>
				</div>
				<span ng-bind-html="question.question.question" class="question-text"></span>


				<div ng-show="question.answer_id.length > 0">
					<h2 class="text-gray sub-heading">Your Answer:</h2>
					<div ng-bind-html="getPreviousAnswer()"></div>
				</div>

				<h2 class="text-gray sub-heading">Sample Answer:</h2>
				<div ng-bind-html="question.question.solution"></div>

				<div ng-controller="NoteController">
					<div class="notes col-sm-9" ng-show="sessioninfo">
						<alert ng-repeat="n in question.noted" class="alert question-note alert-info" close="deleteNote($index)">
							<div ng-show="noteBeingEdited == $index" class="note-text col-md-10">
				<!--				<input type="text" class="form-control" ng-model="n" />
				-->
								<textarea class="form-control" ng-model="editedNoteText"></textarea>
							</div>
							<div ng-hide="noteBeingEdited == $index" class="note-text col-md-10" ng-bind-html-unsafe="n | nl2br">
							</div>
							<span class="note-buttons link col-md-1">
								<span class="pull-right" ng-click="editNote($index)" ng-hide="noteBeingEdited == $index">Edit</span>
								<span class="pull-right" ng-click="saveEditedNote($index, editedNoteText)" ng-show="noteBeingEdited == $index">Save</span>
							</span>
							<span class="clearfix"></span>
							
						</alert>
					</div>

					<div class="notes col-lg-offset-1 col-md-9" ng-hide="sessioninfo">
						<alert ng-repeat="n in question.noted" class="alert question-note alert-info"> 
							<div ng-bind-html-unsafe="n | nl2br"></div>
						</alert>
					</div>

					<div class="notesFormContainer col-md-9" ng-show="sessioninfo">
					<i class="fa fa-pencil"></i> Add a note
					<br>
					<textarea id="noteInput" ng-model="question.newNoteText" popover="Your notes will be saved for this quiz only if you choose to save this quiz. However, your notes will not appear on the same question in future quizzes." popover-title="Warning" popover-placement="top" popover-animation="true" popover-trigger="focus" popover-popup-delay="750"></textarea>
	<!--				<button class="btn pull-right btn-ice btn-save-note" ng-click="createNote()" ng-disabled="question.newNoteText == ''">Save note</button>
	-->
					<button class="btn pull-right btn-ice btn-save-note" ng-click="createNote()">Save note</button>
					</div>
				</div>

			</div>
		</div>
</div>

</div>





<div class="navbar navbar-fixed-bottom question-footer" ng-controller="PaginationController">
      <div class="quiznav container col-md-12">
      	<div class="container col-md-12">
        	<div class="pager-wrapper">
	        	<div class="row">
				<!-- Show this snippet if pagination is not enabled -->
				<div class="qlist-container" ng-style="{ 'width': ((questionList.length * 18) | number) + 'px' }" ng-show="pages.length == 0">
      				<div class="questionflagbar text-center" ng-repeat="q in questionList">
	      				<i ng-class="{'fa fa-pencil':q.noted.length > 0}"></i><br>
	      				<div class="questionflagbar-inner text-center" ng-class="{current:$index == qID, 'btn-danger':q.isFlagged == true || previewingFlaggedQuestions}">
	      				<i ng-controller="BookmarkController" class="fa fa-flag" ng-class="{true: 'fa fa-flag fa-white', false: 'fa fa-flag'}[q.isFlagged]" ng-click="toggleBookmark($index)"></i><br>
	      				<button class="btn btn-link text-center btn-sm" ng-click="setPath(q.path)" tooltip-placement="top" tooltip-html-unsafe="{{q.question}}" tooltip-trigger="mouseenter">{{$index + 1}}</button>
	      				</div>
      				</div>

					<!-- Show this snippet if pagination is enabled -->
					<div class="qlist-container" ng-style="{ 'width': ((questionList.length * 18) | number) + 'px' }" ng-repeat="(key, page) in pages" ng-show="currentPage == key">
	      				<div class="questionflagbar text-center" ng-repeat="q in questionList">
		      				<i ng-class="{'fa fa-pencil':q.noted.length > 0}"></i><br>
<!--		      				<div class="questionflagbar-inner text-center" ng-class="{current:$index == qID, 'btn-danger':q.isFlagged == true}">
-->

						<div class="questionflagbar-inner text-center" ng-class="{current: (qID+1) == ((qPageMax*key) + ($index+1)), 'btn-danger': q.isFlagged == true}">
		      				<i class="fa fa-flag" ng-class="{true: 'fa fa-flag fa-white', false: 'fa fa-flag'}[q.isFlagged]" ng-click="toggleBookmark($index)"></i><br>
		      				<button class="btn btn-link text-center btn-sm" ng-click="setPath(q.path)" tooltip-append-to-body="true" tooltip-placement="top" tooltip-html-unsafe="{{q.question}}" tooltip-trigger="mouseenter">
							{{ (qPageMax*key) + ($index + 1) }}
						</button>
		      				</div>
	      				</div>
	      			</div>
	      		</div>
        	</div>
		<div class="row">
			<div class="qpage-container">
				<a class="qpage" ng-repeat="(key, page) in pages" ng-class="{'not-a-link': currentPage == key}" ng-click="selectPage(key)">
					[ {{ (qPageMax*key)+1 }} - {{ qPageMax*(key+1) }} ]
				</a>
			</div>
		</div>
      	</div>
      </div>
</div>
</div> <!-- QuizletController -->
</div> <!-- SessionController -->
</div><!-- PreviewController >
