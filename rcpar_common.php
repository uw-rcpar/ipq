<?php  
/***
  *
  * Shared Functions including mysql connection.
  *
  */
  
/**
 * change this to production
 */ 
define('LOGIN_URL', 'https://www.rogercpareview.com/user/login');
define('REQUEST_URL','https://www.rogercpareview.com/course_services/user/login');
 
function mysql_conn() {
  //production:  "localhost","root","NClUYZQ2s0zT","testcenter"
  $mysql_server = 'localhost';
  $mysql_username = 'rcpar';
  $mysql_password = 'CPAdev2013';
  $mysql_dbname = 'testcenter';
  
  /*$mysql_server = 'localhost';
  $mysql_username = 'root';
  $mysql_password = '';
  $mysql_dbname = 'testcenter';*/

  // Database Connections
    $con = mysqli_connect($mysql_server, $mysql_username, $mysql_password, $mysql_dbname);
  if (mysqli_connect_errno()) {
      die('Could not connect: ' . mysqli_connect_error());
  }
  return $con;
}

function get_sid()
{
    $sid=0;
    if (isset($_COOKIE['data'])) {
        //student id and name are in a json string inside a cookie
        $cookie_obj=json_decode($_COOKIE['data']);
        $cookie_data = (array) $cookie_obj;
        $sid=$cookie_data['studentID'];
    }
    return $sid;
}

function get_user_roles() {
  $con = mysql_conn();
  $student = get_student_data();
  $roles = array();
  $sections = array();
  
	if (isset($student['studentID'])) {
    $results = mysqli_query($con,"SELECT * FROM students WHERE id = " . $student['studentID']);
    $row = mysqli_fetch_array($results);
	
	$sections = get_sections($row);
  }
  $roles['student'] = $student;
  $roles['sections'] = $sections;
  return $roles;
}

/*
 * this is where we determine what sections the student has access to
 */
function get_student_data() {
	$student_data = null;
	if (isset($_COOKIE['data'])) {
		//student id and name are in a json string inside a cookie
		$cookie_obj = json_decode($_COOKIE['data']);
		$cookie_data = (array) $cookie_obj;
		$student_data = $cookie_data;
	}
	return $student_data;
}

/*
 * this is where we determine what sections the student has access to
 */
 
function get_last_login()
{
    $last_login=0;
    if (isset($_COOKIE['data'])) {
        //student id and name are in a json string inside a cookie
        $cookie_obj=json_decode($_COOKIE['data']);
        $cookie_data = (array) $cookie_obj;
        $last_login = (isset($cookie_data['login_date'])) ? $cookie_data['login_date'] : 0 ;
        }

    return $last_login;
}


/*
    
 * pass the student array from the db to get the sections
 * "6":"far course active",
    "7":"aud course active",
    "8":"reg course active",
    "9":"bec course active"
*/
function get_sections($student)
{
        
    $roles=array();
    $access = json_decode($student['access'], TRUE);
    
    //var_dump($access['USER']['ROLES']);
    //exit;
    
    foreach ($access['USER']['ROLES'] as $section) {
        
        
        if ($section == "far course active" && !in_array('far', $roles)) {
            $roles[]='far';
        }
        
        if ($section == "aud course active" && !in_array('aud', $roles)) {
            $roles[]='aud';
        }
        if ($section == "reg course active" && !in_array('reg', $roles)) {
            $roles[]='reg';
        }
        if ($section == "bec course active" && !in_array('bec', $roles)) {
            $roles[]='bec';
        }                       
            
        
    }
    
    return $roles;      
    
}


/**
 * Returns login info including entitlements
 * @param $credentials array of username and password
 */
function login_user($credentials){
    
    $user_data = http_build_query($credentials);
    
    $curl = curl_init(REQUEST_URL);
    curl_setopt($curl, CURLOPT_HTTPHEADER, array('Accept: application/json')); // Accept JSON response
    curl_setopt($curl, CURLOPT_POST, 1); // Do a regular HTTP POST
    curl_setopt($curl, CURLOPT_POSTFIELDS, $user_data); // Set POST data
    curl_setopt($curl, CURLOPT_HEADER, FALSE);  // Ask to not return Header
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($curl, CURLOPT_FAILONERROR, TRUE);
    curl_setopt($curl, CURLOPT_COOKIESESSION, true);
    $response = curl_exec($curl);
    //echo $response;
    //exit;
    //return $response;
    $http_code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
    if ($http_code == 200) {
        $logged_user = json_decode($response);
        curl_close($curl);
        return  $logged_user ;
    }
    else {
        $http_message = curl_error($curl);
        $result = array('message' => $http_message);
        curl_close($curl);
        return $result;
    }
};

/**
 * extract the sections for entitlements. Returns json encoded string that is stored in db
 * @param object $logged_user - return from drupal login service
 */    

function get_entitlement_sections($ent,$username,$uid,$email){
    
    $sections=array();
    $USER=array();
    $index=array('FAR','AUD','REG','BEC');
    $access_json='';
    foreach ($index as $i) {
       
    //check each section
    if (array_search ($i,$ent) !== FALSE){
    						
    	$sections[]=$i;
        
    }
    }
    
    //Store user data in array for db. Angular understands the following format:
    /*
     "USER":{

    "NAME":"Dainel Garcia",
    "ROLES":{
        "2":"authenticated user",
        "7":"aud course active",
        "6":"far course active",
        "4":"student",
        "9":"bec course active",
        "8":"reg course active"
    },
    "STUDENTID":40400,
    "UID":40400,
    "EMAIL":"dainelg@hotmail.com"

        }
     */
    foreach ($sections as $section) {
        
        switch ($section) {
            case 'FAR':
                $USER['ROLES']['6']="far course active";
                break;
            
            case 'AUD':
                $USER['ROLES']['7']="aud course active";
                break;
                
            case 'REG':
                $USER['ROLES']['8']="reg course active";
                break;
                
            case 'BEC':
                $USER['ROLES']['9']="bec course active";
                break;
            
        }
        
    };
    
    //no ROLES, no access
    if (!isset($USER['ROLES'])) {
        return $access_json;
    }
    
    $USER['NAME']=$username;
    $USER['UID']=$uid;
    $USER['EMAIL']=$email;
    
    $access['USER']=$USER;
    $access_json=json_encode($access);
    
    return $access_json;
    
};

?>