<?php
header('Content-Type: application/json; charset=utf-8');
//echo '{"section":"aud","chapters":[{"chapter":"Audit Standards and Engagement Planning","id":"1","topics":[{"topic":"Steps in the Audit Process","id":"2"},{"topic":"Quality Control","id":"5"},{"topic":"Planning Procedures","id":"3"},{"topic":"Generally Accepted Auditing Standards","id":"1"},{"topic":"Research Task Format","id":"6"},{"topic":"Audit Risk","id":"4"}]},{"chapter":"Internal Control","id":"2","topics":[{"topic":"Investing and Financing Cycle","id":"11"},{"topic":"Operating Cycles","id":"9"},{"topic":"Internal Control","id":"7"},{"topic":"Documentation of Internal Control Structure","id":"12"},{"topic":"Personnel and Payroll Cycle","id":"10"},{"topic":"Understanding the Internal Control Structure","id":"8"},{"topic":"Internal Control Reports and Communications","id":"13"}]},{"chapter":"Audit Sampling","id":"3","topics":[{"topic":"Probability Proportional to Size Sampling","id":"16"},{"topic":"Audit Sampling","id":"14"},{"topic":"Variables Sampling","id":"15"}]},{"chapter":"Professional Responsibilities and Ethics","id":"4","topics":[{"topic":"Sarbanes Oxley","id":"18"},{"topic":"International Ethics Standards for Accountants","id":"19"},{"topic":"Professional Responsibilities and Ethics","id":"17"}]},{"chapter":"Audit Evidence","id":"5","topics":[{"topic":"Financial Statement Accoints. Inventories","id":"25"},{"topic":"Financial Statement Accounts. Cash, Recievables","id":"23"},{"topic":"Management\u2019s Assertions","id":"21"},{"topic":"Financial Statement Accoints. PPE through Payrolll","id":"26"},{"topic":"Financial Statement Accoints. Investments and Marketable Securities","id":"24"},{"topic":"Analytical Procedures","id":"22"},{"topic":"Audit Evidence","id":"20"}]}]}';
echo '{ "chapters" : [ { "chapter" : "Audit Standards and Engagement Planning",
        "id" : "1",
        "topics" : [ { "id" : "2",
              "m" : 0,
              "t" : 0,
              "topic" : "Steps in the Audit Process"
            },
            { "id" : "5",
              "m" : "1",
              "t" : 0,
              "topic" : "Quality Control"
            },
            { "id" : "3",
              "m" : "1",
              "t" : 0,
              "topic" : "Planning Procedures"
            },
            { "id" : "1",
              "m" : "4",
              "t" : 0,
              "topic" : "Generally Accepted Auditing Standards"
            },
            { "id" : "6",
              "m" : 0,
              "t" : "1",
              "topic" : "Research Task Format"
            },
            { "id" : "4",
              "m" : "1",
              "t" : 0,
              "topic" : "Audit Risk"
            }
          ]
      },
      { "chapter" : "Internal Control",
        "id" : "2",
        "topics" : [ { "id" : "9",
              "m" : 0,
              "t" : 0,
              "topic" : "Operating Cycles"
            },
            { "id" : "7",
              "m" : 0,
              "t" : 0,
              "topic" : "Internal Control"
            },
            { "id" : "12",
              "m" : 0,
              "t" : 0,
              "topic" : "Documentation of Internal Control Structure"
            },
            { "id" : "10",
              "m" : 0,
              "t" : 0,
              "topic" : "Personnel and Payroll Cycle"
            },
            { "id" : "8",
              "m" : 0,
              "t" : 0,
              "topic" : "Understanding the Internal Control Structure"
            },
            { "id" : "13",
              "m" : 0,
              "t" : 0,
              "topic" : "Internal Control Reports and Communications"
            },
            { "id" : "11",
              "m" : 0,
              "t" : 0,
              "topic" : "Investing and Financing Cycle"
            }
          ]
      },
      { "chapter" : "Audit Sampling",
        "id" : "3",
        "topics" : [ { "id" : "16",
              "m" : 0,
              "t" : 0,
              "topic" : "Probability Proportional to Size Sampling"
            },
            { "id" : "14",
              "m" : 0,
              "t" : 0,
              "topic" : "Audit Sampling"
            },
            { "id" : "15",
              "m" : 0,
              "t" : 0,
              "topic" : "Variables Sampling"
            }
          ]
      },
      { "chapter" : "Professional Responsibilities and Ethics",
        "id" : "4",
        "topics" : [ { "id" : "18",
              "m" : 0,
              "t" : 0,
              "topic" : "Sarbanes Oxley"
            },
            { "id" : "19",
              "m" : 0,
              "t" : 0,
              "topic" : "International Ethics Standards for Accountants"
            },
            { "id" : "17",
              "m" : 0,
              "t" : 0,
              "topic" : "Professional Responsibilities and Ethics"
            }
          ]
      },
      { "chapter" : "Audit Evidence",
        "id" : "5",
        "topics" : [ { "id" : "25",
              "m" : 0,
              "t" : 0,
              "topic" : "Financial Statement Accoints. Inventories"
            },
            { "id" : "23",
              "m" : 0,
              "t" : 0,
              "topic" : "Financial Statement Accounts. Cash, Recievables"
            },
            { "id" : "21",
              "m" : 0,
              "t" : 0,
              "topic" : "Management’s Assertions"
            },
            { "id" : "26",
              "m" : 0,
              "t" : 0,
              "topic" : "Financial Statement Accoints. PPE through Payrolll"
            },
            { "id" : "24",
              "m" : 0,
              "t" : 0,
              "topic" : "Financial Statement Accoints. Investments and Marketable Securities"
            },
            { "id" : "22",
              "m" : 0,
              "t" : 0,
              "topic" : "Analytical Procedures"
            },
            { "id" : "20",
              "m" : 0,
              "t" : 0,
              "topic" : "Audit Evidence"
            }
          ]
      }
    ],
  "section" : "aud"
}';