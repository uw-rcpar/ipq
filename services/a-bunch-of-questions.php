<?php
header('Content-Type: application/json; charset=utf-8');
echo '
{ "questions" : [ { "answer_id" : "3",
        "chapter_id" : "1",
        "choices" : [ { "choice" : "The disclosures provide reasonable assurance that the financial statements are free of material misstatement.",
              "explanation" : "Explanation holding text 1",
              "id" : "1",
              "is_correct" : "0"
            },
            { "choice" : "The auditor tested the overall internal control.",
              "explanation" : "Explanation holding text 2",
              "id" : "2",
              "is_correct" : "0"
            },
            { "choice" : "An audit includes evaluating the reasonableness of significant estimates made by management.",
              "explanation" : "Explanation holding text 3",
              "id" : "3",
              "is_correct" : "1"
            },
            { "choice" : "The financial statements are consistent with those of the prior period.",
              "explanation" : "Explanation holding text 4",
              "id" : "4",
              "is_correct" : "0"
            }
          ],
        "col1_header" : null,
        "col2_header" : null,
        "col3_header" : null,
        "col4_header" : null,
        "difficulty" : "1",
        "explanation" : "In the Auditors responsibility paragraph, it mentions that and auditor evaluates the appropriateness of accounting policies used and the reasonableness of significant accounting estimates made by management.  The requirement is to identify the statement that is included in the auditor’s standard report. Answer (c) is correct because the auditor\'s standard report states that an audit includes assessing significant estimates made by management; the other replies provide information not directly mentioned in a standard report. ",
        "id" : "1",
        "question" : "Which of the following statements is a basic element of the auditor\'s standard report?",
        "section" : "aud",
        "topic_id" : "1",
        "video_id" : "147"
      }, 
	{ "answer_id" : "3",
        "chapter_id" : "1",
        "choices" : [ { "choice" : "The disclosures provide reasonable assurance that the financial statements are free of material misstatement.",
              "explanation" : "Explanation holding text 1",
              "id" : "1",
              "is_correct" : "0"
            },
            { "choice" : "The auditor tested the overall internal control.",
              "explanation" : "Explanation holding text 2",
              "id" : "2",
              "is_correct" : "0"
            },
            { "choice" : "An audit includes evaluating the reasonableness of significant estimates made by management.",
              "explanation" : "Explanation holding text 3",
              "id" : "3",
              "is_correct" : "1"
            },
            { "choice" : "The financial statements are consistent with those of the prior period.",
              "explanation" : "Explanation holding text 4",
              "id" : "4",
              "is_correct" : "0"
            }
          ],
        "col1_header" : null,
        "col2_header" : null,
        "col3_header" : null,
        "col4_header" : null,
        "difficulty" : "1",
        "explanation" : "In the Auditors responsibility paragraph, it mentions that and auditor evaluates the appropriateness of accounting policies used and the reasonableness of significant accounting estimates made by management.  The requirement is to identify the statement that is included in the auditor’s standard report. Answer (c) is correct because the auditor\'s standard report states that an audit includes assessing significant estimates made by management; the other replies provide information not directly mentioned in a standard report. ",
        "id" : "1",
        "question" : "Which of the following statements is a basic element of the auditor\'s standard report?",
        "section" : "aud",
        "topic_id" : "1",
        "video_id" : "147"
      }
] }

';