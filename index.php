<?php
header("Expires: Tue, 01 Jan 2000 00:00:00 GMT");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

//of we go to setup.php regarless
header("Location: setup.php");

//no cookie, no showie. Cookie set on authorize.php
if (isset($_COOKIE['data'])){
	//back to login
	header("Location: setup.php");
	die();
};
?>

<!DOCTYPE html>
<html ng-app="Login">
    <head>
        <meta charset="utf-8">
	<meta http-equiv="cache-control" content="no-cache" />
	<meta http-equiv="expires" content="0" />
	<meta http-equiv="pragma" content="no-cache" />
        <title>Please log in</title>
        <link href="css/normalize.css" rel="stylesheet" />
        <link href="bower_components/jquery-ui/themes/smoothness/jquery-ui.min.css" rel="stylesheet" />
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" media="screen" />
        <link href="css/style.css" rel="stylesheet" />

        <script src="bower_components/jquery/jquery.min.js"></script>
        <script src="bower_components/jquery-ui/ui/minified/jquery-ui.min.js" type="text/javascript"></script>
        <script src="js/masonry.js"></script>
        <script src="bower_components/angular/angular.min.js"></script>
        <script src="bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js"></script>
        <script src="js/login.js"></script>

        <style type="text/css">
            form {
                margin:0;
            }
            .modal {
                border:none;
                display: block !important;
            }
            .modal-header {
                border-radius: 6px 6px 0 0;
            }
        </style>
    </head>
    <body ng-controller="LoginController">
    </body>
</html>
